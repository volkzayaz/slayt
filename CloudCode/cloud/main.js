Parse.Cloud.afterSave("CRPreference", function(request) {
	if (request.object.get("preferenceType") == 1) {
		sourceProfileId = request.object.get("sourceProfileId");
		destinationProfileId = request.object.get("destinationProfileId");

		query = new Parse.Query("CRPreference");
		query.equalTo("sourceProfileId", destinationProfileId);
		query.equalTo("destinationProfileId", sourceProfileId);
		query.equalTo("preferenceType", 1);


		sendPush = function(userId, friendId) {
			var currentUserPushQuery = new Parse.Query(Parse.Installation);
			currentUserPushQuery.equalTo("user", userId);

			var friendNameQuery = new Parse.Query("User");
			friendNameQuery.equalTo("objectId", friendId);

			friendNameQuery.first ({
				success: function(user) {
					if (user) {
						friendName = user.get("fullName").split(" ")[0];
						currentUserMessage = "You and " + friendName + " have liked each other!"

						Parse.Push.send({
							where: currentUserPushQuery,
							data: {
								alert: currentUserMessage,
								pushType: 100,
								sourceProfileId: userId,
								destinationProfileId: friendId,
							}
						});
					} else {
						console.log("user not found");
					}
				}
			});		
		}

		createFriend = function(userId, friendId) {
			var Friend = Parse.Object.extend("CRFriend");
			var friend = new Friend();

			friend.save({
				sourceProfileId: userId,
				friendId: friendId
			}, {
				success: function(friend) {
					if (!friend) {
						console.error("Friend not created");
					}
				}, 
				error: function(error) {
					console.error(error.message);
				}
			});
		}

		query.first({
			success: function(like) {
				if (like) {
					createFriend(sourceProfileId, destinationProfileId);
					createFriend(destinationProfileId, sourceProfileId);

					sendPush(sourceProfileId, destinationProfileId);
					sendPush(destinationProfileId, sourceProfileId);
				}		
			},
			error: function() {
				console.error("Got an error " + error.code + " : " + error.message);
			}
		});
	}
});

Parse.Cloud.define("submitTalentToAgency", function(request, response) {
	var Mailgun = require('mailgun');
	Mailgun.initialize('sandboxae0eed4152a3471f9a684de8761ccf4c.mailgun.org', 'key-12xnvd8snpuituvtd0giq9arxl38we00');

	var agentEmail = request.params.agentEmail;
	var agencyName = request.params.agencyName;
	var talentName = request.params.talentName;

	var messageSubject = "Confirmation: "+talentName+" Talent Submission to Slayt";
	var messageBody = "Dear "+agencyName+"!\n\n";
	messageBody = messageBody + "Your talent "+talentName+" has named you to be their primary talent agency. Please confirm that this talent is in fact signed under your agency. If not, please respond to this email and we will remove you as their agency immediately.\n\n";
	messageBody = messageBody + "Best Regards,\nBrandon Epstein\nFounder of Slayt App";

	Mailgun.sendEmail({
		to: agentEmail,
		from: "support@slayt.com",
		subject: messageSubject,
		text: messageBody
	}, {
		success: function(httpResponse) {
			console.log(httpResponse);
			response.success("Email sent!");
		},
		error: function(httpResponse) {
			console.error(httpResponse);
			response.error("Uh oh, something went wrong");
		}
	});
});
