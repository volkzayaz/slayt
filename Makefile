BUILD_DIR= $(shell pwd)/build
SCHEME_NAME=Slayt
WORKSPACE_NAME=Slayt.xcworkspace
IPA_FILE=$(shell pwd)/ipa/$(SCHEME_NAME).ipa

TARGETS=build_app sign_ipa

ipa: $(TARGETS)

build_app:
	xcodebuild -workspace $(WORKSPACE_NAME) -scheme $(SCHEME_NAME) -configuration Release -sdk "iphoneos" build CONFIGURATION_BUILD_DIR=$(BUILD_DIR)

sign_ipa:
	mkdir -p ipa
	xcrun -sdk iphoneos PackageApplication $(BUILD_DIR)/$(SCHEME_NAME).app -o $(IPA_FILE) --verbose

clean:
	xcodebuild -workspace $(WORKSPACE_NAME) -scheme $(SCHEME_NAME) clean
	rm -rf build
	rm -rf ipa
