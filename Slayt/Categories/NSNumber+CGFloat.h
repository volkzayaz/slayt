//
//  NSNumber+CGFloat.h
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (CGFloat)

- (CGFloat)CGFloatValue;

@end
