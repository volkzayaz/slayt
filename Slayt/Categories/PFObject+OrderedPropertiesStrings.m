//
//  PFObject+OrderedPropertiesStrings.m
//  Talent
//
//  Created by Vlad Soroka on 5/8/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "PFObject+OrderedPropertiesStrings.h"
#import "CRUserDefines.h"

@implementation PFObject (OrderedPropertiesStrings)

- (NSArray*)experienceLevelStrings
{
    return @[kexperienceLevelNoExirience,
             kexperienceLevelSomeExirience,
             kexperienceLevelExirienced,
             kexperienceLevelVeryExirienced,
             ];
    
}

- (NSArray*)cupSizeStrings
{
    return @[kCupSizeNotSpecified,
             kCupSizeAA,
             kCupSizeA,
             kCupSizeB,
             kCupSizeC,
             kCupSizeD,
             kCupSizeE,
             kCupSizeF];
}

- (NSArray*)hairLengthStrings
{
    return @[kHairLegthNotSpecified,
             kHairLegthShort,
             kHairLegthMedium,
             kHairLegthLong];
    
}

- (NSArray*)paymentFrequencyStrings
{
    
    return @[kPaymentFrequencyNotSpecified,
             kPaymentFrequencyHourly,
             kPaymentFrequencyDaily,
             kPaymentFrequencyWorkForFree];
}

- (NSArray*)paymentTypeStrings
{
    return @[kPaymentTypeNotSpecified,
             kPaymentTypeHourly,
             kPaymentTypeFlat];
}

- (NSArray*)jacketSizeStrings
{
    return @[kJacketSizeNotSpecified,
             kJacketSizeXS,
             kJacketSizeS,
             kJacketSizeM,
             kJacketSizeL,
             kJacketSizeXL,
             kJacketSize2XL,
             kJacketSize3XL,
             kJacketSize4XL,
             kJacketSize5XL];
}


@end
