//
//  NSNumber+CGFloat.m
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "NSNumber+CGFloat.h"

@implementation NSNumber (CGFloat)

- (CGFloat)CGFloatValue
{
#if CGFLOAT_IS_DOUBLE
    return self.doubleValue;
#else
    return self.floatValue;
#endif
}

@end
