//
//  NSDate+PassedYears.h
//  Slayt
//
//  Created by Vlad Soroka on 6/5/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (PassedYears)

/**
 *  @return - years that passed from this date to current date. In case this is nil, returns NSNotFound
 */
- (NSUInteger) yearsPassed;

@end
