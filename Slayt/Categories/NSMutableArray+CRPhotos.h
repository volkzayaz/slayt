//
//  NSMutableArray+NSMutableArray_CRPhotos.h
//  Talent
//
//  Created by Vlad Soroka on 4/24/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CRPhoto;

/**
 *  Category provides logic for keeping exactly one CRPhoto object in the array. Note that no [save] calls to Parse are made.
 */
@interface NSMutableArray (NSMutableArray_CRPhotos)

/**
 *  if added photo is the first one to be so, it will be marked as the Main one.
 */
- (void)addPhoto:(CRPhoto*)photo;

/**
 *  if the main photo is removed from array the first found CRPhoto will be marked as the main one.
 *  @return - index of new main photo. If none found or not main photo has been deleted - returns NSNotFound. 
 */
- (NSUInteger)removePhoto:(CRPhoto*)photo;

/**
 *  @return - index of main photo. If none found or not main photo has been deleted - returns NSNotFound. 
 */
- (NSUInteger)indexOfMainPhoto;

@end
