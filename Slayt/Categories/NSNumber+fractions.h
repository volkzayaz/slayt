//
//  NSNumber+fractions.h
//  Slayt
//
//  Created by Vlad Soroka on 5/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DIGITS_AFTER_COMA 2

@interface NSNumber (fractions)

- (NSInteger) integerPart;
- (NSInteger) fractionPart;

@end
