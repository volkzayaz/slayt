//
//  NSNumber+fractions.m
//  Slayt
//
//  Created by Vlad Soroka on 5/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "NSNumber+fractions.h"

@implementation NSNumber (fractions)

- (NSInteger) integerPart{
    return self.integerValue;

}

- (NSInteger) fractionPart{
    NSInteger fractionalPart = roundf((self.floatValue - self.integerPart) * pow(10, DIGITS_AFTER_COMA));
    return abs((int)fractionalPart);
}

@end
