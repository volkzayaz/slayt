//
//  NSDate+PassedYears.m
//  Slayt
//
//  Created by Vlad Soroka on 6/5/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "NSDate+PassedYears.h"

@implementation NSDate (PassedYears)

- (NSUInteger)yearsPassed
{
    NSUInteger yearsPassed = NSNotFound;
    
    if(self)
    {
        NSDateComponents *minimumComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:self toDate:[NSDate date] options:NSCalendarWrapComponents];
        yearsPassed = minimumComponents.year;
    }
    
    return yearsPassed;
}

@end
