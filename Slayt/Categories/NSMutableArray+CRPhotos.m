//
//  NSMutableArray+NSMutableArray_CRPhotos.m
//  Talent
//
//  Created by Vlad Soroka on 4/24/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "NSMutableArray+CRPhotos.h"
#import "CRPhoto.h"

@implementation NSMutableArray (NSMutableArray_CRPhotos)

- (void) addPhoto:(CRPhoto *)photo
{
    NSUInteger index = [self indexOfMainPhoto];
    
    if(index == NSNotFound)
    {
        photo.isMain = YES;
    }
    
    [self addObject:photo];
}

- (NSUInteger) removePhoto:(CRPhoto *)photo
{
    [self removeObject:photo];
    
    NSUInteger index = NSNotFound;
    
    if (photo.isMain) {
        index = [self indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            if([obj isKindOfClass:[CRPhoto class]])
            {
                *stop = YES;
                return YES;
            }
            
            return NO;
        }];
        
        if(index != NSNotFound)
        {
            __weak CRPhoto* photo = self[index];
            photo.isMain = YES;
        }
    }
    
    return index;
}

- (NSUInteger)indexOfMainPhoto
{
    NSUInteger index = [self indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:[CRPhoto class]])
        {
            if(((CRPhoto*)obj).isMain)
            {
                *stop = YES;
                return YES;
            }
        }
        
        return NO;
    }];
    
    return index;
}

@end
