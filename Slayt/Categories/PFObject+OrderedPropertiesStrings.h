//
//  PFObject+OrderedPropertiesStrings.h
//  Talent
//
//  Created by Vlad Soroka on 5/8/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Parse/Parse.h>

/**
 *   @discussion Category allows shared access to all PFObjects to stringsArrays of ordered properties(see for example class CRModel property JacketSize).
 *   These properties are accessed via [obj valueForKey:@"|nameOfOrderedProperties| + Strings"] so in case you add new ordered property, be mindfull in naming
 */
@interface PFObject (OrderedPropertiesStrings)

- (NSArray*) jacketSizeStrings;
- (NSArray*) experienceLevelStrings;
- (NSArray*) hairLengthStrings;
- (NSArray*) paymentFrequencyStrings;
- (NSArray*) paymentTypeStrings;
- (NSArray*) cupSizeStrings;

@end
