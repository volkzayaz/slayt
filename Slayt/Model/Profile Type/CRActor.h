//
//  CRActor.h
//  Talent
//
//  Created by Vlad Soroka on 5/5/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Parse/Parse.h>
#import "CRProfileProtocol.h"
#import "CRUserDefines.h"

@interface CRActor : PFObject <PFSubclassing, CRProfileProtocol>

@property (nonatomic, assign) CRExperienceLevel experienceLevel;

@property (nonatomic, assign) CRPaymentFrequency paymentFrequency;
@property (nonatomic, strong) NSNumber* payRate;

@property (nonatomic, strong) NSNumber* weight;
@property (nonatomic, strong) NSString* eyeColor;
@property (nonatomic, strong) NSString* hairColor;

@end
