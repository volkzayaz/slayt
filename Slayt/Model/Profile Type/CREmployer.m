//
//  CREmployer.m
//  Slayt
//
//  Created by Vlad Soroka on 5/22/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CREmployer.h"
#import "PFObject+OrderedPropertiesStrings.h"
#import <Parse/PFObject+Subclass.h>

@implementation CREmployer

@dynamic paymentType;
@dynamic payRate;

@dynamic projectDescription;
@dynamic launchDate;
@dynamic usage;

+(NSString *)parseClassName
{
    return @"Employer";
}

#pragma mark - ProfileProtocol

- (NSString*) profilePrefix
{
    return @"employer.";
}

- (NSString*)profileName
{
    return @"Looking to Hire";
}

- (CRProfileType) profileType
{
    return CRProfileTypeEmployer;
}

- (NSString*) profilePropertiesPlistFileName
{
    return @"EmployerDetailsList";
}

- (NSString*)valueDescriptionForKey:(NSString *)key
{
    NSString* detailString = nil;
    
    if([key isEqualToString:@"launchDate"])
    {
        NSDate* launchDate = self.launchDate;
        
        detailString = [NSDateFormatter localizedStringFromDate:launchDate
                                                      dateStyle:NSDateFormatterShortStyle
                                                      timeStyle:NSDateFormatterShortStyle];
    }
    else if([key isEqualToString:@"payRate"])
    {
        NSString* string;
        if (self.paymentType == CRPaymentTypeHourly)
        {
            string = [NSString stringWithFormat:@"$%li/h",(long)self.payRate.integerValue];
        }
        else if(self.paymentType == CRPaymentTypeFlat)
        {
            string = [NSString stringWithFormat:@"$%li",(long)self.payRate.integerValue];
        }
        else
        {
            string = @"Not Specified";
        }
        detailString = string;
    }
    
    return detailString;
}

- (id<CRProfileProtocol>)duplicateProfile
{
    CREmployer* duplicateEmployer = [CREmployer object];
    
    for(NSString* key in self.allKeys)
    {
        duplicateEmployer[key] = [[self valueForKey:key] copy];
    }
    
    return duplicateEmployer;
}

#pragma mark - private methods

- (void)setObject:(id)object forKeyedSubscript:(NSString *)key
{
    [self willChangeValueForKey:key];
    
    if([object isEqual:[NSNull null]])
    {
        //We need to store nil value in our local CRUser manually, because parse stores [NSNull null] object to our nil properties and that is not something we want
        
        [self setValue:nil forKey:key];
        [self didChangeValueForKey:key];
        return;
    }
    
    if([key isEqualToString:@"paymentType"])
    {
        NSNumber* number = object;
        NSAssert(number.unsignedIntegerValue < CRPaymentTypeCOUNT || number.unsignedIntegerValue < 0, @"Logic Error. An attempt to store value %lu which not belongs to acceptable CRPaymentType range 0<= value  < %lu",(unsigned long)number.unsignedIntegerValue, (unsigned long)CRPaymentFrequencyCOUNT);
        CRPaymentType pt = number.unsignedIntegerValue;
        self.paymentType = pt;
    }
    else
    {
        [super setObject:object forKeyedSubscript:key];
    }
    
    [self didChangeValueForKey:key];
}

- (id)objectForKeyedSubscript:(NSString *)key
{
    if([key isEqualToString:@"paymentType"])
    {
        NSAssert(self.paymentType < self.paymentTypeStrings.count, @"Logic error. paymentTypeStrings was queried for %lu-th objet while having %lu elements",(long)self.paymentType
                 ,(unsigned long)self.paymentTypeStrings.count);
        
        return self.paymentTypeStrings[self.paymentType];
    }
    else
    {
        return [super objectForKey:key];
    }
    
}

@end
