//
//  CRClothingStylist.h
//  Talent
//
//  Created by Vlad Soroka on 5/5/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Parse/Parse.h>
#import "CRProfileProtocol.h"

@interface CRClothingStylist : PFObject <PFSubclassing, CRProfileProtocol>

@property (nonatomic, assign) CRPaymentFrequency paymentFrequency;
@property (nonatomic, strong) NSNumber* payRate;

@property (nonatomic, strong) NSArray* jobTypes;

@end
