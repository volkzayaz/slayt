//
//  CRTalentAgency.m
//  Talent
//
//  Created by Vlad Soroka on 5/12/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRTalentAgency.h"
#import <Parse/PFObject+Subclass.h>
#import "PFObject+OrderedPropertiesStrings.h"

@implementation CRTalentAgency

@dynamic jobTypes;

+ (NSString*)parseClassName
{
    return @"TalentAgency";
}

#pragma mark - CRProfileProtocol

- (NSString*)profilePrefix
{
    return @"talentAgency.";
}

- (NSString*)profileName
{
    return @"Model/Talent Agency";
}

- (CRProfileType)profileType
{
    return CRProfileTypeTalentAgency;
}

- (NSString*)profilePropertiesPlistFileName
{
    return @"TalentAgencyDetailsList";
}

- (NSString*)valueDescriptionForKey:(NSString *)key
{
    NSString* detailString = nil;
    
    if([key isEqualToString:@"jobTypes"])
    {
        NSString* components = @", ";
        detailString = [self.jobTypes componentsJoinedByString:components];
    }
    
    return detailString;
}

- (id<CRProfileProtocol>)duplicateProfile
{
    CRTalentAgency* duplicateTA = [CRTalentAgency object];
    
    for(NSString* key in self.allKeys)
    {
        duplicateTA[key] = [[self valueForKey:key] copy];
    }
    
    return duplicateTA;
}

- (void)setObject:(id)object forKeyedSubscript:(NSString *)key
{
    [self willChangeValueForKey:key];
    
    if([object isEqual:[NSNull null]])
    {
        //We need to store nil value in our local CRUser manually, because parse stores [NSNull null] object to our nil properties and that is not something we want
        
        [self setValue:nil forKey:key];
        [self didChangeValueForKey:key];
        return;
    }
    
    [super setObject:object forKeyedSubscript:key];
    
    [self didChangeValueForKey:key];
}

@end
