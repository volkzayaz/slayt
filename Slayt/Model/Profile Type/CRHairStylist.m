//
//  CRHairStylist.m
//  Talent
//
//  Created by Vlad Soroka on 5/5/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRHairStylist.h"
#import <Parse/PFObject+Subclass.h>

#import "PFObject+OrderedPropertiesStrings.h"

@implementation CRHairStylist

@dynamic paymentFrequency;
@dynamic payRate;
@dynamic experienceLevel;
@dynamic jobTypes;

+ (NSString *)parseClassName
{
    return @"CRHairStylist";
}

#pragma makr - CRProfileProtocol

- (NSString*)profileName
{
    return @"Hair Stylist";
}

- (NSString*)profilePrefix
{
    return @"hairStylist.";
}

- (CRProfileType)profileType
{
    return CRProfileTypeHairStylist;
}

- (NSString*)profilePropertiesPlistFileName
{
    return @"HairlStylistDetailsList";
}

- (NSString*)valueDescriptionForKey:(NSString *)key
{
    NSString* detailString = nil;
    
    if([key isEqualToString:@"jobTypes"])
    {
        NSString* components = @", ";
        detailString = [self.jobTypes componentsJoinedByString:components];
    }
    else if([key isEqualToString:@"payRate"])
    {
        NSString* string;
        if(self.paymentFrequency == CRPaymentFrequencyWorkForFree)
        {
            string = @"Will work for free";
        }
        else if (self.paymentFrequency == CRPaymentFrequencyHourly)
        {
            string = [NSString stringWithFormat:@"$%li/h",(long)self.payRate.integerValue];
        }
        else if(self.paymentFrequency == CRPaymentFrequencyDaily)
        {
            string = [NSString stringWithFormat:@"$%li/d",(long)self.payRate.integerValue];
        }
        else
        {
            string = @"Not Specified";
        }
        detailString = string;
    }
    
    
    return detailString;
}

- (id<CRProfileProtocol>)duplicateProfile
{
    CRHairStylist* duplicateHS = [CRHairStylist object];
    
    for(NSString* key in self.allKeys)
    {
        duplicateHS[key] = [[self valueForKey:key] copy];
    }
    
    return duplicateHS;
}

#pragma mark - CRProfileProtocol

- (CRPaymentFrequency)_paymentFrequency
{
    return self.paymentFrequency;
}

- (void)_setPaymentFrequency:(CRPaymentFrequency)paymentFrequency
{
    self.paymentFrequency = paymentFrequency;
}

- (NSNumber *)_payRate
{
    return self.payRate;
}

#pragma mark - privateMethods

- (void)setObject:(id)object forKeyedSubscript:(NSString *)key
{
    [self willChangeValueForKey:key];
    
    if([object isEqual:[NSNull null]])
    {
        //We need to store nil value in our local CRUser manually, because parse stores [NSNull null] object to our nil properties and that is not something we want
        
        [self setValue:nil forKey:key];
        [self didChangeValueForKey:key];
        return;
    }
    
    if([key isEqualToString:@"paymentFrequency"])
    {
        NSNumber* number = object;
        NSAssert(number.unsignedIntegerValue < CRPaymentFrequencyCOUNT || number.unsignedIntegerValue < 0, @"Logic Error. An attempt to store value %lu which not belongs to acceptable CRPaymentFrequency range 0<= value  < %lu",(unsigned long)number.unsignedIntegerValue, (unsigned long)CRPaymentFrequencyCOUNT);
        CRPaymentFrequency pf = number.unsignedIntegerValue;
        self.paymentFrequency = pf;
    }
    else if([key isEqualToString:@"experienceLevel"])
    {
        NSNumber* number = object;
        NSAssert(number.unsignedIntegerValue < CRExperienceLevelCOUNT || number.unsignedIntegerValue < 0, @"Logic Error. An attempt to store value %lu which not belongs to acceptable CRExperienceLeveL range 0<= value  < %lu",(unsigned long)number.unsignedIntegerValue, (unsigned long)CRExperienceLevelCOUNT);
        CRExperienceLevel el = number.unsignedIntegerValue;
        self.experienceLevel = el;
    }
    else
    {
        [super setObject:object forKeyedSubscript:key];
    }
    
    [self didChangeValueForKey:key];
}

- (id)objectForKeyedSubscript:(NSString *)key
{
    if([key isEqualToString:@"experienceLevel"])
    {
        NSAssert(self.experienceLevel < self.experienceLevelStrings.count, @"Logic error. experienceLevelStrings was queried for %lu-th objet while having %lu elements",(long)self.experienceLevel,(unsigned long)self.experienceLevelStrings.count);
        
        return self.experienceLevelStrings[self.experienceLevel];
    }
    else if([key isEqualToString:@"paymentFrequency"])
    {
        NSAssert(self.paymentFrequency < self.paymentFrequencyStrings.count, @"Logic error. paymentFrequencyStrings was queried for %lu-th objet while having %lu elements",(long)self.paymentFrequency
                 ,(unsigned long)self.paymentFrequencyStrings.count);
        
        return self.paymentFrequencyStrings[self.paymentFrequency];
    }
    else
    {
        return [super objectForKey:key];
    }
    
}


@end
