//
//  CRProfileProtocol.h
//  Talent
//
//  Created by Vlad Soroka on 5/6/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRUserDefines.h"

@class CRUser;

static NSString* isHidingForSearchKey = @"hideForSearch";

@protocol CRProfileProtocol <NSObject>
@required


/**
 *  @return String object that describes fileName of plist of all the neccessary properties of profileType. It will be used for presenting profile information for the user and editing values of the profile.
 */
- (NSString*)profilePropertiesPlistFileName;

/**
  *  @discussion you use this prefix in defining propertyName field in plists of your specific profileType. For example, you can choose prefix as "model." for Model profile type. Since Model is part of User proifle type and has property height you are able to access height property via user[@"model.height"] and set height value via user[@"model.height"] = @(12).
 *  @return string prefix that will be used to distinguish profileProperties.

 */
- (NSString*)profilePrefix;

/**
 *  @discussion This method is your chance to apply specific display to any profile-specific data that will be shown to the user. 
 *  @return The string that will be shown to the user describing the value for |key|. You may return nil an in this case the app will take responsibility to put string in place.
 */
- (NSString*)valueDescriptionForKey:(NSString*)key;


/**
 *  @return The name of the profile. It will be used in UI as a string that represents your profile 
 */
- (NSString*)profileName;


- (CRProfileType)profileType;

/**
 *  @return local copy of self with all the data that can be edited by the user.
 */
- (id<CRProfileProtocol>)duplicateProfile;

@end
