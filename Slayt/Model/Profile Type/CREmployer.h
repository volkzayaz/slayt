//
//  CREmployer.h
//  Slayt
//
//  Created by Vlad Soroka on 5/22/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Parse/Parse.h>
#import "CRProfileProtocol.h"
#import "CRUserDefines.h"

@interface CREmployer : PFObject <PFSubclassing, CRProfileProtocol>

@property (nonatomic, assign) CRPaymentType paymentType;
@property (nonatomic, strong) NSNumber*     payRate;

@property (nonatomic, strong) NSDate* launchDate;

@property (nonatomic, strong) NSString* usage;
@property (nonatomic, strong) NSString* projectDescription;

@end
