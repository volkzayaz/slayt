//
//  CRModel.h
//  Talent
//
//  Created by Vlad Soroka on 5/5/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Parse/Parse.h>
#import "CRUserDefines.h"

#import "CRProfileProtocol.h"

#import "CRUser.h"

@interface CRModel : PFObject <PFSubclassing, CRProfileProtocol>

@property (nonatomic, strong) NSArray* jobTypes;

///sizes
///Female-specific sizes
@property (nonatomic, strong) NSNumber* bust;
@property (nonatomic, strong) NSNumber* hip;
@property (nonatomic, strong) NSNumber* dress;

///Male-specific sizes
@property (nonatomic, strong) NSNumber* inseam;
@property (nonatomic, strong) NSNumber* neck;
@property (nonatomic, strong) NSNumber* shirtSleeve;

@property (nonatomic, assign) CRJacketSize jacketSize;

///Shared Sizes
@property (nonatomic, strong) NSNumber* waist;
@property (nonatomic, strong) NSNumber* shoe;


///physicalAttributes
@property (nonatomic, strong) NSString* eyeColor;
@property (nonatomic, strong) NSString* hairColor;
@property (nonatomic, strong) NSString* ethnicLook;


///Ordered properties. For example experienceLevel(hairLength,cupSize) does not have any number to be presented. However we know that "experienced" is greater than "no experience". Number Value represents unit that we can compare between each other. NSArray contains strings for all possible values. You can obtain number value with user.experienceLevel. Note that user[@"experienceLevel"] will return corresponding string value. For adding new ordered property you have to name your strings array as <yourPropertyName>Strings and put them into category PFObject+OrderedPropertiesStrings. Also you are responsible for putting code in objectForKeyedSubscript: similar to presented properties
@property (nonatomic, assign) CRExperienceLevel experienceLevel;
@property (nonatomic, assign) CRHairLength hairLength;

@property (nonatomic, assign) CRPaymentFrequency paymentFrequency;
@property (nonatomic, strong) NSNumber* payRate;

@property (nonatomic, assign) CRCupSize cupSize;


@end
