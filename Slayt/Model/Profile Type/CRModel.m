//
//  CRModel.m
//  Talent
//
//  Created by Vlad Soroka on 5/5/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRModel.h"
#import <Parse/PFObject+Subclass.h>
#import "PFObject+OrderedPropertiesStrings.h"
#import "CRUtils.h"

@interface CRModel ()


@end

@implementation CRModel

@dynamic bust;
@dynamic cupSize;
@dynamic waist;
@dynamic hip;
@dynamic shoe;
@dynamic dress;
@dynamic jacketSize;
@dynamic inseam;
@dynamic neck;
@dynamic shirtSleeve;

@dynamic eyeColor;
@dynamic hairColor;
@dynamic hairLength;
@dynamic ethnicLook;

@dynamic jobTypes;
@dynamic experienceLevel;

@dynamic payRate;
@dynamic paymentFrequency;

+ (NSString*)parseClassName
{
    return @"CRModel";
}

- (void)setObject:(id)object forKeyedSubscript:(NSString *)key
{
    [self willChangeValueForKey:key];
    
    if([object isEqual:[NSNull null]])
    {
        //We need to store nil value in our local CRUser manually, because parse stores [NSNull null] object to our nil properties and that is not something we want
        
        [self setValue:nil forKey:key];
        [self didChangeValueForKey:key];
        return;
    }
    
    if([key isEqualToString:@"paymentFrequency"])
    {
        NSNumber* number = object;
        NSAssert(number.unsignedIntegerValue < CRPaymentFrequencyCOUNT || number.unsignedIntegerValue < 0, @"Logic Error. An attempt to store value %lu which not belongs to acceptable CRPaymentFrequency range 0<= value  < %lu",(unsigned long)number.unsignedIntegerValue, (unsigned long)CRPaymentFrequencyCOUNT);
        CRPaymentFrequency pf = number.unsignedIntegerValue;
        self.paymentFrequency = pf;
    }
    else if([key isEqualToString:@"hairLength"])
    {
        NSNumber* number = object;
        NSAssert(number.unsignedIntegerValue < CRHairLengthCOUNT || number.unsignedIntegerValue < 0, @"Logic Error. An attempt to store value %lu which not belongs to acceptable CRHairLength range 0<= value  < %lu",(unsigned long)number.unsignedIntegerValue, (unsigned long)CRHairLengthCOUNT);
        CRHairLength hl = number.unsignedIntegerValue;
        self.hairLength = hl;
        
    }
    else if([key isEqualToString:@"cupSize"])
    {
        NSNumber* number = object;
        NSAssert(number.unsignedIntegerValue < CRCupSizeCOUNT || number.unsignedIntegerValue < 0, @"Logic Error. An attempt to store value %lu which not belongs to acceptable CRCupSize range 0<= value  < %lu",(unsigned long)number.unsignedIntegerValue, (unsigned long)CRCupSizeCOUNT);
        CRCupSize cs = number.unsignedIntegerValue;
        self.cupSize = cs;
    }
    else if([key isEqualToString:@"experienceLevel"])
    {
        NSNumber* number = object;
        NSAssert(number.unsignedIntegerValue < CRExperienceLevelCOUNT || number.unsignedIntegerValue < 0, @"Logic Error. An attempt to store value %lu which not belongs to acceptable CRExperienceLeveL range 0<= value  < %lu",(unsigned long)number.unsignedIntegerValue, (unsigned long)CRExperienceLevelCOUNT);
        CRExperienceLevel el = number.unsignedIntegerValue;
        self.experienceLevel = el;
    }
    else if([key isEqualToString:@"jacketSize"])
    {
        NSNumber* number = object;
        NSAssert(number.unsignedIntegerValue < CRJacketSizeCOUNT || number.unsignedIntegerValue < 0, @"Logic Error. An attempt to store value %lu which not belongs to acceptable CRJacketSize range 0<= value  < %lu",(unsigned long)number.unsignedIntegerValue, (unsigned long)CRJacketSizeCOUNT);
        CRJacketSize js = number.unsignedIntegerValue;
        self.jacketSize = js;
    }
    else
    {
        [super setObject:object forKeyedSubscript:key];
    }
    
    [self didChangeValueForKey:key];
}

- (id)objectForKeyedSubscript:(NSString *)key
{
    if([key isEqualToString:@"experienceLevel"])
    {
        NSAssert(self.experienceLevel < self.experienceLevelStrings.count, @"Logic error. experienceLevelStrings was queried for %lu-th objet while having %lu elements",(long)self.experienceLevel,(unsigned long)self.experienceLevelStrings.count);
        
        return self.experienceLevelStrings[self.experienceLevel];
    }
    else if([key isEqualToString:@"cupSize"])
    {
        NSAssert(self.cupSize < self.cupSizeStrings.count, @"Logic error. CupSizeStrings was queried for %lu-th objet while having %lu elements",(long)self.cupSize,(unsigned long)self.cupSizeStrings.count);
        
        return self.cupSizeStrings[self.cupSize];
    }
    else if([key isEqualToString:@"hairLength"])
    {
        NSAssert(self.hairLength < self.hairLengthStrings.count, @"Logic error. hairLegthStrings was queried for %lu-th objet while having %lu elements",(long)self.hairLength,(unsigned long)self.hairLengthStrings.count);
        
        return self.hairLengthStrings[self.hairLength];
    }
    else if([key isEqualToString:@"paymentFrequency"])
    {
        NSAssert(self.paymentFrequency < self.paymentFrequencyStrings.count, @"Logic error. paymentFrequencyStrings was queried for %lu-th objet while having %lu elements",(long)self.paymentFrequency
                 ,(unsigned long)self.paymentFrequencyStrings.count);
        
        return self.paymentFrequencyStrings[self.paymentFrequency];
    }
    else if([key isEqualToString:@"jacketSize"])
    {
        NSAssert(self.jacketSize < self.jacketSizeStrings.count, @"Logic error. jacketSizeStrings was queried for %lu-th objet while having %lu elements",(long)self.jacketSize
                 ,(unsigned long)self.jacketSizeStrings.count);
        
        return self.jacketSizeStrings[self.jacketSize];
    }
    else
    {
        return [super objectForKey:key];
    }

}

#pragma mark - ProfileProtocol methods

- (CRProfileType)profileType
{
    return CRProfileTypeModel;
}

- (NSString*) profilePrefix
{
    return @"model.";
}

- (NSString*)profileName
{
    return @"Model";
}

- (NSString*)profilePropertiesPlistFileName
{
    return @"ModelDetailsList";
}

- (NSString*) valueDescriptionForKey:(NSString *)key
{
    NSString* detailString = nil;
    
    if([key isEqualToString:@"jobTypes"])
    {
        NSString* components = @", ";
        detailString = [self.jobTypes componentsJoinedByString:components];
    }
    else if([key isEqualToString:@"payRate"])
    {
        NSString* string;
        if(self.paymentFrequency == CRPaymentFrequencyWorkForFree)
        {
            string = @"Will work for free";
        }
        else if (self.paymentFrequency == CRPaymentFrequencyHourly)
        {
            string = [NSString stringWithFormat:@"$%li/h",(long)self.payRate.integerValue];
        }
        else if(self.paymentFrequency == CRPaymentFrequencyDaily)
        {
            string = [NSString stringWithFormat:@"$%li/d",(long)self.payRate.integerValue];
        }
        else
        {
            string = @"Not Specified";
        }
        detailString = string;
    }
    
    return detailString;
}

- (id<CRProfileProtocol>)duplicateProfile
{
    CRModel* duplicateModel = [CRModel object];
    
    for(NSString* key in self.allKeys)
    {
        duplicateModel[key] = [[self valueForKey:key] copy];
    }
    
    return duplicateModel;
}

#pragma mark - private methods

@end
