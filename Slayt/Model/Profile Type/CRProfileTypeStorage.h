//
//  CRProfileTypes.h
//  Talent
//
//  Created by Vlad Soroka on 5/5/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRProfileProtocol.h"
#import "CRUserDefines.h"

/**
 *  @discussion The class is generally a tool for ProfileManagment. It presents an interface o add/remove/edit different profile 
 *  roles and keeps track of buisness logic like "only profile types from one category may be added" or "the very last profile type may not be removed. The manager does not perform any save to parse actions(except for explicit [CRProfileTypeStorageInstance saveAll] calls). It's up to you to perform save on storage file that you will pass in init method.
 */
@interface CRProfileTypeStorage : NSObject

@property (nonatomic, strong, readonly) NSMutableArray* profileTypesArray;

@property (nonatomic, strong, readonly) NSArray* profileTypeGroups;
@property (nonatomic, strong, readonly) NSArray* profileTypeGroupsStrings;

/**
 *  @param storage - property that will be stored on parse. All the profileTypes that are created by this class will be stored to storage. The key is CRProfileType number, the value - PFObject<CRProfileProtocol>*
 *  @discussion initialize the storage as soon as possible. It may take some small amount of time for all ProfileTypes to be fetched from parse
 */
- (instancetype)initWithDictionaryStorage:(NSMutableArray*)storage;

/////addingProfileTypes

/**
 *  Method adds new profile type. You can only add profiles from one profile groups. For example user can be both Model and Actor because they are from the same profile group, but s/he cannot be both photographer and hair stylist because they belong to different profileTypeGroups. You can always check property profileTypeGroups. It contains all groups.
 *  No saving to parse actions are done
 *  @return if profile has been create successfully the reference to it is returned.
 */
- (PFObject<CRProfileProtocol>*)addProfileForType:(CRProfileType)type;

/**
 *  @discussion this method has the same effect as addProfileForType: but you can pass string names of profileTypes. All of them can be found in profileTypeGroupsStrings property of this class.
 */
- (PFObject<CRProfileProtocol>*)addProfileForTypeName:(NSString*)profileTypeName;

///removing profiles

/**
 *  Mehtod will remove given profile from storage
 *  @return YES if profile was successfully removed from storage. No - if error occured.
 */
- (BOOL)removeProfileType:(PFObject<CRProfileProtocol>*)profile;


////accessing profileTypes

/**
 *  @return pointer to the object that was created through addProfileTypeForKey: method OR nil if no profiles were added for that key.
 *  @param type - The key you've used for adding ProfileType.
 *  @discussion method will fetch profile from server if no data avaliable locally and therefore might take a while to complete.
 */
- (PFObject<CRProfileProtocol>*) profileForType:(CRProfileType)type;

/**
 *  @return profile type that has similar prefix to key.
 *  @discussion it is absolutelly normal for key just to start with prefix not match it. Method will fetch profile from server if no data avaliable locally and therefore might take a while to complete.
 */
- (PFObject<CRProfileProtocol>*) profileTypeForPrefixedKey:(NSString*)key;

/**
 *  @discussion Method will take prefixed key and search in ProfileType classes for similar prefix. For example if Model profileType has prefix @"model." and you pass @"model.bust" to this method than you'll receive [CRModel class] object.
 *  @return Class object of appropriate ProfileType or nil if no class registered prefix that was passed.
 */
- (Class) profileTypeClassForPrefixedKey:(NSString*)key;

- (Class) classForProfileTypeName:(NSString*)profileTypeName;

/**
 *  @return String that has format ProfileType1/ProfileType2/ProfileType3 OR @"Not Specified" if no profileTypes are added to the storage
 */
- (NSString*) stringOfAllProfileTypes;

/**
 *  @param - delimiter. You can specify the delimiter.
 */
- (NSString*) stringOfAllProfileTypesWithDelimiter:(NSString*)delimiter;

/**
 *  @return Array of profileTypes strings that were added to the storage.
 */
- (NSArray*) profileTypesStrings;

/**
 *  @discussion Each Profile type belongs to some group. Any user can have profile types withing one group only. This method substracts majority of added profiles to storage from majority of all profileTypes within the group. For example if we add Model to the storage the result of this method will return Actor since it's the only profileType within the group that has not been added yet.
    @return array wich consisted of strings. Each of them represent profileType name which you can both present to user and pass to addProfileForTypeName:
 */
- (NSArray*) arrayOfStringsFromAbsentProfileTypes;

/**
 *  @return - array of all existing ProfileTypes except those that were specified in extraProfileTypes array.
 *  @param - extraProfileTypes. Array of all profileTypes that should not be included in resulting array.
 */
- (NSArray*) stringsArrayOfAllProfilesExclude:(NSArray*)extraProfileTypes;

/**
 *  @discussion Makes a consecutive calls to PFObject's save call on each profileType that was stored on the calling thread. Also deletes all profiles from server wich were removed via removeProfileType: call
 */
- (void)saveAll;
- (void)saveAllInBackground;

/**
 *  @discussion Refreshes all stroed profile types. Method might take a while to complete so consider to run it in background.
 *  @param storage - optional parametr with newly arrived values of profileReferencesArray. Note, that if this parametr is not nil any profiles that were removed via removeProfileType: call will be deleted from server
 */
- (void)refreshProfiles:(NSMutableArray*)storage;

/**
 *  @discussion All the previous profiles will be marked as deleted.
 */
- (void)replaceProfilesWithStorage:(NSMutableArray*)storage;

- (NSError*) fetchAllProfileTypes;

@end
