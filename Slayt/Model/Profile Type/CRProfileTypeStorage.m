//
//  CRProfileTypes.m
//  Talent
//
//  Created by Vlad Soroka on 5/5/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRProfileTypeStorage.h"

#import "CRModel.h"
#import "CRActor.h"
#import "CRClothingStylist.h"
#import "CRHairStylist.h"
#import "CRMakeupArtist.h"
#import "CRPhotographer.h"
#import "CRTalentAgency.h"
#import "CREmployer.h"

@interface CRProfileTypeStorage ()

@property (nonatomic, readonly ,strong) NSDictionary* profileTypesClasses;//key is kProfileType constant string, value is Class variable

@property (nonatomic, strong) NSMutableArray* profilesToDelete;

@end

@implementation CRProfileTypeStorage

@synthesize profileTypesArray = _profileTypesArray;

- (instancetype)initWithDictionaryStorage:(NSMutableArray *)storage
{
    self = [super init];
    if(self)
    {
        _profileTypeGroups =   @[ @[ @(CRProfileTypeModel), @(CRProfileTypeActor) ],
                                  @[ @(CRProfileTypePhotographer) ],
                                  @[ @(CRProfileTypeHairStylist), @(CRProfileTypeMakeupArtist), @(CRProfileTypeClothingStylist) ],
                                  @[ @(CRProfileTypeTalentAgency) ],
                                  @[ @(CRProfileTypeEmployer) ] ];
        
        _profileTypeGroupsStrings = @[ @[ [CRModel object].profileName, [CRActor object].profileName ],
                                       @[ [CRPhotographer object].profileName ],
                                       @[ [CRHairStylist object].profileName, [CRMakeupArtist object].profileName, [CRClothingStylist object].profileName ],
                                       @[ [CRTalentAgency object].profileName ],
                                       @[ [CREmployer object].profileName     ]     ];
        
        _profileTypesClasses = @{ @(CRProfileTypeModel): [CRModel class],
                                  @(CRProfileTypeActor): [CRActor class],
                                  
                                  @(CRProfileTypeClothingStylist) : [CRClothingStylist class],
                                  @(CRProfileTypeHairStylist) : [CRHairStylist class],
                                  @(CRProfileTypeMakeupArtist) : [CRMakeupArtist class],
                                  @(CRProfileTypePhotographer) : [CRPhotographer class],
                                  
                                  @(CRProfileTypeTalentAgency) : [CRTalentAgency class],
                                  
                                  @(CRProfileTypeEmployer)     : [CREmployer class]
                                  
                                  };
        
        _profilesToDelete = [NSMutableArray array];
        
        self.profileTypesArray = storage;//we intentioally have strong reference(not copying) in order to manipulate with property that will be saved to parse later as part of [CRUser save] acttion.

   }
    
    return self;
}

- (instancetype) init
{
    return [self initWithDictionaryStorage:nil];
}

- (NSError*) fetchAllProfileTypes
{
    for (PFObject<CRProfileProtocol>* obj in self.profileTypesArray)
    {
        NSError *error = nil;
        if (obj && ![obj isKindOfClass:[NSNull class]]) {
            [obj fetchIfNeeded:&error];
            if (error) {
                NSLog(@"CRProfileTypeStorage:initWithDictionaryStorage error: %@", error.localizedDescription);
                return error;
            }
        }
        else
        {
            NSLog(@"ERROR! Passed profileTypeStorage contains Pointer that point to non existing profileType");
        }
    }
    
    return nil;
}

- (void)setProfileTypesArray:(NSMutableArray *)profileTypesArray
{
    _profileTypesArray = profileTypesArray;
}

- (PFObject<CRProfileProtocol>*)addProfileForType:(CRProfileType)type
{
    NSArray* profileGroup = [self profileGroupForType:type];
    if(!profileGroup)
    {
        NSLog(@"WARNING: An attempt to add profileType with unsupported type (CRProfileType)%lx",(long)type);
        return nil;
    }
    
    NSArray* existingProfileTypesKeys = self.profileTypesArray;
    
    NSUInteger index = [existingProfileTypesKeys indexOfObjectPassingTest:^BOOL(PFObject<CRProfileProtocol>* obj, NSUInteger idx, BOOL *stop) {
        *stop = obj.profileType == type;
        return *stop;
    }];
    
    if(index != NSNotFound)
    {
        NSLog(@"WARNING: An attempt to add existing profileType (CRProfileType)%lu",(long)type);
        return nil;//we've alredy got a profileType with |key|
    }
    
    PFObject<CRProfileProtocol>* existingProfile = existingProfileTypesKeys.firstObject;
    
    if(existingProfile)
    {
        NSArray* ourGroup = [self profileGroupForType:existingProfile.profileType];
        if(![ourGroup containsObject:@(type)])
        {
            NSLog(@"WARNING: An attempt to add profileType (CRProfileType)%lu to profiles group %@", (long)type, ourGroup);
            return nil;
        }
    }
    
    Class profileTypeClass = self.profileTypesClasses[@(type)];
    
    NSAssert([profileTypeClass isSubclassOfClass:[PFObject class]], @"%@ is Not PFObject subclass and should not be in profileTypesClasses dictionary",profileTypeClass);
    
    NSAssert([profileTypeClass conformsToProtocol:@protocol(CRProfileProtocol)], @"%@ does not conform to CRProfileProtocol and, therefore, may not be used as profileType class for type (CRProfileType)%lu",profileTypeClass, (long)type);

    PFObject<CRProfileProtocol>* newProfileType = [profileTypeClass object];//if assertion passed then it should respond to +(instancetype)object message
    
    [self.profileTypesArray addObject:newProfileType];
    return newProfileType;
}

- (PFObject<CRProfileProtocol> *)addProfileForTypeName:(NSString *)profileTypeName
{
    CRProfileType __block type = CRProfileTypeCount;
    
    [self.profileTypesClasses enumerateKeysAndObjectsUsingBlock:^(NSNumber* profileType, Class profileClass, BOOL *stop) {
        NSString* proifleNameForCurrentClass = [[profileClass object] profileName];
        if([proifleNameForCurrentClass isEqualToString:profileTypeName])
        {
            *stop = YES;
            type = (CRProfileType)profileType.unsignedIntegerValue;
        }
    }];
    
    if(type == CRProfileTypeCount)
    {
        NSLog(@"WARNING: An attempt to add profileType with unsupported typeName %@",profileTypeName);
        return nil;
    }
    
    return [self addProfileForType:type];
}

- (BOOL)removeProfileType:(PFObject<CRProfileProtocol> *)profile
{
    if(self.profileTypesArray.count == 1)
    {
        NSLog(@"WARNING: An attempt to remove very last profile %@",profile);
        return NO;
    }
    
    [self.profilesToDelete addObject:profile];
    [self.profileTypesArray removeObject:profile];
    return YES;
}

- (PFObject<CRProfileProtocol>*)profileForType:(CRProfileType)type
{
    PFObject<CRProfileProtocol>* __block profileType = nil;
    
    Class askedProfileClass = self.profileTypesClasses[@(type)];
    
    [self.profileTypesArray enumerateObjectsUsingBlock:^(PFObject<CRProfileProtocol>* obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:askedProfileClass])
        {
            profileType = obj;
            *stop = YES;
        }
    }];
    
    if(profileType && !profileType.isDataAvailable)
    {
        NSLog(@"WARNING: Logic Error. Profile Type %@ is still needs to be fetched after initialization.",profileType);
    }
    
    return profileType;
}

- (PFObject<CRProfileProtocol>*)profileTypeForPrefixedKey:(NSString *)key
{
    NSArray* arrayOfAllProfiles = self.profileTypesArray;
    PFObject<CRProfileProtocol>* __block rightProfile = nil;
    
    [arrayOfAllProfiles enumerateObjectsUsingBlock:^(id<PFSubclassing,CRProfileProtocol> obj, NSUInteger idx, BOOL *stop) {
        if([key hasPrefix:[obj profilePrefix]])
        {
            *stop = YES;
            rightProfile = obj;
        }
    }];
    
    if(rightProfile && !rightProfile.isDataAvailable)
    {
        NSLog(@"WARNING: Logic Error. Profile Type %@ is still needs to be fetched after initialization.",rightProfile);
    }
    
    return rightProfile;
}

- (Class)profileTypeClassForPrefixedKey:(NSString *)key
{
    NSArray* allProfileTypeClasses = self.profileTypesClasses.allValues;
    Class __block rightProfileTypeClass = nil;
    
    [allProfileTypeClasses enumerateObjectsUsingBlock:^(Class obj, NSUInteger idx, BOOL *stop) {
        NSString* profileTypePrefix = [[obj object] profilePrefix];
        
        if([key hasPrefix:profileTypePrefix])
        {
            *stop = YES;
            rightProfileTypeClass = obj;
        }
    }];
    
    return rightProfileTypeClass;
}

- (Class)classForProfileTypeName:(NSString *)profileTypeName
{
    Class __block rightProfileClass = nil;
    
    [self.profileTypesClasses enumerateKeysAndObjectsUsingBlock:^(id key, Class profileClass, BOOL *stop) {
        if([[[profileClass object] profileName] isEqualToString:profileTypeName])
        {
            *stop = YES;
            rightProfileClass = profileClass;
        }
    }];
    
    return rightProfileClass;
}

- (NSString*)stringOfAllProfileTypes
{
    return [self stringOfAllProfileTypesWithDelimiter:@"/"];
}

- (NSArray*)stringsArrayOfAllProfilesExclude:(NSArray *)extraProfileTypes
{
    NSMutableArray* result = [NSMutableArray array];
    
    ///specs say that the order of profileTypes must be the following model/actor/photographer/hair stylist...
    ///we will orginize our enumaration of ProfileTypes in that order and than sort dictionary keys using Enumaration values of each profile Type
    NSArray* allKeys = [self.profileTypesClasses allKeys];
    
    NSArray* sortedKeys =[allKeys sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:nil ascending:YES]]];
    
    for(id key in sortedKeys)
    {
        Class ProfileTypeClass = self.profileTypesClasses[key];
        
        if(![extraProfileTypes containsObject:key])
        {
            [result addObject:[[ProfileTypeClass object] profileName]];
        }
    }
    
    return [result copy];
}

- (NSString*)stringOfAllProfileTypesWithDelimiter:(NSString *)delimiter
{
    NSMutableString* string = [NSMutableString string];
    
    [self.profileTypesArray enumerateObjectsUsingBlock:^(PFObject<CRProfileProtocol>* obj, NSUInteger idx, BOOL *stop) {
        if ([obj respondsToSelector:@selector(profileName)]) {
            [string appendString:obj.profileName];
        }
        
        if(idx != self.profileTypesArray.count - 1)
        {
            [string appendString:delimiter];
        }
    }];
    
    if(string.length == 0)
    {
        return @"Not Specified";
    }
    
    return string;
}

- (NSArray*)arrayOfStringsFromAbsentProfileTypes
{
    CRProfileType groupType = ((id<CRProfileProtocol>)self.profileTypesArray.firstObject).profileType;//all the Profiles from items array must be members of this group, otherwise we'll return nil
    NSArray* neccessaryGroup = [self profileGroupForType:groupType];
    NSUInteger indexOfGroup = [self.profileTypeGroups indexOfObject:neccessaryGroup];
    NSMutableArray* resultArray = [self.profileTypeGroupsStrings[indexOfGroup] mutableCopy];
    
    for(PFObject<CRProfileProtocol>* obj in self.profileTypesArray)
    {
        [resultArray removeObject:obj.profileName];
    }
    
    return resultArray.count == 0 ? nil : resultArray;
}

- (NSArray*)profileTypesStrings
{
    NSArray* result = [self.profileTypesArray valueForKey:@"profileName"];
    
    return result;
}

- (void)saveAll
{
    NSArray* arrayOfAllProfiles = self.profileTypesArray;
    
    [arrayOfAllProfiles enumerateObjectsUsingBlock:^(PFObject* obj, NSUInteger idx, BOOL *stop) {
        NSError* error = nil;
        [obj save:&error];
        if(error)
        {
            NSLog(@"Error saving profileType %@. Details - %@",obj,error);
        }
    }];
    
    [self deleteMarkedProfiles];
}

- (void)saveAllInBackground {
    [self.profileTypesArray enumerateObjectsUsingBlock:^(PFObject* obj, NSUInteger idx, BOOL *stop) {
        [obj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(error) {
                NSLog(@"Error saving profileType %@. Details - %@",obj,error);
            }
        }];
    }];
    
    [self deleteMarkedProfiles];
}

- (void)refreshProfiles:(NSMutableArray *)storage
{
    if(storage)
    {
       [self deleteMarkedProfiles];
        self.profileTypesArray = storage;
    }
    
    NSArray* arrayOfProfiles = self.profileTypesArray;
    
    [arrayOfProfiles enumerateObjectsUsingBlock:^(PFObject* obj, NSUInteger idx, BOOL *stop) {
        NSError* error = nil;
        [obj refresh:&error];
        if(error)
        {
            NSLog(@"Error saving profileType %@. Details - %@",obj,error);
        }
    }];
}

- (void)replaceProfilesWithStorage:(NSMutableArray *)storage
{
    if(storage.count < 1)
    {
        NSLog(@"An attempt to replace profiles with empty storage");
        return;
    }
    
    NSUInteger oldProfilesCount = self.profileTypesArray.count;
    
    [self.profileTypesArray addObjectsFromArray:storage];
    
    for(int i = 0;i<oldProfilesCount;++i)
    {
        [self removeProfileType:self.profileTypesArray[0]];
    }
}

#pragma mark - private methods

///here we'll delete every single profile that was added to ProfilesToDeleteArray
- (void)deleteMarkedProfiles
{
    [self.profilesToDelete enumerateObjectsUsingBlock:^(PFObject<CRProfileProtocol>* obj, NSUInteger idx, BOOL *stop) {
        [obj delete];
    }];
    
    [self.profilesToDelete removeAllObjects];
}

- (NSArray*) profileGroupForType:(CRProfileType)type
{
    NSUInteger __block indexOfGroup = NSNotFound;
    
    [self.profileTypeGroups enumerateObjectsUsingBlock:^(NSArray* obj, NSUInteger idx, BOOL *stop) {
        for(NSString* elem in obj)
        {
            if(((CRProfileType)elem.integerValue) == type)
            {
                indexOfGroup = idx;
                *stop = YES;
                break;
            }
        }
    }];
    
    if(indexOfGroup == NSNotFound)
        return nil;
    else
        return self.profileTypeGroups[indexOfGroup];
}


@end
