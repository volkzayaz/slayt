//
//  CRTalentAgency.h
//  Talent
//
//  Created by Vlad Soroka on 5/12/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Parse/Parse.h>
#import "CRProfileProtocol.h"
#import "CRUserDefines.h"

@interface CRTalentAgency : PFObject <PFSubclassing, CRProfileProtocol>

@property (nonatomic, strong) NSArray* jobTypes;

@end
