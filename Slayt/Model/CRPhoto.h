//
//  CRPhoto.h
//  Talent
//
//  Created by Vlad Soroka on 4/9/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Parse/Parse.h>

@class CRUser;

typedef void(^SetMainPhotoBlock)(NSError*);

static NSString* kUserDidUpdateMainPhotoNotification = @"UserDidUpdateMainPhotoNotification";

@interface CRPhoto : PFObject <PFSubclassing>

@property (nonatomic, strong) CRUser* owner;
@property (nonatomic, strong) PFFile* photoFile;
@property (nonatomic, assign) BOOL isMain;

//typically you would use [photoFile getData] in order to get image file of this CRPhoto object. However if current CRPhoto has not been saved yet, you are not allowed to access the file. In case you really, REALLY need it before save action, you can access it via this property. Note that as soon as the file will be saved to parse this property will become nil.
@property (nonatomic, strong) UIImage* localImage;

- (void)setMain:(BOOL)isMain withCompletitionBlock:(SetMainPhotoBlock)completitionBlock;//makes former main photo change it's isMain property to NO
@end
