//
//  CRAgency.m
//  Talent
//
//  Created by Vlad Soroka on 4/10/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRAgency.h"
#import <Parse/PFObject+Subclass.h>

@implementation CRAgency 

@dynamic name;
@dynamic agentEmail;
@dynamic agentName;
@dynamic agentPhone;

+ (NSString*)parseClassName
{
    return @"CRAgency";
}

@end
