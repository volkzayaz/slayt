//
//  CRMessage.m
//  Slayt
//
//  Created by Dmitry Utenkov on 6/17/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRMessage.h"
#import <Parse/PFObject+Subclass.h>

@implementation CRMessage

@dynamic senderId;
@dynamic destinationId;
@dynamic text;

+ (NSString *)parseClassName {
    return @"CRMessage";
}

@end
