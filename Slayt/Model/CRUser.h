//
//  CRUser.h
//  Talent
//
//  Created by Vlad Soroka on 4/1/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Parse/Parse.h>
#import "CRUserDefines.h"
#import "CRProfileTypeStorage.h"
#import "CRLocationMonitor.h"

@class CRPhoto;
@class CRRatingManager;

@interface CRUser : PFUser <PFSubclassing>

@property (nonatomic, copy) NSString* fullName;

@property (nonatomic, copy) NSString* gender;
@property (nonatomic, strong) NSDate* birthDate;
@property (nonatomic, strong) NSNumber* height;

@property (nonatomic, strong) NSString* homeTown;

@property (nonatomic, strong) PFGeoPoint* recentLocation;
@property (nonatomic, strong) NSDate* locationUpdatedAt;

@property (nonatomic, strong, readonly) CRLocationMonitor* locationMonitor;

@property (nonatomic, copy) NSString* friendsFilter;

///this field will be used to restore most recent filtering settings
@property (nonatomic, strong) NSArray* filterSettings;

///properties for review

//tarnsform these fields into separate entity Agency
@property (nonatomic, copy) NSString* agencyName;
@property (copy, nonatomic) NSString *agentName;
@property (copy, nonatomic) NSString *agentPhone;
@property (copy, nonatomic) NSString *agentEmail;

//method does not save photo to parse. You are responsible for calling save at the time that is appropriate for you
- (CRPhoto*)addPhotoForImage:(UIImage*)image;

/**
 *  Asynchronous requst for all photos that were uploaded by user. The very first photo in the array will be user's main photo
 *  @param block - block to be called on finishing request.
 */
- (void)requestPhotosWithCompletitionBlock:(RequestPhotosCallback)block;

/**
 *  Synchronous version of requestPhotosWithCompletitionBlock
 */
- (NSArray*)requestPhotosWithError:(NSError* __autoreleasing*)error;


/**
 *  Asynchronously requests user's main photo.
 *  @param block - block to be called on finishing request.
 */
-(void)requestMainPhotoWithCompletitionBlock:(RequestMainPhotoCallBack)block;

/**
 *  Synchronous version of requestMainPhoto
 */
- (CRPhoto*)requestMainPhoto:(NSError* __autoreleasing*)error;

////profile managment


/**
 *  Use this class as profileType manager. The property itself is not stored on Parse. All the parse cooperation/saving is made through profileReferencesDictionary. You are strongly discouraged to manipulate profileReferencesDictionary yourself. Instead try to extend profileTypesStorage so it suits your needs.
 */
@property (nonatomic, strong) CRProfileTypeStorage* profileTypes;
@property (nonatomic, strong) NSMutableArray* profileReferencesArray;

/**
 *  Loads from server all the profiles that are assosiated with self. Profiles will be accessible through profileTypes object.
 */
- (void)loadProfiles;

/**
 *  @discussion Retreives profileType for given type parametr and configures array which is propertyList file. The name of the property list file is defined by CRProfileProtocol method for any given type.
 */
- (NSArray*) profilePropertiesArrayForType:(CRProfileType)type;

/**
 *  @discussion Utility method that is responsible for returning userFriendly string describing the given key. You can also pass prefixed value as the key for this method. If profile that is assosiated with this user has prefix(returned from appropriate method of CRProfileProtocol) that is the same for the passed key than, the call will be forwarded to that profile. For example CRModel profileType has @"model." prefix. That means that you can retreive model Bust userFriendly string by passing something like @"model.bust" to this method.
 
    @return UserFriendly information about the key OR nil if you would like to rely on caller in showing the string for this key
 
    @seealso CRProfileProtocol.
 */
- (NSString*) valueDescriptionForKey:(NSString*)key;

/**
 *  All ordered properties are represented by Enums. Each member of Enum has a string that represents enum's member value. The strings are collected in the array and defined in PFObject+OrderedPropertiesStrings category. Their naming convention are 
 *  |propertyName|Strings. This method will return to you appropriate strings array even if you have prefixed propertyName like @"actor.experienceLevel".
 *
 *  @seealso CRModel.cupSize - ordered property, CRProfileProtocol - profilePrefix
 */
- (NSArray*)stringsForPrefixedPropertyName:(NSString*)prefixedPropertyName;

/**
 *  Does not contain objectID/password
 */
- (CRUser*) duplicateUser;

/**
 *  @discussion this pair of methods simulates undone action on CRUser object. Currently it covers all properties of CRUser and all properties of ProfileTypes (as well as their insert\delete actions).  Just send createSnapshot message when you want to save current state of the user => perform any updates on the fields => call undoPerformedChanges to restore state that was saved at the time you had called createSnapshot.
 */
- (void)createSnapshot;

/**
 *  @discussion snapshot will be destroyed as soon as undone action is performed
 */
- (void)undoPerformedChanges;

@end
