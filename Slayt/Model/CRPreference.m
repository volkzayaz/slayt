//
//  CRLike.m
//  Slayt
//
//  Created by Dmitry Utenkov on 5/22/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRPreference.h"
#import <Parse/PFObject+Subclass.h>

@implementation CRPreference

@dynamic sourceProfileId;
@dynamic destinationProfileId;
@dynamic preferenceType;

+ (NSString*)parseClassName
{
    return NSStringFromClass([self class]);
}

@end
