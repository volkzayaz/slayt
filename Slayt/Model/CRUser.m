//
//  CRUser.m
//  Talent
//
//  Created by Vlad Soroka on 4/1/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRUser.h"
#import <Parse/PFObject+Subclass.h>
#import "CRUtils.h"
#import "NSNumber+fractions.h"
#import "CRPhoto.h"
#import "CRRatingManager.h"

@interface CRUser ()

@property (nonatomic, strong) CRUser* snapshot;

@end

@implementation CRUser

@dynamic profileReferencesArray;

@dynamic gender;
@dynamic birthDate;

@dynamic height;

@dynamic homeTown;
@dynamic recentLocation;
@dynamic locationUpdatedAt;

@dynamic friendsFilter;

@dynamic filterSettings;

@dynamic agencyName;
@dynamic agentEmail;
@dynamic agentPhone;
@dynamic agentName;

@dynamic fullName;

@synthesize profileTypes = _profileTypes;
@synthesize snapshot = _snapshot;
@synthesize locationMonitor = _locationMonitor;

- (void)refresh:(NSError *__autoreleasing *)error
{
    [super refresh:error];
    
    [self.profileTypes refreshProfiles:self.profileReferencesArray];
}

// TODO: lowest priopity task
///move from KVO model to multiple delegates model

- (void)setObject:(id)object forKeyedSubscript:(NSString *)key
{
    id<CRProfileProtocol> profile = [self.profileTypes profileTypeForPrefixedKey:key];
    
    if (profile) {//the key passed to us is formed of prefix and properyName therefore we need to cleave properyName and pass it to the profile
        NSString* propertyName = [[CRUtils sharedInstance] substringOfString:key withoutPrefix:[profile profilePrefix]];
        profile[propertyName] = object;//passing object to the appropriate profile
    }
    else//the key is consisted of a raw propertyName so it's our responsibility to store it to the right place
    {
        [self willChangeValueForKey:key];
        
        if([object isEqual:[NSNull null]])
        {
            //We need to store nil value in our local CRUser manually, because parse stores [NSNull null] object to our nil properties and that is not something we want
            
            [self setValue:nil forKey:key];
            [self didChangeValueForKey:key];
            return;
        }
        
        
        [super setObject:object forKeyedSubscript:key];
        [self didChangeValueForKey:key];
    }
}

- (id)objectForKeyedSubscript:(NSString *)key
{
    id<CRProfileProtocol> profile = [self.profileTypes profileTypeForPrefixedKey:key];
    
    if (profile) {//the key passed to us is formed of prefix and properyName therefore we need to cleave properyName and pass it to the profile
        
        NSString* propertyName = [[CRUtils sharedInstance] substringOfString:key withoutPrefix:[profile profilePrefix]];
        
        return profile[propertyName];//passing request to the appropriate profile
    }
    else//the key is consisted of a raw propertyName so it's our responsibility to return appropriate object
    {
        return [super objectForKeyedSubscript:key];
    }
}

- (id)valueForKey:(NSString *)key
{
    PFObject<CRProfileProtocol>* profile = [self.profileTypes profileTypeForPrefixedKey:key];
    
    if (profile) {//the key passed to us is formed of prefix and properyName therefore we need to cleave properyName and pass it to the profile
        NSString* propertyName = [[CRUtils sharedInstance] substringOfString:key withoutPrefix:[profile profilePrefix]];
        
        return [profile valueForKey:propertyName];//passing request to the appropriate profile
    }
    else
    {
       return [super valueForKey:key];
    }
}

- (CRPhoto*)addPhotoForImage:(UIImage *)image
{
    CRPhoto* newPhoto = [CRPhoto object];
    newPhoto.photoFile = [PFFile fileWithData:UIImageJPEGRepresentation(image,1.0f)];
    newPhoto.owner = self;
    return newPhoto;
}

- (void)requestPhotosWithCompletitionBlock:(RequestPhotosCallback)block
{
    PFQuery* query = [CRPhoto query];
    [query whereKey:@"owner" equalTo:self];
    [query orderByDescending:@"isMain"];
    [query findObjectsInBackgroundWithBlock:block];
}

- (NSArray*)requestPhotosWithError:(NSError *__autoreleasing *)error
{
    PFQuery* query = [CRPhoto query];
    [query whereKey:@"owner" equalTo:self];
    [query orderByDescending:@"isMain"];
    return  [query findObjects:error];
}

- (void)requestMainPhotoWithCompletitionBlock:(RequestMainPhotoCallBack)block
{
    PFQuery* query = [CRPhoto query];
    [query whereKey:@"owner" equalTo:self];
    [query whereKey:@"isMain" equalTo:@(YES)];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSAssert(objects.count <= 1, @"User should have not more than one main photo");
        if(error && block)
        {
            block(nil,error);
        }
        
        CRPhoto* mainPhoto = objects.firstObject;
        block(mainPhoto,error);
    }];
}

- (CRPhoto*)requestMainPhoto:(NSError *__autoreleasing *)error
{
    PFQuery* query = [CRPhoto query];
    [query whereKey:@"owner" equalTo:self];
    [query whereKey:@"isMain" equalTo:@(YES)];
    
    NSArray* objects = [query findObjects:error];
    
    NSAssert(objects.count <= 1, @"User should have not more than one main photo");
    if (error != NULL && *error)
    {
        NSLog(@"Error occured during fetching main photo: %@", (*error).localizedDescription);
        return nil;
    }
    
    CRPhoto* mainPhoto = objects.firstObject;
    
    return mainPhoto;
}

- (void)loadProfiles
{
    if(!self.profileReferencesArray)
    {
        self.profileReferencesArray = [NSMutableArray array];
    }
    
    self.profileTypes = [[CRProfileTypeStorage alloc] initWithDictionaryStorage:self.profileReferencesArray];
}

- (NSArray*)profilePropertiesArrayForType:(CRProfileType)type
{
    id<CRProfileProtocol> profile = [self.profileTypes profileForType:type];
    
    if(!profile)
    {
        return nil;
    }
    
    NSString* plistName = [profile profilePropertiesPlistFileName];
    
    NSString *plistFilePath = [[NSBundle mainBundle] pathForResource: plistName ofType: @"plist"];
    NSArray* modelAttributesArray = [NSArray arrayWithContentsOfFile:plistFilePath];
    
    NSString* genderSpecificSizesPlistPath = nil;
    
    if(type == CRProfileTypeModel)
    {
        if([kGenderMale isEqualToString:self.gender])
        {
            genderSpecificSizesPlistPath = [[NSBundle mainBundle] pathForResource:@"Male's sizes" ofType:@"plist"];
        }
        else if([kGenderFemale isEqualToString:self.gender])
        {
            genderSpecificSizesPlistPath = [[NSBundle mainBundle] pathForResource:@"Female's sizes" ofType:@"plist"];
        }
    }
    
    // TODO: not urgent
    //currently we are going down through hierarchy and making mutable copies of every level in order to replace the single array on the third level. Probably there's a faster/smarter way of achieving this goal.
    if(genderSpecificSizesPlistPath)
    {
        NSArray* specificSizesArray = [NSArray arrayWithContentsOfFile:genderSpecificSizesPlistPath];
        
        NSAssert(specificSizesArray, @"File %@ was not found", genderSpecificSizesPlistPath);
        
        NSMutableArray* rootArray = [modelAttributesArray mutableCopy];
        NSUInteger index = [rootArray indexOfObjectPassingTest:^BOOL(NSDictionary* obj, NSUInteger idx, BOOL *stop) {
            NSString* identifier = obj[@"identifier"];
            if([identifier isEqualToString:@"sizes"])
            {
                *stop = YES;
                return YES;
            }
            else
            {
                return NO;
            }
        }];
        
        NSAssert(index!=NSNotFound, @"Section with identifier 'sizes' was not found in file %@", plistFilePath);
        
        NSMutableDictionary* sizeSectionInfo = [rootArray[index] mutableCopy];
        
        [sizeSectionInfo removeObjectForKey:@"properties"];
        [sizeSectionInfo setObject:specificSizesArray forKey:@"properties"];
        
        [rootArray replaceObjectAtIndex:index withObject:sizeSectionInfo];
        
        return rootArray;
    }
    
    return modelAttributesArray;
}

- (NSString*) valueDescriptionForKey:(NSString *)key
{
    PFObject<CRProfileProtocol>* profile = [self.profileTypes profileTypeForPrefixedKey:key];
    
    if (profile) {//the key passed to us is formed of prefix and properyName therefore we need to cleave properyName and pass it to the profile
        NSString* propertyName = [[CRUtils sharedInstance] substringOfString:key withoutPrefix:[profile profilePrefix]];
        
        return [profile valueDescriptionForKey:propertyName];//passing request to the appropriate profile
    }
    else
    {
        NSString* detailsString = nil;
        
        if([key isEqualToString:@"birthDate"] && self.birthDate)
        {
            NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents* components = [calendar components:NSCalendarUnitYear fromDate:self.birthDate toDate:[NSDate date] options:0];
            
            detailsString = [NSString stringWithFormat:@"%li",(long)components.year];
        }
        else if([key isEqualToString:@"height"] && self.height)
        {
            NSInteger feet = self.height.integerPart;
            NSInteger inches = self.height.fractionPart;
            
            detailsString = [NSString stringWithFormat:@"%li ft/%li in",(long)feet,(long)inches];
        }
        
        return detailsString;
    }
}

- (NSArray*)stringsForPrefixedPropertyName:(NSString *)prefixedPropertyName
{
    NSArray* result = nil;
    
    NSString* stringsArrayName = [NSString stringWithFormat:@"%@Strings",prefixedPropertyName];
    
    Class ProfileTypeClass = [self.profileTypes profileTypeClassForPrefixedKey:prefixedPropertyName];

    if (ProfileTypeClass) {
        stringsArrayName = [NSString stringWithFormat:@"%@Strings",[[CRUtils sharedInstance] substringOfString:prefixedPropertyName withoutPrefix:[[ProfileTypeClass object] profilePrefix]]];
    }
    
    if(![self respondsToSelector:NSSelectorFromString(stringsArrayName)])
    {
        NSLog(@"WARNING: StringDictionary was not found. Check that you have %@ in PFObject+OrderedPropertiesStrings category", stringsArrayName);
        return nil;
    }
    else
    {
        SEL selector = NSSelectorFromString(stringsArrayName);
        IMP imp = [self methodForSelector:selector];
        NSArray* (*func)(id, SEL) = (void *)imp;
        result = func(self, selector);
    }

    return result;
}

- (CRUser*) duplicateUser
{
    CRUser* duplicateUser = [CRUser object];
    for(NSString* key in [self allKeys])
    {
        duplicateUser[key] = [[self valueForKey:key] copy];
    }
    
    NSArray* profileTypes = self.profileTypes.profileTypesArray;
    NSMutableArray* duplicateProfileTypes = [NSMutableArray arrayWithCapacity:profileTypes.count];
    
    for(id<CRProfileProtocol> proifle in profileTypes)
    {
        id<CRProfileProtocol> profileTypeDuplicate = [proifle duplicateProfile];
        [duplicateProfileTypes addObject:profileTypeDuplicate];
    }

    duplicateUser.profileTypes = nil;
    duplicateUser.profileTypes = [[CRProfileTypeStorage alloc] initWithDictionaryStorage:duplicateProfileTypes];
    
    return duplicateUser;
}

- (void)createSnapshot
{
    self.snapshot = [self duplicateUser];
}

- (void)undoPerformedChanges
{
    // Agency related keys should not be reverted on editing cancel
    NSArray *keysToExclude = @[@"agencyName", @"agentName", @"agentPhone", @"agentEmail"];
    
    for(NSString* key in [self allKeys])
    {
        if ( ![keysToExclude containsObject:key] ) {
            if([[self.snapshot allKeys] containsObject:key])//this value had existed before user started editing his profile
            {
                self[key] = [self.snapshot valueForKey:key];
            }
            else//this value did not exist before the user edited his profile
            {
                self[key] = [NSNull null];
            }
        }
    }

    NSArray* profileTypes = self.snapshot.profileTypes.profileTypesArray;
    NSMutableArray* duplicateProfileTypes = [NSMutableArray arrayWithCapacity:profileTypes.count];
    
    for(id<CRProfileProtocol> proifle in profileTypes)
    {
        id<CRProfileProtocol> profileTypeDuplicate = proifle;
        [duplicateProfileTypes addObject:profileTypeDuplicate];
    }
    
    [self.profileTypes replaceProfilesWithStorage:duplicateProfileTypes];
    self.profileReferencesArray = duplicateProfileTypes;
    
    self.snapshot = nil;
}

#pragma mark - private methods

- (CRProfileTypeStorage*)profileTypes
{
    if(!_profileTypes)
    {
        [self loadProfiles];
    }
    return _profileTypes;
}

- (CRLocationMonitor*)locationMonitor
{
    if(!_locationMonitor)
    {
        _locationMonitor = [[CRLocationMonitor alloc] initWithUser:self];
    }
    
    return _locationMonitor;
}

@end
