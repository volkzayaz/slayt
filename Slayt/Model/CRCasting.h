//
//  CRCating.h
//  Slayt
//
//  Created by Vlad Soroka on 6/24/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Parse/Parse.h>
#import "CRUser.h"

@interface CRCasting : PFObject <PFSubclassing>

@property (nonatomic) CRUser* author;

@property (nonatomic) NSString* title;
@property (nonatomic) NSString* description;
@property (nonatomic) NSString* usage;

@property (nonatomic) NSString* location;

///Array of NSDate* objects
@property (nonatomic) NSArray* castingPeriods;


@end
