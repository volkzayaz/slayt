//
//  CRJob.h
//  Slayt
//
//  Created by Vlad Soroka on 6/27/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Parse/Parse.h>
#import "CRUser.h"

typedef NS_ENUM(NSUInteger, CRJobStatus)
{
    CRJobStatusNew = 0,
    CRJobStatusCurrent,
    CRJobStatusPast,
    
    CRJobStatusCOUNT
};

@interface CRJob : PFObject <PFSubclassing>

@property (nonatomic) CRUser* author;

@property (nonatomic) NSString* title;
@property (nonatomic) NSString* description;
@property (nonatomic) NSString* usage;

@property (nonatomic) NSString* location;

///Array of NSDate* objects
@property (nonatomic) NSArray* castingPeriods;

@property (nonatomic) CRJobStatus jobStatus;

@end
