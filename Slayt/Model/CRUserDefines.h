//
//  CRUserDefines.h
//  Talent
//
//  Created by Vlad Soroka on 4/18/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#ifndef Talent_CRUserDefines_h
#define Talent_CRUserDefines_h

#define MAX_ALLOWED_USER_PHOTOS 10

@class CRPhoto;

typedef void(^RequestPhotosCallback)(NSArray* photos, NSError* error);
typedef void(^RequestMainPhotoCallBack)(CRPhoto* photo, NSError* error);
typedef void(^UploadPhotoCallback)(BOOL succeeded, NSError *error);


///userProfiles

typedef NS_ENUM(NSUInteger, CRProfileType)
{
    CRProfileTypeModel = 1,
    CRProfileTypeActor,
    
    CRProfileTypePhotographer,
    
    CRProfileTypeHairStylist,
    CRProfileTypeMakeupArtist,
    CRProfileTypeClothingStylist,
    
    CRProfileTypeTalentAgency,
    
    CRProfileTypeEmployer,
    
    CRProfileTypeCount
};


///gender

static NSString * kGenderFemale = @"Female";
static NSString * kGenderMale = @"Male";

///experience level
typedef NS_ENUM(NSUInteger, CRExperienceLevel)
{
    CRExperienceLevel0 = 0,
    CRExperienceLevel1,
    CRExperienceLevel2,
    CRExperienceLevel3,
    
    CRExperienceLevelCOUNT
};

static const NSString* kexperienceLevelNoExirience = @"No Experience";
static const NSString* kexperienceLevelSomeExirience = @"Some Experience";
static const NSString* kexperienceLevelExirienced = @"Experienced";
static const NSString* kexperienceLevelVeryExirienced = @"Very Experienced";

///cupSize
typedef NS_ENUM(NSUInteger, CRCupSize)
{
    CRCupSizeNotSpecified = 0,
    CRCupSizeAA,
    CRCupSizeA,
    CRCupSizeB,
    CRCupSizeC,
    CRCupSizeD,
    CRCupSizeE,
    CRCupSizeF,
    
    CRCupSizeCOUNT
};

static const NSString* kCupSizeNotSpecified = @"Not Specified";
static const NSString* kCupSizeAA = @"AA";
static const NSString* kCupSizeA = @"A";
static const NSString* kCupSizeB = @"B";
static const NSString* kCupSizeC = @"C";
static const NSString* kCupSizeD = @"D";
static const NSString* kCupSizeE = @"E";
static const NSString* kCupSizeF = @"F";

///hairLength
typedef NS_ENUM(NSUInteger, CRHairLength)
{
    CRHairLengthNotSpecified = 0,
    CRHairLengthShort,
    CRHairLengthMedium,
    CRHairLengthLong,
    
    CRHairLengthCOUNT
};

static const NSString* kHairLegthNotSpecified = @"Not Specified";
static const NSString* kHairLegthShort = @"Short";
static const NSString* kHairLegthMedium = @"Medium";
static const NSString* kHairLegthLong = @"Long";

///paymentFrequency
typedef NS_ENUM(NSUInteger,CRPaymentFrequency){
    CRPaymentFrequencyNotSpecified = 0,
    CRPaymentFrequencyHourly,
    CRPaymentFrequencyDaily,
    CRPaymentFrequencyWorkForFree,
    
    CRPaymentFrequencyCOUNT
};

static const NSString* kPaymentFrequencyNotSpecified = @"Not Specified";
static const NSString* kPaymentFrequencyHourly = @"Hourly";
static const NSString* kPaymentFrequencyDaily = @"Daily";
static const NSString* kPaymentFrequencyWorkForFree = @"Will work for free";

///paymentType
#warning - assumption made.
//  for now we are using the EditPayRate controller for CRPaymentType fields which is not very honest since CRPaymentType is not the exact copy of CRPaymentFrequency. In order to work properly one assumption should be true - CRPaymentFrequencyNotSpecified,CRPaymentFrequencyHourly, CRPaymentFrequencyDaily MUST be the first three items in CRPaymentFrequency enumaration
typedef NS_ENUM(NSUInteger, CRPaymentType){
    CRPaymentTypeNotSpecified = 0,
    
    CRPaymentTypeHourly = CRPaymentFrequencyHourly,
    CRPaymentTypeFlat = CRPaymentFrequencyDaily,
    
    CRPaymentTypeCOUNT
};

static const NSString* kPaymentTypeNotSpecified = @"Not Specified";
static const NSString* kPaymentTypeHourly = @"Hourly";
static const NSString* kPaymentTypeFlat = @"Flat";

///Jacket

typedef NS_ENUM(NSUInteger,CRJacketSize){
    CRJacketSizeNotSpecified = 0,
    CRJacketSizeXS,
    CRJacketSizeS,
    CRJacketSizeM,
    CRJacketSizeL,
    CRJacketSizeXL,
    CRJacketSize2XL,
    CRJacketSize3XL,
    CRJacketSize4XL,
    CRJacketSize5XL,
    
    CRJacketSizeCOUNT,
};

static const NSString* kJacketSizeNotSpecified = @"Not Specified";
static const NSString* kJacketSizeXS = @"X-Small";
static const NSString* kJacketSizeS = @"Small";
static const NSString* kJacketSizeM = @"Medium";
static const NSString* kJacketSizeL = @"Large";
static const NSString* kJacketSizeXL = @"X-Large";
static const NSString* kJacketSize2XL = @"2X-Large";
static const NSString* kJacketSize3XL = @"3X-Large";
static const NSString* kJacketSize4XL = @"4X-Large";
static const NSString* kJacketSize5XL = @"5X-Large";




#endif
