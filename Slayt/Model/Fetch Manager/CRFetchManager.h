//
//  CRFetchManager.h
//  Slayt
//
//  Created by Vlad Soroka on 6/11/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRUser.h"

static NSString* kDidFetchUserInfoNotification = @"fetchedUserInfo";

///user for which fetching was performed
static NSString* kUserInfoKeyFetchedUser = @"fetchedUser";
///Error, if it occured during fetching
static NSString* kUserInfoKeyError = @"fetchedError";

@interface CRFetchManager : NSObject

+ (instancetype)sharedManager;

- (void) fetchAllInfoInBackgroundForUser:(CRUser*)user;

- (BOOL) isUserFetchedWithID:(NSString*)userID errorReason:(NSError*)outError;

@end
