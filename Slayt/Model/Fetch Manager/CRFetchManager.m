//
//  CRFetchManager.m
//  Slayt
//
//  Created by Vlad Soroka on 6/11/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFetchManager.h"

@interface CRFetchManager()

@property (nonatomic, strong) NSMutableDictionary* failedFetchedUserIDs;
@property (nonatomic, strong) NSMutableArray* successfullyFetchedUserIDs;

@end
static CRFetchManager *sharedManager = nil;
@implementation CRFetchManager

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[CRFetchManager alloc] init];
        sharedManager.successfullyFetchedUserIDs = [NSMutableArray array];
        sharedManager.failedFetchedUserIDs = [NSMutableDictionary dictionary];
    });
    
    return sharedManager;
}


- (BOOL) isUserFetchedWithID:(NSString *)userID errorReason:(NSError *)outError
{
    if([self.successfullyFetchedUserIDs containsObject:userID])
    {
        outError = nil;
        return YES;
    }
    
    if([self.failedFetchedUserIDs.allKeys containsObject:userID])
    {
        outError = self.failedFetchedUserIDs[userID];
    }
    else
    {
        outError = nil;
    }
    
    return NO;
}

- (void) fetchAllInfoInBackgroundForUser:(CRUser *)user
{
    dispatch_queue_t queue = dispatch_queue_create("com.slayt.userFetchingQueue", DISPATCH_QUEUE_CONCURRENT);
    
    NSError __block * fetchingError = nil;
    
    dispatch_async(queue, ^{
        NSError* error = [self fetchProfileTypesForUser:user];
        fetchingError = error;
    });
    
    
    dispatch_barrier_async(queue, ^{
        if(fetchingError)
        {
            [self.failedFetchedUserIDs setValue:fetchingError forKey:user.objectId];
        }
        else
        {
            if(![self.successfullyFetchedUserIDs containsObject:user.objectId])
            {
                [self.successfullyFetchedUserIDs addObject:user.objectId];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self postCompletitionNotoficationForUser:user withError:fetchingError];
        });
    });
}

- (NSError*)fetchProfileTypesForUser:(CRUser*)user
{
    return [user.profileTypes fetchAllProfileTypes];
}

#pragma mark - private methods

- (void)postCompletitionNotoficationForUser:(CRUser*)user withError:(NSError*)error
{
    NSMutableDictionary* dictionary = [NSMutableDictionary dictionary];
    
    [dictionary setValue:user forKey:kUserInfoKeyFetchedUser];
    if(error)
    {
        [dictionary setValue:error forKey:kUserInfoKeyError];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidFetchUserInfoNotification
                                                        object:self
                                                      userInfo:dictionary];
}

@end
