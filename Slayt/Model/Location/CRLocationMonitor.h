//
//  CRLocationMonitor.h
//  Slayt
//
//  Created by Vlad Soroka on 6/10/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>


extern const NSTimeInterval kLocationUpdateInterval;

@class CRUser;

@interface CRLocationMonitor : NSObject

/**
 *  @param user - this user is assumed to be the one we are monitoring location for. Location manager makes a weak reference to user object
 */
- (instancetype)initWithUser:(CRUser*)user;

- (void)startMonitoring;

/**
 *  @discussion Make sure you call this method before sign out user that was passed in init method.
 */
- (void)stopMonitoring;

- (BOOL) isLocationExpired;
- (BOOL) isLocationServicesEnabled;

@end
