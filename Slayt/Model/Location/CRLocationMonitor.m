//
//  CRLocationMonitor.m
//  Slayt
//
//  Created by Vlad Soroka on 6/10/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRLocationMonitor.h"
#import "CRUser.h"

const NSTimeInterval kLocationUpdateInterval = 10;//60 * 60 * 3; // 3 hours

@interface CRLocationMonitor()

@property (nonatomic, weak) CRUser* trackingUser;

//Timer will schedule the next moment in time when we need to update userLocation.
@property (nonatomic, strong) NSTimer* monitorTimer;

@end

@implementation CRLocationMonitor

- (void)dealloc
{
    [self.monitorTimer invalidate];
}

- (instancetype)initWithUser:(CRUser *)user
{
    self = [super init];
    
    if(self)
    {
        self.trackingUser = user;
    }

    return self;
}

- (void)startMonitoring
{
    NSTimeInterval secondsLeftTillUpdate;
    
    if([self isLocationUpdateIntervalExpired:&secondsLeftTillUpdate])
    {
        [self updateLocation];
    }
    else
    {
        [self scheduleUpdateLocationIn:secondsLeftTillUpdate];
    }
}

- (void) stopMonitoring
{
    [self cancelScheduledUpdateLocation];
}

- (void) updateLocation
{
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        if(!error)
        {

            self.trackingUser.locationUpdatedAt = [NSDate date];
            self.trackingUser.recentLocation = geoPoint;
            [self.trackingUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {

            }];

            [self scheduleUpdateLocationIn:kLocationUpdateInterval];
        }
        else
        {
            NSLog(@"Error updating location - %@",error);
            //[self scheduleUpdateLocationIn:5];
        }
    }];
}

- (void)scheduleUpdateLocationIn:(NSTimeInterval)seconds
{
    if([self.monitorTimer isValid])
    {
        [self.monitorTimer invalidate];
    }
   
    self.monitorTimer = [NSTimer timerWithTimeInterval:seconds
                                                target:self
                                              selector:@selector(updateLocation)
                                              userInfo:nil
                                               repeats:NO];
    
    [[NSRunLoop mainRunLoop] addTimer:self.monitorTimer forMode:NSDefaultRunLoopMode];
}

- (void) cancelScheduledUpdateLocation
{
    [self.monitorTimer invalidate];
    self.monitorTimer = nil;
}

- (BOOL)isLocationUpdateIntervalExpired:(NSTimeInterval*)timeIntervalTillUpdate
{
    NSTimeInterval answer = 0;
    
    NSDate* lastUpdatedAt = self.trackingUser.locationUpdatedAt;
    NSDate* today = [NSDate date];
    
    if(!lastUpdatedAt)
    {
        timeIntervalTillUpdate = &answer;
        return YES;
    }
    
    NSDateComponents* components = [[NSCalendar currentCalendar] components:NSSecondCalendarUnit
                                                 fromDate:lastUpdatedAt
                                                   toDate:today
                                                  options: 0];
    answer = [components second];
    timeIntervalTillUpdate = &answer;
    
    return answer >= kLocationUpdateInterval;
}

- (BOOL) isLocationExpired
{
    return [self isLocationUpdateIntervalExpired:nil];
}

- (BOOL) isLocationServicesEnabled
{
    return [CLLocationManager locationServicesEnabled];
}

@end
