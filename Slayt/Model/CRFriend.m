//
//  CRFriend.m
//  Slayt
//
//  Created by Dmitry Utenkov on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFriend.h"
#import <Parse/PFObject+Subclass.h>

@implementation CRFriend

@dynamic sourceProfileId;
@dynamic friendId;

+ (NSString *)parseClassName {
    return @"CRFriend";
}

@end
