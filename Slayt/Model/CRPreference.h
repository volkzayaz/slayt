//
//  CRLike.h
//  Slayt
//
//  Created by Dmitry Utenkov on 5/22/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Parse/Parse.h>

@class CRUser;

typedef NS_ENUM(NSUInteger, CRPreferenceType)
{
    CRPreferenceDislike = 0,
    CRPreferenceLike
};

@interface CRPreference : PFObject <PFSubclassing>

@property (nonatomic, copy) NSString *sourceProfileId;

@property (nonatomic, copy) NSString *destinationProfileId;

@property (nonatomic, assign) CRPreferenceType preferenceType;

@end
