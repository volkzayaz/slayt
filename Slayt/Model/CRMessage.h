//
//  CRMessage.h
//  Slayt
//
//  Created by Dmitry Utenkov on 6/17/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Parse/Parse.h>

@interface CRMessage : PFObject <PFSubclassing>

@property (nonatomic, copy) NSString *senderId;

@property (nonatomic, copy) NSString *destinationId;

@property (nonatomic, copy) NSString *text;

@end
