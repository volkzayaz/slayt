//
//  CRCating.m
//  Slayt
//
//  Created by Vlad Soroka on 6/24/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRCasting.h"
#import <Parse/PFObject+Subclass.h>

@implementation CRCasting

@dynamic title;
@dynamic usage;
@dynamic description;

@dynamic author;
@dynamic castingPeriods;

@dynamic location;

+ (NSString*) parseClassName
{
    return @"CRCasting";
}


@end
