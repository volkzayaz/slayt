//
//  CRFilter.h
//  Slayt
//
//  Created by Vlad Soroka on 5/26/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRUser.h"

static NSString* kDisplayTypeSingleString    = @"Single String";
static NSString* kDisplayTypeNumberRange     = @"Number Range";
static NSString* kDisplayTypePayRate         = @"Pay Rate";
static NSString* kDisplayTypeAge             = @"Age";
static NSString* kDisplayTypeMultipleStrings = @"Multiple Strings";
static NSString* kDisplayTypeDistanceRange   = @"Distance Range";
/**
 *  @discussion this display type actually displays strings in the same format as the previous one, but it is designed for orderedProperties .
    @seealso CRUser orderedProperties
 */
static NSString* kDisplayTypeOrderedMultipleStrings = @"Multiple Ordered Strings";



static NSString* kAdditionalDetailsKeyTrailingSymbols = @"trailingSymbols";
static NSString* kAdditionalDetailsKeyPaymentFrequencyPropertyName = @"paymentFrequencyPropertyName";

typedef NS_ENUM(NSUInteger, CRConstraintType){
    CRConstraintTypeEqual = 0,//for both numbers and strings
    
    CRConstraintTypeLessOrEqual,
    CRConstraintTypeMoreOrEqual,
    
    CRConstraintTypeAgeLessOrEqual,
    CRConstraintTypeAgeMoreOrEqual,

    CRConstraintTypeContainsString,//for String containment 
    CRConstraintTypeContainsObject,//will require key to be contained in provided array
    CRConstraintTypeContainsArray,//will require key to contain every single object from provided array value

    CRConstraintTypeSearchRange,
    
    CRConstraintTypeProfileType,
    
    CRConstraintTypeCOUNT
};

@interface CRUserFilter : NSObject

@property (nonatomic, strong, readonly) CRUser* queryingUser;//this user is performing the query

/**
 *  @param  user - last performed filter will be stored into this parametr. Also the filter will use stored filtering values as a starting point. Resulting user set will not include this User.
 */
- (instancetype)initWithUser:(CRUser*)user;

/**
 *  @discussion In case key for this constraintType is already exist it will be overwritten by a new value.
 *  Note that you can store only one constraint of type CRConstraintTypeProfileType. Attempt to store the second CRConstraintTypeProfileType constraint will cause erasement of previous CRConstraintTypeProfileType constraint.
 */
- (void)setConstraintOfType:(CRConstraintType)type
                     forKey:(NSString*)key
                      value:(id)value;

- (void)removeConstraintOfType:(CRConstraintType)type
                        forKey:(NSString*)key;

/**
 *  @return constraint that was set for particular key into appropriate constraint type
 */
- (id)valueFromConstraintType:(CRConstraintType)type
                       forKey:(NSString *)key;

- (void)removeAllConstraints;


- (BOOL) isFilterSet;

/**
 *  @discussion you can access value of the only CRConstraintTypeProfileType constraint
 */
- (NSString*)profileTypeConstraintValue;

/**
 *  @return Array of all constraints that were applied for a given key
 */
- (NSArray*)constraintValueForKey:(NSString*)key;

/**
 *  @discussion If you've added constraint for some key, you can receive user frienldly description of this constraint by passing the same key you had passed at the time of constraint creation
 *  @param displayType - string from kDisplayType group. Parametr will determine the rules to display constraint for gicen key
    @param trailingSymbols - any symbols that will come right after the value for given key
 */
- (NSString*)descriptionForKey:(NSString*)key
               withDisplayType:(NSString*)displayType
             additionalDetails:(NSDictionary*)additionalDetails;


/**
 *  @discussion you receive PFQuery equivalent of user's filterSettings
 */
- (PFQuery*)chosenQuery;

/**
 *  @discussion call this method to save all the filter settings that are applied at this moment.
 *  @param needsSaveOnServer - whether or not you want to save filter settings to server immediatelly. If so, [user save] method will be called. Saving will be performed in background.
 */
- (void)saveFilterSettingsForceParseSave:(BOOL)needsSaveOnServer;

@end
