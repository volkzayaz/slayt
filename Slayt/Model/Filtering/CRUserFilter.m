//
//  CRFilter.m
//  Slayt
//
//  Created by Vlad Soroka on 5/26/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRUserFilter.h"
#import "CRUtils.h"
#import "PFObject+OrderedPropertiesStrings.h"
#import "NSDate+PassedYears.h"

@interface CRUserFilter ()

@property (nonatomic, strong) NSMutableArray* filterSettings;


@end

@implementation CRUserFilter

- (instancetype)initWithUser:(CRUser*)user
{
   self = [super init];
    
    if(self)
    {
        _queryingUser = user;
        if(user.filterSettings == nil ||
           user.filterSettings.count != CRConstraintTypeCOUNT)//the new constraint type was added and the user has outdated filterSettings array, so we need to substitute it
        {
            NSMutableArray *filterArray = [NSMutableArray arrayWithCapacity:CRConstraintTypeCOUNT];
            for(int i = 0; i<CRConstraintTypeCOUNT;++i)
            {
                [filterArray addObject:[NSMutableDictionary dictionary]];
            }
            
            self.filterSettings = filterArray;
        }
        else
        {
            NSMutableArray* array = [NSKeyedUnarchiver unarchiveObjectWithData:
                                     [NSKeyedArchiver archivedDataWithRootObject:user.filterSettings]];

            self.filterSettings = array;
        }
       
    }
    
    return self;
}

- (void)setConstraintOfType:(CRConstraintType)type
                     forKey:(NSString *)key
                      value:(id)value
{
    NSMutableDictionary* sameConstraintsDictionary = self.filterSettings[type];

    if(!sameConstraintsDictionary)
    {
        NSLog(@"Unsupported constraint type %i passed to addConstraintOfType:forkey:value: method",type);
        return;
    }
    
    if(type == CRConstraintTypeMoreOrEqual||
       type == CRConstraintTypeLessOrEqual)
    {
        if(![value isKindOfClass:[NSNumber class]] && ![value isKindOfClass:[NSDate class]])
        {
            NSLog(@"ERROR: %@ cannot be used as a constraint of type %i. %i supports only NSNumbers and NSDates",value,type,type);
            return;
        }
    }
    
    else if(type==CRConstraintTypeContainsObject)
    {
        if(![value isKindOfClass:[NSArray class]])
        {
            NSLog(@"ERROR: %@ cannot be used as a constraint of type %i. %i supports only NSArrays",value,type,type);
            return;
        }
    }
    
    else if(type == CRConstraintTypeContainsString)
    {
        if(![value isKindOfClass:[NSString class]])
        {
            NSLog(@"ERROR: %@ cannot be used as a constraint of type %i. %i supports only NSStrings",value,type,type);
            return;
        }
    }
    
    else if(type == CRConstraintTypeContainsArray)
    {
        if(![value isKindOfClass:[NSArray class]])
        {
            NSLog(@"ERROR: %@ cannot be used as a constraint of type %i. %i supports only NSArray",value,type,type);
            return;
        }
    }
    
    else if (type == CRConstraintTypeProfileType)
    {
        if(sameConstraintsDictionary.allValues.count > 0)
        {
            //before saving new ProfileType value we'll erase all constraints related to the previous profileType.
            //You can save and restore any constraints that you want to stay alive after user changes profileType in his filters.
            //Note that if you stay alive filters from different profileTypes the resulting query will most likely be knowingly false for all users. For example there are no user who are expirienced photographers AND have C cup size at the same time.
            
            [self eraseProfileSpecificConstraints];
        }
    }
    
    if([value isKindOfClass:[NSArray class]])
    {
        if(((NSArray*)value).count == 0)
        {
            [sameConstraintsDictionary removeObjectForKey:key];
            return;
        }
    }
    
    if(value == nil)
    {
        NSLog(@"An attempt to set nil constraint for key %@. No Actions will be done",key);
        return;
    }

    [sameConstraintsDictionary setObject:value forKey:key];
}

- (BOOL) isFilterSet
{
    BOOL __block answer = NO;
    
    [self.filterSettings enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL *stop) {
        answer |= obj.count != 0;
    }];
    
    return answer;
}

- (void)removeConstraintOfType:(CRConstraintType)type
                        forKey:(NSString *)key
{
    if(type == CRConstraintTypeProfileType)
    {
        [self eraseProfileSpecificConstraints];
    }
    
    NSMutableDictionary* particularDictionary = self.filterSettings[type];
    [particularDictionary removeObjectForKey:key];
}

- (void) removeAllConstraints
{
    [self.filterSettings enumerateObjectsUsingBlock:^(NSMutableDictionary* obj, NSUInteger idx, BOOL *stop) {
        [obj removeAllObjects];
    }];
}

- (void) saveFilterSettingsForceParseSave:(BOOL)needsSaveOnServer
{
    self.queryingUser.filterSettings = self.filterSettings;
    
    if(needsSaveOnServer)
    {
        [self.queryingUser saveInBackground];
    }
}

- (PFQuery*)chosenQuery
{
    PFQuery* resultingQuery = [self queryFromDictionary];
    
    return resultingQuery;
}

- (NSString*)descriptionForKey:(NSString *)key
               withDisplayType:(NSString *)displayType
             additionalDetails:(NSDictionary *)additionalDetails
{
    NSString* valueDescription = @"Any";
    NSArray* allConstraintsForKey = [self constraintValueForKey:key];
    
    if([displayType isEqualToString:kDisplayTypeSingleString])
    {
        NSString* value = allConstraintsForKey.firstObject;
        
        if(value && value.length > 0)
        {
            valueDescription = value;
        }
    }
    else if ([displayType isEqualToString:kDisplayTypeMultipleStrings])
    {
        NSArray* value = allConstraintsForKey.firstObject;
        
        if (value && value.count > 0)
        {
            valueDescription = [value componentsJoinedByString:@", "];
        }
    }
    else if([displayType isEqualToString:kDisplayTypeNumberRange])
    {
        NSNumber* bottomLimit = [self valueFromConstraintType:CRConstraintTypeMoreOrEqual forKey:key];
        NSNumber* upperLimit  = [self valueFromConstraintType:CRConstraintTypeLessOrEqual forKey:key];
        
        if(bottomLimit || upperLimit)
        {
            NSString* trailingSymbols = additionalDetails[kAdditionalDetailsKeyTrailingSymbols];
            valueDescription = [[CRUtils sharedInstance] rangeStringForSmallerNumber:bottomLimit
                                                                        biggerNumber:upperLimit
                                                                     trailingSymblos:trailingSymbols
                                                                           delimiter:@" - "];
        }
    }
    else if ([displayType isEqualToString:kDisplayTypeOrderedMultipleStrings])
    {
        NSArray* orderedValues = allConstraintsForKey.firstObject;
        if(orderedValues && orderedValues.count > 0)
        {
            NSArray* stringsForOrderedValues = [self.queryingUser stringsForPrefixedPropertyName:key];
            
            NSMutableString* resultingString = [NSMutableString string];
            NSUInteger count = orderedValues.count;
            [orderedValues enumerateObjectsUsingBlock:^(NSNumber* orderedIndex, NSUInteger idx, BOOL *stop) {
                [resultingString appendString:[stringsForOrderedValues objectAtIndex:orderedIndex.unsignedIntegerValue]];
                
                if(idx != count - 1)
                {
                    [resultingString appendString:@", "];
                }
            }];
            valueDescription = resultingString;
        }
    }
    else if ([displayType isEqualToString:kDisplayTypePayRate])
    {
        NSString* paymentFrequencyPropertyName = additionalDetails[kAdditionalDetailsKeyPaymentFrequencyPropertyName];
        
        NSNumber* paymentFrequencyNumber = [self valueFromConstraintType:CRConstraintTypeEqual
                                                            forKey:paymentFrequencyPropertyName];
        if(paymentFrequencyNumber)
        {
            CRPaymentFrequency paymentFrequency = paymentFrequencyNumber.unsignedIntegerValue;
            
            NSNumber* bottomPayRate = [self valueFromConstraintType:CRConstraintTypeMoreOrEqual forKey:key];
            NSNumber* upperPayRate = [self valueFromConstraintType:CRConstraintTypeLessOrEqual forKey:key];
            

            if(paymentFrequency == CRPaymentFrequencyNotSpecified ||
               paymentFrequency == CRPaymentFrequencyWorkForFree ||
               (!bottomPayRate && !upperPayRate))
            {
                valueDescription = [[self.queryingUser paymentFrequencyStrings] objectAtIndex:paymentFrequency];
            }
            else
            {
                NSString* trailingSymbols = nil;
                
                if(paymentFrequency == CRPaymentFrequencyHourly)
                {
                    trailingSymbols = @"$/h";
                }
                else
                {
                    trailingSymbols = @"$/d";
                }
                
                valueDescription = [[CRUtils sharedInstance] rangeStringForSmallerNumber:bottomPayRate
                                                                            biggerNumber:upperPayRate
                                                                         trailingSymblos:trailingSymbols
                                                                               delimiter:@" - "];
            }
        }
    }
    else if([displayType isEqualToString:kDisplayTypeAge])
    {
        NSNumber* olderThanYears = [self valueFromConstraintType:CRConstraintTypeAgeMoreOrEqual
                                                     forKey:key];
        NSNumber* youngerThanYears = [self valueFromConstraintType:CRConstraintTypeAgeLessOrEqual
                                                   forKey:key];
        
        if(olderThanYears && youngerThanYears)
        {
            valueDescription = [NSString stringWithFormat:@"%i - %i",[olderThanYears unsignedIntegerValue],[youngerThanYears unsignedIntegerValue]];
        }
        else if (olderThanYears && !youngerThanYears)
        {
            valueDescription = [NSString stringWithFormat:@"Older than %i",[olderThanYears unsignedIntegerValue]];
        }
        else if (!olderThanYears && youngerThanYears)
        {
            valueDescription = [NSString stringWithFormat:@"Under %i",[youngerThanYears unsignedIntegerValue]];
        }
    }
    else if ([displayType isEqualToString:kDisplayTypeDistanceRange])
    {
        NSNumber* distanceInMiles = [self valueFromConstraintType:CRConstraintTypeSearchRange
                                                           forKey:key];
        
        if(distanceInMiles && distanceInMiles.integerValue > 0)
        {
            valueDescription = [NSString stringWithFormat:@"Under %@ miles",distanceInMiles];
        }
        else
        {
            valueDescription = @"Worldwide";
        }
    }

    if(allConstraintsForKey && allConstraintsForKey.count > 0 && [valueDescription isEqualToString:@"Any"])
    {
        valueDescription = @"Filter is set";
    }
    
    return valueDescription;
}

- (id)valueFromConstraintType:(CRConstraintType)type forKey:(NSString *)key
{
    NSDictionary* particularConstraintType = self.filterSettings[type];
    
    id value = [particularConstraintType objectForKey:key];
    
    return value;
}

- (NSString*)profileTypeConstraintValue
{
    NSDictionary* particularConstraintType = self.filterSettings[CRConstraintTypeProfileType];
    
    NSAssert(particularConstraintType.allKeys.count <= 1, @"CRConstraintTypeProfileType is not designed to contain more than one Constraint of CRConstraintTypeProfileType");
    
    return particularConstraintType.allValues.firstObject;
}

- (NSArray*)constraintValueForKey:(NSString *)key
{
    NSMutableArray* values = [NSMutableArray arrayWithArray:[self.filterSettings valueForKey:key]];
    [values removeObjectIdenticalTo:[NSNull null]];
    
    if(values.count == 0)
    {
        values = nil;
    }
    
    return values;
}

- (PFQuery*)queryFromDictionary
{
    ///this is the resulting query
    PFQuery* userQuery = [CRUser query];
    
    NSArray* filterSettings = self.filterSettings;
    NSMutableDictionary* profileTypesQueryDictionary = [NSMutableDictionary dictionary];
    
    CRProfileTypeStorage* storage = [[CRProfileTypeStorage alloc] init];
    
    [filterSettings enumerateObjectsUsingBlock:^(NSDictionary* specificConstraintDictionary, NSUInteger constraintIndex, BOOL *stop) {
        [specificConstraintDictionary enumerateKeysAndObjectsUsingBlock:^(NSString* key, id obj, BOOL *stop) {
            PFQuery* queryToApplyConstraintTo = userQuery;//by this time we still don't know which Parse class the obj belongs to
            NSString* propertyName = key;//either the key is just the name of the property or the key has prefix of some profileType. If second case is occured if(ProfileTypeClass){} will handle this.
            
            Class ProfileTypeClass = [storage profileTypeClassForPrefixedKey:key];//here we are answering the question "which class is containing property with prefixed name |key|"
            
            if(ProfileTypeClass)//this means that value for this key is formed via prefixed property like @"model.height"
            {
                NSString* profileTypePrefix = [[ProfileTypeClass object] profilePrefix];//obtaining that prefix
                
                PFQuery* profileTypeQuery = profileTypesQueryDictionary[profileTypePrefix];
                if(!profileTypeQuery)///this is the first occurence of this profileType property in filterDictionary
                {
                    profileTypeQuery = [ProfileTypeClass query];///creating query of appropriate class
                    profileTypesQueryDictionary[profileTypePrefix] = profileTypeQuery;///storing it in the dictionary so we could use this query in the next occurences of this profileType in filterSettings
                }
                
                queryToApplyConstraintTo = profileTypeQuery;
                propertyName = [[CRUtils sharedInstance] substringOfString:key withoutPrefix:profileTypePrefix];
            }
            
            switch (constraintIndex) {
                case CRConstraintTypeEqual:
                {
                    [queryToApplyConstraintTo whereKey:propertyName equalTo:obj];
                    break;
                }
                case CRConstraintTypeLessOrEqual:
                {
                    [queryToApplyConstraintTo whereKey:propertyName lessThanOrEqualTo:obj];
                    break;
                }
                case CRConstraintTypeMoreOrEqual:
                {
                    [queryToApplyConstraintTo whereKey:propertyName greaterThanOrEqualTo:obj];
                    break;
                }
                case CRConstraintTypeAgeLessOrEqual:
                {
                   //here we are getting the age of the user. The constraint means "The user should be N years or younger". That means that we need the user's birthDate be greater than N-years-ago date.
                    NSNumber* yearsTillNow = obj;
                    NSDate *today = [[NSDate alloc] init];
                    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                    NSDateComponents *components = [[NSDateComponents alloc] init];
                    components.year = - (int)(yearsTillNow.unsignedIntegerValue);
                    
                    NSDate* dateConstraint = [calendar dateByAddingComponents:components toDate:today options:0];
                    
                    [queryToApplyConstraintTo whereKey:propertyName greaterThanOrEqualTo:dateConstraint];
                    
                    break;
                }
                case CRConstraintTypeAgeMoreOrEqual:
                {
                    //here we are getting the age of the user. The constraint means "The user should be N years or older". That means that we need the user's birthDate be less than N-years-ago date.
                    NSNumber* yearsTillNow = obj;
                    NSDate *today = [[NSDate alloc] init];
                    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                    NSDateComponents *components = [[NSDateComponents alloc] init];
                    components.year = - (int)(yearsTillNow.unsignedIntegerValue);
                    
                    NSDate* dateConstraint = [calendar dateByAddingComponents:components toDate:today options:0];
                    
                    [queryToApplyConstraintTo whereKey:propertyName lessThanOrEqualTo:dateConstraint];
                    
                    break;
                }
                case CRConstraintTypeContainsObject:
                {
                    [queryToApplyConstraintTo whereKey:propertyName containedIn:obj];
                    break;
                }
                case CRConstraintTypeContainsString:
                {
                    [queryToApplyConstraintTo whereKey:propertyName containsString:obj];
                    break;
                }
                case CRConstraintTypeContainsArray:
                {
                    [queryToApplyConstraintTo whereKey:propertyName containsAllObjectsInArray:obj];
                    break;
                }
                case CRConstraintTypeSearchRange:
                {
                    NSNumber* searchRange = obj;
                    if (searchRange.doubleValue > 0)
                    {
                        if(self.queryingUser.recentLocation)
                        {
                            [queryToApplyConstraintTo whereKey:propertyName
                                                  nearGeoPoint:self.queryingUser.recentLocation
                                                   withinMiles:searchRange.doubleValue];
                        }
                        else
                        {
                            NSLog(@"WARNING: An attempt to apply geopoint constraint with nil recentLocation property. GeoPoint Constraint will be skipped");
                        }
                    }
                    else if(searchRange.doubleValue < 0)
                    {
                        NSLog(@"WARNING: Negative search range distance was set to searchRange constraint. This constraint will be skipped");
                    }
                    break;
                }
                case CRConstraintTypeProfileType:
                {
                    NSAssert([obj isKindOfClass:[NSString class]], @"CRConstraintTypeProfileType constraints must have an NSString(profile name) as value");
                    NSString* profileName = obj;
                    
                    Class ProfileTypeClass = [[[CRProfileTypeStorage alloc] init] classForProfileTypeName:profileName];
                    NSString* proifleTypePrefix = [[ProfileTypeClass object] profilePrefix];
                    
                    PFQuery* queryForProfileTypeClass = [profileTypesQueryDictionary objectForKey:proifleTypePrefix];
                    if(ProfileTypeClass &&      //an existing profileType class was passed to us
                       !queryForProfileTypeClass)//query for this class has not been created yet
                    {
                        [profileTypesQueryDictionary setObject:[ProfileTypeClass query] forKey:proifleTypePrefix];
                    }
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }];
    }];
    
    [profileTypesQueryDictionary enumerateKeysAndObjectsUsingBlock:^(id key, PFQuery* profileTypeQuery, BOOL *stop)
    {
        [profileTypeQuery whereKey:isHidingForSearchKey notEqualTo:@YES];
        
        [userQuery whereKey:@"profileReferencesArray" matchesQuery:profileTypeQuery];
    }];
    
    return userQuery;
}

#pragma mark - private methods

- (void)eraseProfileSpecificConstraints
{
    //before saving new ProfileType value we'll erase all constraints related to the previous profileType.
    //You can save and restore any constraints that you want to stay alive after user changes profileType in his filters.
    //Note that if you stay alive filters from different profileTypes the resulting query will most likely be knowingly false for all users. For example there are no user who are expirienced photographers AND have C cup size at the same time.
    
    id locationValue = [self valueFromConstraintType:CRConstraintTypeEqual
                                              forKey:@"homeTown"];
    [self removeAllConstraints];
    
    if(locationValue)
    {
        [self setConstraintOfType:CRConstraintTypeEqual
                           forKey:@"homeTown"
                            value:locationValue];
    }
}

@end
