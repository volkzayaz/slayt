//
//  CRPhoto.m
//  Talent
//
//  Created by Vlad Soroka on 4/9/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Parse/PFObject+Subclass.h>
#import "CRPhoto.h"
#import "CRUser.h"

static dispatch_queue_t queue;

@implementation CRPhoto

@dynamic owner;
@dynamic photoFile;
@dynamic isMain;

@synthesize localImage = _localImage;

+ (NSString*)parseClassName
{
    return @"CRPhoto";
}

+ (dispatch_queue_t)queue {
    if (!queue) {
        queue = dispatch_queue_create("com.slayt.CRPhotoQueue", 0);
    }
    return queue;
}

- (void)setOwner:(CRUser*)owner
{
    self[@"owner"] = owner;
    
    PFQuery* query = [PFQuery queryWithClassName:[CRPhoto parseClassName]];
    [query whereKey:@"owner" equalTo:self.owner];
    [query whereKey:@"isMain" equalTo:@YES];
    
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        //Typically the user should have exactly one mainPhoto. But sometimes he can have none for a short period of time. For example when the user has just uploaded his very first photo
        NSAssert(number <= 1,@"Logic error user %@ has more than one mainPhoto.", owner.username);

    }];
}



- (void)setMain:(BOOL)isMain withCompletitionBlock:(SetMainPhotoBlock)completitionBlock
{
    if(isMain == YES)
    {
        PFQuery* query = [PFQuery queryWithClassName:[CRPhoto parseClassName]];
        [query whereKey:@"owner" equalTo:self.owner];
        [query whereKey:@"isMain" equalTo:@YES];
        
        __weak typeof(self) weakSelf = self;
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if(!error)
            {
                NSAssert(objects.count <= 1,@"User should have not more than one mainPhoto");
                if(objects.firstObject != self)
                {
                    CRPhoto* formerMainPhoto = objects.firstObject;
                    formerMainPhoto[@"isMain"] = @NO;
                    [formerMainPhoto saveInBackground];
                    
                    weakSelf[@"isMain"] = @YES;
                    [weakSelf saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        completitionBlock(error);
                    }];
                }
            }
            else
            {
                completitionBlock(error);
            }
        }];
    }
}

@end
