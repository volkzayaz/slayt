//
//  CRJob.m
//  Slayt
//
//  Created by Vlad Soroka on 6/27/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRJob.h"
#import <Parse/PFObject+Subclass.h>

@implementation CRJob

@dynamic title;
@dynamic usage;
@dynamic description;

@dynamic author;
@dynamic castingPeriods;

@dynamic location;

@dynamic jobStatus;

+ (NSString*) parseClassName
{
    return @"CRJob";
}

@end
