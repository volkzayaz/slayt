//
//  CRAgency.h
//  Talent
//
//  Created by Vlad Soroka on 4/10/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Parse/Parse.h>

@interface CRAgency : PFObject <PFSubclassing>

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* agentName;
@property (nonatomic, strong) NSNumber* agentPhone;
@property (nonatomic, strong) NSString* agentEmail;

@end
