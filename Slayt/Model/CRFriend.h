//
//  CRFriend.h
//  Slayt
//
//  Created by Dmitry Utenkov on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Parse/Parse.h>

@interface CRFriend : PFObject <PFSubclassing>

@property (nonatomic, copy) NSString *sourceProfileId;

@property (nonatomic, copy) NSString *friendId;

@end
