//
//  CRBarColors.m
//  Slayt
//
//  Created by Dmitry Utenkov on 6/9/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRBarColors.h"
#import "UIColorFromRGB.h"

@implementation CRBarColors

+ (void)setColors {
    // Removing of 1px line under navigation bar for all controllers
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGBHex(0x262938)];
    
    
    NSDictionary *attrs = @{ NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:0],
                             NSForegroundColorAttributeName : [UIColor whiteColor] };
    
    [[UINavigationBar appearance] setTitleTextAttributes:attrs];
    
    [[UIToolbar appearance] setTintColor:[UIColor whiteColor]];
    [[UIToolbar appearance] setBarTintColor:UIColorFromRGBHex(0x262938)];
}

+ (void)resetColors {
    [[UINavigationBar appearance] setBackgroundImage:nil
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:nil];
    
    [[UINavigationBar appearance] setTintColor:nil];
    [[UINavigationBar appearance] setBarTintColor:nil];
    
    [[UIToolbar appearance] setTintColor:nil];
    [[UIToolbar appearance] setBarTintColor:nil];
}

@end
