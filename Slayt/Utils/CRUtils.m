//
//  CRUtils.m
//  Talent
//
//  Created by Vlad Soroka on 4/1/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRUtils.h"
#import <Parse/Parse.h>
#import "Constants.h"

#import "CRProfileTypeStorage.h"

//parse Subclasses
#import "CRUser.h"
#import "CRPhoto.h"
#import "CRAgency.h"
#import "CRModel.h"
#import "CRActor.h"
#import "CRMakeupArtist.h"
#import "CRHairStylist.h"
#import "CRClothingStylist.h"
#import "CRPhotographer.h"
#import "CRTalentAgency.h"
#import "CRPreference.h"
#import "CREmployer.h"
#import "CRRatingManager.h"
#import "CRFriend.h"
#import "CRMessage.h"
#import "CRCasting.h"
#import "CRJob.h"

@interface CRUtils () <UIAlertViewDelegate>

@property (nonatomic, copy) void (^alertCompletionBlock)(void) ;

@end

@implementation CRUtils

+ (instancetype)sharedInstance
{
    static CRUtils* ivar = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ivar = [[CRUtils alloc] init];
    });
    return ivar;
}

- (void)setUpParse {
    [CRUser registerSubclass];
    [CRPhoto registerSubclass];
    [CRAgency registerSubclass];
    [CRModel registerSubclass];
    [CRActor registerSubclass];
    [CRMakeupArtist registerSubclass];
    [CRHairStylist registerSubclass];
    [CRClothingStylist registerSubclass];
    [CRPhotographer registerSubclass];
    [CRTalentAgency registerSubclass];
    [CRPreference registerSubclass];
    [CREmployer registerSubclass];
    [CRFriend registerSubclass];
    [CRMessage registerSubclass];
    [CRCasting registerSubclass];
    [CRJob registerSubclass];
    
    //register all the subclasses before setting appKey
    [Parse setApplicationId:PARSE_APP_ID clientKey:PARSE_CLIENT_KEY];
    //making every single PFObject be visible for everyone unless special ACL rules are stated
    PFACL *defaultACL = [PFACL ACL];
    [defaultACL setPublicReadAccess:YES];
    [defaultACL setPublicWriteAccess:YES];
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
    
    if ([CRUser currentUser]) {
        [self fetchCurrentUserData];
    }
}

- (void)fetchCurrentUserData {
    // Call of the ratingManager property lazily instantiate object and perform requests
    // for user's prefrences (likes\dislikes)
    [[CRRatingManager sharedInstance] login];
}

- (void)createRoundRectCorners:(UIRectCorner)corners forView:(UIView *)view
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(3, 3)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

- (CGSize)sizeForString:(NSString *)string withFont:(UIFont *)font toFitWidth:(CGFloat)width
{
    if(!string)
    {
        return CGSizeMake(0, 0);
    }
    
    CGRect rect = [string boundingRectWithSize:CGSizeMake(width, 9999)
                                       options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{NSFontAttributeName:font}
                                       context:nil];
    return rect.size;
}

- (NSString*) stringFromString:(NSString *)stringSeparatedByComma toFitWidth:(CGFloat)width forFont:(UIFont *)font
{
    if(width <= 0 || !stringSeparatedByComma)
    {
        return nil;
    }
    
    NSString* components = @", ";
    NSMutableString* string = [NSMutableString stringWithFormat:@"%@ and more",stringSeparatedByComma];
    
    CGSize size = [string sizeWithAttributes:
                   @{NSFontAttributeName:
                         font}];
    
    NSUInteger skipedWords = 0;
    
    while (size.width > width) {
        NSRange range = [string rangeOfString:components];
        
        if(range.location != NSNotFound)
        {
            range.length = range.location + components.length;
            range.location = 0;
            [string deleteCharactersInRange:range];
        }
        else
        {
            break;
        }
        
        size = [string sizeWithAttributes:
                @{NSFontAttributeName:
                      font}];
        skipedWords ++;
    }
    
    NSRange range = [string rangeOfString:@" and more"];
    [string deleteCharactersInRange:range];
    
    if(skipedWords)
    {
        [string appendFormat:@" and %lu more",(unsigned long)skipedWords];
    }
    
    return string;
}

- (NSString*) rangeStringForSmallerNumber:(NSNumber *)smallNumber
                             biggerNumber:(NSNumber *)biggerNumber
                          trailingSymblos:(NSString *)trailingSymbols
                                delimiter:(NSString *)delimiter
{
    NSString* result = nil;
    
    if(!trailingSymbols)
    {
        trailingSymbols = @"";
    }
    
    if(smallNumber != nil && biggerNumber != nil)//both are specified
    {
        result = [NSString stringWithFormat:@"%i%@%@%i%@",smallNumber.unsignedIntegerValue,trailingSymbols,delimiter,biggerNumber.unsignedIntegerValue,trailingSymbols];
    }
    else if (smallNumber || biggerNumber)//at least one is specified
    {
        if(smallNumber)
        {
            result = [NSString stringWithFormat:@"More than %i%@",smallNumber.unsignedIntegerValue,trailingSymbols];
        }
        else if (biggerNumber)
        {
            result = [NSString stringWithFormat:@"Less than %i%@",biggerNumber.unsignedIntegerValue,trailingSymbols];
        }
    }
    
    return result;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    CGSize scaledSize = newSize;
    float scaleFactor = 1.0;
    if( image.size.width > image.size.height ) {
        scaleFactor = image.size.width / image.size.height;
        scaledSize.width = newSize.width;
        scaledSize.height = newSize.height / scaleFactor;
    }
    else {
        scaleFactor = image.size.height / image.size.width;
        scaledSize.height = newSize.height;
        scaledSize.width = newSize.width / scaleFactor;
    }
    
    UIGraphicsBeginImageContextWithOptions( scaledSize, NO, 0.0 );
    CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
    [image drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

#pragma mark - Facebook login helpers

- (void)loginUserWithFacebookWithCompletitionBlock:(PFUserResultBlock)block {
    NSArray* permissions = @[ @"basic_info", @"email", @"user_birthday" ];
    
    [PFFacebookUtils logInWithPermissions:permissions block:^(PFUser *user, NSError *error) {
        if (error && error.code != FBErrorLoginFailedOrCancelled) {
            NSLog(@"Error authorizing with facebook. Details - %@",error);
            NSString *message = [NSString stringWithFormat:@"Authorization error\n %@",[error localizedDescription]];
            [[CRUtils sharedInstance] showAlertWithMessage:message];
            return;
        }
        
        if (!user) {
            NSLog(@"The user cancelled the Facebook login.");
            NSString *message = [NSString stringWithFormat:@"Authorization error. Check your Facebook account settings"];
            [[CRUtils sharedInstance] showAlertWithMessage:message];
            return;
        } else {
            [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (error) {
                    NSLog(@"Error requesting user's information. Details - %@",error);
                    return;
                }
                
                FBGraphObject* responce = result;
                
                // Check for user existance
                
                PFQuery *query = [CRUser query];
                [query whereKey:@"username" equalTo:[NSString stringWithString:responce[@"email"]]];
                
                [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    if (error) {
                        NSLog(@"%@", error.localizedDescription);
                        return;
                    } else {
                        if (objects.count == 0) {
                            if (user.isNew) {
                                CRUser* newUser = [CRUser currentUser];
                                
                                newUser.username = [NSString stringWithString:responce[@"email"]];
                                newUser.email = [NSString stringWithString:responce[@"email"]];
                                newUser.fullName = [NSString stringWithString:responce[@"name"]];
                                
                                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                                NSDate* date = [dateFormatter dateFromString:[NSString stringWithString:responce[@"birthday"]]];
                                newUser.birthDate = date;
                                
                                NSString* gender = responce[@"gender"];
                                
                                if([gender isEqualToString:@"male"]) {
                                    newUser.gender = kGenderMale;
                                } else if([gender isEqualToString:@"female"]) {
                                    newUser.gender = kGenderFemale;
                                }
                                
                                [newUser saveInBackground];
                            }
                            block (user, error);
                        } else {
                            
                        }
                    }
                }];
            }];
        }
    }];
}


- (NSString*)substringOfString:(NSString *)string withoutPrefix:(NSString *)prefix
{
    NSRange prefixRange = [string rangeOfString:prefix];
    NSString* resultString = [string substringWithRange:NSMakeRange(prefixRange.length, string.length - prefixRange.length)];
    
    return resultString;
}

- (void)showAlertWithMessage:(NSString *)message completion:(void (^)(void))completion {
    self.alertCompletionBlock = completion;
    UIAlertView *alert = [self alertWithMessage:message];
    alert.delegate = self;
    [alert show];
}

- (void)showAlertWithMessage:(NSString *)message {
    [[self alertWithMessage:message] show];
}

- (UIAlertView *)alertWithMessage:(NSString *)message {
    return [[UIAlertView alloc] initWithTitle:@"Slayt"
                                      message:message
                                     delegate:self
                            cancelButtonTitle:@"OK"
                            otherButtonTitles:nil, nil];
}

- (BOOL)isUserExists:(NSString *)email {
    PFQuery *query = [CRUser query];
    [query whereKey:@"username" equalTo:email];
    NSArray *existingUsers = [query findObjects];
    if (existingUsers.count > 0) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - UIAlertViewDelegate protocol implementation

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (self.alertCompletionBlock) {
        self.alertCompletionBlock();
        self.alertCompletionBlock = nil;
    }
}

- (void)removeAllData {
    
    NSMutableArray *classes = [NSMutableArray array];
    
    [classes addObject:[CRPhoto class]];
    [classes addObject:[CRAgency class]];
    [classes addObject:[CRModel class]];
    [classes addObject:[CRActor class]];
    [classes addObject:[CRMakeupArtist class]];
    [classes addObject:[CRHairStylist class]];
    [classes addObject:[CRHairStylist class]];
    [classes addObject:[CRClothingStylist class]];
    [classes addObject:[CRPhotographer class]];
    [classes addObject:[CRTalentAgency class]];
    [classes addObject:[CRPreference class]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        PFQuery *usersQuery = [CRUser query];
        usersQuery.limit = 1000;
        
        NSArray *users = [usersQuery findObjects];
        
        for (CRUser *user in users) {
            if ([CRUser logInWithUsername:user.username password:@"1111"]) {
                [[CRUser currentUser] delete];
            }
        }
        NSLog(@"Users removed");
        
        for (Class class in classes) {
            PFQuery *query = nil;
            if (class == [CRUser class]) {
                query = [CRUser query];
            } else {
                query = [PFQuery queryWithClassName:[class parseClassName]];
            }
            [query setLimit:1000];
            
            NSArray *objects = [query findObjects];
            for (PFObject *object in objects) {
                [object delete];
            }
            NSLog(@"%@ objects removed", NSStringFromClass(class));
        }
        
        NSLog(@"All objects removed");
    });
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self removeAllData];
    }
}

@end
