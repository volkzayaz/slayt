//
//  CRGrabKitReceiverProtocol.h
//  Talent
//
//  Created by Dmitry Utenkov on 5/7/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CRGrabKitReceiverProtocol <NSObject>

@property (nonatomic, strong) NSArray *photos;

+ (instancetype)sharedInstance;

- (void)grabKitPhotoPickerDidPickPhotos:(id)photos;

@end
