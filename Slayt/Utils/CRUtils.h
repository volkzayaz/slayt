//
//  CRUtils.h
//  Talent
//
//  Created by Vlad Soroka on 4/1/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRUserDefines.h"
#import "CRProfileProtocol.h"

@interface CRUtils : NSObject <UIActionSheetDelegate>

+ (instancetype)sharedInstance;

- (void)setUpParse;
- (void)createRoundRectCorners:(UIRectCorner)corners forView:(UIView*)view;

/**
 *  @return size that will be suitable for displaying given string with given font. 
 *  @param width - width constraint for calculation
 */
- (CGSize) sizeForString:(NSString*)string withFont:(UIFont*)font toFitWidth:(CGFloat)width;

/**
 *  The method compose string in format @"string1, string2, string3 and X more" to fit in a given width. String1, String2... - are strings 
 *  from given string. X - number of strings that were not put into sequence
 */
- (NSString*) stringFromString:(NSString*)stringSeparatedByComma
                    toFitWidth:(CGFloat)width
                       forFont:(UIFont*)font;

/**
 *  @discussion returns string formated this way 17'' - 21''.  
 *  17 and 21 are passed Numbers.
 *  '' - passed trailing symbols
 *  "-" - passed delimiter
 */
- (NSString*) rangeStringForSmallerNumber:(NSNumber*)smallNumber
                             biggerNumber:(NSNumber*)biggerNumber
                          trailingSymblos:(NSString*)trailingSymbols
                                delimiter:(NSString*)delimiter;

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

- (void)loginUserWithFacebookWithCompletitionBlock:(PFUserResultBlock)block;


- (NSString*)substringOfString:(NSString*)string
                 withoutPrefix:(NSString*)prefix;

- (void)showAlertWithMessage:(NSString *)message;
- (void)showAlertWithMessage:(NSString *)message completion:(void (^)(void))completion;

- (BOOL)isUserExists:(NSString *)email;

- (void)removeAllData;

@end
