//
//  CRFacebookGrabKitReceiver.h
//  Talent
//
//  Created by Dmitry Utenkov on 5/7/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRGrabKitReceiverProtocol.h"

@class CRFacebookGrabKitReceiver;

@protocol FacebookReceiverDelegate <NSObject>

- (void)receiver:(CRFacebookGrabKitReceiver *)receiver didLoadImage:(UIImage *)image;

- (NSUInteger)currentPhotosNumber;

- (NSUInteger)maxPhotosNumber;

@end

@interface CRFacebookGrabKitReceiver : NSObject <CRGrabKitReceiverProtocol>

@property (nonatomic) id<FacebookReceiverDelegate> delegate;

@end
