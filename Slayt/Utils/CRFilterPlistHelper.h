//
//  CRFilterPlistHelper.h
//  Slayt
//
//  Created by Vlad Soroka on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRUserFilter.h"

@interface CRFilterPlistHelper : NSObject

/**
 *  @discussion method takes into account all details related to filters that were set in passed filter to return the most appropriate details array.
 */
+ (NSArray*)specificProfileDetailsForFilter:(CRUserFilter*)filter;

@end
