//
//  CRProfileGenerator.h
//  Talent
//
//  Created by Dmitry Utenkov on 5/13/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRProfileGenerator : NSObject

- (void)generateUsers:(NSUInteger)count;

@end
