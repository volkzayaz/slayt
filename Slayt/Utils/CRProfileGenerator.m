//
//  CRProfileGenerator.m
//  Talent
//
//  Created by Dmitry Utenkov on 5/13/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRProfileGenerator.h"
#import "CRUser.h"
#import "CRPhoto.h"
#import "Constants.h"

@interface CRProfileGenerator ()

@property (nonatomic, copy) NSMutableArray *profiles;

@end

@implementation CRProfileGenerator

- (void)setProfiles:(NSMutableArray *)profiles {
    _profiles = [profiles mutableCopy];
}

- (void)generateUsers:(NSUInteger)count {
    if (count > 20) {
        count = 20;
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.randomuser.me/?results=%ld", (unsigned long)count]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               NSMutableDictionary * users = [NSJSONSerialization JSONObjectWithData:data
                                                                                             options:NSJSONReadingAllowFragments
                                                                                               error:nil];
                               [users writeToFile:[NSTemporaryDirectory() stringByAppendingPathComponent:@"users.plist"] atomically:NO];
                               self.profiles = users[@"results"];
                               [self performSelectorInBackground:@selector(createUser) withObject:nil];
                           }];
}

- (void)createUser {
    for (NSDictionary *userDictionary in self.profiles) {
        NSDictionary *user = userDictionary[@"user"];
        CRUser *newUser = [CRUser object];
        
        newUser.fullName = [[NSString stringWithFormat:@"%@ %@", user[@"name"][@"first"], user[@"name"][@"last"]] capitalizedString];
        newUser.username = user[@"email"];
        newUser.email = user[@"email"];
        newUser.password = @"1111";
        
        [newUser signUp];
        
        NSURL *imageUrl = [NSURL URLWithString:user[@"picture"]];
        NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
        CRPhoto *photo = [[CRUser currentUser] addPhotoForImage:[UIImage imageWithData:imageData]];
        photo.isMain = YES;
        
        [photo save];
        
        [newUser.profileTypes addProfileForTypeName:@"Model"];
        
        [newUser save];
        NSLog(@"User generated: %@", newUser.email);
    }
    [self createTestUser];
}

- (void)createTestUser {
    PFQuery *query = [CRUser query];
    [query whereKey:@"username" equalTo:PARSE_TEST_USER_EMAIL];
    if ([query findObjects].count == 0) {
        CRUser* user = [CRUser object];
        
        user.username = PARSE_TEST_USER_EMAIL;
        user.password = PARSE_TEST_USER_PASSWORD;
        user.email = PARSE_TEST_USER_EMAIL;
        user.fullName = PARSE_TEST_USER_FULLNAME;
        
        [user signUp];
        
        [user.profileTypes addProfileForType:CRProfileTypeModel];
        [user save];
        [CRUser logOut];
        NSLog(@"Test user generated");
    }
}

@end
