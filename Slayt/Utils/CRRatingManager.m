//
//  CRRatingManager.m
//  Slayt
//
//  Created by Dmitry Utenkov on 5/26/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRRatingManager.h"
#import "CRUser.h"
#import "CRPreference.h"
#import "CRPhoto.h"
#import "CRUserFilter.h"
#import "CRFetchManager.h"

@interface CRRatingManager ()

@property (nonatomic, strong, readwrite) CRUser *currentProfile;

@property (nonatomic, strong) NSMutableArray *viewedProfiles;
@property (nonatomic, strong, readonly) NSSet *viewedProfilesId;

@property (nonatomic, strong) dispatch_queue_t queue;

@property (nonatomic, strong) NSTimer *updateTimer;

@end

#if DEBUG
static const NSUInteger dislikeExpirationTimeInterval = 120;
#else
static const NSUInteger dislikeExpirationTimeInterval = 600;
#endif

@implementation CRRatingManager

- (NSSet *)viewedProfilesId {
    NSMutableSet *mutableSet = [NSMutableSet set];
    for (CRPreference *preference in self.viewedProfiles) {
        [mutableSet addObject:preference.destinationProfileId];
    }
    return [NSSet setWithSet:mutableSet];
}

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        _viewedProfiles = [NSMutableArray array];
        _queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    }
    return self;
}

- (void)requestPreferences {
    if (![CRUser currentUser]) {
        return;
    }
    dispatch_async(self.queue, ^{
        PFQuery *preferencesQuery = [PFQuery queryWithClassName:[CRPreference parseClassName]];
        [preferencesQuery whereKey:@"sourceProfileId" equalTo:[CRUser currentUser].objectId];
        
        NSArray *preferences = [preferencesQuery findObjects];
        if (preferences.count > 0) {
            [self.viewedProfiles addObjectsFromArray:preferences];
            [self removeOutdatedDislikes:self.viewedProfiles];
            
        }
        [self requestProfiles:1];
    });
}

- (void)removeOutdatedDislikes:(NSMutableArray *)allPreferences {
    NSIndexSet *outdatedDislikesIndexSet = [allPreferences indexesOfObjectsWithOptions:NSEnumerationConcurrent
                                                                           passingTest:^BOOL(CRPreference *preference, NSUInteger idx, BOOL *stop)
                                            {
                                                return  preference.preferenceType == CRPreferenceDislike &&
                                                [[NSDate date] timeIntervalSinceDate:preference.createdAt] > dislikeExpirationTimeInterval;
                                            }];
    
    [outdatedDislikesIndexSet enumerateIndexesWithOptions:NSEnumerationConcurrent
                                               usingBlock:^(NSUInteger idx, BOOL *stop)
     {
         [allPreferences[idx] deleteInBackground];
     }];
    
    [allPreferences removeObjectsAtIndexes:outdatedDislikesIndexSet];
}



- (void)requestProfiles:(NSUInteger)profilesCount {
    dispatch_async(self.queue, ^{
        CRUserFilter* filter = [[CRUserFilter alloc] initWithUser:[CRUser currentUser]];
        
        PFQuery *query = [filter chosenQuery];

        if([CRUser currentUser].objectId)
        {
            ///this is temporary fix for RaceCondition crash.
            ///In case user logs out [CRUser currentUser] set to nil
            ///Which leads to setting nil constraint on @"objectId" key
            [query whereKey:@"objectId" notEqualTo:[CRUser currentUser].objectId];
        }
        
        [query whereKey:@"objectId" notContainedIn:self.viewedProfilesId.allObjects];
        [query includeKey:@"profileReferencesArray"];
        [query orderByAscending:@"createdAt"];
        query.limit = profilesCount;
        
        NSError* error = nil;
        NSArray *objects = [query findObjects:&error];
        
        [objects enumerateObjectsUsingBlock:^(CRUser* obj, NSUInteger idx, BOOL *stop) {
            [[CRFetchManager sharedManager] fetchAllInfoInBackgroundForUser:obj];
        }];

        
        if (error) {
            NSLog(@"Rating manager error: %@", error.localizedDescription);
            return;
        }
        
        if (![CRUser currentUser]) {
            return;
        }
        
        if (objects.count > 0) {
            if (self.currentProfile != objects[0]) {
                CRPhoto *profilePhoto = [objects[0] requestMainPhoto:nil];
                if (![profilePhoto.photoFile isDataAvailable]) {
                    [profilePhoto.photoFile getDataInBackgroundWithBlock:nil];
                }
                self.currentProfile = objects[0];
                if ([self.delegate respondsToSelector:@selector(ratingManager:didLoadProfile:)]) {
                    [self.delegate ratingManager:self didLoadProfile:self.currentProfile];
                }
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [NSTimer scheduledTimerWithTimeInterval:5
                                                 target:self
                                               selector:@selector(checkNewUsers:)
                                               userInfo:nil
                                                repeats:NO];
            });
            self.currentProfile = nil;
            if ([self.delegate respondsToSelector:@selector(ratingManagerDidNotFoundProfiles:)]) {
                [self.delegate ratingManagerDidNotFoundProfiles:self];
            }
        }
    });
}

- (void)login {
    [self requestPreferences];
}

- (void)logout {
    self.delegate = nil;
    [self.updateTimer invalidate];
    self.currentProfile = nil;
    [self.viewedProfiles removeAllObjects];
}

- (void)reloadProfiles {
    [self.updateTimer invalidate];
    self.currentProfile = nil;
    [self removeOutdatedDislikes:self.viewedProfiles];
    [self requestProfiles:1];
}

- (void)rateCurrentProfile:(CRPreferenceType)preferenceType {
    if (self.currentProfile) {
        CRPreference *preference = [CRPreference object];
        preference.sourceProfileId = [CRUser currentUser].objectId;
        preference.destinationProfileId = self.currentProfile.objectId;
        preference.preferenceType = preferenceType;
        [preference saveInBackground];
        
        [self.viewedProfiles addObject:preference];
        [self removeOutdatedDislikes:self.viewedProfiles];
    }

    [self requestProfiles:1];
}

- (void)checkNewUsers:(NSTimer *)timer {
    [timer invalidate];
    [self reloadProfiles];
}

@end
