//
//  CRUserManager.h
//  Talent
//
//  Created by Dmitry Utenkov on 5/12/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const CRLoginManagerEmailKey;
FOUNDATION_EXPORT NSString *const CRLoginManagerPasswordKey;

@class CRLoginManager;

@protocol CRUserManagerDelegate <NSObject>

- (void)userManagerDidSuccessfullLogin:(CRLoginManager *)maanger;

- (void)userManagerDidFail:(NSError *)error;

@optional

- (void)userManagerNeedsToLinkFacebookAccount:(CRLoginManager *)userManager;

- (void)userManagerNeedsToRegisterFacebookAccount:(CRLoginManager *)userManager;

@end


@interface CRLoginManager : NSObject

+ (instancetype)manager;

@property (nonatomic, assign) id<CRUserManagerDelegate> delegate;

- (void)login:(NSDictionary *)userInfo;

- (void)loginWithFacebook;

- (void)linkFacebookUser:(NSString *)password;

@end
