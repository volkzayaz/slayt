//
//  CRFilterPlistHelper.m
//  Slayt
//
//  Created by Vlad Soroka on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterPlistHelper.h"
#import "CRProfileTypeStorage.h"

#import "CRModel.h"

@implementation CRFilterPlistHelper

+ (NSArray*)specificProfileDetailsForFilter:(CRUserFilter *)filter
{
    ///profile specificCells
    NSString* profileTypeName = [filter profileTypeConstraintValue];
    Class ProfileTypeClass = [[[CRProfileTypeStorage alloc] init] classForProfileTypeName:profileTypeName];
    NSArray* specificProfileDetails = nil;
    NSString *specificPlistName = nil;
    
    if(ProfileTypeClass)
    {
        specificPlistName = [[ProfileTypeClass object] profilePropertiesPlistFileName];
        NSString *specificPlistFilePath = [[NSBundle mainBundle] pathForResource:specificPlistName ofType: @"plist"];
        specificProfileDetails = [NSArray arrayWithContentsOfFile:specificPlistFilePath];
    }
    
    if([profileTypeName isEqualToString:[[CRModel object] profileName]])
    {
        NSString* genderConstraint = [filter constraintValueForKey:@"gender"].firstObject;
        NSString* genderSpecificSizesPlistPath = nil;
        
        if([kGenderMale isEqualToString:genderConstraint])
        {
            genderSpecificSizesPlistPath = [[NSBundle mainBundle] pathForResource:@"Male's sizes" ofType:@"plist"];
        }
        else if([kGenderFemale isEqualToString:genderConstraint])
        {
            genderSpecificSizesPlistPath = [[NSBundle mainBundle] pathForResource:@"Female's sizes" ofType:@"plist"];
        }
        
        // TODO: not urgent
        //currently we are going down through hierarchy and making mutable copies of every level in order to replace the single array on the third level. Probably there's a faster/smarter way of achieving this goal.
        if(genderSpecificSizesPlistPath)
        {
            NSArray* specificSizesArray = [NSArray arrayWithContentsOfFile:genderSpecificSizesPlistPath];
            
            NSAssert(specificSizesArray, @"File %@ was not found", genderSpecificSizesPlistPath);
            
            NSMutableArray* rootArray = [specificProfileDetails mutableCopy];
            NSUInteger index = [rootArray indexOfObjectPassingTest:^BOOL(NSDictionary* obj, NSUInteger idx, BOOL *stop) {
                NSString* identifier = obj[@"identifier"];
                if([identifier isEqualToString:@"sizes"])
                {
                    *stop = YES;
                    return YES;
                }
                else
                {
                    return NO;
                }
            }];
            
            NSAssert(index!=NSNotFound, @"Section with identifier 'sizes' was not found in file %@", specificPlistName);
            
            NSMutableDictionary* sizeSectionInfo = [rootArray[index] mutableCopy];
            
            [sizeSectionInfo removeObjectForKey:@"properties"];
            [sizeSectionInfo setObject:specificSizesArray forKey:@"properties"];
            
            [rootArray replaceObjectAtIndex:index withObject:sizeSectionInfo];
            
            return rootArray;
        }
    }
    
    return specificProfileDetails;
}

@end
