//
//  CRFacebookGrabKitReceiver.m
//  Talent
//
//  Created by Dmitry Utenkov on 5/7/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFacebookGrabKitReceiver.h"
#import "CRUser.h"
#import "CRPhoto.h"

@interface CRFacebookGrabKitReceiver ()

@property (nonatomic, strong) dispatch_queue_t imageLoadingQueue;

@end

@implementation CRFacebookGrabKitReceiver

@synthesize photos = _photos;

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        _imageLoadingQueue = dispatch_queue_create("com.slayt.facebookImageLoadingQueue", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

- (void)grabKitPhotoPickerDidPickPhotos:(id)photos {
    if ([photos isKindOfClass:[NSArray class]] && [photos count] > 0) {
        self.photos = [NSArray arrayWithArray:photos];
    }
    
    NSUInteger maxPhotosNumber = [self.delegate maxPhotosNumber];
    NSUInteger currentPhotosNumber = [self.delegate currentPhotosNumber];
    if (self.photos.count > (maxPhotosNumber - currentPhotosNumber)) {
        self.photos = [self.photos subarrayWithRange:NSMakeRange(0, (maxPhotosNumber - currentPhotosNumber))];
    }
    
    dispatch_sync(self.imageLoadingQueue, ^{
        for (id photo in self.photos) {
            NSURL *url = [[[photo images] firstObject] URL];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
            if ([self.delegate respondsToSelector:@selector(receiver:didLoadImage:)]) {
                [self.delegate receiver:self didLoadImage:image];
            }
        }
    });
}

@end
