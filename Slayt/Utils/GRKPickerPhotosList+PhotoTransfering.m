//
//  GRKPickerPhotosList+PhotoTransfering.m
//  Talent
//
//  Created by Dmitry Utenkov on 5/7/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFacebookGrabKitReceiver.h"
#import "GRKPickerViewController.h"
#import "GRKPickerPhotosList.h"
#import "GRKPickerAlbumsList.h"

@interface GRKPickerViewController (PrivateMethodsPrototypes)

- (NSArray *)selectedPhotos;

@end

@implementation GRKPickerViewController (PropertyExpose)

- (NSMutableDictionary *)selectedPhotosDictionary {
    return _selectedPhotos;
}

@end

@implementation GRKPickerPhotosList (PhotoTransfering)

- (void)doneButtonTapped:(id)sender {
    [[CRFacebookGrabKitReceiver sharedInstance] grabKitPhotoPickerDidPickPhotos:[[GRKPickerViewController sharedInstance] selectedPhotos]];
    [[[GRKPickerViewController sharedInstance] selectedPhotosDictionary] removeAllObjects];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

@implementation GRKPickerAlbumsList (PhotoTransfering)

- (void)doneButtonTapped:(id)sender {
    [[CRFacebookGrabKitReceiver sharedInstance] grabKitPhotoPickerDidPickPhotos:[[GRKPickerViewController sharedInstance] selectedPhotos]];
    [[[GRKPickerViewController sharedInstance] selectedPhotosDictionary] removeAllObjects];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
