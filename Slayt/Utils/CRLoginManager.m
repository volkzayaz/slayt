//
//  CRUserManager.m
//  Talent
//
//  Created by Dmitry Utenkov on 5/12/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRLoginManager.h"
#import <Parse/Parse.h>
#import "CRUser.h"
#import "CRPhoto.h"

#import "CRFetchManager.h"

NSString *const CRLoginManagerEmailKey = @"email";
NSString *const CRLoginManagerPasswordKey = @"password";

@implementation CRLoginManager

+ (instancetype)manager {
    return [[self alloc] init];
}

- (void)login:(NSDictionary *)userInfo {
    if ([CRUser currentUser]) {
        [[PFFacebookUtils session] closeAndClearTokenInformation];
        [CRUser logOut];
    }
    
    NSAssert([CRUser currentUser] == nil, @"Current user should be empty before login attempt");
    
    NSString *email = userInfo[CRLoginManagerEmailKey];
    NSString *password = userInfo[CRLoginManagerPasswordKey];
    
    [CRUser logInWithUsernameInBackground:email password:password block:^(PFUser *user, NSError *error) {
        if (!error && user == [CRUser currentUser]) {
            [[CRFetchManager sharedManager] fetchAllInfoInBackgroundForUser:(CRUser*)user];
            [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kUserDidUpdateMainPhotoNotification
                                                                                                 object:nil]];
            if ([self.delegate respondsToSelector:@selector(userManagerDidSuccessfullLogin:)]) {
                [self.delegate userManagerDidSuccessfullLogin:self];
            }
        } else  {
            [self failWithError:error customMessage:@"Incorrect login and/or password"];
        }
    }];
}

- (void)linkFacebookUser:(NSString *)password {
    NSString *email = [CRUser currentUser].email;
    CRUser *faceboookUser = [CRUser currentUser];
    [faceboookUser delete];
    [CRUser logInWithUsernameInBackground:email
                                 password:password
                                    block:^(PFUser *user, NSError *error)
     {
         if (!error && user == [CRUser currentUser]) {
             [[CRFetchManager sharedManager] fetchAllInfoInBackgroundForUser:(CRUser*)user];
             NSArray* permissions = @[ @"basic_info", @"email", @"user_birthday" ];
             [PFFacebookUtils linkUser:[CRUser currentUser]
                           permissions:permissions
                                 block:^(BOOL succeeded, NSError *error) {
                                     if (!error && succeeded) {
                                         if ([self.delegate respondsToSelector:@selector(userManagerDidSuccessfullLogin:)]) {
                                             [self.delegate userManagerDidSuccessfullLogin:self];
                                         }
                                     } else {
                                         [self failWithError:error customMessage:nil];
                                     }
                                 }];
         } else {
             [self failWithError:error customMessage:@"Wrong password. Please, try again"];
         }
     }];
}

- (void)loginWithFacebook {
    NSArray* permissions = @[ @"basic_info", @"email", @"user_birthday" ];
    
    [PFFacebookUtils initializeFacebook];
    [PFFacebookUtils logInWithPermissions:permissions block:^(PFUser *user, NSError *error) {
        
        if (!error && user) {
            [[CRFetchManager sharedManager] fetchAllInfoInBackgroundForUser:(CRUser*)user];
            [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (error) {
                    NSLog(@"Error requesting user's information. Details - %@",error);
                    
                    [self failWithError:error customMessage:nil];
                    return;
                }
                
                FBGraphObject* responce = result;
                
                PFQuery *query = [CRUser query];
                [query whereKey:@"username" equalTo:[NSString stringWithString:responce[@"email"]]];
                [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    if (error) {
                        [self failWithError:error customMessage:nil];
                        return;
                    } else {
                        // Fill Parse account crederntials
                        if (objects.count != 0) {
                            if ([PFFacebookUtils isLinkedWithUser:objects.firstObject]) {
                                if ([self.delegate respondsToSelector:@selector(userManagerDidSuccessfullLogin:)]) {
                                    [self.delegate userManagerDidSuccessfullLogin:self];
                                }
                            } else {
                                user.email = [NSString stringWithString:responce[@"email"]];
                                if ([self.delegate respondsToSelector:@selector(userManagerNeedsToLinkFacebookAccount:)]) {
                                    [self.delegate userManagerNeedsToLinkFacebookAccount:self];
                                }
                            }
                        } else {
                            if ([self.delegate respondsToSelector:@selector(userManagerNeedsToRegisterFacebookAccount:)]) {
                                [self.delegate userManagerNeedsToRegisterFacebookAccount:self];
                            }
                        }
                    }
                }];
            }];
        }
        
        if (error) {
            if (error.code != FBErrorLoginFailedOrCancelled) {
                NSString *message = [NSString stringWithFormat:@"Authorization error\n %@",[error localizedDescription]];
                [self failWithError:error customMessage:message];
            } else {
                NSString *message = [NSString stringWithFormat:@"Authorization error. Check your Facebook account settings"];
                [self failWithError:error customMessage:message];
            }
            return;
        }
        
        if (!user) {
            NSString *message = [NSString stringWithFormat:@"Authorization error. Check your Facebook account settings"];
            [self failWithError:error customMessage:message];
            return;
        }
    }];
}

#pragma mark - Utility methods

- (void)failWithError:(NSError *)error customMessage:(NSString *)customMessage {
    NSError *customError = error;
    if (customMessage || customMessage.length != 0) {
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:error.userInfo];
        userInfo[NSLocalizedDescriptionKey] = customMessage;
        customError = [NSError errorWithDomain:error.domain code:error.code userInfo:userInfo];
    }
    
    if ([self.delegate respondsToSelector:@selector(userManagerDidFail:)]) {
        [self.delegate userManagerDidFail:customError];
    }
}

@end
