//
//  CRBarColors.h
//  Slayt
//
//  Created by Dmitry Utenkov on 6/9/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRBarColors : NSObject

+ (void)setColors;
+ (void)resetColors;

@end
