//
//  CRRatingManager.h
//  Slayt
//
//  Created by Dmitry Utenkov on 5/26/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRPreference.h"

@class CRUser;
@class CRRatingManager;

@protocol CRRatingManagerDelegate <NSObject>

- (void)ratingManager:(CRRatingManager *)manager didLoadProfile:(CRUser *)loadedProfile;
- (void)ratingManagerDidNotFoundProfiles:(CRRatingManager *)manager;

@end

@interface CRRatingManager : NSObject

@property (nonatomic, weak) id<CRRatingManagerDelegate> delegate;
@property (nonatomic, strong, readonly) CRUser *currentProfile;

+ (instancetype)sharedInstance;

- (void)rateCurrentProfile:(CRPreferenceType)preferenceType;

- (void)login;
- (void)logout;
- (void)reloadProfiles;

@end
