//
//  CRUserTests.m
//  Talent
//
//  Created by Vlad Soroka on 5/12/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CRUser.h"
#import "PFObject+OrderedPropertiesStrings.h"
#import "CRModel.h"
#import "OCMock.h"

@interface CRUserTests : XCTestCase

@property (nonatomic, strong) CRUser* testObject;

@end

@implementation CRUserTests

- (void)setUp
{
    [super setUp];
    
    self.testObject = [CRUser object];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testStringsForPrefixedPropertyName
{
    NSArray* rightArray = [self.testObject jacketSizeStrings];
    
    NSArray* gottenArray = [self.testObject stringsForPrefixedPropertyName:@"jacketSize"];
    
    XCTAssert([rightArray isEqualToArray:gottenArray], @"We expect the arrays to contain the same values");
    
    id<CRProfileProtocol> profile = [self.testObject.profileTypes addProfileForType:CRProfileTypeModel];
    
    NSString* keyName = [NSString stringWithFormat:@"%@jacketSize",profile.profilePrefix];
    
    gottenArray = [self.testObject stringsForPrefixedPropertyName:keyName];
    XCTAssert([rightArray isEqualToArray:gottenArray], @"We expect the arrays to be equal even if the prefixed value is passed");
}

/**
 *  Test is dependent on CRProfileStorage tests. So if this test fails make sure that all of ProfileTypesTests succeeded
 */
- (void)testDuplicateUser{
    
    self.testObject.fullName = @"LeBron James";
    id mockModelProfile = [OCMockObject mockForClass:[CRModel class]];
    
    [[[mockModelProfile stub] andReturn:@"someId"] objectId];
    [[[mockModelProfile stub] andReturn:@"CRModel"] parseClassName];
    [[[mockModelProfile stub] andReturn:@"somePrefix"] profilePrefix];
    [[[mockModelProfile stub] andReturnValue:OCMOCK_VALUE(YES)] isDataAvailable];
    [[[mockModelProfile expect] andReturn:[CRModel object]] duplicateProfile];
    
    [[[mockModelProfile stub] andDo:^(NSInvocation *invoc) {
        [[[mockModelProfile stub] andReturn:@(20)] bust];
    }] fetch:[OCMArg anyObjectRef]];
    
    self.testObject.profileReferencesArray = [NSMutableArray arrayWithObject:mockModelProfile];

    CRUser* duplicatedUser = [self.testObject duplicateUser];
    
    for(NSString* key in [duplicatedUser allKeys])
    {
        id property = duplicatedUser[key];
        XCTAssertEqualObjects(property, self.testObject[key], @"We expect that every single property should be equal in the duplicated user and the original user. Assertion failed for key %@",key);
    }
    
    XCTAssertNotNil(duplicatedUser.profileTypes, @"We expect profileTypes property to be determined after duplicate call");
    
    duplicatedUser.fullName = @"Dwyane Wade";
    XCTAssertNotEqualObjects(duplicatedUser.fullName, self.testObject.fullName, @"We want duplicte object to be independent from original object. i.e. changing of any property of duplicate object does not lead to changing of original object");
    
    [mockModelProfile verify];
}

- (void)testUndoChanges
{
    self.testObject.fullName = @"Hello Kitty";
    
    CRModel* modelProfile = (CRModel*)[self.testObject.profileTypes addProfileForType:CRProfileTypeModel];
    modelProfile.bust = @(20);
    
    [self.testObject createSnapshot];
    
    self.testObject.fullName = @"Dwyde Howard";
    self.testObject.birthDate = [NSDate date];
    [self.testObject.profileTypes addProfileForType:CRProfileTypeActor];
    modelProfile.bust = @(10);
    modelProfile.hip = @(30);
    
    [self.testObject undoPerformedChanges];
    
    modelProfile = (CRModel*)[self.testObject.profileTypes profileForType:CRProfileTypeModel];
    
    XCTAssertEqualObjects(self.testObject.fullName, @"Hello Kitty", @"Full name must be restored to the previous value");
    XCTAssertNil(self.testObject.birthDate, @"Birth date must be nil since it was not specified before we've created snapshot of testObject");
    XCTAssertEqual(self.testObject.profileTypes.profileTypesArray.count, 1, @"testObject should be restored with exactly one profile type that was added before snapshot - modelProfileType");
    XCTAssertEqualObjects(modelProfile.bust, @(20), @"Model profile type must be restored with values that were set before snapshot.");
    XCTAssertNil(modelProfile.hip, @"Model profile type must not contain hip property since it was determined after we'd created snapshot");
}

@end
