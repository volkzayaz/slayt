//
//  CRLocationMonitor.m
//  Slayt
//
//  Created by Vlad Soroka on 6/10/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "CRUser.h"
#import "CRLocationMonitor.h"

@interface CRLocationMonitorTests : XCTestCase

@property (nonatomic, strong) CRLocationMonitor* testObject;
@property (nonatomic, strong) id mockUser;
@property (nonatomic, strong) id mockGeoPoint;

@end

@implementation CRLocationMonitorTests

- (void)setUp
{
    [super setUp];
    
    self.mockUser = [OCMockObject mockForClass:[CRUser class]];
    self.mockGeoPoint = [OCMockObject mockForClass:[PFGeoPoint class]];
    
    self.testObject = [[CRLocationMonitor alloc] initWithUser:self.mockUser];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInitialLocationUpdate
{
    [self stubLocationUpdateDate:nil];///location field has never been updated before
    [[self.mockGeoPoint expect] geoPointForCurrentLocationInBackground:[OCMArg any]];
    
    [self.testObject startMonitoring];
    
    [self.mockGeoPoint verify];
}

- (void)testExpiredUpdateInterval
{
    //locationUpdateInterval has expired
    [self stubLocationUpdateDate:[NSDate dateWithTimeInterval:-(kLocationUpdateInterval)
                                                    sinceDate:[NSDate date]]];
    [[self.mockGeoPoint expect] geoPointForCurrentLocationInBackground:[OCMArg any]];
    [self.testObject startMonitoring];
    [self.mockGeoPoint verify];
}

- (void)testValidUpdateInterval
{
    ///location Update is still valid(even though it'll expire in 10 seconds)
    [self stubLocationUpdateDate:[NSDate dateWithTimeInterval:-(kLocationUpdateInterval - 10)
                                                    sinceDate:[NSDate date]]];
    [self stubGeoPointsWithBlock:^{
        XCTFail(@"Expiration interval is still valid. We shouldn't request user location yet.");
    }];
    
    [self.testObject startMonitoring];
}

#pragma mark - utility methods

///desired date to be returned
- (void)stubLocationUpdateDate:(NSDate*)date
{
    [[[self.mockUser stub] andReturn:date] locationUpdatedAt];
}

//// block to be invoked instead of geoPointForCurrentLocationInBackground: call
- (void)stubGeoPointsWithBlock:(void(^)(void))block
{
    [[[self.mockGeoPoint stub] andDo:^(NSInvocation * inv) {
        block();
    }] geoPointForCurrentLocationInBackground:[OCMArg any]];
}


@end
