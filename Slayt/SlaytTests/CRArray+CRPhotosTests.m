//
//  CRArray+CRPhotosTests.m
//  Talent
//
//  Created by Vlad Soroka on 4/24/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSMutableArray+CRPhotos.h"
#import "CRPhoto.h"
#import "OCMock.h"
#import "CRUtils.h"

@interface CRArray_CRPhotosTests : XCTestCase

@property (nonatomic, strong) NSMutableArray* array;

@property (nonatomic, strong) CRPhoto* photo1;
@property (nonatomic, strong) CRPhoto* photo2;
@property (nonatomic, strong) CRPhoto* photo3;

@end

@implementation CRArray_CRPhotosTests

- (void)setUp
{
    [super setUp];
    
    /*[[CRUtils sharedInstance] setUpParse];
    
    self.photo1 = [CRPhoto object];
    self.photo2 = [CRPhoto object];
    self.photo3 = [CRPhoto object];
    
    self.photo1.isMain = NO;
    self.photo2.isMain = NO;
    self.photo3.isMain = NO;
    
    self.array = [NSMutableArray array];*/
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testAddPhoto
{
    /*[self.array addPhoto:self.photo1];
    
    XCTAssertFalse(!self.photo1.isMain, @"The very first photo added to the array should be main");
    
    [self.array addPhoto:self.photo2];
    [self.array addPhoto:self.photo3];
    
    NSIndexSet* indexSet = [self.array indexesOfObjectsPassingTest:^BOOL(CRPhoto* obj, NSUInteger idx, BOOL *stop) {
        return obj.isMain;
    }];
    
    XCTAssertEqual(indexSet.count, 1, @"By the end of operations array should contain exactly one main photo");*/
}

@end
