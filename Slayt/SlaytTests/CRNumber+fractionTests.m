//
//  CRNumber+fractionTests.m
//  Slayt
//
//  Created by Vlad Soroka on 5/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSNumber+fractions.h"

@interface CRNumber_fractionTests : XCTestCase

@property (nonatomic, strong) NSNumber* testObject;

@end

@implementation CRNumber_fractionTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testIntegerPart
{
    self.testObject = [NSNumber numberWithDouble:21.75];
    NSInteger integerPart = [self.testObject integerPart];
    XCTAssertEqual(integerPart, (NSInteger)21, @"Integer parts must be equal");
    
    self.testObject = @(24);
    integerPart = [self.testObject integerPart];
    XCTAssertEqual(integerPart, (NSInteger)24, @"Integer parts must be equal");

    self.testObject = @(-769.938461653);
    integerPart = [self.testObject integerPart];
    XCTAssertEqual(integerPart, (NSInteger)-769, @"Integer parts must be equal");
    
    self.testObject = @(0.0);
    integerPart = [self.testObject integerPart];
    XCTAssertEqual(integerPart, (NSInteger)0, @"Integer parts must be equal");
}

- (void)testFractionParts
{
    self.testObject = [NSNumber numberWithDouble:21.75];
    NSInteger integerPart = [self.testObject fractionPart];
    XCTAssertEqual(integerPart, (NSInteger)75, @"Fraction parts must be equal");
    
    self.testObject = @(24);
    integerPart = [self.testObject fractionPart];
    XCTAssertEqual(integerPart, (NSInteger)0, @"Fraction parts must be equal");
    
    self.testObject = @(-769.93);
    integerPart = [self.testObject fractionPart];
    XCTAssertEqual(integerPart, (NSInteger)93, @"Fraction parts must be equal");
    
    self.testObject = @(0.0);
    integerPart = [self.testObject fractionPart];
    XCTAssertEqual(integerPart, (NSInteger)0, @"Fraction parts must be equal");
    
    self.testObject = @(17.1);
    integerPart = [self.testObject fractionPart];
    XCTAssertEqual(integerPart, (NSInteger)10, @"Fraction parts must be equal since we are intrested in last 2 digits");
    
    self.testObject = @(17.01);
    integerPart = [self.testObject fractionPart];
    XCTAssertEqual(integerPart, (NSInteger)1, @"Fraction parts must be equal since we are intrested in last 2 digits");
}

@end
