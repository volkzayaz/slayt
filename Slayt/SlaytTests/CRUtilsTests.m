//
//  CRUtilsTests.m
//  Slayt
//
//  Created by Vlad Soroka on 6/5/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CRUtils.h"

@interface CRUtilsTests : XCTestCase

@property (nonatomic, strong) CRUtils* testObject;

@end

@implementation CRUtilsTests

- (void)setUp
{
    [super setUp];
    
    self.testObject = [CRUtils sharedInstance];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRangeString
{
    NSString* trailisngSymbols = @" m.";
    NSString* delimiter = @" / ";
    
    NSString* result = [self.testObject rangeStringForSmallerNumber:@(12)
                                                       biggerNumber:@(20)
                                                    trailingSymblos:trailisngSymbols
                                                          delimiter:delimiter];
    
    NSString* expectedResult = [NSString stringWithFormat:@"12%@%@20%@",trailisngSymbols,delimiter,trailisngSymbols];
    XCTAssert([result isEqualToString:expectedResult], @"resulted String has wrong format");
    
    result = [self.testObject rangeStringForSmallerNumber:nil
                                             biggerNumber:@(25)
                                          trailingSymblos:trailisngSymbols
                                                delimiter:delimiter];
    expectedResult = [NSString stringWithFormat:@"Less than 25%@",trailisngSymbols];
    
    XCTAssert([result isEqualToString:expectedResult], @"resulted String has wrong format");
    
    result = [self.testObject rangeStringForSmallerNumber:@(1)
                                             biggerNumber:nil
                                          trailingSymblos:trailisngSymbols
                                                delimiter:delimiter];
    expectedResult = [NSString stringWithFormat:@"More than 1%@",trailisngSymbols];
    
    XCTAssert([result isEqualToString:expectedResult], @"resulted String has wrong format");
    
    result = [self.testObject rangeStringForSmallerNumber:@(1)
                                             biggerNumber:@(17)
                                          trailingSymblos:nil
                                                delimiter:delimiter];
    
    expectedResult = [NSString stringWithFormat:@"1%@17",delimiter];

    XCTAssert([result isEqualToString:expectedResult], @"resulted String has wrong format %@",expectedResult);

}

@end
