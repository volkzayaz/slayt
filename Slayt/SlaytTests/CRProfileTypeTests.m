//
//  CRProfileTypeTests.m
//  Talent
//
//  Created by Vlad Soroka on 5/5/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CRProfileTypeStorage.h"
#import "CRModel.h"
#import "CRUser.h"

@interface CRProfileTypeTests : XCTestCase

@property (nonatomic, strong) CRUser* user;

@end

@implementation CRProfileTypeTests

- (void)setUp
{
    [super setUp];
    
    self.user = [CRUser object];
    self.user.profileReferencesArray = [NSMutableArray array];
    
    self.user.profileTypes = [[CRProfileTypeStorage alloc] initWithDictionaryStorage:self.user.profileReferencesArray];
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testInit
{
    XCTAssertNotNil(self.user.profileTypes.profileTypeGroups, @"ProfileTypeGroups should be determined after init method");
    
    NSUInteger zero = 0;
    XCTAssertEqual([self.user.profileTypes.profileTypesArray count], zero, @"Number of added profiles should be 0 just after we initialied class");
}

- (void)testBlankProfileTypes
{
    PFObject* profileType = [self.user.profileTypes profileForType:12345];
    XCTAssertNil(profileType, @"profileType should be nil if no profiles were added");
    
    profileType = [self.user.profileTypes profileForType:CRProfileTypeMakeupArtist];
    XCTAssertNil(profileType, @"profileType should be nil if no profiles were added");
}

- (void)testNumberOfProfilesAdded
{
    [self.user.profileTypes addProfileForType:CRProfileTypeModel];
    NSUInteger uInt = 1;
    XCTAssertEqual([self.user.profileTypes.profileTypesArray count], uInt, @"Number of added profiles should be 1 just after we added first profile");
    
    [self.user.profileTypes addProfileForType:CRProfileTypeActor];
    uInt = 2;
    XCTAssertEqual([self.user.profileTypes.profileTypesArray count],uInt, @"Number of added profiles should be 2 just after we added second profile");
}

- (void)testAddModelProfileType
{
    [self.user.profileTypes addProfileForType:CRProfileTypeModel];
    
    id profileType = [self.user.profileTypes profileForType:CRProfileTypeModel];
    XCTAssertTrue([profileType isKindOfClass:[CRModel class]], @"We should have CRModel class object after we've added profileType with kProfileTypeModel key");
}

- (void)testAddProfilesFromDifferentGroups
{
    [self.user.profileTypes addProfileForType:12345];
    XCTAssert([self.user.profileTypes.profileTypesArray count] == 0, @"Number of added profiles should be 0 since we've added profile with unexisting key");
    
    [self.user.profileTypes addProfileForType:CRProfileTypeClothingStylist];
    [self.user.profileTypes addProfileForType:CRProfileTypeMakeupArtist];
    [self.user.profileTypes addProfileForType:CRProfileTypeHairStylist];
    
    XCTAssert([self.user.profileTypes.profileTypesArray count] == 3, @"Number of added profiles should be 3 since we've added 3 profiles from the same group");
    
    [self.user.profileTypes addProfileForType:CRProfileTypePhotographer];
    
    XCTAssert([self.user.profileTypes.profileTypesArray count] == 3, @"Number of added profiles should be 3 after we've added fourth property because it belongs to the other group");
}

- (void) testAddProfilesWithTypeName
{
    id obj = [self.user.profileTypes addProfileForTypeName:@"someRandomName"];
    XCTAssertNil(obj, @"obj should be nil since we've added profile for unexisting key");
    XCTAssertEqual([self.user.profileTypes.profileTypesArray count],(NSUInteger)0, @"Number of added profiles should be 0 since we've added profile with unexisting key");
    
    [self.user.profileTypes addProfileForTypeName:((NSArray*)self.user.profileTypes.profileTypeGroupsStrings.firstObject).firstObject];
    
    XCTAssertEqual([self.user.profileTypes.profileTypesArray count], (NSUInteger)1, @"Number of added profiles should be 1 since we've added profile with existing key");
}

- (void)testMultipleAddProfileTypeForSameType
{
    [self.user.profileTypes addProfileForType:CRProfileTypeClothingStylist];
    [self.user.profileTypes addProfileForType:CRProfileTypeClothingStylist];
    
    XCTAssertEqual([self.user.profileTypes.profileTypesArray count], (NSUInteger)1, @"Number of added profiles should be 1 after we've added same profile type twice");
}

- (void)testProfileTypeForPrefixedKey
{
    CRModel* createdObject = (CRModel*)[self.user.profileTypes addProfileForType:CRProfileTypeModel];
    
    CRModel* receivedObject = (CRModel*)[self.user.profileTypes profileTypeForPrefixedKey:[NSString stringWithFormat:@"%@%@",[createdObject profilePrefix],@"whateverString"]];
    
    XCTAssertEqualObjects(createdObject, receivedObject, @"Objects must be equal for prefixedKey");
    
    CRModel* wrongObject = (CRModel*)[self.user.profileTypes profileTypeForPrefixedKey:@"noSuchKeyExist"];
    
    XCTAssertNil(wrongObject, @"Wrong object must be nil since it is the result of not existed key");
}

- (void)testAllProfileTypes
{
    [self.user.profileTypes addProfileForTypeName:@"ahahahaSuchKeyDoesNotExist"];
    id hairStylist = [self.user.profileTypes addProfileForType:CRProfileTypeHairStylist];
    id clothingStylist = [self.user.profileTypes addProfileForType:CRProfileTypeClothingStylist];
    
    NSArray* profiles = self.user.profileTypes.profileTypesArray;
    
    XCTAssertEqual(profiles.count, (NSUInteger)2, @"profiles array should contain 2 items since we've added 2 items to storage");
    XCTAssert([profiles containsObject:hairStylist], @"we expect hair stylist to be in the allProfiles array since we've added it");
    XCTAssert([profiles containsObject:clothingStylist], @"we expect clothing stylist to be in the allProfiles array since we've added it");
}

- (void)testRemoveProfile
{
    id model = [self.user.profileTypes addProfileForType:CRProfileTypeModel];
    [self.user.profileTypes addProfileForType:CRProfileTypeActor];
    
    [self.user.profileTypes removeProfileType:model];
    
    XCTAssertEqual([self.user.profileTypes.profileTypesArray count], (NSUInteger)1, @"Only one profile should have staied in storage by this time");
}

- (void)testArrayOfProfileStrings
{
    [self.user.profileTypes addProfileForType:CRProfileTypeMakeupArtist];
    [self.user.profileTypes addProfileForType:CRProfileTypeHairStylist];
    
    NSArray *array = [self.user.profileTypes arrayOfStringsFromAbsentProfileTypes];
    
    XCTAssertEqual(array.count, (NSUInteger)1, @"Array must contain only one item since other 2 out of 3 profileTypes have already been added to storage");
    
    [self.user.profileTypes addProfileForType:CRProfileTypeClothingStylist];
    
    array = [self.user.profileTypes arrayOfStringsFromAbsentProfileTypes];
    XCTAssertNil(array, @"array must be nil since we've added all the possible profiles within the group");
}


@end
