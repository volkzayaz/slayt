//
//  CRSUerFilterTests.m
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CRUserFilter.h"
#import "OCMock.h"

@interface CRSUerFilterTests : XCTestCase

@property (nonatomic, strong) CRUserFilter* testObject;

@end

@implementation CRSUerFilterTests

- (void)setUp
{
    [super setUp];
    
    id userMock = [OCMockObject mockForClass:[CRUser class]];
    [[[userMock stub] andReturn:[NSMutableArray array]] filterSettings];
    
    self.testObject = [[CRUserFilter alloc] initWithUser:userMock];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDescriptionForKey
{

}

@end
