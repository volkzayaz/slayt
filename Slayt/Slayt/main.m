//
//  main.m
//  Talent
//
//  Created by Vlad Soroka on 4/1/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CRAppDelegate class]));
    }
}
