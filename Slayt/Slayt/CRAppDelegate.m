//
//  CRAppDelegate.m
//  Talent
//
//  Created by Vlad Soroka on 4/1/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRAppDelegate.h"
#import "CRUtils.h"
#import <HockeySDK/HockeySDK.h>
#import "GrabKit.h"
#import "CRGrabKitConfigurator.h"
#import "CRUser.h"
#import "CRFriendNotificationViewController.h"
#import "CRBarColors.h"
#import "UIColorFromRGB.h"
#import "SWRevealViewController.h"
#import "CRFriendsListViewController.h"
#import "CRFetchManager.h"
#import "CRFriendsManager.h"

@implementation CRAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window.backgroundColor = UIColorFromRGBHex(0x454958);
    [[CRUtils sharedInstance] setUpParse];
    [GRKConfiguration initializeWithConfigurator:[[CRGrabKitConfigurator alloc] init]];
    [CRBarColors setColors];

#ifndef DEBUG
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"444491357b3052124df474964dd9262e"];
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    [[[BITHockeyManager sharedHockeyManager] crashManager] setCrashManagerStatus:BITCrashManagerStatusAutoSend];
#endif
    
    
    
#if !TARGET_IPHONE_SIMULATOR
    // Register for push notifications
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
#endif

    if ([CRUser currentUser])
    {
        [[CRFetchManager sharedManager] fetchAllInfoInBackgroundForUser:[CRUser currentUser]];
        [[CRFriendsManager sharedInstance] reloadFriends];
        SWRevealViewController *revealVC = (SWRevealViewController *)self.window.rootViewController;
        if ([revealVC.rightViewController conformsToProtocol:@protocol(CRFriendsManagerDelegate)]) {
            [CRFriendsManager sharedInstance].delegate = (id<CRFriendsManagerDelegate>)[revealVC rightViewController];
        }
    }
    
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    if ([PFFacebookUtils session]) {
        [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
    } else {
        [FBAppCall handleDidBecomeActiveWithSession:[FBSession activeSession]];
    }
    
    [[CRUser currentUser].locationMonitor startMonitoring];
}

- (BOOL) application:(UIApplication *)application
             openURL:(NSURL *)url
   sourceApplication:(NSString *)sourceApplication
          annotation:(id)annotation
{
    BOOL urlHasBeenHandledByDispatcher = [[GRKConnectorsDispatcher sharedInstance] dispatchURLToConnectingServiceConnector:url];
    
    if ( urlHasBeenHandledByDispatcher  ) {
        return YES;
    } else {
        return [FBAppCall handleOpenURL:url
                      sourceApplication:sourceApplication
                            withSession:[PFFacebookUtils session]];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[FBSession activeSession] close];
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    if ([CRUser currentUser]) {
        [currentInstallation setObject:[CRUser currentUser].objectId forKey:@"user"];
    }
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"%@", error.description);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    BOOL canHandlePush = YES;
    if ([CRUser currentUser]) {
        if (canHandlePush) {
            NSString *friendId = [CRUser currentUser].objectId == userInfo[@"sourceProfileId"] ?
            userInfo[@"sourceProfileId"] : userInfo[@"destinationProfileId"];
            [CRFriendNotificationViewController showNotificationScreen:friendId];
            
            // TODO: Temporary solution
            if ([application.keyWindow.rootViewController isKindOfClass:[SWRevealViewController class]]) {
                SWRevealViewController *revealVC = (SWRevealViewController *)application.keyWindow.rootViewController;
                id rightVC = revealVC.rightViewController;
                if ([rightVC conformsToProtocol:@protocol(CRFriendsManagerDelegate)]) {
                    [CRFriendsManager sharedInstance].delegate = (id<CRFriendsManagerDelegate>)rightVC;
                    [[CRFriendsManager sharedInstance] reloadFriends];
                }
            }
        } else {
            [PFPush handlePush:userInfo];
        }
    }
}

@end
