//
//  CRAgenciesListViewController.m
//  Slayt
//
//  Created by Dmitry Utenkov on 6/4/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRAgenciesListViewController.h"
#import <MessageUI/MessageUI.h>
#import "CRUser.h"
#import <Parse/PF_MBProgressHUD.h>
#import "UIColorFromRGB.h"

// Custom class doesn't hide navigation bar on search
// http://stackoverflow.com/questions/2813118/prevent-a-uisearchdisplaycontroller-from-hiding-the-navigation-bar
@interface CustomSearchDisplayController : UISearchDisplayController

@end

@implementation CustomSearchDisplayController

- (void)setActive:(BOOL)visible animated:(BOOL)animated {
    [super setActive: visible animated: animated];
    [self.searchContentsController.navigationController setNavigationBarHidden: NO animated: NO];
}

@end

@interface CRAgenciesListViewController () <UISearchDisplayDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) NSArray *agencies;
@property (nonatomic, strong) NSArray *filteredAgencies;

@end

@implementation CRAgenciesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *agenciesListPath = [[NSBundle mainBundle] pathForResource:@"AgenciesList" ofType:@"csv"];
    NSError *error = nil;
    NSString *csvString = [NSString stringWithContentsOfFile:agenciesListPath
                                                    encoding:NSUTF8StringEncoding
                                                       error:&error];
    self.agencies = [csvString componentsSeparatedByString:@",\n"];
    
    UIColor *grayColor = [UIColor colorWithRed:89.0f/255.0f green:89.0f/255.0f blue:89.0f/255.0f alpha:1.0f];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont systemFontOfSize:14]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:grayColor];
}

#pragma mark - Actions

- (void)openAgencySubmissionForm {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        [mailViewController setToRecipients:@[@"support@slayt.co"]];
        [mailViewController setSubject:@"New Agency submission"];
        
        NSString *messageText = [NSString stringWithFormat:@"Dear Slayt,\n\nPlease submit my agency %@ to Slayt.\n\nBest Regards,\n%@", self.searchDisplayController.searchBar.text, [CRUser currentUser].fullName];
        [mailViewController setMessageBody:messageText isHTML:NO];
        mailViewController.mailComposeDelegate = self;
        mailViewController.navigationBar.tintColor = self.navigationController.navigationBar.barTintColor;
        [self presentViewController:mailViewController animated:YES completion:nil];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto://support@slayt.co"]];
    }
}

#pragma mark - UITableViewDataSource protocol implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return self.filteredAgencies.count == 0 ? 2 : self.filteredAgencies.count;
    } else {
        return self.agencies.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"AgencyCell"];
    
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:89.0f/255.0f blue:89.0f/255.0f alpha:1.0f];
    
    if (tableView == self.searchDisplayController.searchResultsTableView &&
        self.filteredAgencies.count == 0)
    {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"No search results";
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        } else if (indexPath.row == 1) {
            NSString *text = @"Add your agency to the list";
            NSDictionary *attrs = @{ NSFontAttributeName            : [UIFont boldSystemFontOfSize:16.0f],
                                     NSForegroundColorAttributeName : UIColorFromRGB(240, 133, 88),
                                     NSUnderlineStyleAttributeName  :  @(NSUnderlineStyleSingle) };
            cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:text attributes:attrs];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        }
    } else {
        NSArray *dataSourceArray = tableView == self.searchDisplayController.searchResultsTableView ? self.filteredAgencies : self.agencies;
        cell.textLabel.text = dataSourceArray[indexPath.row];
    }
    
    cell.selectionStyle = tableView == self.searchDisplayController.searchResultsTableView ? UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleGray;
    
    BOOL selectedCell = [cell.textLabel.text isEqualToString:self.selectedAgencyName];
    cell.accessoryType = selectedCell ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate protocol implementation

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchDisplayController.searchResultsTableView && self.filteredAgencies.count == 0) {
        if (indexPath.row == 1) {
            [[[UIAlertView alloc] initWithTitle:@"Slayt"
                                        message:@"Would you like to submit your agency to Slayt?"
                                       delegate:self
                              cancelButtonTitle:@"NO"
                              otherButtonTitles:@"YES", nil] show];
        }
        return nil;
    } else {
        self.selectedAgencyName = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
        return indexPath;
    }
}

#pragma mark - UISearchDisplayDelegate protocol implementation

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    NSMutableArray * matches = [NSMutableArray arrayWithArray:self.agencies];
    [matches filterUsingPredicate:[NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", searchString]];
    self.filteredAgencies = [NSArray arrayWithArray:matches];
    
    if (self.filteredAgencies.count == 0) {
        controller.searchResultsTableView.separatorColor = [UIColor clearColor];
        
    } else {
        controller.searchResultsTableView.separatorColor = nil;
    }
    self.searchDisplayController.searchResultsTableView.scrollEnabled = !(self.filteredAgencies.count == 0);
    
    return YES;
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller {
    self.searchDisplayController.searchResultsTableView.scrollEnabled = YES;
}

#pragma mark - UIAlertViewDelegate protocol implementaion

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        [self openAgencySubmissionForm];
    }
}

#pragma mark - MFMailComposerViewControllerDelegate protocol implementaion

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    void (^completion)(void) = nil;
    
    switch (result) {
        case MFMailComposeResultSent:
        case MFMailComposeResultSaved:
        {
            completion = ^{
                PF_MBProgressHUD *hud = [[PF_MBProgressHUD alloc] initWithView:self.view];
                [self.view addSubview:hud];
                hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark"]];
                hud.mode = PF_MBProgressHUDModeCustomView;
                NSString *text = MFMailComposeResultSaved ? @"Message saved" : @"Message sent";
                hud.labelText = text;
                [hud show:YES];
                [hud hide:YES afterDelay:2];
            };
            [self.searchDisplayController setActive:NO animated:NO];
        }
            break;
            
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:completion];
}

@end
