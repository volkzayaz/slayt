//
//  CRFriendListCellTableViewCell.h
//  Slayt
//
//  Created by Dmitry Utenkov on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRFriendListCellTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *friendImageView;

@property (nonatomic, weak) IBOutlet UILabel *friendNameLabel;

@property (nonatomic, weak) IBOutlet UILabel *friendLastMessageLabel;

@property (nonatomic, weak) IBOutlet UILabel *unreadMessagesNumberLabel;

@end
