//
//  CRValueSelectionTableViewController.m
//  Talent
//
//  Created by Vlad Soroka on 4/15/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRSingleSelectionController.h"
#import "CRUser.h"
#import "CRBaseEditController_Protected.h"

@interface CRSingleSelectionController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) UITableView* tableView;

@end

@implementation CRSingleSelectionController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect navigationBarFrame = self.navigationController.navigationBar.frame;
    CGRect tableViewFrame = self.view.frame;
    tableViewFrame.size.height -= navigationBarFrame.size.height + navigationBarFrame.origin.y;//we want tableView to occupy all the space between navigation bar and bottom of our view
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:tableViewFrame style:UITableViewStyleGrouped];
    
    self.tableView = tableView;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    id defaultValue = self.editingObject[self.propertyName];
    
    if(defaultValue)
    {
        self.chosenObject = defaultValue;
    }
    else
    {
        self.chosenObject = self.choiseOptions.firstObject;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.choiseOptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"ValueCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = self.choiseOptions[indexPath.row];
    cell.textLabel.textColor = [UIColor colorWithRed:0x59/255.f green:0x59/255.f blue:0x59/255.f alpha:1];
    
    if([self.chosenObject isEqual:self.choiseOptions[indexPath.row]])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.chosenObject = self.choiseOptions[indexPath.row];
    [self.tableView reloadData];
}

@end
