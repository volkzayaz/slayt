//
//  CREditStringController.h
//  Talent
//
//  Created by Vlad Soroka on 4/28/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRBaseEditValuesController.h"
#import "CRTextField.h"

@interface CREditStringController : CRBaseEditValuesController{
@protected CRTextField __weak * _textField;
}

@end
