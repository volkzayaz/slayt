//
//  CRBaseEditValuesController.m
//  Talent
//
//  Created by Vlad Soroka on 4/16/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRBaseEditValuesController.h"
#import "CRBaseEditController_Protected.h"
#import "CRUser.h"

@interface CRBaseEditValuesController ()

@property (nonatomic, assign) CRBaseEditValuesSaveType saveType;

@end

@implementation CRBaseEditValuesController

- (instancetype)initWithSaveType:(CRBaseEditValuesSaveType)type
{
    self = [super init];
    if(self)
    {
        self.saveType = type;
    }
    
    return self;
}

- (instancetype)init
{
    return [self initWithSaveType:CRBaseEditValuesSaveTypeDirect];
}

///uncomment following for allowing user to cancel his/her changes and leave the controller
/*
- (void)cancelClicked:(id)sender {
    [self preCancelAction];
    
    [self.navigationController popViewControllerAnimated:YES];
}
*/

- (void)doneClicked:(id)sender {
    
    [self preSavingAction];
    
    [self preSavingActionWithSavingBlock:^{//in case subclass decides to override only preSavingAction method this block will be invoked immediatelly
        NSAssert((self.editingObject&&self.propertyName) || self.saveType, @"Logic error. Either editingUser and propertyName or saveType must be provided before save action");
        
        if(!self.chosenObject)
        {
            //if you really need to store nil value you can use [NSNull null] object
            NSLog(@"WARNING: an attempt to save nil object to %@ property. No actions have been done",self.propertyName);
        }
        else
        {
            if(self.saveType == CRBaseEditValuesSaveTypeDelegate)
            {
                if([self.delegate respondsToSelector:@selector(editValueController:didEditValue:)])
                {
                    [self.delegate editValueController:self didEditValue:self.chosenObject];
                }
            }
            else if (self.saveType == CRBaseEditValuesSaveTypeDirect)
            {
                self.editingObject[self.propertyName] = self.chosenObject;
            }
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.chosenObject = self.editingObject[self.propertyName];
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_photo_back_active"] style:UIBarButtonItemStyleBordered target:self action:@selector(doneClicked:)];
    backItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = backItem;
    
    //Uncomment the following to allow cancel changes button.
    /*
    UIBarButtonItem* cancelItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(cancelClicked:)];
    doneItem.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = cancelItem;
    */
    
    self.view.backgroundColor = [UIColor colorWithRed:201.f/255.f green:203.f/255.f blue:212.f/255.f alpha:1.f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)preSavingAction
{ ; }

- (void)preSavingActionWithSavingBlock:(SavingBlock)block
{
    block();
}

- (void)preCancelAction
{ ; }

@end
