//
//  CRBaseEditValuesController.h
//  Talent
//
//  Created by Vlad Soroka on 4/16/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRUser.h"


/**
 *  You can edit values in two ways:
 *  1) You can receive the delegate callback with edited value, and save it in your specific way. To use it just pass
 *  CRBaseEditValuesSaveTypeDelegate into init mehtod of CRBaseEditValuesController or it's subsclass and set delegate property.
 *
 *  2) You can choose direct save action. To do so, you need to pass CRBaseEditValuesSaveTypeDirect into init method and provide class with
 *  editingUser and propertyName fields. If the user press "done" button user[propertyName] = chosenValue will be called with all 
 *  appropriate checks.
 */
typedef NS_ENUM(NSUInteger, CRBaseEditValuesSaveType){
    CRBaseEditValuesSaveTypeDelegate=1,
    CRBaseEditValuesSaveTypeDirect,
};

@class CRBaseEditValuesController;

@protocol CRBaseEditValuesProtocol <NSObject>
@required
- (void)editValueController:(CRBaseEditValuesController*)controller didEditValue:(id) value;

@end

@interface CRBaseEditValuesController : UIViewController

- (instancetype)initWithSaveType:(CRBaseEditValuesSaveType)type;

@property (nonatomic, assign) id<CRBaseEditValuesProtocol> delegate;

@property (nonatomic, strong) NSString* propertyName;
@property (nonatomic, strong) PFObject* editingObject;

//used for single selection from these options.
@property (nonatomic, strong) NSArray* choiseOptions;

/*
 *  Array corresponding to UserDetailsList.plist
 *
 */
@property (nonatomic, strong) NSArray* entityProperties;

/*
 *  You can use this object to retrieve any additional information about currently editing property. All info is stored in entityProperties array.
 */
@property (nonatomic, strong) NSIndexPath* propertyIndexPath;

@end
