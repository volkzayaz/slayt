//
//  CREditOrderedValueController.m
//  Talent
//
//  Created by Vlad Soroka on 4/18/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CREditOrderedValueController.h"
#import "CRBaseEditController_Protected.h"

#import "CRUtils.h"

@interface CREditOrderedValueController ()

@end

@implementation CREditOrderedValueController

- (void)viewDidLoad
{
    self.choiseOptions = [(CRUser*)self.editingObject stringsForPrefixedPropertyName:self.propertyName];
    
    [super viewDidLoad];
}

- (void)preSavingAction
{
    if([self.chosenObject isKindOfClass:[NSString class]])
    {
        NSNumber* number = @([self.choiseOptions indexOfObject:self.chosenObject]);
        
        if(!number)
        {
            NSLog(@"Error occured during save action of %@ property. Selected object is not contained in choiseOptions list.", self.propertyName);
        }
        
        self.chosenObject = number;
    }
    else
    {
        NSLog(@"Something is wrong. At the end of selecting single option of ordered property %@ a non sting was put to the chosenObject.",self.propertyName);
    }
}

@end
