//
//  CREditTextController.m
//  Slayt
//
//  Created by Vlad Soroka on 5/23/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CREditTextController.h"
#import "CRBaseEditController_Protected.h"

@interface CREditTextController ()

@property (nonatomic, strong) UITextView* textView;

@end

@implementation CREditTextController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITextView* tw = [[UITextView alloc] initWithFrame:CGRectMake(0, 15, self.view.frame.size.width, 240)];
    tw.backgroundColor = [UIColor whiteColor];
    
    self.textView = tw;
    [self.view addSubview:tw];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString* value = self.editingObject[self.propertyName];
    
    if(value){
        self.textView.text = value;
    }
    else
    {
        self.textView.text = @"Leave a couple of sentences about your project here.";
        [self.textView selectAll:self];
    }
    
    [self.textView becomeFirstResponder];
}

- (void)preSavingAction
{
    NSString* text = self.textView.text;
    if(text.length > 0)
    {
        self.chosenObject = text;
    }
    else
    {
        self.chosenObject = [NSNull null];
    }
}


@end
