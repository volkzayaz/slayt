//
//  CRMultipleSelectionController.m
//  Talent
//
//  Created by Vlad Soroka on 4/16/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRMultipleSelectionController.h"
#import "CRUser.h"
#import "CRBaseEditController_Protected.h"

@interface CRMultipleSelectionController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) UITableView* tableView;
@property (nonatomic, strong) NSMutableArray* chosenObjects;

@end

@implementation CRMultipleSelectionController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect navigationBarFrame = self.navigationController.navigationBar.frame;
    CGRect tableViewFrame = self.view.frame;
    tableViewFrame.size.height -= navigationBarFrame.size.height + navigationBarFrame.origin.y;//we want tableView to occupy all the space between navigation bar and bottom of our view
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:tableViewFrame style:UITableViewStyleGrouped];
    
    self.tableView = tableView;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.chosenObjects = self.chosenObject ? [NSMutableArray arrayWithArray:(NSArray*)self.chosenObject] : [NSMutableArray array];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.choiseOptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"ValueCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = self.choiseOptions[indexPath.row];
    cell.textLabel.textColor = [UIColor colorWithRed:0x59/255.f green:0x59/255.f blue:0x59/255.f alpha:1];
    
    if([self.chosenObjects containsObject:self.choiseOptions[indexPath.row]])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.chosenObjects containsObject:self.choiseOptions[indexPath.row]])
    {
        [self.chosenObjects removeObject:self.choiseOptions[indexPath.row]];
    }
    else
    {
        [self.chosenObjects addObject:self.choiseOptions[indexPath.row]];
    }
    

    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)preSavingAction
{
    if(self.chosenObjects.count == 0)
    {
        self.chosenObject = [NSNull null];
        return;
    }
    
    self.chosenObject = [self.chosenObjects copy];
}

@end
