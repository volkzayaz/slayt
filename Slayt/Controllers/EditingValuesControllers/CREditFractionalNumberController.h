//
//  CREditFractionalNumberController.h
//  Slayt
//
//  Created by Vlad Soroka on 5/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRBaseEditValuesController.h"

/**
 *  @discussion You want to use this class when you deal with non metric measure units and would like to store them in NSNumber. For example you may want to edit Feet/inches number. The value 6' 11'' will be stored as 6.11 decimal number;
 */
@interface CREditFractionalNumberController : CRBaseEditValuesController

@end
