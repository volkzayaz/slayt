//
//  CREditInchesControllerViewController.m
//  Slayt
//
//  Created by Vlad Soroka on 5/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CREditInchesController.h"

@interface CREditInchesController ()

@end

@implementation CREditInchesController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString* displayPropertyName = self.entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"displayName"];
    
    self->_textField.placeholder = [NSString stringWithFormat:@"%@ in inches",displayPropertyName];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
