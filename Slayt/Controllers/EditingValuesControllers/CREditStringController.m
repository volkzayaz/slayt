//
//  CREditStringController.m
//  Talent
//
//  Created by Vlad Soroka on 4/28/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CREditStringController.h"
#import "CRBaseEditController_Protected.h"

@interface CREditStringController ()

@property (nonatomic, weak) CRTextField* textField;

@end

@implementation CREditStringController

@synthesize textField = _textField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CRTextField* tf = [[CRTextField alloc] initWithFrame:CGRectMake(0, 15, self.view.frame.size.width, 40)];
    tf.backgroundColor = [UIColor whiteColor];
    tf.placeholder = @"Not Specified";
    
    NSString* value = self.editingObject[self.propertyName];
    
    if(value){
        tf.text = value;
    }
    
    self.textField = tf;
    [self.view addSubview:tf];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.textField becomeFirstResponder];
    
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)preSavingAction
{
    NSString* text = self.textField.text;
    if(text.length > 0)
    {
        self.chosenObject = text;
    }
    else
    {
        self.chosenObject = [NSNull null];
    }
}

@end
