//
//  CREditPayRateController.h
//  Talent
//
//  Created by Vlad Soroka on 4/17/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRBaseEditValuesController.h"

//we'll work only with payRate property of user. PaymentFrequency property will be passed further for editing
@interface CREditPayRateController : CRBaseEditValuesController//not designed for reusing by other properties

@end
