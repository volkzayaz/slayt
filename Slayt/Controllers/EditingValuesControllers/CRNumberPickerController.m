//
//  CRNumberPickerController.m
//  Talent
//
//  Created by Vlad Soroka on 4/17/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRNumberPickerController.h"
#import "CRBaseEditController_Protected.h"
#import "CRUtils.h"

@interface CRNumberPickerController () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, weak) UIPickerView* picker;

@property (nonatomic, strong) NSNumber* beginsWithNumber;
@property (nonatomic, strong) NSNumber* endsWithNumers;
@property (nonatomic, strong) NSNumber* step;

@end

@implementation CRNumberPickerController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIPickerView* picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    picker.delegate = self;
    picker.dataSource = self;
    
    self.picker = picker;
    [self.view addSubview:picker];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSArray* entityProperties = self.entityProperties;
    
    self.beginsWithNumber = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"beginsWith"];
    self.endsWithNumers = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"endsWith"];
    self.step = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"step"];
    
    NSNumber* currentNumber = self.editingObject[self.propertyName];
    
    if(currentNumber)
    {
        [self.picker selectRow:(currentNumber.floatValue - self.beginsWithNumber.floatValue)/self.step.floatValue
                   inComponent:0
                      animated:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)preSavingAction
{
    NSInteger row = [self.picker selectedRowInComponent:0];
    
    self.chosenObject = @(self.beginsWithNumber.floatValue + (row * self.step.floatValue));
}


#pragma picker delegate/datasource

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger value = (self.endsWithNumers.floatValue - self.beginsWithNumber.floatValue)/self.step.floatValue;
    
    value ++;//if we got N trees in a row and a bench between each two trees we've got N-1 benches, but we'd like to have N of them.
    
    return value;
}

- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return @(self.beginsWithNumber.floatValue + (row * self.step.floatValue)).stringValue;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSNumber* selectedNumber = @(self.beginsWithNumber.floatValue + (row * self.step.floatValue));
    
    self.chosenObject = selectedNumber;
}

@end
