//
//  CREditOrderedValueController.h
//  Talent
//
//  Created by Vlad Soroka on 4/18/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRBaseEditValuesController.h"
#import "CRSingleSelectionController.h"

@interface CREditOrderedValueController : CRSingleSelectionController

@end
