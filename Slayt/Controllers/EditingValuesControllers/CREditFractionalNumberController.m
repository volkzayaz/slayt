//
//  CREditFractionalNumberController.m
//  Slayt
//
//  Created by Vlad Soroka on 5/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CREditFractionalNumberController.h"
#import "CRBaseEditController_Protected.h"
#import "NSNumber+fractions.h"

#define INTEGER_PICKER_WIDTH 140
#define FRACTION_PICKER_WIDTH 180

@interface CREditFractionalNumberController () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, weak) UIPickerView* integerPicker;
@property (nonatomic, strong) NSNumber* beginsWithInt;
@property (nonatomic, strong) NSNumber* endsWithInt;
@property (nonatomic, strong) NSNumber* stepInt;
@property (nonatomic, strong) NSNumber* defaultInt;
@property (nonatomic, strong) NSString* unitsInteger;

@property (nonatomic, weak) UIPickerView* fractionPicker;
@property (nonatomic, strong) NSNumber* beginsWithFract;
@property (nonatomic, strong) NSNumber* endsWithFract;
@property (nonatomic, strong) NSNumber* stepFract;
@property (nonatomic, strong) NSNumber* defaultFract;
@property (nonatomic, strong) NSString* unitsFraction;

@end

@implementation CREditFractionalNumberController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIPickerView* pickerI = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 25, INTEGER_PICKER_WIDTH, 200)];
    pickerI.delegate = self;
    pickerI.dataSource = self;
    self.integerPicker = pickerI;
    pickerI.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pickerI];
    
    UIPickerView* pickerF = [[UIPickerView alloc] initWithFrame:CGRectMake(INTEGER_PICKER_WIDTH, 25, FRACTION_PICKER_WIDTH, 200)];
    pickerF.delegate = self;
    pickerF.dataSource = self;
    self.fractionPicker = pickerF;
    pickerF.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pickerF];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSArray* entityProperties = self.entityProperties;
    
    self.beginsWithInt = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"IntegerPart"][@"beginsWith"];
    self.endsWithInt = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"IntegerPart"][@"endsWith"];
    self.stepInt = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"IntegerPart"][@"step"];
    self.defaultInt = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"IntegerPart"][@"defaultValue"];
    self.unitsInteger = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"IntegerPart"][@"unit"];
    
    self.beginsWithFract = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"FractionPart"][@"beginsWith"];
    self.endsWithFract = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"FractionPart"][@"endsWith"];
    self.stepFract = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"FractionPart"][@"step"];
    self.defaultFract = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"FractionPart"][@"defaultValue"];
    self.unitsFraction = entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"FractionPart"][@"unit"];

    
    NSNumber* currentNumber = self.editingObject[self.propertyName];
    
    float integerPart = self.defaultInt.floatValue;
    float fractionalPart = self.defaultFract.floatValue;
    
    if(currentNumber)
    {
        integerPart = currentNumber.integerPart;
        fractionalPart = currentNumber.fractionPart;
    }
    
    [self.integerPicker selectRow:(integerPart - self.beginsWithInt.floatValue)/self.stepInt.floatValue
                      inComponent:0
                         animated:NO];
    
    [self.fractionPicker selectRow:(fractionalPart - self.beginsWithFract.floatValue)/self.stepFract.floatValue
                       inComponent:0
                          animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) preSavingAction
{
    //accessing selected values
    NSInteger selectedInteger = self.beginsWithInt.integerValue + ([self.integerPicker selectedRowInComponent:0] * self.stepInt.integerValue);
    float selectedFraction = self.beginsWithFract.floatValue + ([self.fractionPicker selectedRowInComponent:0] * self.stepFract.floatValue);
    
    ///presenting chosen number as [integerPart].[fractionalPart]
    float result = selectedInteger;
    result += selectedFraction / pow(10, DIGITS_AFTER_COMA);
    
    self.chosenObject = [NSNumber numberWithFloat:result];
}

#pragma picker delegate/datasource

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 1)
    {
        return 1;//this row is where measure unit will be displayed
    }
    
    NSInteger numberOfRows = 0;
    
    if(pickerView == self.integerPicker)
    {
        numberOfRows = (self.endsWithInt.floatValue - self.beginsWithInt.floatValue)/self.stepInt.floatValue;
        numberOfRows++;//if we got N trees in a row and a bench between each two trees we've got N-1 benches, but we'd like to have N of them.
    }
    else if (pickerView == self.fractionPicker)
    {
        numberOfRows = (self.endsWithFract.floatValue - self.beginsWithFract.floatValue)/self.stepFract.floatValue;
        numberOfRows++;//if we got N trees in a row and a bench between each two trees we've got N-1 benches, but we'd like to have N of them.
    }
    
    return numberOfRows;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    CGFloat sizeForSecondRow = 35;
    
    if(pickerView == self.integerPicker)
    {
        if(component == 0)
        {
            return INTEGER_PICKER_WIDTH - sizeForSecondRow;
        }
        else
        {
            return sizeForSecondRow;
        }
    }
    else
    {
        if(component == 0)
        {
            return 55 - sizeForSecondRow;
        }
        else
        {
            return sizeForSecondRow;
        }
    }
}

- (UIView*) pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UILabel *)view
{
    UILabel* label = nil;
    
    CGSize rowSize = [pickerView rowSizeForComponent:component];

    label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,rowSize.width,rowSize.height)];
    label.adjustsFontSizeToFitWidth = YES;
    
    if(pickerView == self.integerPicker)
    {
        if(component == 0)
        {
            label.textAlignment = NSTextAlignmentRight;
            label.text = @(self.beginsWithInt.floatValue + (row * self.stepInt.floatValue)).stringValue;
        }
        else if(component == 1)
        {
            label.textAlignment = NSTextAlignmentLeft;
            label.text = [NSString stringWithFormat:@"   %@",self.unitsInteger];
        }
    }
    else if (pickerView == self.fractionPicker)
    {
        if(component == 0)
        {
            label.textAlignment = NSTextAlignmentRight;
            label.text = @(self.beginsWithFract.floatValue + (row * self.stepFract.floatValue)).stringValue;
        }
        else if(component == 1)
        {
            label.textAlignment = NSTextAlignmentLeft;
            label.text = [NSString stringWithFormat:@"   %@",self.unitsFraction];
        }
    }
    
    return label;
}

@end
