//
//  CRValueSelectionTableViewController.h
//  Talent
//
//  Created by Vlad Soroka on 4/15/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRBaseEditValuesController.h"

@interface CRSingleSelectionController : CRBaseEditValuesController
    
@end
