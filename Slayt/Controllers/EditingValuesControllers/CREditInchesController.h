//
//  CREditInchesControllerViewController.h
//  Slayt
//
//  Created by Vlad Soroka on 5/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CREditNumberController.h"

@interface CREditInchesController : CREditNumberController

@end
