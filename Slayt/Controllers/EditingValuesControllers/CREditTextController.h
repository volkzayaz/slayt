//
//  CREditTextController.h
//  Slayt
//
//  Created by Vlad Soroka on 5/23/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRBaseEditValuesController.h"

@interface CREditTextController : CRBaseEditValuesController

@end
