//
//  CRBaseEditController_CRBaseEditController_Protected.h
//  Talent
//
//  Created by Vlad Soroka on 4/16/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRBaseEditValuesController.h"

typedef void(^SavingBlock)();

@interface CRBaseEditValuesController ()
/*
 *  Value of this object will be stored to the user's property.
 *  @seealso propertyName
 */
@property (nonatomic, strong) id<NSObject> chosenObject;

/*
 *  Method called by CRBaseEditValuesController right before save action. It's your last chance to modify chosenObject or any other 
 *  properties before save action.
 */
- (void)preSavingAction;
- (void)preSavingActionWithSavingBlock:(SavingBlock)block;
- (void)preCancelAction;
@end
