//
//  CREditPayRateController.m
//  Talent
//
//  Created by Vlad Soroka on 4/17/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CREditPayRateController.h"
#import "CRRateCell.h"
#import "CRBaseEditController_Protected.h"
#import "CREditOrderedValueController.h"

@interface CREditPayRateController ()<UITableViewDataSource, UITableViewDelegate, CRRateCellDelegate>

@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, weak) CRRateCell* cell;

@property (nonatomic, assign) CRPaymentFrequency savedPaymentFrequency;
@property (nonatomic, assign) CRPaymentFrequency currentPaymentFrequncy;

@property (nonatomic, strong) NSString* paymentFrequencyPropertyName;
@end

@implementation CREditPayRateController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];

    self.paymentFrequencyPropertyName = self.entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"numberPropertyName"];
    
    //we'll store payment frequency once before making any changes to PayRate. And in case user canceles it, we'll restore paymentFrequency property using this value
    self.savedPaymentFrequency = (CRPaymentFrequency)(((NSNumber*)[self.editingObject valueForKey:self.paymentFrequencyPropertyName]).unsignedIntegerValue);
}

- (void)viewWillAppear:(BOOL)animated
{
    self.currentPaymentFrequncy = (CRPaymentFrequency)(((NSNumber*)[self.editingObject valueForKey:self.paymentFrequencyPropertyName]).unsignedIntegerValue);
    
    if (self.currentPaymentFrequncy == CRPaymentFrequencyWorkForFree) {
        self.chosenObject = @(0);
    }
    else
    {
        self.chosenObject = self.editingObject[self.propertyName];
    }
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.currentPaymentFrequncy == CRPaymentFrequencyWorkForFree ||
       self.currentPaymentFrequncy == CRPaymentFrequencyNotSpecified)
    {
        return 1;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)//paymentType
    {
        UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"PaymentCell"];
        cell.textLabel.text = @"Payment";
        cell.textLabel.textColor = [UIColor colorWithRed:0x80/255.f green:0x7f/255.f blue:0xb0/255.f alpha:1];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        NSString* paymentType = self.editingObject[self.paymentFrequencyPropertyName];
        
        cell.detailTextLabel.text = !paymentType ? @"Not Specified" : paymentType;
        
        return cell;
    }
    else if(indexPath.section == 1)//Rate
    {
        CRRateCell* cell = [[CRRateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RateCell"];
        cell.delegate = self;

        NSString* paymentType = self.editingObject[self.paymentFrequencyPropertyName];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ Rate", !paymentType ? @"" : paymentType];

        cell.field.text = ((NSNumber*)self.editingObject[self.propertyName]).stringValue;
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        self.cell = cell;
        
        return cell;
    }
    else
    {
        return nil;
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section != 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        CRBaseEditValuesController* editController = [[CREditOrderedValueController alloc] initWithSaveType:CRBaseEditValuesSaveTypeDirect];

        editController.propertyName = self.paymentFrequencyPropertyName;
        editController.editingObject = self.editingObject;
        
        editController.navigationItem.title = @"Payment";
        [self.navigationController pushViewController:editController animated:YES];
    }
}

- (void)didEnterValue:(NSString *)value inRateCell:(CRRateCell *)cell
{
    NSNumber* number = @(value.integerValue);
    if(!number)
    {
        number = @0;
    }
    
    self.chosenObject = number;
}

- (void)preCancelAction
{
    self.editingObject[self.paymentFrequencyPropertyName] = @(self.savedPaymentFrequency);
}

@end
