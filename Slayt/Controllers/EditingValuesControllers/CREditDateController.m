//
//  CREditDateController.m
//  Talent
//
//  Created by Vlad Soroka on 4/17/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CREditDateController.h"
#import "CRBaseEditController_Protected.h"

@interface CREditDateController ()

@property (nonatomic, weak) UIDatePicker* datePicker;

@end

@implementation CREditDateController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILabel* datePickerTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width, 30)];
    
    NSString* pickerTitle = self.entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"hintText"];
    
    NSDate* fromDate = self.entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"dateRangeFrom"];
    NSDate* toDate = self.entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"dateRangeTo"];
    
    if(!fromDate)
    {
        fromDate = [NSDate date];
    }
    
    if(!toDate)
    {
        toDate = [NSDate date];
    }
    
    datePickerTitle.text = pickerTitle;
    datePickerTitle.textColor = [UIColor colorWithRed:120./255.f green:119./255.f blue:172./255.f alpha:1.];

    [self.view addSubview:datePickerTitle];
    
    UIDatePicker* dp = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 100)];
    dp.datePickerMode = UIDatePickerModeDate;
    NSDate* bd = self.editingObject[self.propertyName];
    [dp setDate:bd?bd:[NSDate date]];
    dp.maximumDate = toDate;
    dp.minimumDate = fromDate;
    dp.backgroundColor = [UIColor whiteColor];
    
    NSNumber* showTime = self.entityProperties[self.propertyIndexPath.section][@"properties"][self.propertyIndexPath.row][@"showTime"];
    
    if([showTime boolValue])
    {
        dp.datePickerMode = UIDatePickerModeDateAndTime;
    }
    
    self.datePicker = dp;
    [self.view addSubview:dp];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)preSavingAction
{
    self.chosenObject = self.datePicker.date;
}

@end
