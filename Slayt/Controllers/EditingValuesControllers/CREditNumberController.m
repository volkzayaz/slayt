//
//  CREditNumberController.m
//  Talent
//
//  Created by Vlad Soroka on 4/17/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CREditNumberController.h"
#import "CRTextField.h"
#import "CRBaseEditController_Protected.h"

@interface CREditNumberController ()

@property (nonatomic, weak) CRTextField* textField;

@end

@implementation CREditNumberController

@synthesize textField = _textField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CRTextField* textField = [[CRTextField alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 45)];
    textField.keyboardType = UIKeyboardTypeDecimalPad;
    textField.backgroundColor = [UIColor whiteColor];
    textField.placeholder = @"Not Specified";
    
    NSNumber* value = self.editingObject[self.propertyName];
    
    if(value)
    {
        textField.text = value.stringValue;
    }
    
    self.textField = textField;
    [self.view addSubview:textField];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.textField becomeFirstResponder];
    
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)preSavingAction
{
    NSString* enteredText = self.textField.text;
    if([enteredText isEqualToString:@""])
    {
        self.chosenObject = [NSNull null];
        return;
    }
    
    NSString* locilizedNumberString = [enteredText stringByReplacingOccurrencesOfString:[[NSLocale currentLocale] objectForKey: NSLocaleDecimalSeparator] withString:@"."];
    
    NSNumber* number = @(locilizedNumberString.floatValue);
        
    self.chosenObject = number;
}

@end
