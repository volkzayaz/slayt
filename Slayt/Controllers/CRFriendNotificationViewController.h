//
//  CRFriendNotificationViewController.h
//  Slayt
//
//  Created by Dmitry Utenkov on 5/27/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRFriendNotificationViewController : UIViewController

+ (void)showNotificationScreen:(NSString *)friendId;

@end
