//
//  CREditProfileController.m
//  Talent
//
//  Created by Vlad Soroka on 4/2/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CREditProfileController.h"

#import "CREditSpecificProfileViewController.h"
#import "CRChooseLocationController.h"
#import "CREditStringController.h"
#import "CRMultipleSelectionController.h"

#import "CRUser.h"
#import "CRPhoto.h"

#import "CRUtils.h"

#import "iCarousel.h"
#import "CRCarouselPhotoView.h"
#import <Parse/PF_MBProgressHUD.h>

#import "CRRoleTypeCell.h"

#import "NSMutableArray+CRPhotos.h"
#import "GrabKit.h"
#import "GRKPickerViewController.h"
#import "GRKPickerAlbumsList.h"
#import "CRFacebookGrabKitReceiver.h"

#import "CRBarColors.h"
#import "UIColorFromRGB.h"

#define NAME_ROW_INDEX 0
#define LOCATION_ROW_INDEX 1

@interface CREditProfileController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate, CRPhotoViewDelegate, SWTableViewCellDelegate, iCarouselDataSource, iCarouselDelegate, CRBaseEditValuesProtocol, FacebookReceiverDelegate, UINavigationControllerDelegate>

- (IBAction)cancelEditing:(id)sender;
- (IBAction)doneEditing:(id)sender;

@property (weak, nonatomic) IBOutlet iCarousel *photoCarousel;
@property (nonatomic, strong) PF_MBProgressHUD* HUD;

@property (nonatomic, strong) NSMutableArray* photos;
@property (nonatomic, strong) NSMutableArray* photosToDelete;//photos that were deleted will be removed from pasrse only on Done button clicked
@property (nonatomic, strong) NSArray* progressImagesArray;

@property (nonatomic, strong) CRUser* editingUser;
@property (nonatomic, strong) NSArray* profileTypes;

//is designed for observing purpose.
@property (nonatomic, assign) BOOL isRequestingPhotos;

@property (nonatomic, strong) CRPhoto *currentMainPhoto;

@end

@implementation CREditProfileController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if(self)
    {
        [self designatedInitializer];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        [self designatedInitializer];
    }
    return self;
}

- (void)designatedInitializer
{
    self.photosToDelete = [NSMutableArray array];
    
    self.isRequestingPhotos = YES;//prefarbly it ought to be the only place where isRequestingPhotos sets to YES.

    [[CRUser currentUser] createSnapshot];
    self.editingUser = [CRUser currentUser];
    
    self.profileTypes = [self.editingUser.profileTypes profileTypesArray];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError* error = nil;
        NSArray* photos = [self.editingUser requestPhotosWithError:&error];
        
        if(!error)
        {
            self.photos = [photos mutableCopy];
        }
        else
        {
            NSLog(@"Error on requesting user photos");
        }
        
        self.isRequestingPhotos = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            if(self.isViewLoaded && self.view.window)//is ViewController currently visible
            {
                [self.photoCarousel reloadData];
                NSUInteger centralItem = 0;
                if([self.photoCarousel numberOfItems] > 1)
                {
                    centralItem = 1;
                }
                [self.photoCarousel scrollToItemAtIndex:centralItem animated:NO];
                [self.tableView reloadData];
            }});
    });
}

- (NSArray*)progressImagesArray
{
    if(!_progressImagesArray)
    {
        _progressImagesArray = @[[UIImage imageNamed:@"loader1"],
                                 [UIImage imageNamed:@"loader2"],
                                 [UIImage imageNamed:@"loader3"],
                                 [UIImage imageNamed:@"loader4"],
                                 [UIImage imageNamed:@"loader5"],
                                 [UIImage imageNamed:@"loader6"]];
    }
    
    return _progressImagesArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.HUD = [[PF_MBProgressHUD alloc] initWithView:self.view];
    self.HUD.userInteractionEnabled = YES;
    [self.navigationController.view addSubview:self.HUD];
    
    self.photoCarousel.type = iCarouselTypeLinear;
    self.photoCarousel.bounceDistance = 0.3f;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Helvetica Neue-Regular" size:18],
      NSFontAttributeName, nil]];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
    NSUInteger centralItem = 0;
    if([self.photoCarousel numberOfItems] > 1)
    {
        centralItem = 1;
    }
    [self.photoCarousel scrollToItemAtIndex:centralItem animated:NO];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    self.progressImagesArray = nil;
}

- (IBAction)cancelEditing:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Are you sure you want to go away without saving changes?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"YES", nil] show];
}

- (IBAction)doneEditing:(id)sender {
    
    self.HUD.labelText = @"Applying changes";
    [self.HUD show:YES];
    
    NSUInteger indexOfMainPhoto = [self.photos indexOfMainPhoto];
    NSDictionary* userInfo = @{};
    if(indexOfMainPhoto != NSNotFound)
    {
        CRPhoto* mainPhoto = self.photos[indexOfMainPhoto];
        userInfo = @{@"mainPhoto": mainPhoto};
    }
    
    //we are relying on assumption that EditController is the only place you can change your photos as well as any other information about your profile.
    
    
    //savePhotos
    dispatch_queue_t queue = dispatch_queue_create("com.slayt.performEditingQueue", DISPATCH_QUEUE_CONCURRENT);
    
    for(CRPhoto* photo in self.photos)
    {
        dispatch_async(queue, ^{
            [photo save];
        });
    }

    for (CRPhoto* photoToDelete in self.photosToDelete)
    {
        dispatch_async(queue, ^{
            [photoToDelete delete];
        });
    }
    
    CRProfileTypeStorage* storage = self.editingUser.profileTypes;
    
    dispatch_async(queue, ^{
        [storage saveAll];
    });
    
    CRUser* user = self.editingUser;
    dispatch_async(queue, ^{
        [user save];
    });
    
    dispatch_barrier_async(queue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.HUD hide:YES];
            
            [self.navigationController popViewControllerAnimated:YES];
        });
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidUpdateMainPhotoNotification
                                                            object:nil
                                                          userInfo:nil];
    });
}

#pragma mark - tableView dataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section != 0)
    {
        return 25;
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == tableView.numberOfSections - 1)
    {
        NSInteger numberOfRows = [self.editingUser.profileTypes.profileTypesArray count];
        
        BOOL isUnaddedPorfileExist = self.editingUser.profileTypes.arrayOfStringsFromAbsentProfileTypes.count > 0;
        if(isUnaddedPorfileExist)
        {
            numberOfRows += 1;//the "add new profile" row
        }
        
        return numberOfRows;
    }
    else
    {
        //name and location
        return 2;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == tableView.numberOfSections - 1 &&//the last section of tableView
       indexPath.row != self.editingUser.profileTypes.profileTypesArray.count)//the row index is within acceptable range
    {
        CRRoleTypeCell* cell = [tableView dequeueReusableCellWithIdentifier:@"roleTypeCell" forIndexPath:indexPath];
        
        PFObject<CRProfileProtocol>* profile = self.profileTypes[indexPath.row];
        
        cell.textLabel.text = profile.profileName;
        
        BOOL hideForSearch = [profile[isHidingForSearchKey] boolValue];
        NSString* showForSearchTitle = hideForSearch ? @"Show for\n search" : @"Hide for\n search";
        cell.rightUtilityButtons = [self rightButtonsWithFirstButtonTitle:showForSearchTitle];
        cell.showProfileType = !hideForSearch;
        
        cell.delegate = self;
        
        return cell;
    }
    
    static NSString *CellIdentifier = @"editProfileCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    CRUser* user = self.editingUser;
    
    if(indexPath.section == 0)
    {
        if(indexPath.row == NAME_ROW_INDEX)
        {
            cell.textLabel.text = @"Name";
            cell.detailTextLabel.text = [user.fullName isEqualToString:@""] || !user.fullName ? @"Not Specified" : user.fullName;
        }
        else if(indexPath.row == LOCATION_ROW_INDEX)
        {
            cell.textLabel.text = @"Location";
            cell.detailTextLabel.text = [user.homeTown isEqualToString:@""] || !user.homeTown ? @"Not specified" : user.homeTown;
        }
    }
    else
    {
        if(indexPath.row == self.editingUser.profileTypes.profileTypesArray.count)//the row which comes right after all profiles that were added
        {
            cell.textLabel.textColor = [UIColor colorWithRed:240./255.f green:113./255.f blue:88./255.f alpha:1.];
            cell.textLabel.text = @"Add another profile type";
            cell.detailTextLabel.text = @"";
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate protocol implementation 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(indexPath.section == 0)
    {
        if(indexPath.row == NAME_ROW_INDEX)
        {
            CREditStringController* controller = [[CREditStringController alloc] init];
            
            controller.propertyName = @"fullName";
            controller.editingObject = self.editingUser;
            
            [self.navigationController pushViewController:controller animated:YES];
            
            controller.navigationItem.title = @"Name";
        }
        else if (indexPath.row == LOCATION_ROW_INDEX)
        {
            CRChooseLocationController* editLocation = [[CRChooseLocationController alloc] init];
            
            editLocation.editingObject = self.editingUser;
            editLocation.propertyName = @"homeTown";
            editLocation.initialLocation = self.editingUser.homeTown;
            editLocation.navigationItem.title = @"Location";
            
            [self.navigationController pushViewController:editLocation animated:YES];
        }
    }
    else if (indexPath.section == tableView.numberOfSections - 1)
    {
        if(indexPath.row == self.editingUser.profileTypes.profileTypesArray.count)
        {
            CRBaseEditValuesController* controller = [[CRMultipleSelectionController alloc] initWithSaveType:CRBaseEditValuesSaveTypeDelegate];
            controller.delegate = self;
            controller.choiseOptions = [self.editingUser.profileTypes arrayOfStringsFromAbsentProfileTypes];
            
            [self.navigationController pushViewController:controller animated:YES];
            controller.navigationItem.title = @"Add profile type";
        }
        else if (indexPath.row < self.editingUser.profileTypes.profileTypesArray.count)
        {
            [self performSegueWithIdentifier:@"EditProfileSegue" sender:cell];
        }
        else
        {
            //this index path selection is not designed to do anything
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"EditProfileSegue"]) {
        CREditSpecificProfileViewController* vc = segue.destinationViewController;
        NSIndexPath* selectedIndexPath = [self.tableView indexPathForCell:(UITableViewCell*)sender];
        PFObject<CRProfileProtocol>* profile = self.profileTypes[selectedIndexPath.row];
        vc.specificProfile = profile;
        vc.editingUser = self.editingUser;
    }
}

#pragma mark - iCarouselDelegate methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    if(self.isRequestingPhotos)
    {
        return 1;//in case we are in process of requesting photos we'll just show one spinner
    }
    
    NSUInteger photosCount = self.photos.count;
    return photosCount + 1;//current user's photo +1
}

- (UIView*)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(CRCarouselPhotoView *)view
{
    
    
    if(self.isRequestingPhotos)//case when we need to display only one spinner
    {
        UIImageView* spinnerView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,100,100)];
        spinnerView.contentMode = UIViewContentModeCenter;
        spinnerView.animationImages = self.progressImagesArray;
        spinnerView.animationDuration = 0.7;
        [spinnerView startAnimating];
        
        return spinnerView;
    }
    
    if(index==0)
    {
        UIImageView* addPhotoView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,100,100)];
        addPhotoView.layer.masksToBounds = YES;
        addPhotoView.layer.cornerRadius = 3.f;
        addPhotoView.image = [UIImage imageNamed:@"AddPhoto"];
        addPhotoView.contentMode = UIViewContentModeCenter;
        addPhotoView.backgroundColor = [UIColor whiteColor];
        return addPhotoView;
    }
    
    CRPhoto* photoToShow = (CRPhoto*)self.photos[index-1];
    
    CRCarouselPhotoView* carouselPhotoView = view && [view isKindOfClass:[CRCarouselPhotoView class]] ? view : [[CRCarouselPhotoView alloc] initWithFrame:CGRectMake(0, 0, 110, 110)];
    
    carouselPhotoView.delegate = self;
    
    carouselPhotoView.photoView.contentMode = UIViewContentModeScaleAspectFill;
    
    if(photoToShow.localImage)//means we've just added the photo to the users photoCollection and it has not been saved to server yet
    {
        [carouselPhotoView setHighligted:photoToShow.isMain];
        carouselPhotoView.photoView.image = photoToShow.localImage;
    }
    else
    {
        carouselPhotoView.photoView.file = photoToShow.photoFile;
        carouselPhotoView.photoView.animationImages = self.progressImagesArray;
        carouselPhotoView.photoView.image = self.progressImagesArray.firstObject;//placeholder image
        [carouselPhotoView.photoView startAnimating];

        [carouselPhotoView.photoView loadInBackground:^(UIImage *image, NSError *error) {
            if(!error)
            {
                photoToShow.localImage = image;
                [carouselPhotoView.photoView stopAnimating];
                carouselPhotoView.photoView.animationImages = nil;
                
                [UIView transitionWithView:carouselPhotoView.photoView
                                  duration:0.5
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    carouselPhotoView.photoView.image = image;
                                }
                                completion:^(BOOL finished) {
                                    [carouselPhotoView setHighligted:photoToShow.isMain];
                                }];
            }
            else
            {
                NSLog(@"Error loading image on EditProfileScreen. Details - %@",error.description);
            }
            
        }];
    }
    return carouselPhotoView;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    if(index == 0) {
        if (self.photos.count >= MAX_ALLOWED_USER_PHOTOS) {
            [[CRUtils sharedInstance] showAlertWithMessage:[NSString stringWithFormat:@"Sorry, but you've reached %i photos per account limit.",MAX_ALLOWED_USER_PHOTOS]];
            return;
        }
        
        UIActionSheet* photoSourceActionSheet = [[UIActionSheet alloc] initWithTitle:@""
                                                                            delegate:self
                                                                   cancelButtonTitle:@"Cancel"
                                                              destructiveButtonTitle:nil
                                                                   otherButtonTitles:@"Import from Facebook", @"Choose from Library", nil];
        [photoSourceActionSheet showInView:self.view];
    }
    else
    {
        NSUInteger indexOfFormerMainPhoto = [self.photos indexOfObjectPassingTest:^BOOL(CRPhoto* obj, NSUInteger idx, BOOL *stop) {
            if(obj.isMain)
            {
                *stop = YES;
            }
            return obj.isMain;
        }];
        
        if(indexOfFormerMainPhoto != NSNotFound)
        {
            CRPhoto* formerMainPhoto = (CRPhoto*)self.photos[indexOfFormerMainPhoto];
            formerMainPhoto.isMain = NO;
            [self.photoCarousel reloadItemAtIndex:indexOfFormerMainPhoto + 1 animated:YES];
        }
        
        CRPhoto* photo = (CRPhoto*)self.photos[index-1];
        photo.isMain = YES;
        [self.photoCarousel reloadItemAtIndex:index animated:NO];
    }
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option) {
        case iCarouselOptionSpacing:{
            return value * 1.1;//making size between views 10% bigger
        }
    
        default:
            return value;
    }
}

#pragma mark - ActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)//Facebook source
    {
        [self presentFacebookImagePicker];
    }
    else if(buttonIndex == 1)//Device library
    {
        [CRBarColors resetColors];
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        self.navigationController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
    else if(buttonIndex == 2)//cancel
    {
        
    }
}

- (void)presentFacebookImagePicker {
    void (^showFacebookPhotoPickerBlock)(void) = ^{
        GRKFacebookGrabber *grabber = [[GRKFacebookGrabber alloc] init];
        GRKPickerAlbumsList * albumsList = [[GRKPickerAlbumsList alloc] initWithGrabber:grabber andServiceName:@"Facebook"];
        
        UINavigationController *navVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SWFrontNavigationController"];
        navVC.viewControllers = @[albumsList];
        [self presentViewController:navVC animated:YES completion:^{
            [[CRFacebookGrabKitReceiver sharedInstance] setDelegate:self];
        }];
    };
    
    FBSessionStateHandler handler = ^(FBSession *session, FBSessionState status, NSError *error) {
        if (FB_ISSESSIONOPENWITHSTATE(status)) {
            showFacebookPhotoPickerBlock();
        } else if (error) {
            if (status == FBSessionStateClosedLoginFailed) {
                NSString *message = [NSString stringWithFormat:@"Authorization error. Check your Facebook account settings"];
                [[CRUtils sharedInstance] showAlertWithMessage:message];
            }
            NSLog(@"FBSession: %@", session.description);
            NSLog(@"Status: %@", @(status).description);
            NSLog(@"Error: %@", error.description);
        }
    };
    
    if ([[FBSession activeSession] isOpen]) {
        [self checkFacebookSession];
    } else {
        if ([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded) {
            [[FBSession activeSession] openWithCompletionHandler:handler];
        } else {
            
            [FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"user_photos", @"user_photo_video_tags"]
                                               allowLoginUI:YES
                                          completionHandler:handler];
        }
    }
}

- (void)checkFacebookSession {
    
    void (^showFacebookPhotoPickerBlock)(void) = ^{
        GRKFacebookGrabber *grabber = [[GRKFacebookGrabber alloc] init];
        GRKPickerAlbumsList * albumsList = [[GRKPickerAlbumsList alloc] initWithGrabber:grabber andServiceName:@"Facebook"];
        
        UINavigationController *navVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SWFrontNavigationController"];
        navVC.viewControllers = @[albumsList];
        [self presentViewController:navVC animated:YES completion:^{
            [[CRFacebookGrabKitReceiver sharedInstance] setDelegate:self];
        }];
    };
    
    [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (result) {
            NSLog(@"session is valid");
            showFacebookPhotoPickerBlock();
        } else if ([FBSession activeSession].state == FBSessionStateClosedLoginFailed) {
            NSString *message = [NSString stringWithFormat:@"Authorization error. Check your Facebook account settings"];
            [[CRUtils sharedInstance] showAlertWithMessage:message];
        } else {
            NSLog(@"facebook error: %@", error.description);
        }
    }];
}

#pragma mark - ImagePickerdelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [CRBarColors setColors];
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [self addImage:chosenImage];
    picker.delegate = nil;
    self.navigationController.delegate = nil;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)addImage:(UIImage *)image {
    CRPhoto* newPhoto = [self.editingUser addPhotoForImage:image];
    newPhoto.localImage = image;
    [self.photos addPhoto:newPhoto];
    [self.photoCarousel insertItemAtIndex:self.photos.count animated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [CRBarColors setColors];
    picker.delegate = nil;
    self.navigationController.delegate = nil;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - CRPhotoViewDelegate

- (void)didPressDeleteOnPhotoView:(CRCarouselPhotoView *)view
{
    NSInteger index = [self.photoCarousel indexOfItemView:view];
    [self.photoCarousel removeItemAtIndex:index animated:YES];
    
    CRPhoto* photoToDelete = self.photos[index - 1];

    NSUInteger indexOfNewMainPhoto = [self.photos removePhoto:photoToDelete];
    [self.photosToDelete addObject:photoToDelete];
    
    [self.photoCarousel reloadItemAtIndex:indexOfNewMainPhoto + 1 animated:YES];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)//revert Changes
    {
        [[CRUser currentUser] undoPerformedChanges];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Swipable cell delegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath* cellIndexPath = [self.tableView indexPathForCell:cell];
    PFObject<CRProfileProtocol>* profile = self.profileTypes[cellIndexPath.row];
    
    switch (index) {
        case 0://showForSearch
        {
            BOOL showProfileStatus = [(NSNumber*)profile[isHidingForSearchKey] boolValue];
            profile[isHidingForSearchKey] = [NSNumber numberWithBool:!showProfileStatus];
            [self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]]
                                  withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
        case 1://delete
        {
            if([self.editingUser.profileTypes removeProfileType:profile])
            {
                self.profileTypes = [self.editingUser.profileTypes profileTypesArray];
                
                [cell hideUtilityButtonsAnimated:YES];
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:self.tableView.numberOfSections - 1] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"" message:@"You cannot remove the last profile" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                [cell hideUtilityButtonsAnimated:YES];
            }
            
            break;
        }
        default:
            break;
    }
    
    return;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

#pragma mark - CRBaseEditControlle

- (void)editValueController:(CRBaseEditValuesController *)controller didEditValue:(id)value
{
    if([value isKindOfClass:[NSArray class]])//we have received new updates for profileType
    {
        NSArray* newProfiles = value;
        
        [newProfiles enumerateObjectsUsingBlock:^(NSString* profileName, NSUInteger idx, BOOL *stop) {
            [self.editingUser.profileTypes addProfileForTypeName:profileName];
        }];
        
        self.profileTypes = [self.editingUser.profileTypes profileTypesArray];

        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:self.tableView.numberOfSections - 1] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - private methods

- (NSArray *)rightButtonsWithFirstButtonTitle:(NSString*)title
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonColor:[UIColor colorWithRed:82./255.f green:162./255.f blue:212./255.f alpha:1.0]
                                            title:title
                                             font:[UIFont fontWithName:@"HelveticaNeue" size:14]];
    
    [rightUtilityButtons sw_addUtilityButtonColor:[UIColor colorWithRed:220./255.f green:93./255.f blue:93./255.f alpha:1.0f]
                                            title:@"Delete"
                                             font:[UIFont fontWithName:@"HelveticaNeue" size:16]];

    return rightUtilityButtons;
}

#pragma mark - FacebookReceiverDelegate protocol implementation

- (NSUInteger)maxPhotosNumber {
    return 10;
}

- (NSUInteger)currentPhotosNumber {
    return self.photos.count;
}

- (void)receiver:(CRFacebookGrabKitReceiver *)receiver didLoadImage:(UIImage *)image {
    [self addImage:image];
}

#pragma mark UINavigationControllerDelegate protocol implmentation

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    if ([navigationController isKindOfClass:[UIImagePickerController class]])
    {
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.backgroundColor = UIColorFromRGBHex(0x262938);
        navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
}

@end
