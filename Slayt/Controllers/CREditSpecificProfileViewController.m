//
//  CREditModelViewController.m
//  Talent
//
//  Created by Dmitry Utenkov on 4/14/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CREditSpecificProfileViewController.h"
#import "CRUtils.h"

#import "CRBaseEditValuesController.h"
#import "CRSingleSelectionController.h"
#import "CREditPayRateController.h"
#import "CREditAgencyViewController.h"

#import <Parse/PF_MBProgressHUD.h>

#import "NSNumber+CGFloat.h"

@interface CREditSpecificProfileViewController ()

@property (nonatomic, strong) NSArray *entityProperties;

@property (nonatomic, strong) PF_MBProgressHUD* HUD;
@end

@implementation CREditSpecificProfileViewController

- (void) dealloc
{
    [self.editingUser removeObserver:self forKeyPath:@"gender"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.editingUser addObserver:self forKeyPath:@"gender" options:NSKeyValueObservingOptionNew context:nil];
    
    self.HUD = [[PF_MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
    
    
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_photo_back_active"] style:UIBarButtonItemStyleBordered target:self action:@selector(popController)];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.title = self.specificProfile.profileName;
}

- (void)popController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

- (void)setSpecificProfile:(PFObject<CRProfileProtocol> *)specificProfile
{
    _specificProfile = specificProfile;
    
    if(self.editingUser)
    {
        [self refreshEntityProperties];
    }
}

- (void)setEditingUser:(CRUser *)editingUser
{
    _editingUser = editingUser;
    
    if(self.specificProfile)
    {
        [self refreshEntityProperties];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger sections = self.entityProperties.count;
    return sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rows = [self.entityProperties[section][@"properties"] count];
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber* height = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"rowHeight"];
    if(!height)
    {
        height = @(44);//defaultValue
    }
    
    return height.CGFloatValue;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSString* headerTitle = self.entityProperties[section][@"sectionHeader"];
    NSNumber* headerHeight = self.entityProperties[section][@"headerHeight"];
    
    if(headerHeight)
    {
        return headerHeight.CGFloatValue;
    }
    else if (headerTitle)//the title for header was provided but the height was not
    {
        return 25;
    }
    else
    {
        return 0;
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString* headerTitle = self.entityProperties[section][@"sectionHeader"];
    NSNumber* headerHeight = self.entityProperties[section][@"headerHeight"];
    
    if(!headerTitle)
    {
        return nil;
    }
    
    CGFloat labelHeight = 25;
    
    if(!headerHeight)
    {
        headerHeight = @(labelHeight);
    }
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, headerHeight.CGFloatValue)];
    UILabel* headerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(17, headerHeight.CGFloatValue - labelHeight, self.view.frame.size.width-17, labelHeight)];
    headerTitleLabel.text = headerTitle;
    [headerTitleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    [headerTitleLabel setTextColor:[UIColor colorWithRed:0x80/255.f green:0x7f/255.f blue:0xb0/255.f alpha:1]];
    [headerView addSubview:headerTitleLabel];
    return headerView;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"cellIdentifier"];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"displayName"];
    cell.textLabel.minimumScaleFactor = 0.5f;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    NSString* key = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"propertyName"];
    
    id value = self.editingUser[key];
    
    NSString* detailString = [self.editingUser valueDescriptionForKey:key];
    
    if(!detailString)
    {
        if(value == nil)
        {
            value = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"defaultValue"];
        }
        
        detailString = [value description];
        
        NSString* trailingSymbols = value == nil ? nil : self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"trailingSymbols"];
        
        if(trailingSymbols && self.editingUser[key])
        {
            detailString = [NSString stringWithFormat:@"%@%@",detailString,trailingSymbols];
        }
    }
    
    CGRect rectToFit = cell.frame;
    detailString = [[CRUtils sharedInstance] stringFromString:detailString
                                                   toFitWidth:rectToFit.size.width - 50
                                                      forFont:cell.detailTextLabel.font];
    
    cell.detailTextLabel.text = detailString;
    
    cell.detailTextLabel.minimumScaleFactor = 0.5f;
    cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSNumber* skipEdit = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"skipEdit"];
    BOOL skipingEdit = [skipEdit boolValue];
    if(skipingEdit)
    {
        return;
    }
    
    NSString* editingClass = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"editingClass"];
    
    if ([editingClass isEqualToString:NSStringFromClass([CREditAgencyViewController class])]) {
        [self performSegueWithIdentifier:@"AgencySegue" sender:self];
        return;
    }
    
    NSString* propertyName = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"propertyName"];
    NSArray* choiseOptions = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"choiseOptions"];
    NSString* displayName = self.entityProperties[indexPath.section][@"properties"][indexPath.row][@"displayName"];
    
    CRBaseEditValuesController* editController;
    
    if(editingClass)
    {
        Class classToEdit = NSClassFromString(editingClass);
        if([classToEdit isSubclassOfClass:[CRBaseEditValuesController class]])
        {
             editController = [[classToEdit alloc] initWithSaveType:CRBaseEditValuesSaveTypeDirect];
        }
        else
        {
            NSLog(@"WARNING: EditingClass is not a subclass of CRBaseEditValuesController. Check didSelectRowAtIndexPath: of CREditModelVC");
        }
    }
    else
    {
        NSLog(@"%@ property does not have editing class. No actions will be done. Include it's editing class into plist or handle it's responce manually in didSelectRow method.", propertyName);
        return;
    }

    NSAssert(editController!=nil, @"EditController must be determined on clicking on cell");
    
    editController.choiseOptions = choiseOptions;
    editController.propertyName = propertyName;
    editController.editingObject = self.editingUser;
    editController.entityProperties = self.entityProperties;
    editController.propertyIndexPath = indexPath;
    editController.navigationItem.title = displayName;
    
    [self.navigationController pushViewController:editController animated:YES];

}

- (void)refreshEntityProperties
{
    id<CRProfileProtocol> profileType = self.specificProfile;
    
    if(profileType)
    {
        self.entityProperties = [self.editingUser profilePropertiesArrayForType:self.specificProfile.profileType];
        [self.tableView reloadData];
    }
}

#pragma mark - observing

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([object isEqual:self.editingUser] && [keyPath isEqualToString:@"gender"])
    {
        [self refreshEntityProperties];
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


@end
