//
//  CRWebViewController.h
//  Slayt
//
//  Created by Dmitry Utenkov on 5/28/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRWebViewController : UIViewController

@property (nonatomic, copy) NSURL *htmlPath;

@end
