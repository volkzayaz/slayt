//
//  CRMenuController.m
//  Talent
//
//  Created by Vlad Soroka on 4/2/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRUser.h"
#import "CRPhoto.h"

#import "CRMenuController.h"
#import "SWRevealViewController.h"
#import "CRRevealTableCell.h"
#import "UIImage+Resizing.h"
#import "CRProfileController.h"
#import "CRHomeViewController.h"
#import "CRCastingsViewController.h"

#pragma mark - Constants

static const NSUInteger kProfileCellIndex   = 0;
static const NSUInteger kHomeCellIndex      = 1;

static const NSUInteger kProfileThumbnailSize   = 32;
static const NSUInteger kRowHeight              = 64;

static NSString *CellIdentifier = @"RevealMenuCell";

@interface CRMenuController ()

@property (nonatomic, strong) UIImage *profileThumbnail;
@property (nonatomic, strong) NSArray *menuItems;

@property (nonatomic, strong) NSIndexPath *lastSelectedPath;

@end

@implementation CRMenuController

- (void)setProfileThumbnail:(UIImage *)profileThumbnail {
    _profileThumbnail = profileThumbnail;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateProfileCell];
    });
}

#pragma mark - View lifecycle

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)awakeFromNib {
    self.lastSelectedPath = [NSIndexPath indexPathForRow:kHomeCellIndex inSection:0];
    self.menuItems = @[ @{ @"Title"     : @"PROFILE",
                           @"IconName"  : @"",
                           @"SegueName" : @"ProfileSegue" },
                        
                        @{ @"Title"     : @"HOME",
                           @"IconName"  : @"HomeIcon",
                           @"SegueName" : @"HomeSegue" },
                        
                        @{ @"Title"     : @"MESSAGES",
                           @"IconName"  : @"MessagesIcon",
                           @"SegueName" : @"" },
                        
                        @{ @"Title"     : @"CASTINGS",
                           @"IconName"  : @"CastingsIcon",
                           @"SegueName" : @"CastingsSegue" },
                        
                        @{ @"Title"     : @"JOBS",
                           @"IconName"  : @"JobsIcon",
                           @"SegueName" : @"" },
                        
                        @{ @"Title"     : @"SETTINGS",
                           @"IconName"  : @"SettingsIcon",
                           @"SegueName" : @"SettingsSegue" },
                        
                        @{ @"Title"     : @"INVITE",
                           @"IconName"  : @"InviteIcon",
                           @"SegueName" : @"" } ];
    self.profileThumbnail = [self placehoderImageFromImage:[UIImage imageNamed:@"noImage"]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProfileThumbnail:)
                                                 name:kUserDidUpdateMainPhotoNotification
                                               object:nil];
    [self updateProfileThumbnail:nil];
    
    // Hides extra lines of table view;
    self.tableView.tableFooterView = [UIView new];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // TODO: rewrite for necessary requests
    [self updateProfileThumbnail:nil];
    [self.tableView reloadData];
    UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:self.lastSelectedPath];
    [selectedCell setSelected:YES animated:NO];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)selectMenuItemAtIndex:(CRMenuItem)itemIndex {
    self.lastSelectedPath = [NSIndexPath indexPathForRow:itemIndex inSection:0];
}

#pragma mark - Segues

#warning multiple switching from home page to Profile page causes memory leak
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        self.lastSelectedPath = [self.tableView indexPathForCell:sender];
    } else {
        NSUInteger menuItemIndex = [self.menuItems indexOfObjectPassingTest:^BOOL(NSDictionary *menuItem, NSUInteger index, BOOL *stop) {
            return [menuItem[@"SegueName"] isEqualToString:segue.identifier];
        }];
        if (menuItemIndex != NSNotFound) {
            self.lastSelectedPath = [NSIndexPath indexPathForRow:menuItemIndex inSection:0];
        }
    }
    
    if ([segue.identifier isEqualToString:@"ProfileSegue"]) {
        CRProfileController *profileVC = segue.destinationViewController;
        profileVC.profile = [CRUser currentUser];
    }
    
    if ([segue.identifier isEqualToString:@"CastingsSegue"]) {
        CRCastingsViewController *profileVC = segue.destinationViewController;
        profileVC.profileTypes=[CRUser currentUser].profileTypes.profileTypesArray;
    }
    
    if ([segue isKindOfClass:[SWRevealViewControllerSegue class]]) {
        SWRevealViewControllerSegue* rvcs = (SWRevealViewControllerSegue*) segue;
        
        SWRevealViewController* rvc = self.revealViewController;
        
        NSAssert( rvc != nil, @"oops! must have a revealViewController" );
        NSAssert( [rvc.frontViewController isKindOfClass:[UINavigationController class]], @"oops!  for this segue we want a permanent navigation controller in the front!" );
        
        rvcs.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            UINavigationController* nc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWFrontNavigationController"];
            // TODO: think of better solution.
            //we've just allocated navigation controller with existing root viewController, and then replaced it(rootViewController) with our viewController. It'd be perfect to eliminate the neccesity of allocating and then releasing the initial root vc
            [nc setViewControllers:@[dvc]];
            [dvc navigationItem].leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"RevealIcon"]
                                                                                      style:UIBarButtonItemStyleBordered
                                                                                     target:[dvc revealViewController]
                                                                                     action:@selector( revealToggle: )];
            [rvc pushFrontViewController:nc animated:YES];
        };
    }
}

#pragma mark - UITableViewDataSource protocol implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CRRevealTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = self.menuItems[indexPath.row][@"Title"];
    
    if (indexPath.row == kProfileCellIndex) {
        cell.imageView.layer.masksToBounds = YES;
        cell.imageView.layer.cornerRadius = kProfileThumbnailSize / 2;
        cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
        cell.imageView.image = self.profileThumbnail;
    } else {
        if ([self.menuItems[indexPath.row][@"IconName"] length] > 0) {
            [cell setIcon:[UIImage imageNamed:self.menuItems[indexPath.row][@"IconName"]]];
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate protocol implementation

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *oldSelectedCell = [tableView cellForRowAtIndexPath:self.lastSelectedPath];
    [oldSelectedCell setSelected:NO animated:NO];
    
    UITableViewCell *newSelectedCell = [tableView cellForRowAtIndexPath:indexPath];
    [newSelectedCell setSelected:YES animated:YES];
    
    if ([self.menuItems[indexPath.row][@"Title"] isEqualToString:@"MESSAGES"]) {
        [self.revealViewController rightRevealToggle:newSelectedCell];
    } else {
        self.lastSelectedPath = indexPath;
        if ([self.menuItems[indexPath.row][@"SegueName"] length] > 0) {
            [self performSegueWithIdentifier:self.menuItems[indexPath.row][@"SegueName"] sender:newSelectedCell];
        }
    }
}

#pragma mark - Update profile thumbnail notification handler

- (void)updateProfileThumbnail:(NSNotification *)notification {
    [[CRUser currentUser] requestMainPhotoWithCompletitionBlock:^(CRPhoto *photo, NSError *error) {
        if (!error && photo) {
            [photo.photoFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (!error) {
                    self.profileThumbnail = [self placehoderImageFromImage:[UIImage imageWithData:data]];
                } else {
                    self.profileThumbnail = [self placehoderImageFromImage:[UIImage imageNamed:@"noImage"]];
                }
            }];
        } else {
            self.profileThumbnail = [self placehoderImageFromImage:[UIImage imageNamed:@"noImage"]];
        }
    }];
}

- (void)updateProfileCell {
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:kProfileCellIndex inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationNone];
    if ([indexPath isEqual:self.lastSelectedPath]) {
        UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:self.lastSelectedPath];
        [selectedCell setSelected:YES animated:NO];
    }
}

- (UIImage *)placehoderImageFromImage:(UIImage *)image {
    return [UIImage squareImageWithImage:image
                            scaledToSize:CGSizeMake(kProfileThumbnailSize, kProfileThumbnailSize)];
}

@end
