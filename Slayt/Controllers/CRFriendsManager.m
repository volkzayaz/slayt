//
//  CRFriendsManager.m
//  Slayt
//
//  Created by Dmitry Utenkov on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFriendsManager.h"
#import "CRUser.h"
#import "CRFriend.h"

@interface CRFriendsManager ()

@property (nonatomic, strong, readwrite) NSArray *friends;

@end

@implementation CRFriendsManager

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)reloadFriends {
    void (^errorBlock)(NSError *) = ^(NSError *error){
        if (error) {
            NSLog(@"Error CRFriend query: %@", error.localizedDescription);
        }
        if ([self.delegate respondsToSelector:@selector(friendsManagerDidNotFoundFriends:)]) {
            [self.delegate friendsManagerDidNotFoundFriends:self];
        }
    };
    
    if (![CRUser currentUser]) {
        errorBlock(nil);
    } else {
        PFQuery *query = [PFQuery queryWithClassName:[CRFriend parseClassName]];
        [query whereKey:@"sourceProfileId" equalTo:[CRUser currentUser].objectId];
        [query orderByDescending:@"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (objects.count > 0) {
                PFQuery *userQuery = [CRUser query];
                [userQuery whereKey:@"objectId" containedIn:[objects valueForKey:@"friendId"]];
                
                Class filterClass = [[[CRProfileTypeStorage alloc] init] classForProfileTypeName:[CRUser currentUser].friendsFilter];
                if (filterClass) {
                    PFQuery *filterQuery = [PFQuery queryWithClassName:NSStringFromClass(filterClass)];
                    [userQuery whereKey:@"profileReferencesArray" matchesQuery:filterQuery];
                }
                
                [userQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    if (objects.count > 0) {
                        if ([self.delegate respondsToSelector:@selector(friendsManager:didLoadFriends:)]) {
                            [self.delegate friendsManager:self didLoadFriends:objects];
                        }
                        self.friends = [NSArray arrayWithArray:objects];
                        //[self fetchFriendsData];
                    } else {
                        errorBlock(error);
                    }
                }];
            } else {
                errorBlock(error);
            }
        }];
    }
}

//- (void)fetchFriendsData {
//    for (CRUser *friend in self.friends) {
//        self.friendsDictionary[friend.objectId] = [NSMutableDictionary dictionary];
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
//                       {
//                           self.friendsDictionary[friend.objectId][@"fullName"] = friend.fullName;
//                           
//                           if ([self.delegate respondsToSelector:@selector(friendsManagerDidFetchFriendData:)]) {
//                               [self.delegate <# selector name#>];
//                           }
//                           
//                           [self updateFriendListItem:friend];
//                           
//                           CRPhoto *friendPhoto = [friend requestMainPhoto:nil];
//                           [friendPhoto.photoFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//                               UIImage *friendImage = [UIImage imageWithData:data];
//                               self.friendsDictionary[friend.objectId][@"friendImage"] = friendImage ? friendImage : [UIImage imageNamed:@"noImage"];
//                               [self updateFriendListItem:friend];
//                           }];
//                       });
//    }
//}

@end
