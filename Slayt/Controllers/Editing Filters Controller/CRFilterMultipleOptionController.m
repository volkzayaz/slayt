//
//  CREditSingleOptionController.m
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterMultipleOptionController.h"
#import "CRBaseFilterEditingController_protected.h"

@interface CRFilterMultipleOptionController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) UITableView* tableView;
@property (nonatomic, strong) NSArray* choiseOptions;

@property (nonatomic, strong) NSMutableArray* chosenOptions;

@end

@implementation CRFilterMultipleOptionController

@synthesize chosenOptions = _chosenOptions;
@synthesize choiseOptions = _choiseOptions;
@synthesize tableView = _tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect navigationBarFrame = self.navigationController.navigationBar.frame;
    CGRect tableViewFrame = self.view.frame;
    tableViewFrame.size.height -= navigationBarFrame.size.height + navigationBarFrame.origin.y;//we want tableView to occupy all the space between navigation bar and bottom of our view
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:tableViewFrame style:UITableViewStyleGrouped];
    
    self.tableView = tableView;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.choiseOptions = self.entityProperties[self.entityIndexPath.section][@"properties"][self.entityIndexPath.row][@"choiseOptions"];
    
    NSArray* constraintsForPropertyName = [self.editingFilter constraintValueForKey:self.propertyName];
    NSMutableArray* firstConstraint = [[constraintsForPropertyName firstObject] mutableCopy];
    self.chosenOptions = firstConstraint;
    if(!self.chosenOptions)
    {
        self.chosenOptions = [NSMutableArray array];
    }
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 1;///Option "Any"
    }
    
    return self.choiseOptions.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"ValueCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.section == 0)
    {
        cell.textLabel.text = @"Any";
    }
    else
    {
        cell.textLabel.text = self.choiseOptions[indexPath.row];
    }

    cell.textLabel.textColor = [UIColor colorWithRed:0x59/255.f green:0x59/255.f blue:0x59/255.f alpha:1];
    
    if(([self.chosenOptions containsObject:self.choiseOptions[indexPath.row]] && indexPath.section == 1) ||
        (self.chosenOptions.count == 0 && indexPath.section == 0))
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        [self.chosenOptions removeAllObjects];
        
        [self.tableView reloadData];
    }
    else
    {
        if([self.chosenOptions containsObject:self.choiseOptions[indexPath.row]])
        {
            [self.chosenOptions removeObject:self.choiseOptions[indexPath.row]];
        }
        else
        {
            [self.chosenOptions addObject:self.choiseOptions[indexPath.row]];
        }
        
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}

- (void)applyFilters
{
    [self.editingFilter setConstraintOfType:CRConstraintTypeContainsObject
                                     forKey:self.propertyName
                                      value:[self.chosenOptions copy]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
