//
//  CRFilterAgencyController.m
//  Slayt
//
//  Created by Vlad Soroka on 6/6/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterAgencyController.h"
#import "CRBaseFilterEditingController_protected.h"

@interface CRFilterAgencyController () <UISearchBarDelegate>

@property (nonatomic, weak) UISearchBar* searchBar;

@property (nonatomic, strong) NSArray* fullAgenciesList;
@property (nonatomic, strong) NSString* previousSearchText;

@end

@implementation CRFilterAgencyController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    UISearchBar* searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)];
    searchBar.delegate = self;
    searchBar.placeholder = @"Search through agencies";
    
    self->_tableView.tableHeaderView = searchBar;
    self.searchBar = searchBar;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *agenciesListPath = [[NSBundle mainBundle] pathForResource:@"AgenciesList" ofType:@"csv"];
    NSError *error = nil;
    NSString *csvString = [NSString stringWithContentsOfFile:agenciesListPath
                                                    encoding:NSUTF8StringEncoding
                                                       error:&error];
    self.fullAgenciesList = [csvString componentsSeparatedByString:@",\n"];
    
    self.choiseOptions = self.fullAgenciesList;
    [self->_tableView reloadData];
}

#pragma mark - searchBar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", searchText];
    NSArray* newDatasourceArray = nil;
    
    if(self.previousSearchText.length < searchText.length)
    {
        newDatasourceArray = [self.choiseOptions filteredArrayUsingPredicate:predicate];
    }
    else
    {
        newDatasourceArray = [self.fullAgenciesList filteredArrayUsingPredicate:predicate];
    }
    
    if(!newDatasourceArray || newDatasourceArray.count == 0)
    {
        newDatasourceArray = self.fullAgenciesList;
    }
    self.choiseOptions = newDatasourceArray;
    [self->_tableView reloadData];
    
    self.previousSearchText = searchText;
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

@end
