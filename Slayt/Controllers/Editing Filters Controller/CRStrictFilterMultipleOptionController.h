//
//  CRStrictFilterMultipleOptionController.h
//  Slayt
//
//  Created by Vlad Soroka on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterMultipleOptionController.h"

/**
 *  @discussion The only difference from CRFilterMultipleOptionController is that resulting query will require a given key to Contain every single chosen option. 
 *  For example, let us say that key jobTypes contains values [@"delivery boy"] and chosen options by user are [@"office manager",@"plumber",@"delivery boy"]. CRFilterMultipleOptionController will apply filter that will be true for key jobTypes since value @"delivery boy" is Contained in user selected options. 
 *  CRStrictFilterMultipleOptionController interprete chosen options in a different manner. This class will require key jobTypes to contain all of the values from chosen options. If we return to our example, CRStrictFilterMultipleOptionController will add filter that is false for key jobTypes since our user is only a deliver boy but he is neither plumber nor office manager.
 */
@interface CRStrictFilterMultipleOptionController : CRFilterMultipleOptionController

@end
