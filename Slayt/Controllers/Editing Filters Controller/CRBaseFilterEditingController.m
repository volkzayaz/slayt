//
//  CRBaseFilterEditingController.m
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRBaseFilterEditingController.h"
#import "CRBaseFilterEditingController_protected.h"

@interface CRBaseFilterEditingController ()

@end

@implementation CRBaseFilterEditingController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //uncomment the following to allow filter cancelation
    /*
    UIBarButtonItem* cancelItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(cancelClicked:)];
    cancelItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = cancelItem;
    */
    
    UIBarButtonItem* doneItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_photo_back_active"] style:UIBarButtonItemStyleBordered target:self action:@selector(doneClicked:)];
    doneItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = doneItem;
    
    self.view.backgroundColor = [UIColor colorWithRed:201.f/255.f green:203.f/255.f blue:212.f/255.f alpha:1.f];
}

- (void)doneClicked:(id)sender
{
    [self applyFilters];
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)cancelClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)applyFilters
{ ; }

@end
