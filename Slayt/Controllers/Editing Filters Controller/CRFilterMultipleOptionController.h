//
//  CREditSingleOptionController.h
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRBaseFilterEditingController.h"

/**
 *  @discussion Class allows user to select between multiple options. The resulting query will return true if a value behind given propertyName is CONTAINED in selected options.
 */
@interface CRFilterMultipleOptionController : CRBaseFilterEditingController{
    @protected NSMutableArray* _chosenOptions;
    @protected NSArray* _choiseOptions;
    
    @protected UITableView __weak * _tableView;
}

@end
