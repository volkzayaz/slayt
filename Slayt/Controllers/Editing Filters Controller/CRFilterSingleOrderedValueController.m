//
//  CRFilterSingleOrderedValueController.m
//  Slayt
//
//  Created by Vlad Soroka on 6/3/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterSingleOrderedValueController.h"
#import "CRBaseFilterEditingController_protected.h"
#import "CRProfileTypeStorage.h"
#import "CRUtils.h"

@interface CRFilterSingleOrderedValueController ()

///Most of ordered properties start with NotSpecified enumarationItem. Since we don't want "Not specified" value occure in the list of avaliable options we'll eject it form the list. The flag shows whether or not we've ejected "Not specified" item
@property (nonatomic, assign) BOOL didEjectDefaultValue;

@end

@implementation CRFilterSingleOrderedValueController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    Class ProfileTypeClass = [[[CRProfileTypeStorage alloc] init] profileTypeClassForPrefixedKey:self.propertyName];
    NSString* stringsArrayName = [NSString stringWithFormat:@"%@Strings",[[CRUtils sharedInstance] substringOfString:self.propertyName
                                                                                                       withoutPrefix:[[ProfileTypeClass object] profilePrefix]]];
    
    
    SEL selector = NSSelectorFromString(stringsArrayName);
    IMP imp = [self.editingFilter.queryingUser methodForSelector:selector];
    NSArray* (*func)(id, SEL) = (void *)imp;
    NSArray *result = func(self.editingFilter.queryingUser, selector);
    
    ///this will fail to work if the default value is not "Not Specified".
    if([result.firstObject isEqualToString:@"Not Specified"])
    {
        NSMutableArray* temp = [result mutableCopy];
        [temp removeObjectAtIndex:0];
        result = temp;
        
        self.didEjectDefaultValue = YES;
    }
    
    self.choiseOptions = result;
    
    NSNumber* chosenIndex = [self.editingFilter valueFromConstraintType:CRConstraintTypeEqual
                                                               forKey:self.propertyName];
    
    if(chosenIndex)
    {
        NSUInteger orderedValue = chosenIndex.unsignedIntegerValue;
        if(self.didEjectDefaultValue)
        {
            orderedValue --;
        }
        
        NSNumber* chosenValue = [result objectAtIndex:orderedValue];
        
        self->_chosenOption = chosenValue;
    }

    [self->_tableView reloadData];
}

- (void)applyFilters
{
    NSNumber* chosenIndex = @([self.choiseOptions indexOfObject:self->_chosenOption]);
    
    if(chosenIndex.unsignedIntegerValue != NSNotFound)
    {
        NSUInteger orderedNumber = [chosenIndex unsignedIntegerValue];
        if(self.didEjectDefaultValue)
        {
            orderedNumber ++;
        }
        
        chosenIndex = @(orderedNumber);
        
        [self.editingFilter setConstraintOfType:CRConstraintTypeEqual
                                         forKey:self.propertyName
                                          value:chosenIndex];
    }
    else
    {
        [self.editingFilter removeConstraintOfType:CRConstraintTypeEqual
                                            forKey:self.propertyName];
    }
}

@end
