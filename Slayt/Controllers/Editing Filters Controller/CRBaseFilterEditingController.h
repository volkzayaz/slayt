//
//  CRBaseFilterEditingController.h
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRUserFilter.h"

@interface CRBaseFilterEditingController : UIViewController

@property (nonatomic, strong) CRUserFilter* editingFilter;//this filter will be edited through subclasses
@property (nonatomic, strong) NSString* propertyName;

@property (nonatomic, strong) NSArray* entityProperties;
@property (nonatomic, strong) NSIndexPath* entityIndexPath;

@end
