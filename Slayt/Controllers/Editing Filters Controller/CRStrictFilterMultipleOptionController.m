//
//  CRStrictFilterMultipleOptionController.m
//  Slayt
//
//  Created by Vlad Soroka on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRStrictFilterMultipleOptionController.h"
#import "CRBaseFilterEditingController_protected.h"

@interface CRStrictFilterMultipleOptionController ()

@end

@implementation CRStrictFilterMultipleOptionController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)applyFilters
{
    [self.editingFilter setConstraintOfType:CRConstraintTypeContainsArray
                                     forKey:self.propertyName
                                      value:[self->_chosenOptions copy]];
}

@end
