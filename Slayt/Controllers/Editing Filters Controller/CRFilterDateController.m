//
//  CRFilterDateController.m
//  Slayt
//
//  Created by Vlad Soroka on 6/3/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterDateController.h"
#import "CRBaseFilterEditingController_protected.h"
#import "NSDate+PassedYears.h"

@interface CRFilterDateController ()

@end

@implementation CRFilterDateController

- (void)viewWillAppear:(BOOL)animated
{
    self.lowerSelectedValue = [self.editingFilter valueFromConstraintType:CRConstraintTypeAgeMoreOrEqual
                                                                   forKey:self.propertyName];
    
    self.upperSelectedValue = [self.editingFilter valueFromConstraintType:CRConstraintTypeAgeLessOrEqual
                                                                   forKey:self.propertyName];

    [super viewWillAppear:animated];
}

- (void)applyFilters
{
    NSNumber* bottomLimit = @(self.rangeSlider.selectedMinimumValue);
    NSNumber* upperLimit = @(self.rangeSlider.selectedMaximumValue);
    
    if(self.rangeSlider.selectedMinimumValue != self.rangeSlider.minimumValue)
    {
        [self.editingFilter setConstraintOfType:CRConstraintTypeAgeMoreOrEqual
                                         forKey:self.propertyName
                                          value:bottomLimit];
    }
    else
    {
        [self.editingFilter removeConstraintOfType:CRConstraintTypeAgeMoreOrEqual forKey:self.propertyName];
    }
    
    if(self.rangeSlider.selectedMaximumValue != self.rangeSlider.maximumValue)
    {
        [self.editingFilter setConstraintOfType:CRConstraintTypeAgeLessOrEqual
                                         forKey:self.propertyName
                                          value:upperLimit];
    }
    else
    {
        [self.editingFilter removeConstraintOfType:CRConstraintTypeAgeLessOrEqual forKey:self.propertyName];
    }
}

@end
