//
//  CRFilterPayRateController.m
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterPayRateController.h"
#import "CRFilterSingleOrderedValueController.h"
#import "CRFilterRangeValueController.h"
#import "CRBaseFilterEditingController_protected.h"

@interface CRFilterPayRateController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) UITableView* tableView;

@property (nonatomic, strong) NSString* paymentFrequencyPropertyName;
@property (nonatomic, strong) NSString* payRatePropertyName;

@end

@implementation CRFilterPayRateController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    UITableView* tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    self.tableView = tableView;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];
    
    self.paymentFrequencyPropertyName = self.entityProperties[self.entityIndexPath.section][@"properties"][self.entityIndexPath.row][@"numberPropertyName"];
    self.payRatePropertyName = self.propertyName;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

#pragma mark - tableView datasource/delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSNumber* value = [self.editingFilter valueFromConstraintType:CRConstraintTypeEqual
                                                           forKey:self.paymentFrequencyPropertyName];
    
    CRPaymentFrequency paymentFrequency = value.unsignedIntegerValue;
    if(paymentFrequency == CRPaymentFrequencyNotSpecified ||
       paymentFrequency == CRPaymentFrequencyWorkForFree)
    {
        return 1;
    }
    
    return 2;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"PaymentCell"];
    cell.textLabel.textColor = [UIColor colorWithRed:0x80/255.f green:0x7f/255.f blue:0xb0/255.f alpha:1];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(indexPath.section == 0)
    {
        cell.textLabel.text = @"Payment";
        
        NSArray* strings = [self.editingFilter.queryingUser stringsForPrefixedPropertyName:self.paymentFrequencyPropertyName];
        NSNumber* value = [self.editingFilter valueFromConstraintType:CRConstraintTypeEqual
                                                               forKey:self.paymentFrequencyPropertyName];
        if(value)
        {
            cell.detailTextLabel.text = strings[value.unsignedIntegerValue];
        }
        else
        {
            cell.detailTextLabel.text = @"Any";
        }

    }
    else if (indexPath.section == 1)
    {
        cell.textLabel.text = @"Pay Rate";
        
        cell.detailTextLabel.text = [self.editingFilter descriptionForKey:self.payRatePropertyName
                                                          withDisplayType:kDisplayTypeNumberRange
                                                        additionalDetails:@{kAdditionalDetailsKeyTrailingSymbols:   @"$"}  ];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CRBaseFilterEditingController* controller = nil;
    
    if(indexPath.section == 0)///paymentFrequency
    {
        controller = [[CRFilterSingleOrderedValueController alloc] init];
        
        controller.propertyName = self.paymentFrequencyPropertyName;
    }
    else if (indexPath.section == 1)//pay Rate
    {
        controller = [[CRFilterRangeValueController alloc] init];
        
        controller.propertyName = self.payRatePropertyName;
    }
    
    controller.editingFilter = self.editingFilter;
    controller.entityProperties = self.entityProperties;
    controller.entityIndexPath = self.entityIndexPath;
    
    if(!controller)
    {
        NSAssert(controller != nil, @"Logic Error. FilterController has has not been initialized during didSelectRowAtIndexPath: method of CRFilterPayRateController");
        return;
    }
    
    [self.navigationController pushViewController:controller animated:YES];
    
    controller.navigationItem.title = @"Pay Rate";
}

- (void) applyFilters
{
    NSNumber* setPaymantFrequency = [self.editingFilter valueFromConstraintType:CRConstraintTypeEqual forKey:self.paymentFrequencyPropertyName];
    
    CRPaymentFrequency pf = setPaymantFrequency.unsignedIntegerValue;
    if (pf == CRPaymentFrequencyNotSpecified ||
        pf == CRPaymentFrequencyWorkForFree)
    {
        [self.editingFilter removeConstraintOfType:CRConstraintTypeLessOrEqual
                                            forKey:self.payRatePropertyName];
        [self.editingFilter removeConstraintOfType:CRConstraintTypeMoreOrEqual
                                            forKey:self.payRatePropertyName];

    }
}

@end
