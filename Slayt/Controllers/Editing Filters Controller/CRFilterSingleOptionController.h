//
//  CRFilterSingleOptionController.h
//  Slayt
//
//  Created by Vlad Soroka on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRBaseFilterEditingController.h"

@interface CRFilterSingleOptionController : CRBaseFilterEditingController{
    @protected id _chosenOption;
    @protected UITableView __weak * _tableView;
}

@property (nonatomic, copy) NSArray* choiseOptions;

@end
