//
//  CRFilterSingleOptionController.m
//  Slayt
//
//  Created by Vlad Soroka on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterSingleOptionController.h"
#import "CRBaseFilterEditingController_protected.h"

@interface CRFilterSingleOptionController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) UITableView* tableView;

@property (nonatomic, strong) id chosenOption;

///title of option that stands for "Any" option
@property (nonatomic, strong) NSString* anyOptionTitle;

@end

@implementation CRFilterSingleOptionController

@synthesize chosenOption = _chosenOption;
@synthesize tableView = _tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];

    CGRect navigationBarFrame = self.navigationController.navigationBar.frame;
    CGRect tableViewFrame = self.view.frame;
    tableViewFrame.size.height -= navigationBarFrame.size.height + navigationBarFrame.origin.y;//we want tableView to occupy all the space between navigation bar and bottom of our view
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:tableViewFrame style:UITableViewStyleGrouped];
    
    self.tableView = tableView;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.choiseOptions = self.entityProperties[self.entityIndexPath.section][@"properties"][self.entityIndexPath.row][@"choiseOptions"];
    self.anyOptionTitle = self.entityProperties[self.entityIndexPath.section][@"properties"][self.entityIndexPath.row][@"anyOptionTitle"];
    
    if(!self.anyOptionTitle)
    {
        self.anyOptionTitle = @"Any";
    }
    
    NSArray* constraintsForPropertyName = [self.editingFilter constraintValueForKey:self.propertyName];
    self.chosenOption = constraintsForPropertyName.firstObject;
        
    [self.tableView reloadData];
}

#pragma mark - tableView dataSource/delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 1;///Option "Any"
    }
    
    return self.choiseOptions.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"ValueCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.section == 0)
    {
        cell.textLabel.text = self.anyOptionTitle;
    }
    else
    {
        cell.textLabel.text = self.choiseOptions[indexPath.row];
    }
    
    cell.textLabel.textColor = [UIColor colorWithRed:0x59/255.f green:0x59/255.f blue:0x59/255.f alpha:1];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    if(([self.chosenOption isEqual:self.choiseOptions[indexPath.row]] && indexPath.section == 1) ||
       (self.chosenOption == nil && indexPath.section == 0))
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        self.chosenOption = nil;
    }
    else
    {
        self.chosenOption = self.choiseOptions[indexPath.row];
    }
    
    [self.tableView reloadData];
}

- (void)applyFilters
{
    if(self.chosenOption)
    {
        [self.editingFilter setConstraintOfType:CRConstraintTypeEqual
                                         forKey:self.propertyName
                                          value:self.chosenOption];
    }
    else
    {
        [self.editingFilter removeConstraintOfType:CRConstraintTypeEqual
                                            forKey:self.propertyName];
    }

}



@end
