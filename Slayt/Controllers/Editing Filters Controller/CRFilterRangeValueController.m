//
//  CRFilterRangeValueController.m
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterRangeValueController.h"
#import "CRBaseFilterEditingController_protected.h"

@interface CRFilterRangeValueController ()


@end

@implementation CRFilterRangeValueController

- (void)viewWillAppear:(BOOL)animated
{
    self.upperSelectedValue = [self.editingFilter valueFromConstraintType:CRConstraintTypeLessOrEqual forKey:self.propertyName];
    self.lowerSelectedValue = [self.editingFilter valueFromConstraintType:CRConstraintTypeMoreOrEqual forKey:self.propertyName];

    [super viewWillAppear:animated];
}


- (void)applyFilters
{
    NSNumber* bottomLimit = @(self.rangeSlider.selectedMinimumValue);
    NSNumber* upperLimit = @(self.rangeSlider.selectedMaximumValue);
    
    if(self.rangeSlider.selectedMaximumValue != self.rangeSlider.maximumValue)
    {
        [self.editingFilter setConstraintOfType:CRConstraintTypeLessOrEqual forKey:self.propertyName value:upperLimit];
    }
    else
    {
        [self.editingFilter removeConstraintOfType:CRConstraintTypeLessOrEqual forKey:self.propertyName];
    }
    
    if(self.rangeSlider.selectedMinimumValue != self.rangeSlider.minimumValue)
    {
        [self.editingFilter setConstraintOfType:CRConstraintTypeMoreOrEqual forKey:self.propertyName value:bottomLimit];
    }
    else
    {
        [self.editingFilter removeConstraintOfType:CRConstraintTypeMoreOrEqual forKey:self.propertyName];
    }
    
}

@end
