//
//  CRFilterOrderedValuesController.m
//  Slayt
//
//  Created by Vlad Soroka on 6/3/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterOrderedValuesController.h"
#import "CRBaseFilterEditingController_protected.h"
#import "CRProfileTypeStorage.h"
#import "CRUtils.h"

@interface CRFilterOrderedValuesController ()

///Most of ordered properties start with NotSpecified enumarationItem. Since we don't want "Not specified" value occure in the list of avaliable options we'll eject it form the list. The flag shows whether or not we've ejected "Not specified" item
@property (nonatomic, assign) BOOL didEjectDefaultValue;

@end

@implementation CRFilterOrderedValuesController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    Class ProfileTypeClass = [[[CRProfileTypeStorage alloc] init] profileTypeClassForPrefixedKey:self.propertyName];
    NSString* stringsArrayName = [NSString stringWithFormat:@"%@Strings",[[CRUtils sharedInstance] substringOfString:self.propertyName
                                                                                                       withoutPrefix:[[ProfileTypeClass object] profilePrefix]]];


    SEL selector = NSSelectorFromString(stringsArrayName);
    IMP imp = [self.editingFilter.queryingUser methodForSelector:selector];
    NSArray* (*func)(id, SEL) = (void *)imp;
    NSArray *result = func(self.editingFilter.queryingUser, selector);

    ///this will fail to work if the default value is not "Not Specified".
    if([result.firstObject isEqualToString:@"Not Specified"])
    {
        NSMutableArray* temp = [result mutableCopy];
        [temp removeObjectAtIndex:0];
        result = temp;
        
        self.didEjectDefaultValue = YES;
    }
    
    self->_choiseOptions = result;
    self->_chosenOptions = [NSMutableArray array];
    
    //this array is consisted from numbers which indicate the selected values
    NSArray* chosenValueArray = [self.editingFilter valueFromConstraintType:CRConstraintTypeContainsObject
                                                                     forKey:self.propertyName];
    
    [chosenValueArray enumerateObjectsUsingBlock:^(NSNumber* value, NSUInteger idx, BOOL *stop) {
        NSUInteger orderedValue = value.unsignedIntegerValue;
        if(self.didEjectDefaultValue)
        {
            orderedValue --;
        }
        
        [self->_chosenOptions addObject:[result objectAtIndex:orderedValue]];
    }];
    
    [self->_tableView reloadData];
}

- (void)applyFilters
{
    NSMutableArray* resultingArray = [NSMutableArray array];
    
    [self->_chosenOptions enumerateObjectsUsingBlock:^(NSString* option, NSUInteger idx, BOOL *stop) {
        NSUInteger orderedNumber = [self->_choiseOptions indexOfObject:option];
        if(self.didEjectDefaultValue)
        {
            orderedNumber ++;
        }
        
        NSNumber* orderedValue = @(orderedNumber);
        
        [resultingArray addObject:orderedValue];
    }];
    
    [self.editingFilter setConstraintOfType:CRConstraintTypeContainsObject
                                     forKey:self.propertyName
                                      value:resultingArray];
}


@end
