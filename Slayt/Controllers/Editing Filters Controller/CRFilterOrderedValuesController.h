//
//  CRFilterOrderedValuesController.h
//  Slayt
//
//  Created by Vlad Soroka on 6/3/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterMultipleOptionController.h"

///the only reason to create a separete class for ordered properties - assumption that we can use Less and More queries on them. Since we are not implemeting this feature it might be a good idea to remove ALL Ordered values code and unite these properies with a simple String Value properties like HairColor of CRModel
@interface CRFilterOrderedValuesController : CRFilterMultipleOptionController

@end
