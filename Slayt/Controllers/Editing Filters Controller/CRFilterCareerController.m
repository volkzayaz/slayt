//
//  CRFilterCareerController.m
//  Slayt
//
//  Created by Vlad Soroka on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterCareerController.h"
#import "CRBaseFilterEditingController_protected.h"
#import "CRProfileTypeStorage.h"

@interface CRFilterCareerController ()

@end

@implementation CRFilterCareerController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSArray* allProfileTypes = [[[CRProfileTypeStorage alloc] init] stringsArrayOfAllProfilesExclude:@[@(CRProfileTypeEmployer)]];
    self.choiseOptions = allProfileTypes;
    
    [self->_tableView reloadData];
}

- (void)applyFilters
{
    if(self->_chosenOption)
    {
        [self.editingFilter setConstraintOfType:CRConstraintTypeProfileType
                                         forKey:self.propertyName
                                          value:self->_chosenOption];
    }
    else
    {
        [self.editingFilter removeConstraintOfType:CRConstraintTypeProfileType
                                            forKey:self.propertyName];
    }

}

@end
