//
//  CRFilterSearchRange.m
//  Slayt
//
//  Created by Vlad Soroka on 6/10/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterSearchRange.h"
#import "CRBaseFilterEditingController_protected.h"

@interface CRFilterSearchRange ()

@property (nonatomic, strong) NSArray* rangesInMiles;

@end

@implementation CRFilterSearchRange

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.rangesInMiles = @[@25,
                           @50,
                           @100,
                           @200];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSUInteger indexOfPreChosenObject = [self.rangesInMiles indexOfObject:self->_chosenOption];
    
    if(indexOfPreChosenObject != NSNotFound)
    {
        self->_chosenOption = self.choiseOptions[indexOfPreChosenObject];
    }
    
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if(status != kCLAuthorizationStatusAuthorized)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"You need to enable location services in order to use search range filtering" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.editingFilter.queryingUser.locationMonitor startMonitoring];
    }
}

- (void)applyFilters
{
    NSUInteger indexOfChosenObject = [self.choiseOptions indexOfObject:self->_chosenOption];
    
    if(indexOfChosenObject != NSNotFound)
    {
        NSNumber* searchRange = self.rangesInMiles[indexOfChosenObject];
        [self.editingFilter setConstraintOfType:CRConstraintTypeSearchRange
                                         forKey:self.propertyName
                                          value:searchRange];
    }
    else
    {
        [self.editingFilter removeConstraintOfType:CRConstraintTypeSearchRange
                                            forKey:self.propertyName];
    }
}

@end
