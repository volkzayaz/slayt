//
//  CRBaseFilterEditingController_protected.h
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRBaseFilterEditingController.h"

@interface CRBaseFilterEditingController ()

/**
 *  @discussion This mehtod is called by CRBaseFilterEditingController. Here your subclass should apply any filters that were set by user. You can use |propertyName| and |filter| properties of CRBaseFilterEditingController for this goal.
 */
- (void)applyFilters;

@end
