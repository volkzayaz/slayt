//
//  CRBaseFilterRangeController.h
//  Slayt
//
//  Created by Vlad Soroka on 6/5/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRBaseFilterEditingController.h"
#import "RangeSlider.h"

@interface CRBaseFilterRangeController : CRBaseFilterEditingController

@property (nonatomic, weak, readonly) RangeSlider* rangeSlider;

@property (nonatomic, strong) NSNumber* lowerSelectedValue;
@property (nonatomic, strong) NSNumber* upperSelectedValue;

@end
