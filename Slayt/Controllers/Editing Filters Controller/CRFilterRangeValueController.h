//
//  CRFilterRangeValueController.h
//  Slayt
//
//  Created by Vlad Soroka on 5/30/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRBaseFilterRangeController.h"
#import "RangeSlider.h"

@interface CRFilterRangeValueController : CRBaseFilterRangeController

@end
