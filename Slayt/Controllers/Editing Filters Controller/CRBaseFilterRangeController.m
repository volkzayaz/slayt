//
//  CRBaseFilterRangeController.m
//  Slayt
//
//  Created by Vlad Soroka on 6/5/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//
#import "CRBaseFilterRangeController.h"
#import "CRBaseFilterEditingController_protected.h"

#define DEFAULT_BOTTOM_VALUE 0
#define DEFAULT_UPPER_VALUE 100

@interface CRBaseFilterRangeController ()

@property (nonatomic, weak) UILabel* bottomLimitLabel;
@property (nonatomic, weak) UILabel* upperLimitLabel;

@property (nonatomic, strong) NSString* trailingSymbols;

@end

@implementation CRBaseFilterRangeController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIColor* indicatorsColor = [UIColor colorWithRed:128./255. green:127./255. blue:176./255. alpha:1.];
    
    UIView* whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, self.view.bounds.size.width, 100)];
    whiteView.backgroundColor = [UIColor whiteColor];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(17, 5, 100, 30)];
    titleLabel.textColor = [UIColor colorWithRed:89./255. green:89./255. blue:89./255. alpha:1.];
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.text = self.navigationItem.title;
    
    [whiteView addSubview:titleLabel];
    
    ///BottomLabel
    UILabel* bottomLab = [[UILabel alloc] initWithFrame:CGRectMake(40, 40, 40, 40)];
    bottomLab.textAlignment = NSTextAlignmentCenter;
    bottomLab.textColor = indicatorsColor;
    bottomLab.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    [whiteView addSubview:bottomLab];
    self.bottomLimitLabel = bottomLab;
    
    ///UpperLabel
    UILabel* upperLab = [[UILabel alloc] initWithFrame:CGRectMake(240, 40, 40, 40)];
    upperLab.textAlignment = NSTextAlignmentCenter;
    upperLab.textColor = indicatorsColor;
    upperLab.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    [whiteView addSubview:upperLab];
    
    self.upperLimitLabel = upperLab;
    
    ///range slider
    
    //if user selects bottom number on the filter, than we'll remove constraint "more than bottom limit". The same applied to the upperLimit
    NSNumber* bottomLimit = self.entityProperties[self.entityIndexPath.section][@"properties"][self.entityIndexPath.row][@"bottomFilterLimit"];
    if(!bottomLimit)
    {
        bottomLimit = @(DEFAULT_BOTTOM_VALUE);
        NSLog(@"WARNING: Bottom limit for Filtering %@ property is not determined0",self.propertyName);
    }
    
    NSNumber* upperLimit = self.entityProperties[self.entityIndexPath.section][@"properties"][self.entityIndexPath.row][@"upperFilterLimit"];
    if(!upperLimit)
    {
        upperLimit = @(DEFAULT_UPPER_VALUE);
        NSLog(@"WARNING: Upper limit for Filtering %@ property is not determined",self.propertyName);
    }
    
    self.trailingSymbols = self.entityProperties[self.entityIndexPath.section][@"properties"][self.entityIndexPath.row][@"trailingSymbols"];
    if(!self.trailingSymbols)
    {
        self.trailingSymbols = @"";
    }
    
    CGRect bounds = CGRectMake(0.0f, 10.0f, self.view.bounds.size.width, 100);
    RangeSlider *slider =  [[RangeSlider alloc] initWithFrame:bounds];
    slider.minimumValue = bottomLimit.floatValue;
    slider.maximumValue = upperLimit.floatValue;
    
    slider.minimumRange = 1;
    
    [slider addTarget:self action:@selector(updateRangeLabel:) forControlEvents:UIControlEventValueChanged];
    [whiteView addSubview:slider];
    
    _rangeSlider = slider;
    
    [self.view addSubview:whiteView];
}

- (void)viewWillAppear:(BOOL)animated
{
    if(!self.lowerSelectedValue)
    {
        self.lowerSelectedValue = @(self.rangeSlider.minimumValue);
    }
    
    if(!self.upperSelectedValue)
    {
        self.upperSelectedValue = @(self.rangeSlider.maximumValue);
    }
    
    [self setMaximumSelectedRange:self.upperSelectedValue];
    [self setMinimumSelectedRange:self.lowerSelectedValue];
    
#warning call code from viewWillAppear after RangleSlider layouts its' subviews or move indicatorLabels to RangeSlider
    self.upperLimitLabel.hidden = YES;
    self.bottomLimitLabel.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self moveUpperLabel];
    [self moveBottomLabel];
}

- (void)updateRangeLabel:(id)sender
{
    if(sender == self.rangeSlider)
    {
        [self setMaximumSelectedRange:@(self.rangeSlider.selectedMaximumValue)];
        [self setMinimumSelectedRange:@(self.rangeSlider.selectedMinimumValue)];
    }
}

#pragma mark - private methods

- (void) setMaximumSelectedRange:(NSNumber*)number
{
    if(!number || number.floatValue > self.rangeSlider.maximumValue)
    {
        self.rangeSlider.selectedMaximumValue = self.rangeSlider.maximumValue;
        number = @(self.rangeSlider.maximumValue);
    }
    else
    {
        self.rangeSlider.selectedMaximumValue = number.floatValue;
    }
    
    NSString* labelText = @(number.integerValue).stringValue;
    if(self.rangeSlider.selectedMaximumValue == self.rangeSlider.maximumValue)
    {
        labelText = [NSString stringWithFormat:@"> %@",labelText];
    }
    
    self.upperLimitLabel.text = [labelText stringByAppendingString:self.trailingSymbols];
    
    [self moveUpperLabel];
}

- (void) setMinimumSelectedRange:(NSNumber*)number
{
    if(!number || number.floatValue < self.rangeSlider.minimumValue)
    {
        self.rangeSlider.selectedMinimumValue = self.rangeSlider.minimumValue;
        number = @(self.rangeSlider.minimumValue);
    }
    else
    {
        self.rangeSlider.selectedMinimumValue = number.floatValue;
    }
    
    NSString* labelText = @(number.integerValue).stringValue;
    if(self.rangeSlider.selectedMinimumValue == self.rangeSlider.minimumValue)
    {
        labelText = [NSString stringWithFormat:@"< %@",labelText];
    }
    
    self.bottomLimitLabel.text = [labelText stringByAppendingString:self.trailingSymbols];
    
    [self moveBottomLabel];
}

- (void)moveBottomLabel
{
    [self.bottomLimitLabel sizeToFit];
    CGPoint newCenter = CGPointMake(self.rangeSlider.minThumb.center.x,
                                    self.rangeSlider.minThumb.center.y - 15);
    self.bottomLimitLabel.center = newCenter;
    
    
    self.bottomLimitLabel.hidden = NO;
}

- (void) moveUpperLabel
{
    [self.upperLimitLabel sizeToFit];
    CGPoint newCenter = CGPointMake(self.rangeSlider.maxThumb.center.x,
                                    self.rangeSlider.maxThumb.center.y - 15);
    self.upperLimitLabel.center = newCenter;
    
    self.upperLimitLabel.hidden = NO;
}


@end

