//
//  CRMainController.h
//  Talent
//
//  Created by Vlad Soroka on 4/1/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRHomeViewController : UIViewController

@property (nonatomic, assign) BOOL showsLoginScreenAnimated;

@end
