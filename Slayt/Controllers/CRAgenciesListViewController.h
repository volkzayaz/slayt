//
//  CRAgenciesListViewController.h
//  Slayt
//
//  Created by Dmitry Utenkov on 6/4/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRAgenciesListViewController : UITableViewController

@property (nonatomic, copy) NSString *selectedAgencyName;

@end
