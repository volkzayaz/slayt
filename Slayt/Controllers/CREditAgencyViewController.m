//
//  CREditAgencyViewController.m
//  Slayt
//
//  Created by Dmitry Utenkov on 5/28/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CREditAgencyViewController.h"
#import "CRTextFieldTableViewCell.h"
#import "CRUtils.h"
#import "CRUser.h"
#import "CRAgenciesListViewController.h"
#import <Parse/PF_MBProgressHUD.h>

@interface CREditAgencyViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong, readwrite) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *items;

@property (nonatomic, strong) UITextField *agentNameTextField;
@property (nonatomic, strong) UITextField *agentPhoneTextField;
@property (nonatomic, strong) UITextField *agentEmailTextField;

@property (nonatomic, strong) IBOutlet UIBarButtonItem *signInButton;

@property (nonatomic, strong) IBOutlet UITableViewCell *agencyNameCell;

@property (nonatomic, assign, readonly, getter = isAgencyNameSelected) BOOL agencyNameSelected;
@property (nonatomic, assign, readonly, getter = isTextFieldsFilled) BOOL textFieldsFilled;

@property (nonatomic, strong) PF_MBProgressHUD *hud;

@end

@implementation CREditAgencyViewController

- (void)awakeFromNib {
    self.items = @[ @{ @"Placeholder"       : @"Name of Agency",
                       @"CellIdentifier"    : @"AgencyNameCell" },
                    
                    @{ @"Placeholder"       : @"Name of Agent",
                       @"CellIdentifier"    : @"TextCell" },
                    
                    @{ @"Placeholder"       : @"Agent Phone",
                       @"CellIdentifier"    : @"PhoneCell" },
                    
                    @{ @"Placeholder"       : @"Agent E-mail",
                       @"CellIdentifier"    : @"EmailCell" } ];
}

- (BOOL)isAgencyNameSelected {
    return ![self.agencyNameCell.detailTextLabel.text isEqualToString:@"Not Specified"];
}

- (BOOL)isTextFieldsFilled {
    return  self.agentEmailTextField.text.length != 0 &&
            self.agentPhoneTextField.text.length != 0 &&
            self.agentEmailTextField.text.length != 0 &&
            self.isAgencyNameSelected;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // For keyboard dismissal on touch
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    self.hud = [[PF_MBProgressHUD alloc] initWithView:self.view];
    self.hud.labelText = @"Submitting...";
    [self.view addSubview:self.hud];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.signInButton.enabled = self.isTextFieldsFilled;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AgencyListSegue"]) {
        [self.view endEditing:YES];
        CRAgenciesListViewController *agencyListVC = segue.destinationViewController;
        if (self.isAgencyNameSelected) {
            agencyListVC.selectedAgencyName = self.agencyNameCell.detailTextLabel.text;
        }
    }
}

#pragma mark - UITableViewDataSource protocol implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id returnCell = nil;
    if (! [self.items[indexPath.row][@"CellIdentifier"] isEqualToString:@"AgencyNameCell"] ) {
        CRTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.items[indexPath.row][@"CellIdentifier"]];
        cell.textField.placeholder = self.items[indexPath.row][@"Placeholder"];
        
        switch (indexPath.row) {
            case 1:
                self.agentNameTextField = cell.textField;
                self.agentNameTextField.text = [CRUser currentUser].agentName;
                break;
            case 2:
                self.agentPhoneTextField = cell.textField;
                self.agentPhoneTextField.text = [CRUser currentUser].agentPhone;
                break;
            case 3:
                self.agentEmailTextField = cell.textField;
                self.agentEmailTextField.text = [CRUser currentUser].agentEmail;
                break;
            default:
                break;
        }
        returnCell = cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.items[indexPath.row][@"CellIdentifier"]];
        self.agencyNameCell = cell;
        returnCell = cell;
        if ([CRUser currentUser].agencyName.length > 0) {
            cell.textLabel.text = [CRUser currentUser].agencyName;
            cell.textLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:89.0f/255.0f blue:89.0f/255.0f alpha:1.0f];
            cell.detailTextLabel.text = @"";
        }
    }
    
    return returnCell;
}

#pragma mark - UITableViewDelegate protocol implmentation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self dismissKeyboard:nil];
}

#pragma mark - UITextFieldDelegate protocol implementation

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField == self.agentNameTextField) {
        [self.agentPhoneTextField becomeFirstResponder];
    } else if (textField == self.agentEmailTextField && self.isTextFieldsFilled) {
        [self signInToAgency];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.text.length == 1 && string.length == 0) {
        self.signInButton.enabled = NO;
    } else {
        self.signInButton.enabled = self.isTextFieldsFilled;
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    self.signInButton.enabled = NO;
    return YES;
}

#pragma mark UIGestureRecognizerDelegate protocol implementation

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ! [touch.view isDescendantOfView:self.tableView];
}

#pragma mark - Actions

- (void)dismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)signInButtonTapped:(id)sender {
    [self signInToAgency];
}

#pragma mark - Utility methods

- (void)signInToAgency {
    [self dismissKeyboard:nil];
    [self.hud show:YES];
    
    NSDictionary *params = @{ @"agentEmail" : self.agentEmailTextField.text,
                              @"agencyName" : self.agencyNameCell.textLabel.text,
                              @"talentName" : [CRUser currentUser].fullName };
    
    [PFCloud callFunctionInBackground:@"submitTalentToAgency"
                       withParameters:params
                                block:^(NSArray *results, NSError *error) {
                                    if (error) {
                                        NSString *errorMessage = [NSString stringWithFormat:@"Sign in to Agency error: %@", error.localizedDescription];
                                        [[CRUtils sharedInstance] showAlertWithMessage:@"Error occured. Try again later or contact technical support"];
                                        NSLog(@"%@", errorMessage);
                                    } else {
                                        [CRUser currentUser].agencyName = self.agencyNameCell.textLabel.text;
                                        [CRUser currentUser].agentName = self.agentNameTextField.text;
                                        [CRUser currentUser].agentPhone = self.agentPhoneTextField.text;
                                        [CRUser currentUser].agentEmail = self.agentEmailTextField.text;
                                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            [[CRUser currentUser] save];
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [self.navigationController popViewControllerAnimated:YES];
                                                [[CRUtils sharedInstance] showAlertWithMessage:@"The request was sent to your talent agency. If the agency does not respond, we will remove the agency from your profile info."];
                                            });
                                        });
                                    }
                                    [self.hud hide:YES];
                                }];
}

- (IBAction)didAgencyNameSelected:(UIStoryboardSegue *)segue {
    self.agencyNameCell.detailTextLabel.text = @"";
    self.agencyNameCell.textLabel.text = [segue.sourceViewController selectedAgencyName];
    self.agencyNameCell.textLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:89.0f/255.0f blue:89.0f/255.0f alpha:1.0f];
}



@end
