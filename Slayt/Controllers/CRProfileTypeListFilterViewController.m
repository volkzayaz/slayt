//
//  CRProfileTypeListFilterViewController.m
//  Slayt
//
//  Created by Dmitry Utenkov on 6/10/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRProfileTypeListFilterViewController.h"
#import "CRProfileTypeStorage.h"
#import "UIColorFromRGB.h"

@interface CRProfileTypeListFilterViewController ()

@property (nonatomic, strong) NSArray *items;

@property (nonatomic, strong) NSIndexPath *selectedCellPath;

@end

@implementation CRProfileTypeListFilterViewController

- (NSIndexPath *)selectedCellPath {
    if (self.selectedProfileType.length == 0) {
        return [NSIndexPath indexPathForRow:NSNotFound inSection:0];
    } else if ([self.selectedProfileType isEqualToString:@"All Messages"]) {
        return [NSIndexPath indexPathForRow:0 inSection:0];
    } else {
        return [NSIndexPath indexPathForRow:[self.items indexOfObject:self.selectedProfileType] inSection:1];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.items = [[[CRProfileTypeStorage alloc] init] stringsArrayOfAllProfilesExclude:nil];
    
    if (!self.selectedCellPath) {
        self.selectedCellPath = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - Actions

- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([self.delegate respondsToSelector:@selector(profileFilter:didSelectProfileType:)]) {
        [self.delegate profileFilter:self didSelectProfileType:self.selectedProfileType];
    }
}

#pragma mark - UITableViewDataSource protocol implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section == 0 ? 1 : self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileTypeListCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ProfileTypeListCell"];
        cell.textLabel.textColor = UIColorFromRGBHex(0x595959);
    }
    if (indexPath.section == 0) {
        cell.textLabel.text = @"All Messages";
    } else {
        cell.textLabel.text = self.items[indexPath.row];
    }
    
    if ([indexPath isEqual:self.selectedCellPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate protocol implementation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if ([self.selectedCellPath isEqual:indexPath]) {
        return;
    }
    
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:self.selectedCellPath];
    oldCell.accessoryType = UITableViewCellAccessoryNone;
    
    if ([self.items containsObject:newCell.textLabel.text]) {
        self.selectedProfileType = self.items[indexPath.row];
    } else {
        self.selectedProfileType = @"All Messages";
    }
}

@end
