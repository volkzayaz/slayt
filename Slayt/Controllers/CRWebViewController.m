//
//  CRWebViewController.m
//  Slayt
//
//  Created by Dmitry Utenkov on 5/28/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRWebViewController.h"

@interface CRWebViewController ()

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (strong) NSMutableArray *castings;

@end

@implementation CRWebViewController

- (void)setHtmlPath:(NSURL *)htmlPath {
    _htmlPath = [htmlPath copy];
    NSError *error = nil;
    NSString *privacyPolicyText = [NSString stringWithContentsOfURL:self.htmlPath
                                                           encoding:NSUTF8StringEncoding
                                                              error:&error];
    if (!error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.webView loadHTMLString:privacyPolicyText baseURL:nil];
        });
    }
}

@end
