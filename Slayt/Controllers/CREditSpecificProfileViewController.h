//
//  CREditModelViewController.h
//  Talent
//
//  Created by Dmitry Utenkov on 4/14/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRUser.h"

@interface CREditSpecificProfileViewController : UITableViewController

@property (nonatomic, strong) CRUser *editingUser;
@property (nonatomic, strong) PFObject<CRProfileProtocol>* specificProfile;

@end
