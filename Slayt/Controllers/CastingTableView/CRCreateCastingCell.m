//
//  CRCreateCastingCell.m
//  Slayt
//
//  Created by Filipp Duhov on 6/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRCreateCastingCell.h"

@implementation CRCreateCastingCell

- (void)awakeFromNib {
    self.createCastingButton.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
