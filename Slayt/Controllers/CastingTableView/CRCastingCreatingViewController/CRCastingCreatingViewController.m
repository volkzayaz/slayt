//
//  CRCastingCreatingViewController.m
//  Slayt
//
//  Created by Filipp Duhov on 6/20/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRCastingCreatingViewController.h"
#import "CREditableTableViewCell.h"
#import "CREditableMultilineTableViewCell.h"
#import "CRDateAndTimeTableViewCell.h"
#import "CRChooseTableViewCell.h"
#import "CRPushButtonTableViewCell.h"
#import "CRChooseLocationController.h"
#import "CRCasting.h"

@interface CRCastingCreatingViewController () <CRBaseEditValuesProtocol>

@property (weak) UITextView *descriptionTextView;
@property (strong) CRCasting *castring;

@end

@implementation CRCastingCreatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSLog(@"%@",self.castring);
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    NSLog(@"dismissKeyboard");
    [self.view endEditing:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
        case 3:
            return 1;
        case 2:
            return 4;
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    switch (indexPath.section) {
        case 0:
            cell = [tableView dequeueReusableCellWithIdentifier:editableCellIdentifier forIndexPath:indexPath];
            break;
        case 1:
            cell = [tableView dequeueReusableCellWithIdentifier:editableMultilineCellIdentifier forIndexPath:indexPath];
            self.descriptionTextView=[(CREditableMultilineTableViewCell *)cell contentTextView];
            break;
        case 2:
            if (indexPath.row==0) {
                cell = [tableView dequeueReusableCellWithIdentifier:dateAndTimeCellIdentifier forIndexPath:indexPath];
                CRDateAndTimeTableViewCell *dataAndTimeCell=(CRDateAndTimeTableViewCell *)cell;
                NSDate *startsDate=[NSDate date];
                NSDate *endsDate=[NSDate dateWithTimeIntervalSince1970:0];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
                NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
                [timeFormatter setDateFormat:@"h:mm a"];
                
                dataAndTimeCell.startsDate.text=[dateFormatter stringFromDate:startsDate];
                dataAndTimeCell.startsTime.text=[timeFormatter stringFromDate:startsDate];
                dataAndTimeCell.endsDate.text=[dateFormatter stringFromDate:endsDate];
                dataAndTimeCell.endsTime.text=[timeFormatter stringFromDate:endsDate];
            } else {
                NSString *valueString=nil;
                NSString *cellIdentifier=nil;
                switch (indexPath.row) {
                    case 1:
                        valueString=@"New York";
                        cellIdentifier=chooseLocationCellIdentifier;
                        break;
                    case 2:
                        valueString=@"$25/h";
                        cellIdentifier=chooseRateCellIdentifier;
                        break;
                    case 3:
                        valueString=@"Not Specified";
                        cellIdentifier=chooseUsageCellIdentifier;
                        break;
                    default:
                        break;
                }
                cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                CRChooseTableViewCell *chooseCell=(CRChooseTableViewCell *)cell;
                [chooseCell.value setText:valueString];
                [chooseCell.value sizeToFit];
            }
            break;
        case 3:
            cell = [tableView dequeueReusableCellWithIdentifier:pushCellIdentifier forIndexPath:indexPath];
            break;
        default:
            break;
    }    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"PROJECT TITLE";
        case 1:
            return @"PROJECT DESCRIPTION";
        case 2:
            return @"DETAILS";
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        CGSize size = [self.descriptionTextView sizeThatFits:self.descriptionTextView.frame.size];
        if (size.height) {
            return size.height+10;
        }
    }
    if (indexPath.section == 2&&indexPath.row==0) {
        return 89;
    }
    if (indexPath.section == 3) {
        return 90;
    }
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==3) {
        return 0;
    }
    return 35;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section==3) return [UIView new];
    UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, [self tableView:tableView heightForHeaderInSection:section])];
    UILabel *headerLabel=[[UILabel alloc] initWithFrame:CGRectZero];
    [headerLabel setText:[self tableView:tableView titleForHeaderInSection:section]];
    [headerLabel sizeToFit];
    headerLabel.frame=({
        CGRect frame=headerLabel.frame;
        frame.origin.x=15;
        frame.origin.y=12;
        frame;
    });
    [headerLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    [headerLabel setTextColor:[UIColor colorWithRed:128.0/256 green:127.0/256 blue:176.0/256 alpha:1]];
    [headerView addSubview:headerLabel];
    return headerView;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CREditableMultilineTableViewCell *cell = (CREditableMultilineTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
        case 1:
            [cell.contentTextView becomeFirstResponder];
            break;
        case 2:
            switch (indexPath.row) {
                case 0:
                    break;
                case 1: {
                    CRChooseLocationController* editLocation = [[CRChooseLocationController alloc] init];
                    
//                    editLocation.editingObject = self.editingUser;
                    editLocation.propertyName = @"homeTown";
//                    editLocation.initialLocation = self.editingUser.homeTown;
                    editLocation.navigationItem.title = @"Location";
                    
                    [self.navigationController pushViewController:editLocation animated:YES];
                }
                default:
                    break;
            }
        default:
            break;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CRBaseEditValuesProtocol
- (void)editValueController:(CRBaseEditValuesController *)controller didEditValue:(id)value {
    
}

@end
