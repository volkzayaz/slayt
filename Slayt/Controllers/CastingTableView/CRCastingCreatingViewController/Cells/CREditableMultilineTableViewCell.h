//
//  CREditableMultilineTableViewCell.h
//  Slayt
//
//  Created by Filipp Duhov on 6/23/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CREditableTableViewCell.h"

#define editableMultilineCellIdentifier @"EditableMultiline"

@interface CREditableMultilineTableViewCell : CREditableTableViewCell

@property (assign) IBOutlet UITableView *superTableView;

@end
