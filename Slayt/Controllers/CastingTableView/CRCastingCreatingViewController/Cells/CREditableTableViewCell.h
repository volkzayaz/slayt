//
//  CREditableTableViewCell.h
//  Slayt
//
//  Created by Filipp Duhov on 6/20/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

#define editableCellIdentifier @"Editable"

@interface CREditableTableViewCell : UITableViewCell <UITextViewDelegate>

@property (assign) IBOutlet UITextView *contentTextView;

@end
