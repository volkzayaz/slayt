//
//  CRChooseTableViewCell.h
//  Slayt
//
//  Created by Filipp Duhov on 6/20/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

#define chooseLocationCellIdentifier @"ChooseLocation"
#define chooseRateCellIdentifier @"ChooseRate"
#define chooseUsageCellIdentifier @"ChooseUsage"

@interface CRChooseTableViewCell : UITableViewCell

@property (weak) IBOutlet UILabel *title;
@property (weak) IBOutlet UILabel *value;

@end
