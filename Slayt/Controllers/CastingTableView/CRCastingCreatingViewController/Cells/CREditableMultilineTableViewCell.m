//
//  CREditableMultilineTableViewCell.m
//  Slayt
//
//  Created by Filipp Duhov on 6/23/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CREditableMultilineTableViewCell.h"

@implementation CREditableMultilineTableViewCell

- (void)textViewDidChange:(UITextView *)textView {
    [self.superTableView beginUpdates];
    [self.superTableView endUpdates];
    [self scrollToCursorForTextView:textView];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

- (void)scrollToCursorForTextView:(UITextView*)textView {
    CGRect cursorRect = [textView caretRectForPosition:textView.selectedTextRange.start];
    cursorRect = [self.superTableView convertRect:cursorRect fromView:textView];
    if (![self rectVisible:cursorRect]) {
        cursorRect.size.height += 8; // To add some space underneath the cursor
        [self.superTableView scrollRectToVisible:cursorRect animated:YES];
    }
}

- (BOOL)rectVisible:(CGRect)rect {
    CGRect visibleRect;
    visibleRect.origin = self.superTableView.contentOffset;
    visibleRect.origin.y += self.superTableView.contentInset.top;
    visibleRect.size = self.superTableView.bounds.size;
    visibleRect.size.height -= self.superTableView.contentInset.top + self.superTableView.contentInset.bottom;
    
    return CGRectContainsRect(visibleRect, rect);
}

@end
