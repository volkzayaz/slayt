//
//  CRDateAndTimeTableViewCell.h
//  Slayt
//
//  Created by Filipp Duhov on 6/20/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

#define dateAndTimeCellIdentifier @"DateAndTime"

@interface CRDateAndTimeTableViewCell : UITableViewCell

@property (weak) IBOutlet UILabel *startsDate;
@property (weak) IBOutlet UILabel *startsTime;
@property (weak) IBOutlet UILabel *endsDate;
@property (weak) IBOutlet UILabel *endsTime;

@end
