//
//  CRPushButtonTableViewCell.h
//  Slayt
//
//  Created by Filipp Duhov on 6/20/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

#define pushCellIdentifier @"Push"

@interface CRPushButtonTableViewCell : UITableViewCell

@property (weak) IBOutlet UIButton *publishButton;

@end
