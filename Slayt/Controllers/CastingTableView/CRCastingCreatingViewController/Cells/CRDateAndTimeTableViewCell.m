//
//  CRDateAndTimeTableViewCell.m
//  Slayt
//
//  Created by Filipp Duhov on 6/20/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRDateAndTimeTableViewCell.h"

@implementation CRDateAndTimeTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
