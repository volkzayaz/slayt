//
//  CRPushButtonTableViewCell.m
//  Slayt
//
//  Created by Filipp Duhov on 6/20/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRPushButtonTableViewCell.h"

@implementation CRPushButtonTableViewCell

- (void)awakeFromNib {
    self.publishButton.layer.cornerRadius = 3;
}

@end
