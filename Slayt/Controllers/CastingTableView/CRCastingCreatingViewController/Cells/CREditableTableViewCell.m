//
//  CREditableTableViewCell.m
//  Slayt
//
//  Created by Filipp Duhov on 6/20/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CREditableTableViewCell.h"

#define MAX_NUMBER_OF_LINES_ALLOWED 1

@implementation CREditableTableViewCell

- (void)awakeFromNib {
    self.contentTextView.text=@"Not Specified";
    self.contentTextView.textContainer.lineFragmentPadding = 0;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@"Not Specified"]) {
        textView.text=@"";
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text=@"Not Specified";
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSMutableString *string = [NSMutableString stringWithString:textView.text];
    [string replaceCharactersInRange:range withString:text];
    
    NSUInteger numberOfLines = 0;
    for (NSUInteger i = 0; i < string.length; i++) {
        if ([[NSCharacterSet newlineCharacterSet] characterIsMember:[string characterAtIndex: i]]) {
            numberOfLines++;
        }
    }
    return numberOfLines < MAX_NUMBER_OF_LINES_ALLOWED;
}

@end
