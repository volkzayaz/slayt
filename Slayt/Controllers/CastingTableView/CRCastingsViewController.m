//
//  CRCastingsViewController.m
//  Slayt
//
//  Created by Filipp Duhov on 6/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRCastingsViewController.h"
#import "SWRevealViewController.h"
#import <MessageUI/MessageUI.h>
#import "CRProfileProtocol.h"
#import "CRUserDefines.h"

@interface CRCastingsViewController () <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

@property (assign) BOOL isCreateCastingButtonVisible;

@end

@implementation CRCastingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)setProfileTypes:(NSArray *)profileTypes {
    if (profileTypes!=_profileTypes) {
        _profileTypes=profileTypes;
        self.isCreateCastingButtonVisible=NO;
        __block CRCastingsViewController *weakSelf=self;
        [profileTypes enumerateObjectsUsingBlock:^(id<CRProfileProtocol> obj, NSUInteger idx, BOOL *stop) {
            if([obj profileType] == CRProfileTypeEmployer) {
                weakSelf.isCreateCastingButtonVisible=YES;
                *stop = YES;
            }
        }];
    }
}

- (IBAction)messageButtonTapped:(id)sender {
    [self.revealViewController rightRevealToggleAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.isCreateCastingButtonVisible?1:0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    switch (indexPath.section) {
        case 0:
            cell = [tableView dequeueReusableCellWithIdentifier:@"CreateCasting" forIndexPath:indexPath];
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"CastingCreatingSegue" sender:self];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
