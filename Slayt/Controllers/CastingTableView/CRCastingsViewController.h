//
//  CRCastingsViewController.h
//  Slayt
//
//  Created by Filipp Duhov on 6/19/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRCastingsViewController : UITableViewController

@property (nonatomic,retain) NSArray *profileTypes;

@end
