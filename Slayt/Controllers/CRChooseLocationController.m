//
//  CRChooseLocationControllerViewController.m
//  Talent
//
//  Created by Vlad Soroka on 4/15/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRChooseLocationController.h"
#import "CRBaseEditController_Protected.h"
#import <objc/runtime.h>
#import <Parse/PF_MBProgressHUD.h>

#define PLACEMARK_ASSOSIATED_KEY "placemarkKey"
#define PRESAVING_BLOCK_ASSOSIATED_KEY "blockKey"

@interface CRChooseLocationController () <UIAlertViewDelegate>

@property (nonatomic, strong) PF_MBProgressHUD* HUD;

@end

@implementation CRChooseLocationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIWindow* applicationWindow = [UIApplication sharedApplication].windows.firstObject;
    
    PF_MBProgressHUD* progressHUD = [[PF_MBProgressHUD alloc] initWithWindow:applicationWindow];
    [applicationWindow addSubview:progressHUD];
    self.HUD = progressHUD;
}

- (void)viewWillAppear:(BOOL)animated
{
    self->_textField.placeholder = @"Enter City and State";
    self->_textField.text = self.initialLocation;
}

- (void)preSavingActionWithSavingBlock:(SavingBlock)block
{
    [self->_textField resignFirstResponder];
    
    NSString* query = self->_textField.text;
    
    if(query.length == 0)
    {
        self->_textField.text = query;
        [super preSavingAction];
        block();
        return;
    }
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    self.HUD.labelText = [NSString stringWithFormat:@"Searching for %@",query];
    [self.HUD show:YES];
    [geocoder geocodeAddressString:query completionHandler:^(NSArray *placemarks, NSError *error) {
        [self.HUD hide:YES];
        if(error)
        {
            switch (error.code) {
                case kCLErrorGeocodeFoundNoResult:
                {
                    [[[UIAlertView alloc] initWithTitle:@"Ouch" message:[NSString stringWithFormat:@"We didn't find any locations related to %@. Try to be a bit more specific.",query] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                    return;
                }
                default:
                {
                    NSLog(@"Unhandled error on retreving geocoder info. Details - %@",error);
                    return ;
                }
            }
        }
        
        CLPlacemark* placemark = placemarks.firstObject;
        
        if(!placemark.locality || !placemark.administrativeArea)
        {
          [[[UIAlertView alloc] initWithTitle:@"Ouch" message:[NSString stringWithFormat:@"We didn't find the specific City and State related to %@. Try to be a bit more specific.",query] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            return;
        }
        
        NSString* foundLocation = [NSString stringWithFormat:@"%@, %@",placemark.locality, placemark.administrativeArea];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Location" message:[NSString stringWithFormat:@"Is this your location %@?",foundLocation] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        
        objc_setAssociatedObject(alertView, PLACEMARK_ASSOSIATED_KEY, placemark, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        objc_setAssociatedObject(alertView, PRESAVING_BLOCK_ASSOSIATED_KEY, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        
        [alertView show];
        
    }];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:// approve
        {
            CLPlacemark* placemark = objc_getAssociatedObject(alertView, PLACEMARK_ASSOSIATED_KEY);
            SavingBlock block = objc_getAssociatedObject(alertView, PRESAVING_BLOCK_ASSOSIATED_KEY);
            
            self->_textField.text = [NSString stringWithFormat:@"%@, %@",placemark.locality, placemark.administrativeArea];
            
            [super preSavingAction];//we are asking parent EditingController to handle homeTown property
            
            block();
            break;
        }
        case 0://cancel
        {
            [self->_textField becomeFirstResponder];
            
            break;
        }

        default:
            break;
    }
    
    objc_setAssociatedObject(alertView, PLACEMARK_ASSOSIATED_KEY, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    objc_setAssociatedObject(alertView, PRESAVING_BLOCK_ASSOSIATED_KEY, nil, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
