//
//  CRSignupViewController.m
//  Talent
//
//  Created by Dmitry Utenkov on 4/29/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRSignupViewController.h"
#import "CRTextFieldTableViewCell.h"
#import "CRUser.h"
#import "CRPhoto.h"
#import "CRUtils.h"
#import "CRProfileTypeViewController.h"
#import "CRRatingManager.h"

#import "CRFriendsManager.h"

#import <Parse/PF_MBProgressHUD.h>

@interface CRSignupViewController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSArray *items;

@property (nonatomic, strong) UITextField *activeField;

@property (nonatomic, strong) UITextField *nameTextField;
@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) NSArray *profileTypes;

@property (nonatomic, strong) PF_MBProgressHUD* HUD;

@property (nonatomic, weak) IBOutlet UIButton *signupButton;

@property (nonatomic) BOOL facebookRegistrationInProgress;

@end

@implementation CRSignupViewController

- (void)awakeFromNib {
    self.items = @[ @{ @"Placeholder"       : @"Name",
                       @"CellIdentifier"    : @"SignupTextCell" },
                    
                    @{ @"Placeholder"       : @"Email",
                       @"CellIdentifier"    : @"SignupEmailCell" },
                    
                    @{ @"Placeholder"       : @"Password",
                       @"CellIdentifier"    : @"SignupSecureCell" },
                    
                    @{ @"Placeholder"       : @"Profile Type",
                       @"CellIdentifier"    : @"SignupProfileTypeCell" } ];
    
    self.facebookRegistrationInProgress = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Adding underline to Login button
    NSDictionary *attributes = @{ NSForegroundColorAttributeName : self.loginButton.titleLabel.textColor };
    NSMutableAttributedString *loginButtonTitle = [[NSMutableAttributedString alloc] initWithString:self.loginButton.titleLabel.text
                                                                                         attributes:attributes];
    [loginButtonTitle addAttribute:NSUnderlineStyleAttributeName
                             value:@(NSUnderlineStyleSingle)
                             range:[loginButtonTitle.string rangeOfString:@"Log In"]];
    [self.loginButton setAttributedTitle:loginButtonTitle forState:UIControlStateNormal];
    
    // For keyboard dismissal on touch
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    self.HUD = [[PF_MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
    
}

- (void)keyboardWillBeShown:(NSNotification *)notification {
    CGSize kbSize = [notification.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSUInteger adjustmentHeight = self.loginButton.frame.origin.y - kbSize.height;
    adjustmentHeight /= [UIScreen mainScreen].scale;
    NSNumber *animationTime = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:animationTime.floatValue animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y -= adjustmentHeight;
        self.view.frame = frame;
    }];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    // Async allow to select Profile type cell
    // during keyboard hiding and frame change animations
    NSNumber *animationTime = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:animationTime.floatValue animations:^{
            CGRect frame = self.view.frame;
            NSUInteger origin = [[UIApplication sharedApplication] statusBarFrame].size.height + self.navigationController.navigationBar.frame.size.height;
            frame.origin.y = origin;
            self.view.frame = frame;
        }];
    });
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"LoginSegue"]) {
        [segue.destinationViewController navigationItem].leftBarButtonItem = nil;
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:nil
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:nil
                                                                      action:nil];
        
        self.navigationItem.backBarButtonItem = backButton;
    }
    if ([segue.identifier isEqualToString:@"ProfileTypeSegue"]) {
        [self.view endEditing:YES];
        CRProfileTypeViewController *profileTypesController = segue.destinationViewController;
        profileTypesController.delegate = self;
        profileTypesController.selectedItems = [NSMutableArray arrayWithArray:self.profileTypes];
    }
}

#pragma mark - Actions

- (IBAction)signupFacebookButtonTapped:(id)sender {
    self.facebookRegistrationInProgress = YES;
    [PFFacebookUtils initializeFacebook];
    [[CRUser currentUser] deleteInBackground];
    self.HUD.labelText = @"Connecting to Facebook";
    [self.HUD show:YES];
    
    NSArray* permissions = @[ @"basic_info", @"email", @"user_birthday" ];
    
    [PFFacebookUtils logInWithPermissions:permissions block:^(PFUser *user, NSError *error) {
        if (error && error.code != FBErrorLoginFailedOrCancelled) {
            NSLog(@"Error authorizing with facebook. Details - %@",error);
            NSString *message = [NSString stringWithFormat:@"Authorization error\n %@",[error localizedDescription]];
            [[CRUtils sharedInstance] showAlertWithMessage:message];
            [self.HUD hide:YES];
            return;
        }
        
        if (!user) {
            NSLog(@"The user cancelled the Facebook login.");
            NSString *message = [NSString stringWithFormat:@"Authorization error. Check your Facebook account settings..."];
            [[CRUtils sharedInstance] showAlertWithMessage:message];
            [self.HUD hide:YES];
            return;
        } else {
            
            [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (error) {
                    NSLog(@"Error requesting user's information. Details - %@",error);
                    [self.HUD hide:YES];
                    return;
                }
                
                FBGraphObject* responce = result;
                
                PFQuery *query = [CRUser query];
                [query whereKey:@"username" equalTo:[NSString stringWithString:responce[@"email"]]];
                [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    if (error) {
                        NSLog(@"%@", error.localizedDescription);
                        return;
                    } else {
                        // Fill Parse account crederntials
                        if (objects.count != 0) {
                            if ([PFFacebookUtils isLinkedWithUser:objects.firstObject]) {
                                [self successfullLoginAction];
                                return;
                            }
                        }
                        self.nameTextField.text = [NSString stringWithString:responce[@"name"]];
                        self.emailTextField.text = [NSString stringWithString:responce[@"email"]];
                        [self.passwordTextField becomeFirstResponder];
                    }
                }];
            }];
        }
        [self.HUD hide:YES];
    }];
}

- (IBAction)signupButtonTapped:(id)sender {
    [self signup];
}

- (void)signup {
    
    if ([self isEmptyField]) {
        [[CRUtils sharedInstance] showAlertWithMessage:@"Please fill out all fields"];
        return;
    }
    
    self.HUD.labelText = @"Creating user";
    [self.HUD show:YES];
    
    // Parse account signup
    if (!self.facebookRegistrationInProgress) {
        CRUser *newUser = [CRUser object];
        newUser.username = self.emailTextField.text;
        newUser.fullName = self.nameTextField.text;
        newUser.email = self.emailTextField.text;
        newUser.password = self.passwordTextField.text;
        [self.profileTypes enumerateObjectsUsingBlock:^(NSString* profile, NSUInteger idx, BOOL *stop) {
            [newUser.profileTypes addProfileForTypeName:profile];
        }];

        [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {

                [self successfullLoginAction];
            } else {
                NSString *errorMessage = error.localizedDescription;
                if (error.code == kPFErrorInvalidEmailAddress) {
                    errorMessage = @"Invalid Email";
                } else if (error.code == kPFErrorUserEmailTaken || error.code == kPFErrorUsernameTaken){
                    errorMessage = [NSString stringWithFormat:@"We're sorry! The username %@ is already taken", self.emailTextField.text];
                }
                [[CRUtils sharedInstance] showAlertWithMessage:errorMessage];
            }
            [self.HUD hide:YES];
        }];
    }
    // Facebook signup
    else
    {
        CRUser *newUser = [CRUser currentUser];
        
        // Check for user existance
        
        PFQuery *query = [CRUser query];
        [query whereKey:@"username" equalTo:self.emailTextField.text];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                NSLog(@"%@", error.localizedDescription);
                return;
            } else {
                // Fill Parse account crederntials
                if (objects.count == 0) {
                    if (newUser.isNew) {
                        CRUser* newUser = [CRUser currentUser];
                        newUser.username = self.emailTextField.text;
                        newUser.email = self.emailTextField.text;
                        newUser.fullName = self.nameTextField.text;
                        newUser.password = self.passwordTextField.text;
                        
                        newUser.profileReferencesArray = [NSMutableArray array];
                        [self.profileTypes enumerateObjectsUsingBlock:^(NSString* profile, NSUInteger idx, BOOL *stop) {
                            [newUser.profileTypes addProfileForTypeName:profile];
                        }];
                        [newUser.profileTypes saveAll];

                        [newUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if (!error) {
                                [self successfullLoginAction];
                            }
                        }];
                    }
                }
                // User with email already exists
                else
                {
                    CRUser *faceboookUser = [CRUser currentUser];
                    [faceboookUser delete];
                    [CRUser logInWithUsernameInBackground:self.emailTextField.text
                                                 password:self.passwordTextField.text
                                                    block:^(PFUser *user, NSError *error) {
                                                        if (!error) {
                                                            NSArray* permissions = @[ @"basic_info", @"email", @"user_birthday" ];
                                                            [PFFacebookUtils linkUser:[CRUser currentUser]
                                                                          permissions:permissions
                                                                                block:^(BOOL succeeded, NSError *error) {
                                                                                    [self successfullLoginAction];
                                                                                }];
                                                        } else {
                                                            NSString *message = @"This email already taken";
                                                            [[CRUtils sharedInstance] showAlertWithMessage:message];
                                                            [self.HUD hide:YES];
                                                        }
                                                    }];
                }
            }
        }];
    }
}

- (IBAction)cancelButtonTapped:(id)sender {
    [[CRUser currentUser] deleteInBackground];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource protocol implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = self.items[indexPath.row][@"CellIdentifier"];
    CRTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (![cellIdentifier isEqualToString:@"SignupProfileTypeCell"]) {
        cell.textField.placeholder = self.items[indexPath.row][@"Placeholder"];
        [self setElementFromCell:cell indexPath:indexPath];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate protocol implementation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UITextFieldDelegate protocol implementation

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.emailTextField && self.facebookRegistrationInProgress) {
        [[CRUtils sharedInstance] showAlertWithMessage:@"Attention: If you change your email address, it will no longer be linked to your facebook account"];
    }
    if (!self.activeField) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillHideNotification
                                                      object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillBeShown:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
    }
    self.activeField = textField;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField == self.nameTextField) {
        [self.emailTextField becomeFirstResponder];
    } else if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        [self performSegueWithIdentifier:@"ProfileTypeSegue" sender:nil];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.emailTextField && self.facebookRegistrationInProgress) {
        self.facebookRegistrationInProgress = NO;
    }
    return YES;
}

#pragma mark - CRProfileTypeViewControllerDelegate protocol implementation

- (void)profileTypeViewController:(CRProfileTypeViewController *)profileTypeViewController didSelectProfileTypes:(NSArray *)types {
    self.profileTypes = types;
    UITableViewCell *profileCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:CRSignupProfileTypeCell inSection:0]];
    if (self.profileTypes.count != 0) {
        NSMutableString *profileTypeCellTitle = [[NSMutableString alloc] init];
        for (NSString *type in types) {
            [profileTypeCellTitle appendString:type];
            [profileTypeCellTitle appendString:@"/"];
        }
        profileCell.textLabel.text = [profileTypeCellTitle substringToIndex:profileTypeCellTitle.length-1];
        profileCell.textLabel.textColor = [UIColor blackColor];
    } else {
        profileCell.textLabel.text = @"Profile type";
        profileCell.textLabel.textColor = [UIColor colorWithWhite:0.7f alpha:0.7f];
    }
}

#pragma mark - Utility methods

- (void)setElementFromCell:(CRTextFieldTableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case CRSignupNameCell:
            self.nameTextField = cell.textField;
            break;
        case CRSignupEmailCell:
            self.emailTextField = cell.textField;
            break;
        case CRSignupPasswordCell:
            self.passwordTextField = cell.textField;
            break;
            
        default:
            break;
    }
}

- (BOOL)isEmptyField {
    if (self.nameTextField.text.length == 0) {
        return YES;
    } else if (self.emailTextField.text.length == 0) {
         return YES;
    } else if (self.passwordTextField.text.length == 0) {
         return YES;
    } else if (self.profileTypes.count == 0) {
         return YES;
    } else return NO;
}

- (void)dismissKeyboard:(id)sender {
    // Discussion
    // "Clear" button is starting both resignFirstResponder/becameFirstResponder
    // and gestureRecognizer events
    // To prevent wrong frame change animation need to check was touch within
    // frame of current editing text field
    CGRect activeTextFieldFrame = [self.view convertRect:self.activeField.frame fromView:self.activeField.superview];
    CGPoint touchLocation = [sender locationOfTouch:0 inView:self.view];
    if ( ! CGRectContainsPoint( activeTextFieldFrame, touchLocation ) ) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillBeHidden:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        self.activeField = nil;
    }
    
    [self.view endEditing:NO];
}

- (void)successfullLoginAction {
    [self.HUD hide:YES];
    if ([self.delegate respondsToSelector:@selector(signupViewControllerSuccessfullySignup:)]) {
        [self.delegate signupViewControllerSuccessfullySignup:self];
    }
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kUserDidUpdateMainPhotoNotification
                                                                                         object:nil]];
#if !TARGET_IPHONE_SIMULATOR
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if ([CRUser currentUser]) {
        currentInstallation[@"user"] = [CRUser currentUser].objectId;
    }
    [currentInstallation saveInBackground];
#endif
    
    [[CRRatingManager sharedInstance] login];
    [[CRFriendsManager sharedInstance] reloadFriends];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)unwindToSignupViewController:(UIStoryboardSegue *)unwindSegue {
    id sourceViewController = unwindSegue.sourceViewController;
    if ([sourceViewController isKindOfClass:[CRProfileTypeViewController class]]) {
        CRProfileTypeViewController *profileTypeVC = sourceViewController;
        if ([profileTypeVC.delegate respondsToSelector:@selector(profileTypeViewController:didSelectProfileTypes:)]) {
            [profileTypeVC.delegate profileTypeViewController:profileTypeVC
                                        didSelectProfileTypes:[NSArray arrayWithArray:profileTypeVC.selectedItems]];
        }
        [self signup];
    }
}

@end
