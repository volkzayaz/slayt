//
//  CRFriendNotificationViewController.m
//  Slayt
//
//  Created by Dmitry Utenkov on 5/27/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFriendNotificationViewController.h"
#import "CRUser.h"
#import "CRPhoto.h"
#import "SWRevealViewController.h"
#import "CRMessagesViewController.h"
#import "CRFriendsManager.h"

#import "UIImage+Utilities.h"

@interface CRFriendNotificationViewController ()

@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UILabel *friendNameLabel;

@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, weak) IBOutlet UIImageView *yourImageView;
@property (nonatomic, weak) IBOutlet UIImageView *friendImageView;

@property (nonatomic, copy) NSString *friendId;
@property (nonatomic, strong) CRUser *friendUser;

@property (nonatomic, strong) UIImage *backgroundImage;

- (IBAction)goToConversationButtonTapped:(id)sender;
- (IBAction)stayHereButtonTapped:(id)sender;

@end

@implementation CRFriendNotificationViewController

+ (void)showNotificationScreen:(NSString *)friendId {
    NSString *xibName = @"FriendNotificationView";
    if ([UIScreen mainScreen].bounds.size.height == 568) {
        xibName = @"FriendNotificationView-568h";
    }
    CRFriendNotificationViewController *friendVC = [[self alloc] initWithNibName:xibName bundle:nil];
    friendVC.friendId = friendId;
    [friendVC showFriendNotification];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    self.backgroundImage = [UIImage imageWithView:[UIApplication sharedApplication].windows[0]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.backgroundImageView.image = self.backgroundImage;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (IBAction)goToConversationButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [CRUser currentUser].friendsFilter = @"All Messages";
        [[CRUser currentUser] saveInBackground];
        CRMessagesViewController *messagesVC = [CRMessagesViewController messagesViewController];
        messagesVC.friendUser = self.friendUser;
        
        if ([[[UIApplication sharedApplication].windows[0] rootViewController] isKindOfClass:[SWRevealViewController class]]) {
            SWRevealViewController *revealController = (SWRevealViewController *)[[UIApplication sharedApplication].windows[0] rootViewController];
            if (revealController.frontViewPosition == FrontViewPositionLeftSide) {
                [revealController rightRevealToggleAnimated:NO];
            }
            if (revealController.frontViewPosition == FrontViewPositionRight) {
                [revealController revealToggleAnimated:NO];
            }

            if ([revealController.frontViewController isKindOfClass:[UINavigationController class]]) {
                [(UINavigationController *)revealController.frontViewController pushViewController:messagesVC animated:YES];
            } else {
                [revealController.frontViewController.navigationController pushViewController:messagesVC animated:YES];
            }
            
            [CRUser currentUser].friendsFilter = @"All Messages";
            [[CRUser currentUser] saveInBackground];
            [[CRFriendsManager sharedInstance] reloadFriends];
        }
    }];
}

- (IBAction)stayHereButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showFriendNotification {
    [self view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Setting your photo
        UIImage *yourPhotoImage = self.yourImageView.image;
        CRPhoto *yourPhoto = [[CRUser currentUser] requestMainPhoto:nil];
        if (yourPhoto) {
            yourPhotoImage = [UIImage imageWithData:[yourPhoto.photoFile getData]];
        }
        
        PFQuery *query = [CRUser query];
        [query whereKey:@"objectId" equalTo:self.friendId];
        NSArray *objects = [query findObjects];
        if (objects.count > 0) {
            CRUser *friend = objects[0];
            self.friendUser = friend;
            UIImage *friendImage = self.friendImageView.image;
            CRPhoto *friendPhoto = [friend requestMainPhoto:nil];
            if (friendPhoto) {
                friendImage = [UIImage imageWithData:[friendPhoto.photoFile getData]];
            }
            
            NSString *friendFirstName = [friend.fullName componentsSeparatedByString:@" "][0];
            
            NSString *messageLabelText = [self.messageLabel.text stringByReplacingOccurrencesOfString:@"User" withString:friendFirstName];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.messageLabel.text = messageLabelText;
                self.yourImageView.image = yourPhotoImage;
                self.friendNameLabel.text = friendFirstName;
                self.friendImageView.image = friendImage;
                [[[UIApplication sharedApplication].windows[0] rootViewController] presentViewController:self
                                                                                                animated:YES
                                                                                              completion:nil];
            });
        }
    });
}

@end
