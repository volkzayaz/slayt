//
//  CRProfileControllerView.h
//  Talent
//
//  Created by Vlad Soroka on 5/15/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRProfileSwitchView.h"

@interface CRProfileControllerView : UITableView

@property (weak, nonatomic) IBOutlet UIView *mainHeaderView;
@property (nonatomic, weak) id<CRSwitchProileProtocol> switchDelegate;

- (void)updateSwitchViewWithItems:(NSArray*)items;

@end
