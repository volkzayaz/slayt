//
//  CRProfileControllerView.m
//  Talent
//
//  Created by Vlad Soroka on 5/15/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRProfileControllerView.h"

@interface CRProfileControllerView () 

@property (nonatomic, weak) CRProfileSwitchView* switchView;


@property (nonatomic, assign) CGRect headerFrameWithoutSwitchView;

@end

@implementation CRProfileControllerView

- (void)awakeFromNib
{
    self.headerFrameWithoutSwitchView = self.mainHeaderView.frame;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        CRProfileSwitchView* view = [[CRProfileSwitchView alloc] initWithFrame:CGRectMake(0, 417, 320, 44)
                                                                      delegate:self.switchDelegate
                                                                         items:@[@""]];
        view.hidden = YES;
        self.switchView = view;
        [self addSubview:view];
    }
    return self;
}

- (void)setSwitchDelegate:(id)switchDelegate
{
    if(switchDelegate != self.switchDelegate)
    {
        _switchDelegate = switchDelegate;
        self.switchView.delegate = self.switchDelegate;
    }
}

- (void)updateSwitchViewWithItems:(NSArray*)items
{
    if(items.count <= 1)
    {
        self.mainHeaderView.frame = self.headerFrameWithoutSwitchView;
   
        [self setTableHeaderView:self.mainHeaderView];
        self.switchView.hidden = YES;
    }
    else
    {
        CGRect headerViewFrameWithSwitchView = self.headerFrameWithoutSwitchView;
        headerViewFrameWithSwitchView.size.height += self.switchView.frame.size.height;
        
        self.mainHeaderView.frame = headerViewFrameWithSwitchView;
        [self setTableHeaderView:self.mainHeaderView];
        self.switchView.hidden = NO;
        
        [self.switchView refreshWithItems:items];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
