//
//  CRNavigationStackTransition.h
//  Slayt
//
//  Created by Dmitry Utenkov on 6/16/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRNavigationStackTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end
