//
//  CREditAgencyViewController.h
//  Slayt
//
//  Created by Dmitry Utenkov on 5/28/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRBaseEditValuesController.h"

@interface CREditAgencyViewController : UIViewController

@property (nonatomic, strong, readonly) UITableView *tableView;

@end
