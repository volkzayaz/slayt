//
//  CRSettingsViewController.m
//  Talent
//
//  Created by Dmitry Utenkov on 5/14/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRSettingsViewController.h"
#import <Parse/Parse.h>
#import "CRUser.h"
#import "SWRevealViewController.h"
#import "CRHomeViewController.h"
#import "GrabKit.h"
#import <MessageUI/MessageUI.h>
#import "CRUtils.h"
#import "CRRatingManager.h"
#import "CRFriendsListViewController.h"
#import "CRWebViewController.h"
#import "CRFriendsManager.h"

@interface CRSettingsViewController () <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

@end

@implementation CRSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier hasSuffix:@"WebViewSegue"]) {
        if ([sender isKindOfClass:[UITableViewCell class]]) {
            UITableViewCell *cell = sender;
            [segue.destinationViewController setTitle:cell.textLabel.text];
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                                     style:UIBarButtonItemStyleBordered
                                                                                    target:nil
                                                                                    action:nil];
        }
    }
    
    if ([segue.identifier hasPrefix:@"TermsAndConditions"]) {
        CRWebViewController *webVC = segue.destinationViewController;
        webVC.htmlPath = [[NSBundle mainBundle] URLForResource:@"termsConditions" withExtension:@"html"];
    } else if ([segue.identifier hasPrefix:@"PrivacyPolicy"]) {
        CRWebViewController *webVC = segue.destinationViewController;
        webVC.htmlPath = [[NSBundle mainBundle] URLForResource:@"privacyPolicy" withExtension:@"html"];
    }
}

#pragma mark - Actions

- (IBAction)pushNotificationSwitchValueChanged:(id)sender {
    NSLog(@"Switch value changed");
}

- (void)showContactUsForm {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        [mailViewController setToRecipients:@[@"support@slayt.co"]];
        mailViewController.mailComposeDelegate = self;
        mailViewController.navigationBar.tintColor = self.navigationController.navigationBar.barTintColor;
        [self presentViewController:mailViewController animated:YES completion:nil];
    } else {
        UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle:@"Slayt"
                                                            message:@"You don't have an email account set up. Would you like to do it now?"
                                                           delegate:self
                                                  cancelButtonTitle:@"NO"
                                                  otherButtonTitles:@"YES", nil];
        [mailAlert show];
    }
}

- (IBAction)messageButtonTapped:(id)sender {
    [self.revealViewController rightRevealToggleAnimated:YES];
}

#pragma mark - UITableViewDelegate protocol implementation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section == 0) {
        NSString *segueName = indexPath.row == 0 ? @"PrivacyPolicyWebViewSegue" : @"TermsAndConditionsWebViewSegue";
        [self performSegueWithIdentifier:segueName sender:cell];
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {
        [self showContactUsForm];
    }
    else if (indexPath.section == 2 && indexPath.row == 1) {
        UIActionSheet *logoutSheet = [[UIActionSheet alloc] initWithTitle:@"Do you wish to logout?"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:@"Log Out"
                                                        otherButtonTitles:nil, nil];
        [logoutSheet showInView:self.view];
    }
}

#pragma mark - UIActionSheetDelegate protocol implementation

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
#if !TARGET_IPHONE_SIMULATOR
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        if ([CRUser currentUser]) {
            currentInstallation[@"user"] = @"";
        }
        [currentInstallation saveInBackground];
#endif
        [[GRKFacebookSingleton sharedInstance].facebookSession closeAndClearTokenInformation];
        [[PFFacebookUtils session] closeAndClearTokenInformation];
        [FBSession.activeSession closeAndClearTokenInformation];
        [FBSession.activeSession close];
        [FBSession setActiveSession:nil];
        [[CRRatingManager sharedInstance] logout];
        [[[CRUser currentUser] locationMonitor] stopMonitoring];
        [CRUser logOut];
        
        UINavigationController *navVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SWFrontNavigationController"];
        if ([navVC.topViewController isKindOfClass:[CRHomeViewController class]]) {
            [(CRHomeViewController *)navVC.topViewController setShowsLoginScreenAnimated:YES];
        }
        
        // TODO: TEMPORARY SOLUTION
        if ([[[UIApplication sharedApplication].windows[0] rootViewController] isKindOfClass:[SWRevealViewController class]]) {
            SWRevealViewController *revealVC = (SWRevealViewController *)[[UIApplication sharedApplication].windows[0] rootViewController];
            id rightVC = revealVC.rightViewController;
            if ([rightVC conformsToProtocol:@protocol(CRFriendsManagerDelegate)]) {
                [CRFriendsManager sharedInstance].delegate = (id<CRFriendsManagerDelegate>)rightVC;
                [[CRFriendsManager sharedInstance] reloadFriends];
            }
        }
        
        
        [self.revealViewController pushFrontViewController:navVC animated:NO];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate protocol implementation

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIAlertViewDelegate protocol implementaion

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto://support@slayt.co"]];
    }
}

@end
