//
//  CRRootViewController.m
//  Talent
//
//  Created by Vlad Soroka on 4/1/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRTutorialViewController.h"
#import "iCarousel.h"
#import "CRSignupViewController.h"
#import "CRTutorialScreenViewController.h"

@interface CRTutorialViewController () <iCarouselDataSource, iCarouselDelegate>

@property (weak, nonatomic) IBOutlet UIButton *signupButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet iCarousel *tutorialSlideShow;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@implementation CRTutorialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tutorialSlideShow.type = iCarouselTypeLinear;
    self.tutorialSlideShow.decelerationRate = 0.7;
    self.tutorialSlideShow.pagingEnabled = YES;
    self.tutorialSlideShow.bounces = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - iCarousel delegate

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel {
    if(carousel.currentItemIndex == 0) {
        self.pageControl.hidden = YES;
    } else {
        self.pageControl.hidden = NO;
        self.pageControl.currentPage = carousel.currentItemIndex - 1;
    }
}

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return 5;
}

- (UIView*)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
// TODO: perform lazy loading of each view and nil them at didReceiveMemoryWarning
    UIViewController *tutorialScreen = [[CRTutorialScreenViewController alloc] initWithNibName:[NSString stringWithFormat:@"tutorialScreen%lu",(long)index+1] bundle:nil];
        
    return tutorialScreen.view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    return value;
}

#pragma mark - Storyboard Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"LoginSegue"] ||
        [segue.identifier isEqualToString:@"SignupSegue"])
    {
        id destinationViewController = segue.destinationViewController;
        if ([destinationViewController respondsToSelector:@selector(topViewController)]) {
            destinationViewController = [destinationViewController topViewController];
        }
        if ([destinationViewController respondsToSelector:@selector(setDelegate:)]) {
            [destinationViewController setDelegate:self.signupDelegate];
        }
    }
}

@end
