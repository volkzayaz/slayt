//
//  CRPhotoViewController.h
//  Talent
//
//  Created by Dmitry Utenkov on 4/30/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface CRPhotoViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>

@property (nonatomic, strong) NSMutableArray *photos;
@property (assign) NSUInteger currentPhotoIndex;
@property (nonatomic, weak) IBOutlet iCarousel *carousel;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *previousPhotoButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *nextPhotoButton;

@end
