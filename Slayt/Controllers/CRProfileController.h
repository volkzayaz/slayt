//
//  CRProfileController.h
//  Talent
//
//  Created by Vlad Soroka on 4/2/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CRProfileController;

@protocol CRProfileControllerDelegate <NSObject>

- (void)profileController:(CRProfileController *)controller didRateProfile:(BOOL)liked;

@end

@class CRUser;

@interface CRProfileController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) CRUser *profile;

@property (nonatomic, weak) id<CRProfileControllerDelegate> delegate;

- (void)yesButtonTapped:(id)sender;
- (void)noButtonTapped:(id)sender;

@end
