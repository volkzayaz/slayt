//
//  CRTutorialScreenViewController.m
//  Slayt
//
//  Created by Dmitry Utenkov on 5/21/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRTutorialScreenViewController.h"

@interface CRTutorialScreenViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation CRTutorialScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.textView.text) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSDictionary *attrsDictionary =
        @{ NSFontAttributeName            : [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0f],
           NSForegroundColorAttributeName : [UIColor whiteColor],
           NSParagraphStyleAttributeName  : paragraphStyle };
        
        NSString *text = self.textView.text;
        self.textView.attributedText = [[NSAttributedString alloc] initWithString:text attributes:attrsDictionary];
    }
}

@end
