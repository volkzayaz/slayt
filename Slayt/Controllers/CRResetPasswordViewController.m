//
//  CRResetPasswordViewController.m
//  Talent
//
//  Created by Dmitry Utenkov on 4/30/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRResetPasswordViewController.h"
#import "CRUser.h"
#import "CRUtils.h"
#import <Parse/PF_MBProgressHUD.h>

@interface CRResetPasswordViewController () <UIAlertViewDelegate>

@property (nonatomic, strong) PF_MBProgressHUD* HUD;

@end

@implementation CRResetPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    // For keyboard dismissal on touch
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
}

- (IBAction)resetPasswordButtonTapped:(id)sender {
    self.HUD = [[PF_MBProgressHUD alloc] initWithView:self.view];
    self.HUD.labelText = @"Resetting password";
    self.HUD.removeFromSuperViewOnHide = YES;
    [self.view addSubview:self.HUD];
    [self.HUD show:YES];
    
    PFQuery *query = [CRUser query];
    [query whereKey:@"username" equalTo:self.emailTextField.text];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [self.HUD hide:YES];
        if (objects.count > 0) {
            [PFUser requestPasswordResetForEmailInBackground:self.emailTextField.text];
            NSString *message = @"Instructions for restoring password had been sent to email";
            [[CRUtils sharedInstance] showAlertWithMessage:message completion:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            NSString *message = @"User with this email is not registered";
            [[CRUtils sharedInstance] showAlertWithMessage:message completion:^{
                self.emailTextField.text = @"";
                [self.emailTextField becomeFirstResponder];
            }];
        }
    }];
}

- (void)dismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate protocol implementation

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
