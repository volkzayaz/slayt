//
//  CRFriendsListViewControllerTableViewController.m
//  Slayt
//
//  Created by Dmitry Utenkov on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFriendsListViewController.h"
#import "CRFriend.h"
#import <Parse/PFQuery.h>
#import "CRUser.h"
#import "SWRevealViewController.h"
#import "CRFriendListCellTableViewCell.h"
#import "CRPhoto.h"
#import "CRProfileTypeListFilterViewController.h"
#import "CRProfileTypeStorage.h"
#import "CRMessagesViewController.h"
#import "CRFriendsManager.h"

@interface CRFriendsListViewController () <UITableViewDataSource, UITableViewDelegate,
                                            CRProfileTypeListFilterViewControllerDelegate,
                                            CRFriendsManagerDelegate,
                                            UISearchDisplayDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UINavigationBar *navigationBar;

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@property (nonatomic, copy) NSMutableDictionary *friendsDictionary;

@property (nonatomic, copy) NSString *filteredProfileType;

@property (nonatomic, strong) NSArray *friends;

@end

@implementation CRFriendsListViewController

- (void)awakeFromNib {
    _friendsDictionary = [[NSMutableDictionary alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.friends = [CRFriendsManager sharedInstance].friends;
    [self.tableView reloadData];
    
    [CRFriendsManager sharedInstance].delegate = self;
    [[CRFriendsManager sharedInstance] reloadFriends];
    
    self.view.backgroundColor = self.tableView.backgroundColor;
    self.navigationBar.barTintColor = self.tableView.backgroundColor;
    
    [self.searchDisplayController.searchBar setBackgroundImage:[UIImage imageNamed:@"messagesSearchBarBackground"]
                                                forBarPosition:UIBarPositionAny
                                                    barMetrics:UIBarMetricsDefault];
    // Hides extra lines of table view
    self.tableView.tableFooterView = [UIView new];
    
    self.searchDisplayController.searchResultsTableView.backgroundColor = self.tableView.backgroundColor;
    self.searchDisplayController.searchResultsTableView.separatorColor = self.tableView.separatorColor;
    self.searchDisplayController.searchResultsTableView.tintColor = self.tableView.tintColor;
    self.searchDisplayController.searchResultsTableView.tableFooterView = [UIView new];
}

- (void)fetchFriends {
    for (CRUser *friend in self.friends) {
        self.friendsDictionary[friend.objectId] = [NSMutableDictionary dictionary];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
                       {
                           self.friendsDictionary[friend.objectId][@"fullName"] = friend.fullName;
                           [self updateFriendListItem:friend];
                           
                           CRPhoto *friendPhoto = [friend requestMainPhoto:nil];
                           [friendPhoto.photoFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                               UIImage *friendImage = [UIImage imageWithData:data];
                               self.friendsDictionary[friend.objectId][@"friendImage"] = friendImage ? friendImage : [UIImage imageNamed:@"noImage"];
                               [self updateFriendListItem:friend];
                           }];
                       });
    }
}

- (void)updateFriendListItem:(CRUser *)friendUser {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSUInteger cellIndex = [self.friends indexOfObjectPassingTest:^BOOL(CRUser *friendObject, NSUInteger idx, BOOL *stop) {
                                    return [friendObject.objectId isEqualToString:friendUser.objectId];
                                }];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellIndex inSection:0];
        if ([self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    });
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([CRUser currentUser].friendsFilter) {
        self.navigationBar.topItem.title = [CRUser currentUser].friendsFilter;
    } else {
        self.navigationBar.topItem.title = @"All Messages";
    }
    CGRect frame = self.view.frame;
    frame.origin.x += self.revealViewController.rightViewRevealOverdraw;
    frame.size.width -= self.revealViewController.rightViewRevealOverdraw;
    self.view.frame = frame;
    [self.tableView reloadData];
    [self fetchFriends];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    CGRect frame = self.view.frame;
    frame.origin.x -= self.revealViewController.rightViewRevealOverdraw;
    frame.size.width += self.revealViewController.rightViewRevealOverdraw;
    self.view.frame = frame;
}

#pragma mark - Actions

- (IBAction)filterButtonTapped:(id)sender {
    CRProfileTypeListFilterViewController *filterVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CRProfileTypeListFilterViewController class])];
    filterVC.delegate = self;
    NSString *friendsFilter = [CRUser currentUser].friendsFilter;
    if (!friendsFilter) {
        friendsFilter = @"All Messages";
    }
    filterVC.selectedProfileType = friendsFilter;
    UINavigationController *navVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SWFrontNavigationController"];
    navVC.viewControllers = @[filterVC];
    [self presentViewController:navVC animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource protocol implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.friends.count;
}

- (CRFriendListCellTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CRFriendListCellTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FriendsListCell"];
    CRUser *friend = self.friends[indexPath.row];
    
    cell.friendImageView.image = self.friendsDictionary[friend.objectId][@"friendImage"];
    if (!cell.friendImageView.image) {
        cell.friendImageView.image = [UIImage imageNamed:@"noImage"];
    }
    cell.friendNameLabel.text = self.friendsDictionary[friend.objectId][@"fullName"];
    
    return cell;
}

#pragma mark - UITableViewDelegate protocol implementation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CRMessagesViewController *messagesVC = [CRMessagesViewController messagesViewController];
    messagesVC.friendUser = self.friends[indexPath.row];
    if ([self.revealViewController.frontViewController isKindOfClass:[UINavigationController class]]) {
        [(UINavigationController *)self.revealViewController.frontViewController pushViewController:messagesVC animated:NO];
    } else {
        [self.revealViewController.frontViewController.navigationController pushViewController:messagesVC animated:NO];
    }
    [self.searchDisplayController setActive:NO animated:NO];
    [self.revealViewController rightRevealToggleAnimated:YES];
}

#pragma mark - CRProfileTypeListFilterViewControllerDelegate protocol implementation

- (void)profileFilter:(CRProfileTypeListFilterViewController *)filter didSelectProfileType:(id)profileType {
    if (![[CRUser currentUser].friendsFilter isEqualToString:profileType]) {
        self.navigationBar.topItem.title = profileType;
        [CRUser currentUser].friendsFilter = profileType;
        [[CRUser currentUser] saveInBackground];
        [[CRFriendsManager sharedInstance] reloadFriends];
    }
}

#pragma mark - CRFriendsManagerDelegate protocol implementaion

- (void)friendsManager:(CRFriendsManager *)manager didLoadFriends:(NSArray *)loadedProfile {
    BOOL firstLoad = !self.friends;
    self.friends = [NSArray arrayWithArray:loadedProfile];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        if (firstLoad) {
            [self fetchFriends];
        }
    });
}

#pragma mark - UISearchDisplayDelegate protocol implementaion

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    
    if (searchString.length == 0) {
        self.friends = [CRFriendsManager sharedInstance].friends;
        return YES;
    }
    
    NSArray *friendsNames = [[CRFriendsManager sharedInstance].friends valueForKey:@"fullName"];
    
    NSMutableArray *foundFriendsNames = [NSMutableArray array];
    for (NSString *name in friendsNames) {
        if ([name.lowercaseString hasPrefix:searchString.lowercaseString]) {
            [foundFriendsNames addObject:name];
        }
    }
    
    NSMutableArray *foundFriends = [NSMutableArray array];
    for (CRUser *friend in [CRFriendsManager sharedInstance].friends) {
        if ([foundFriendsNames containsObject:friend.fullName]) {
            [foundFriends addObject:friend];
        }
    }
    
    self.friends = [NSArray arrayWithArray:foundFriends];
    
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.rowHeight = 60;
}

#pragma mark - CRFriendsManagerDelegate protocol implementaion

- (void)friendsManagerDidNotFoundFriends:(CRFriendsManager *)manager {
    self.friends = [NSArray array];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)friendsManagerDidFetchFriendData:(CRUser *)friendUser {
    [self updateFriendListItem:friendUser];
}

@end
