//
//  CRMenuController.h
//  Talent
//
//  Created by Vlad Soroka on 4/2/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

typedef NS_ENUM(NSUInteger, CRMenuItem) {
    CRMenuItemProfile = 0,
    CRMenuItemHome,
    CRMenuItemMessages,
    CRMenuItemCastings,
    CRMenuItemJobs,
    CRMenuItemSettings,
    CRMenuItemInvite,
};

@interface CRMenuController : UITableViewController

- (void)selectMenuItemAtIndex:(CRMenuItem)itemIndex;

@end
