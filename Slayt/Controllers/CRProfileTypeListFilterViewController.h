//
//  CRProfileTypeListFilterViewController.h
//  Slayt
//
//  Created by Dmitry Utenkov on 6/10/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CRProfileTypeListFilterViewController;

@protocol CRProfileTypeListFilterViewControllerDelegate <NSObject>

- (void)profileFilter:(CRProfileTypeListFilterViewController *)filter didSelectProfileType:(id)profileType;

@end

@interface CRProfileTypeListFilterViewController : UITableViewController

@property (nonatomic, weak) id<CRProfileTypeListFilterViewControllerDelegate> delegate;

@property (nonatomic, copy) NSString *selectedProfileType;

@end
