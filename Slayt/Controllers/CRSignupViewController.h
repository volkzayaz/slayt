//
//  CRSignupViewController.h
//  Talent
//
//  Created by Dmitry Utenkov on 4/29/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRProfileTypeViewController.h"

@class CRSignupViewController;

@protocol CRSignupViewControllerDelegate <NSObject>

- (void)signupViewControllerSuccessfullySignup:(CRSignupViewController *)signupViewController;

@end

@interface CRSignupViewController : UIViewController < UITableViewDataSource,
                                                       UITableViewDelegate,
                                                       UITextFieldDelegate,
                                                       CRProfileTypeViewControllerDelegate>

@property (nonatomic, weak) id<CRSignupViewControllerDelegate> delegate;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;

- (IBAction)signupFacebookButtonTapped:(id)sender;
- (IBAction)signupButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@end
