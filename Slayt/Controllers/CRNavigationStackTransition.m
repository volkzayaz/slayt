//
//  CRNavigationStackTransition.m
//  Slayt
//
//  Created by Dmitry Utenkov on 6/16/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRNavigationStackTransition.h"

@implementation CRNavigationStackTransition

#pragma mark - UIViewControllerAnimatedTransitioning protocol implementaion

// This is used for percent driven interactive transitions, as well as for container controllers that have companion animations that might need to
// synchronize with the main animation.
- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.2;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    // 1. obtain state from the context
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    CGRect finalFrame = [transitionContext finalFrameForViewController:fromViewController];
    
    // 2. obtain the container view
    UIView *containerView = [transitionContext containerView];
    
    // 3. set initial state
    //CGRect screenBounds = [[UIScreen mainScreen] bounds];
    //fromViewController.view.frame = CGRectOffset(finalFrame, screenBounds.size.width, 0);
    
    // 4. add the view
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    [containerView insertSubview:toViewController.view belowSubview:fromViewController.view];
    
    // 5. animate
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    
    [UIView animateWithDuration:duration animations:^{
        CGRect fromFrame = fromViewController.view.frame;
        fromFrame.origin.x -= fromFrame.size.width;
        fromViewController.view.frame = fromFrame;
        
    } completion:^(BOOL finished) {
        
        // 6. inform the context of completion
        [transitionContext completeTransition:YES];
        
    }];
}

@end
