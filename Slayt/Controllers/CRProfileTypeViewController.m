//
//  CRProfileTypeViewController.m
//  Talent
//
//  Created by Dmitry Utenkov on 4/29/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRProfileTypeViewController.h"
#import "CRUtils.h"
#import "CRProfileTypeStorage.h"

@interface CRProfileTypeViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong) NSArray *items;
@property (strong) NSArray *availableItems;
@property (strong) NSArray *itemsGroups;

@property (weak) IBOutlet UITableView *tableView;

@end

@implementation CRProfileTypeViewController

- (void)awakeFromNib {
    self.selectedItems = [[NSMutableArray alloc] init];
    CRProfileTypeStorage* storage = [[CRProfileTypeStorage alloc] initWithDictionaryStorage:nil];
    
    self.itemsGroups = storage.profileTypeGroupsStrings;
    
    NSMutableArray *mutableItems = [[NSMutableArray alloc] init];
    for (NSArray *group in self.itemsGroups) {
        for (NSString *item in group) {
            [mutableItems addObject:item];
        }
    }
    
    self.items = [NSArray arrayWithArray:mutableItems];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect frame = self.tableView.frame;
    frame.size.height = self.items.count * [self.tableView.delegate tableView:self.tableView
                                                      heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0
                                                                                                 inSection:0]];
    [self updateAvailableItems];
}

- (void) viewWillDisappear:(BOOL)animated {
    // This action triggers by Back button
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        if ([self.delegate respondsToSelector:@selector(profileTypeViewController:didSelectProfileTypes:)]) {
            [self sortSelectedItems];
            [self.delegate profileTypeViewController:self didSelectProfileTypes:[NSArray arrayWithArray:self.selectedItems]];
        }
    }
    [super viewWillDisappear:animated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SignupUnwindSegue"]) {
        [self sortSelectedItems];
    }
}

#pragma mark - UITableViewDataSource protocol implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = self.items[indexPath.row];
    cell.textLabel.enabled = [self isRowAvailable:indexPath];
    
    BOOL isSelected = [self isRowSelected:indexPath];
    cell.accessoryType = isSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    return cell;
}

#pragma mark - UITableViewDelegate protocol implementation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([self isRowSelected:indexPath]) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.selectedItems removeObject:self.items[indexPath.row]];
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.selectedItems addObject:self.items[indexPath.row]];
    }
    [self updateAvailableItems];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self isRowAvailable:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

#pragma mark - Utility methods

- (BOOL)isRowSelected:(NSIndexPath *)indexPath {
    return [self.selectedItems containsObject:self.items[indexPath.row]];
}

- (BOOL)isRowAvailable:(NSIndexPath *)indexPath {
    return [self.availableItems containsObject:self.items[indexPath.row]];
}

- (void)updateAvailableItems {
    if (self.selectedItems.count != 0) {
        NSString *item = [self.selectedItems firstObject];
        NSUInteger index = [self.itemsGroups indexOfObjectPassingTest:^BOOL(NSArray *group, NSUInteger index, BOOL *stop) {
            return [group containsObject:item];
        }];
        
        if (index != NSNotFound) {
            self.availableItems = self.itemsGroups[index];
        }
    } else {
        self.availableItems = self.items;
    }
    
    [self.tableView reloadData];
}

- (void)sortSelectedItems {
    if (self.selectedItems.count <= 1) {
        return;
    } else {
        [self.selectedItems sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSUInteger firstIndex = [self.availableItems indexOfObject:obj1];
            NSUInteger secondIndex = [self.availableItems indexOfObject:obj2];
            return [@(firstIndex) compare:@(secondIndex)];
        }];
    }
}

@end
