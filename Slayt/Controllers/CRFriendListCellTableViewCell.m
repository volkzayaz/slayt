//
//  CRFriendListCellTableViewCell.m
//  Slayt
//
//  Created by Dmitry Utenkov on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFriendListCellTableViewCell.h"

@implementation CRFriendListCellTableViewCell

- (void)prepareForReuse {
    self.friendImageView.image = nil;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    UIColor *backgroundColor = self.unreadMessagesNumberLabel.backgroundColor;
    [super setHighlighted:highlighted animated:animated];
    self.unreadMessagesNumberLabel.backgroundColor = backgroundColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    UIColor *backgroundColor = self.unreadMessagesNumberLabel.backgroundColor;
    [super setSelected:selected animated:animated];
    self.unreadMessagesNumberLabel.backgroundColor = backgroundColor;
}

@end
