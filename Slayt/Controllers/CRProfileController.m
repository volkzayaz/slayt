//
//  CRProfileController.m
//  Talent
//
//  Created by Vlad Soroka on 4/2/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRProfileController.h"
#import "SWRevealViewController.h"
#import "iCarousel.h"
#import "CRUtils.h"
#import "CRUser.h"
#import "CRPhoto.h"
#import "CRPhotoViewController.h"
#import "CRCustomModalSegue.h"
#import "CRProfileControllerView.h"
#import "CRFriendsManager.h"

#import "CRBarColors.h"

#import "NSNumber+CGFloat.h"

#import <Parse/PF_MBProgressHUD.h>

#import "CRModel.h"

#import "CRFetchManager.h"

#import "CRMenuController.h"

@interface CRProfileController () <iCarouselDataSource, iCarouselDelegate, CRSwitchProileProtocol, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet CRProfileControllerView *view;
@property (weak, nonatomic) UIBarButtonItem *revealButtonItem;
@property (weak, nonatomic) IBOutlet iCarousel *photoCarousel;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rolesLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@property (nonatomic, weak) PF_MBProgressHUD* HUD;

@property (nonatomic, strong) NSArray* profileAttributesArray;

@property (nonatomic, strong) NSArray* profilePhotos;
@property (nonatomic, strong) PFObject<CRProfileProtocol>* selectedProfileType;


@end

@implementation CRProfileController

- (void)setSelectedProfileType:(PFObject<CRProfileProtocol> *)selectedProfileType
{
    if(_selectedProfileType != selectedProfileType)
    {
        _selectedProfileType = selectedProfileType;

        self.profileAttributesArray = [self.profile profilePropertiesArrayForType:selectedProfileType.profileType];
    }
}

- (void)dealloc {
    if (self.revealViewController.delegate == self) {
        self.revealViewController.delegate = nil;
    }
    _delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
/*    [[NSNotificationCenter defaultCenter] addObserverForName:kDidFetchUserInfoNotification
                                                      object:[CRFetchManager sharedManager]
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
        NSLog(@"FETCHED");
    }];
  */  
    self.photoCarousel.type = iCarouselTypeLinear;
    self.photoCarousel.pagingEnabled = YES;
    self.photoCarousel.bounces = NO;
    
    self.view.switchDelegate = self;
    
    PF_MBProgressHUD* hud = [[PF_MBProgressHUD alloc] initWithView:self.view];
    self.HUD = hud;
    hud.userInteractionEnabled = YES;
    [self.navigationController.view addSubview:hud];
    
    if ([CRUser currentUser] == self.profile) {
        UIBarButtonItem* revealButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"RevealIcon"]
                                                                         style:UIBarButtonItemStyleBordered
                                                                        target:self.revealViewController
                                                                        action:@selector( revealToggle: )];
        revealButton.tintColor = [UIColor whiteColor];
        self.navigationItem.leftBarButtonItem = revealButton;
        
        self.revealButtonItem = revealButton;
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.tableView addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        self.revealViewController.delegate = self;
        
        UIBarButtonItem* editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(editClicked)];
        editButton.tintColor = [UIColor whiteColor];
        self.navigationItem.rightBarButtonItem = editButton;
    }
    
    if ([CRUser currentUser] == self.profile) {
        self.navigationItem.title = @"My Profile";
    } else if ([[CRUser currentUser].profileTypes profileForType:CRProfileTypeEmployer]) {
        if ([[CRFriendsManager sharedInstance].friends containsObject:self.profile]) {
            [self showHireButton];
        }
    } else {
        self.navigationItem.title = @"";
    }
}

- (void)showHireButton {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_hire"]];
    imageView.userInteractionEnabled = NO;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = imageView.frame;
    button.showsTouchWhenHighlighted = YES;
    [button addSubview:imageView];
    [button addTarget:self
               action:@selector(hireButtonTapped:)
     forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = button;
}

- (void)hireButtonTapped:(id)sender {
    NSLog(@"Hire button tapped");
}

- (void)viewWillAppear:(BOOL)animated
{
    [CRBarColors setColors];

    NSError* error = nil;
    if(![[CRFetchManager sharedManager] isUserFetchedWithID:self.profile.objectId errorReason:error] &&
       !self.profile.isNew)
    {
        NSLog(@"User is not fetched. Error - %@",error);
        [[[UIAlertView alloc] initWithTitle:@"System Error" message:@"System error occured. Try again later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];

    CRUser* __weak user = self.profile;
    
    self.selectedProfileType = self.profile.profileTypes.profileTypesArray.firstObject;
    self.profileAttributesArray = [self.profile profilePropertiesArrayForType:self.selectedProfileType.profileType];
    
    NSString* fullName = user.fullName;
    self.nameLabel.text = fullName == nil || [fullName isEqualToString:@""] ? @"Not Specified" : fullName;
    self.rolesLabel.text = [self.profile.profileTypes stringOfAllProfileTypesWithDelimiter:@" \u2022 "];
    
    [user.profileTypes.profileTypesArray enumerateObjectsUsingBlock:^(id<CRProfileProtocol> obj, NSUInteger idx, BOOL *stop) {
        if([obj profileType] != CRProfileTypeModel)
        {
            self.statusLabel.hidden = YES;
        }
        else
        {
            self.statusLabel.hidden = NO;
            NSString* agencyName = user.agencyName == nil || user.agencyName.length == 0 ? @"unsigned talent" : user.agencyName;
            self.statusLabel.text = agencyName;
            *stop = YES;
        }
    }];
    
    self.locationLabel.text = user.homeTown;
    
    //dispatch_async(dispatch_get_main_queue(), ^{//investigate why the call without dispatch_async freezes switchView
#warning calling updateSwitchViewWithItems: in viewWillAppear cause segmented controll to freeze. It is neccessary to refactor all SwitchView code to move all the UI changing code to layoutSubviews call.
    [self.view performSelector:@selector(updateSwitchViewWithItems:) withObject:self.profile.profileTypes.profileTypesStrings afterDelay:0.4];

   // });
    
    [self.tableView reloadData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSError* __autoreleasing* error = nil;
        
        self.profilePhotos = [user requestPhotosWithError:error];
        if(!error)
        {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.photoCarousel reloadData];
                [self.photoCarousel scrollToItemAtIndex:0 animated:NO];//scrolling to main Photo
            });

        }
        else
        {
            NSLog(@"Error on requesting user photos. Details - %@", [*error description]);
        }
    });

    self.revealViewController.panGestureRecognizer.delegate = self;
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.revealViewController.panGestureRecognizer.delegate = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.profileAttributesArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat heightForRow = 44.;
    
    NSNumber* height = self.profileAttributesArray[indexPath.section][@"properties"][indexPath.row][@"rowHeight"];
    if(height)
    {
        heightForRow = height.CGFloatValue;
    }

    ///if your property potentially has a lot of information to present to user, than you can choose to use dynamic row height. The height will be determined based the amount of characters that are neccessary for displaying the property.
    BOOL isRowHeightDynamic = ((NSNumber*)self.profileAttributesArray[indexPath.section][@"properties"][indexPath.row][@"dynamicRowHeight"]).boolValue;
    if(isRowHeightDynamic)
    {
        NSString* key = self.profileAttributesArray[indexPath.section][@"properties"][indexPath.row][@"propertyName"];
        NSString* detailString = [self.profile valueDescriptionForKey:key];
        if(!detailString)
        {
            detailString = self.profile[key];
        }
        
        CGSize sizeToFit = [[CRUtils sharedInstance] sizeForString:detailString withFont:[UIFont fontWithName:@"HelveticaNeue" size:15] toFitWidth:260];
        if(height.CGFloatValue/2 < sizeToFit.height)
        {
            height = @(height.CGFloatValue + sizeToFit.height);
        }
    }
    
    return heightForRow;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString* headerTitle = self.profileAttributesArray[section][@"sectionHeader"];
    
    if(!headerTitle)
    {
        return nil;
    }
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 25)];
    UILabel* headerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(17, 0, self.view.frame.size.width-17, 25)];
    headerTitleLabel.text = headerTitle;
    [headerTitleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    [headerTitleLabel setTextColor:[UIColor colorWithRed:0x80/255.f green:0x7f/255.f blue:0xb0/255.f alpha:1]];
    [headerView addSubview:headerTitleLabel];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = [self.profileAttributesArray[section][@"properties"] count];
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = self.profileAttributesArray[indexPath.section][@"properties"][indexPath.row][@"cellIdentifier"];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = self.profileAttributesArray[indexPath.section][@"properties"][indexPath.row][@"displayName"];
    cell.textLabel.minimumScaleFactor = 0.5f;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    NSString* key = self.profileAttributesArray[indexPath.section][@"properties"][indexPath.row][@"propertyName"];
    
    id value = self.profile[key];
    
    NSString* detailString = [self.profile valueDescriptionForKey:key];
    
    if(!detailString)
    {
        if(value == nil)
        {
            value = self.profileAttributesArray[indexPath.section][@"properties"][indexPath.row][@"defaultValue"];
        }
        
        detailString = [value description];
        
        NSString* trailingSymbols = value == nil ? nil : self.profileAttributesArray[indexPath.section][@"properties"][indexPath.row][@"trailingSymbols"];
        
        if(trailingSymbols && self.profile[key])
        {
            detailString = [NSString stringWithFormat:@"%@%@",detailString,trailingSymbols];
        }
    }

    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.text = detailString;
    [cell.detailTextLabel sizeToFit];
    
    cell.detailTextLabel.minimumScaleFactor = 0.5f;
    
    return cell;
}

#pragma mark - UITableViewDelegate protocol implementation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PhotoViewSegue"]) {
        CRCustomModalSegue *customSegue = (CRCustomModalSegue *)segue;
        customSegue.animated = YES;
        customSegue.toolbarHidden = NO;
        id destinationViewController = segue.destinationViewController;
        if ([destinationViewController respondsToSelector:@selector(topViewController)]) {
            destinationViewController = [destinationViewController topViewController];
        }
        if ([destinationViewController respondsToSelector:@selector(setPhotos:)]) {
            [destinationViewController setPhotos:self.profilePhotos];
        }
    }   
}

#pragma mark - iCarouselDataSource protocol implementation

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    NSUInteger numberOfPhotos = self.profilePhotos.count;
    return numberOfPhotos == 0 ? 1 : numberOfPhotos;
}

- (UIView*)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    CGRect frame = self.photoCarousel.frame;//better to use this frame
    if(self.profilePhotos.count == 0)
    {
        UIImageView* imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = [UIImage imageNamed:@"noImage"];
        return imageView;
    }
 
    PFImageView* imageView = [[PFImageView alloc] initWithFrame:frame];
    imageView.file = ((CRPhoto*)self.profilePhotos[index]).photoFile;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.userInteractionEnabled = YES;
    [imageView loadInBackground];
    
    return imageView;
}

#pragma mark - iCarouselDelegate protocol implementation

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    if (self.profilePhotos.count > 0) {
        [self performSegueWithIdentifier:@"PhotoViewSegue" sender:carousel];
    }
}
#pragma mark - switchCell protocol

- (void)switchProfileView:(UIView *)view didChangeItemAtIndex:(NSUInteger)index
{
    self.selectedProfileType = self.profile.profileTypes.profileTypesArray[index];
    [self.tableView reloadData];
}


#pragma mark - private methods

- (void)editClicked{
    [self performSegueWithIdentifier:@"editProfileSegue" sender:self];
}

#pragma mark - Actions

- (void)yesButtonTapped:(id)sender {
    if ([self.delegate respondsToSelector:@selector(profileController:didRateProfile:)]) {
        [self.delegate profileController:self didRateProfile:YES];
    }
}

- (void)noButtonTapped:(id)sender {
    if ([self.delegate respondsToSelector:@selector(profileController:didRateProfile:)]) {
        [self.delegate profileController:self didRateProfile:NO];
    }
}

#pragma mark - UIGestureRecognizerDelegate protocol implementation

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint touchLocation = [touch locationInView:self.view];
    if (CGRectContainsPoint(self.photoCarousel.frame, touchLocation)) {
        return self.photoCarousel.currentItemIndex == 0 || self.revealViewController.frontViewPosition == FrontViewPositionRight;
    } else return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - SWRevealViewControllerDelegate protocol implementation

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    if (position == FrontViewPositionRight) {
        self.photoCarousel.userInteractionEnabled = NO;
    }
    if (position == FrontViewPositionLeft) {
        self.photoCarousel.userInteractionEnabled = YES;
    }
    
}

#pragma mark - AlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ///faliled to fetch profiles
    [[CRFetchManager sharedManager] fetchAllInfoInBackgroundForUser:self.profile];
    CRMenuController* controler = (CRMenuController*)[self.revealViewController rearViewController];
    [controler selectMenuItemAtIndex:CRMenuItemHome];
    [controler performSegueWithIdentifier:@"HomeSegue" sender:self];
}

@end
