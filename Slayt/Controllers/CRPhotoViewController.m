//
//  CRPhotoViewController.m
//  Talent
//
//  Created by Dmitry Utenkov on 4/30/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRPhotoViewController.h"
#import "CRPhoto.h"
#import <Parse/Parse.h>

@interface CRPhotoViewController ()

@property (strong) PFImageView *imageView;

@end

@implementation CRPhotoViewController

- (void)setPhotos:(NSMutableArray *)photos {
    _photos = photos;
    if (_photos) {
        self.currentPhotoIndex = 0;
        [self updateButtons];
        [self.carousel reloadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.carousel.pagingEnabled = YES;
    self.carousel.bounces = NO;
    self.navigationController.toolbar.clipsToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.carousel scrollToItemAtIndex:self.currentPhotoIndex animated:NO];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - Actions

- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)previousPhotoButtonTapped:(id)sender {
    if (self.currentPhotoIndex > 0) {
        self.currentPhotoIndex--;
        [self.carousel scrollToItemAtIndex:self.currentPhotoIndex animated:YES];
        [self updateButtons];
    }
}

- (IBAction)nextPhotoButtonTapped:(id)sender {
    if (self.currentPhotoIndex < self.photos.count-1) {
        self.currentPhotoIndex++;
        [self.carousel scrollToItemAtIndex:self.currentPhotoIndex animated:YES];
        [self updateButtons];
    }
}

- (void)updateButtons {
    if (self.photos.count > 0) {
        self.navigationItem.title = [NSString stringWithFormat:@"%lu of %lu", (long)self.currentPhotoIndex+1, (unsigned long)self.photos.count];
        self.previousPhotoButton.enabled = self.currentPhotoIndex != 0;
        self.nextPhotoButton.enabled = self.currentPhotoIndex != self.photos.count-1;
    } else {
        self.navigationItem.title = @"";
        self.previousPhotoButton.enabled = NO;
        self.nextPhotoButton.enabled = NO;
    }
}

#pragma mark - iCarouselDataSource protocol implementation

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return self.photos.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view {
    if (view == nil) {
        view = [[PFImageView alloc] initWithFrame:carousel.bounds];
        view.contentMode = UIViewContentModeScaleAspectFit;
        view.userInteractionEnabled = YES;
        self.imageView = (PFImageView *)view;
    }
    
    self.imageView.file = ((CRPhoto*)self.photos[index]).photoFile;
    [self.imageView loadInBackground];
    return self.imageView;
}

#pragma mark - iCarouselDelegate protocol implementation

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel {
    self.currentPhotoIndex = carousel.currentItemIndex;
    [self updateButtons];
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    BOOL hiddenStatus = !self.navigationController.navigationBarHidden;
    [self.navigationController setNavigationBarHidden:hiddenStatus animated:YES];
    [self.navigationController setToolbarHidden:hiddenStatus animated:YES];
}

@end
