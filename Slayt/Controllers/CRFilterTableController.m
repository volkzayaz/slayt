//
//  CRFIlterTableController.m
//  Slayt
//
//  Created by Vlad Soroka on 5/28/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRFilterTableController.h"
#import "CRBaseFilterEditingController.h"
#import "CRChooseLocationController.h"

#import "CRUtils.h"
#import "CRUserFilter.h"

#import "CRFilterPlistHelper.h"
#import "CRRatingManager.h"

#import "NSNumber+CGFloat.h"

@interface CRFilterTableController ()<UISearchBarDelegate, CRBaseEditValuesProtocol>
@property (weak, nonatomic) IBOutlet UISearchBar *userSearchBar;

@property (nonatomic, strong) NSArray* basicFilterDetails;
@property (nonatomic, strong) NSArray* specificProfileDetails;

///array that contain basicFilterDetails and SpecificProfileDetails
@property (nonatomic, strong) NSArray* filterTableDetails;

@property (nonatomic, assign) BOOL showAllDetails;

@property (nonatomic, strong) CRUserFilter *filter;

@end

@implementation CRFilterTableController


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.filter = [[CRUserFilter alloc] initWithUser:[CRUser currentUser]];
    
    ///basic cells like career type or location distance
    NSString* plistName = @"FilterScreenDetails";
    NSString *plistFilePath = [[NSBundle mainBundle] pathForResource: plistName ofType: @"plist"];
    self.basicFilterDetails = [NSArray arrayWithContentsOfFile:plistFilePath];

    _showAllDetails = NO;
}

- (void)setSpecificProfileDetails:(NSArray *)specificProfileDetails
{
    _specificProfileDetails = specificProfileDetails;
    self.filterTableDetails = [self.basicFilterDetails arrayByAddingObjectsFromArray:self.specificProfileDetails];
}

- (void)setBasicFilterDetails:(NSArray *)basicFilterDetails
{
    _basicFilterDetails = basicFilterDetails;
    self.filterTableDetails = [self.basicFilterDetails arrayByAddingObjectsFromArray:self.specificProfileDetails];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
 
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 90)];
    view.userInteractionEnabled = YES;
    
    UIButton* resetButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 10, 290, 50)];
    resetButton.backgroundColor = [UIColor whiteColor];
    resetButton.layer.cornerRadius = 3;
    [resetButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [resetButton setTitle:@"Reset Filter" forState:UIControlStateNormal];
    [resetButton setTitleColor:[UIColor colorWithRed:240./255. green:113./255. blue:0x88/0xff alpha:1.]
                      forState:UIControlStateNormal];
    
    [resetButton setTitleColor:[UIColor blackColor]
                      forState:UIControlStateHighlighted];
    
    [resetButton addTarget:self action:@selector(resetFilterClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:resetButton];
   
    self.tableView.tableFooterView = view;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.specificProfileDetails = [CRFilterPlistHelper specificProfileDetailsForFilter:self.filter];

    NSString* fullNameConstraint = [self.filter valueFromConstraintType:CRConstraintTypeContainsString
                                                                 forKey:@"fullName"];
    self.userSearchBar.text = fullNameConstraint;
    
    self.tableView.tableFooterView.hidden = ![self.filter isFilterSet];
    [self.tableView reloadData];
}

- (IBAction)applyFiltering:(id)sender {
    [self.filter saveFilterSettingsForceParseSave:YES];
    [[CRRatingManager sharedInstance] reloadProfiles];

    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)cancelFiltering:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:^{

    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber* defaultValue = @(44);
    NSNumber* height = 0;
    
    if ([self showMoreDetailsCell] &&
        indexPath.section == tableView.numberOfSections - 1)//last section
    {
        return defaultValue.CGFloatValue;
    }
    
    height = self.filterTableDetails[indexPath.section][@"properties"][indexPath.row][@"rowHeight"];
    
    if(!height)
    {
        height = defaultValue;//defaultValue
    }
    
    ///if your property potentially has a lot of information to present to user, than you can choose to use dynamic row height. The height will be determined based the amount of characters that are neccessary for displaying the property.
    BOOL isRowHeightDynamic = ((NSNumber*)self.filterTableDetails[indexPath.section][@"properties"][indexPath.row][@"dynamicRowHeight"]).boolValue;
    if(isRowHeightDynamic)
    {
        NSString* key = self.filterTableDetails[indexPath.section][@"properties"][indexPath.row][@"propertyName"];
        NSString* displayType = self.filterTableDetails[indexPath.section][@"properties"][indexPath.row][@"filterDisplayType"];
        
        NSString* detailString = [self.filter descriptionForKey:key withDisplayType:displayType additionalDetails:nil];
        
        CGSize sizeToFit = [[CRUtils sharedInstance] sizeForString:detailString withFont:[UIFont fontWithName:@"HelveticaNeue" size:15] toFitWidth:260];
        if(height.CGFloatValue/2 < sizeToFit.height)
        {
            height = @(height.CGFloatValue + sizeToFit.height);
        }
    }

    return height.CGFloatValue;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([self showMoreDetailsCell] &&
        section == tableView.numberOfSections - 1)//last section
    {
        return 0;
    }
    else if (section == 0)
    {
        return 20;
    }
    
    NSString* headerTitle = self.filterTableDetails[section][@"sectionHeader"];
    if(!headerTitle)
    {
        return 0;
    }
    
    return 25;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([self showMoreDetailsCell] &&
        section == tableView.numberOfSections - 1)//last section
    {
        return nil;
    }
    
    NSString* headerTitle = self.filterTableDetails[section][@"sectionHeader"];
    
    if(!headerTitle)
    {
        return nil;
    }
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 25)];
    UILabel* headerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(17, 0, self.view.frame.size.width-17, 25)];
    headerTitleLabel.text = headerTitle;
    [headerTitleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    [headerTitleLabel setTextColor:[UIColor colorWithRed:0x80/255.f green:0x7f/255.f blue:0xb0/255.f alpha:1]];
    [headerView addSubview:headerTitleLabel];
    return headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numberOfSections = 0;
    
    if(![self showMoreDetailsCell])
    {
        numberOfSections = self.filterTableDetails.count;
    }
    else
    {
        if(_showAllDetails)
        {
            numberOfSections += self.filterTableDetails.count;
        }
        else
        {
            numberOfSections += self.basicFilterDetails.count;//we need to display basic filterDetails regardless of |showAllDetails| state
            numberOfSections += 1;//we will display only one section from specificDetails
        }
        
        numberOfSections++;//More Details cell will be displayed in it's own section.
    }
    
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.numberOfSections == section + 1 && // it's the last section and
       [self showMoreDetailsCell])// we need to display MoreDetails Cell
    {
        return 1;
    }
    
    return [self.filterTableDetails[section][@"properties"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.numberOfSections == indexPath.section + 1 && [self showMoreDetailsCell])
    {
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ExpandDetailsCell" forIndexPath:indexPath];
        
        if(_showAllDetails)
        {
            cell.textLabel.text = @"Less Details";
            cell.accessoryView = [[ UIImageView alloc ]
                                  initWithImage:[UIImage imageNamed:@"arrow_up" ]];
        }
        else
        {
            cell.textLabel.text = @"More Details";
            cell.accessoryView = [[ UIImageView alloc ]
                                  initWithImage:[UIImage imageNamed:@"arrow_down" ]];
        }
        
        return cell;
    }
    
    NSDictionary* currentPropertyDictionary = self.filterTableDetails[indexPath.section][@"properties"][indexPath.row];
    
    NSString *CellIdentifier = currentPropertyDictionary[@"filterCellIdentifier"];
    if(!CellIdentifier)
    {
        CellIdentifier = currentPropertyDictionary[@"cellIdentifier"];
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = currentPropertyDictionary[@"displayName"];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    NSString* propertyName = currentPropertyDictionary[@"propertyName"];
    NSString* displayType = currentPropertyDictionary[@"filterDisplayType"];
    NSString* trailingSymbols = currentPropertyDictionary[@"trailingSymbols"];
    NSString* paymentFrequencyPropertyName = currentPropertyDictionary[@"numberPropertyName"];
    
    NSMutableDictionary* additionalInfoDictionary = [NSMutableDictionary dictionary];
    
    if(trailingSymbols)
    {
        [additionalInfoDictionary setObject:trailingSymbols
                                     forKey:kAdditionalDetailsKeyTrailingSymbols];
    }
    
    if(paymentFrequencyPropertyName)
    {
        [additionalInfoDictionary setObject:paymentFrequencyPropertyName
                                     forKey:kAdditionalDetailsKeyPaymentFrequencyPropertyName];
    }
        
    NSString* propertyDescirption = [self.filter descriptionForKey:propertyName
                                                   withDisplayType:displayType
                                                 additionalDetails:additionalInfoDictionary];
    
    cell.detailTextLabel.text = propertyDescirption;
    cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    
    return cell;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(tableView.numberOfSections == indexPath.section + 1 && [self showMoreDetailsCell])
    {
        _showAllDetails = !_showAllDetails;
        [tableView reloadData];
        
        return;
    }
    
    CRBaseFilterEditingController* editFilterController = nil;
    
    NSString* filteringClass = self.filterTableDetails[indexPath.section][@"properties"][indexPath.row][@"filteringClass"];
    NSString* propertyName = self.filterTableDetails[indexPath.section][@"properties"][indexPath.row][@"propertyName"];
    NSString* displayName = self.filterTableDetails[indexPath.section][@"properties"][indexPath.row][@"displayName"];
    
    //location is a bit special type to filter, so we'll use EditValuesController and handle all the preparetions by ourselves
    if([filteringClass isEqualToString:NSStringFromClass([CRChooseLocationController class])])
    {
        [self pushFilterLocationControllerWithTitle:displayName];
        return;
    }
    
    if(filteringClass)
    {
        editFilterController = [[NSClassFromString(filteringClass) alloc] init];
    }
    
    //NSAssert(editFilterController != nil, @"You are about to push nil view controller. Check that your property have filtering class");
    if(!editFilterController)
    {
        [[[UIAlertView alloc] initWithTitle:@"Ouch" message:[NSString stringWithFormat:@"Filtering hasn't been implemented for %@ yet",displayName] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    editFilterController.editingFilter = self.filter;
    editFilterController.entityProperties = self.filterTableDetails;
    editFilterController.entityIndexPath = indexPath;
    editFilterController.propertyName = propertyName;
    
    [self.navigationController pushViewController:editFilterController animated:YES];
    
    editFilterController.navigationItem.title = displayName;
}

#pragma mark - searchBar delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length != 0)
    {
        [self.filter setConstraintOfType:CRConstraintTypeContainsString
                                  forKey:@"fullName"
                                   value:searchText];
    }
    else
    {
        [self.filter removeConstraintOfType:CRConstraintTypeContainsString
                                     forKey:@"fullName"];
    }
    
    
    self.tableView.tableFooterView.hidden = ![self.filter isFilterSet];
}

#pragma mark - UIControl callbacks

- (void) resetFilterClicked:(id)sender
{
    ///reseting model
    [self.filter removeAllConstraints];
    
    //reseting UI
    self.specificProfileDetails = nil;
    self.userSearchBar.text = @"";
    
    self.tableView.tableFooterView.hidden = YES;
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self.tableView reloadData];
}

#pragma mark - Edit Values delegate

- (void)editValueController:(CRBaseEditValuesController *)controller didEditValue:(id)value
{
    if(![value isEqual:[NSNull null]])
    {
        [self.filter setConstraintOfType:CRConstraintTypeEqual
                                  forKey:@"homeTown"
                                   value:value];
    }
    else
    {
        [self.filter removeConstraintOfType:CRConstraintTypeEqual
                                     forKey:@"homeTown"];
    }
}

#pragma mark - private methods

- (BOOL) showMoreDetailsCell
{
    return self.specificProfileDetails.count > 1;
}

- (void) pushFilterLocationControllerWithTitle:(NSString*)title
{
    CRChooseLocationController* chooseLocationController = [[CRChooseLocationController alloc] initWithSaveType:CRBaseEditValuesSaveTypeDelegate];
    chooseLocationController.delegate = self;
    chooseLocationController.initialLocation = [self.filter valueFromConstraintType:CRConstraintTypeEqual
                                                                             forKey:@"homeTown"];
    [self.navigationController pushViewController:chooseLocationController animated:YES];
    
    chooseLocationController.navigationItem.title = title;
}

@end
