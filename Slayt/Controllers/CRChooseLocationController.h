//
//  CRChooseLocationControllerViewController.h
//  Talent
//
//  Created by Vlad Soroka on 4/15/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CREditStringController.h"

#warning multiple entering/leaving actions causes slow memory leak
@interface CRChooseLocationController : CREditStringController

@property (nonatomic, strong) NSString* initialLocation;

@end
