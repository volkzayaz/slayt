//
//  CRResetPasswordViewController.h
//  Talent
//
//  Created by Dmitry Utenkov on 4/30/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRResetPasswordViewController : UIViewController <UITextFieldDelegate>

@property (weak) IBOutlet UITextField *emailTextField;

- (IBAction)resetPasswordButtonTapped:(id)sender;

@end
