//
//  CRMainController.m
//  Talent
//
//  Created by Vlad Soroka on 4/1/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRHomeViewController.h"
#import "SWRevealViewController.h"
#import "CRUser.h"
#import "CRTutorialViewController.h"
#import "CRMenuController.h"
#import "CRProfileController.h"
#import "CRCustomModalSegue.h"
#import "CRPhoto.h"
#import "CRProfileController.h"
#import "CRRatingManager.h"
#import "CRFriendsListViewController.h"
#import "CRFriendsManager.h"

#import "UIImage+Utilities.h"


#import "CRPreference.h"

#import "CRModel.h"

typedef NS_ENUM(NSUInteger, CRHomeViewControllerState)
{
	CRHomeViewControllerStateDefault,
    CRHomeViewControllerStateFetchingProfiles,
    CRHomeViewControllerStateLikeProfile,
    CRHomeViewControllerStateDislikeProfile,
    CRHomeViewControllerStateNoProfilesFound,
    CRHomeViewControllerStateProfileLoaded
};

typedef NS_ENUM(NSUInteger, CRProfileCardAnimation)
{
	CRProfileCardAnimationNone,
    CRProfileCardAnimationLeft,
    CRProfileCardAnimationRight
};

@interface CRHomeViewController () <CRSignupViewControllerDelegate, CRProfileControllerDelegate, UIGestureRecognizerDelegate, CRRatingManagerDelegate>

@property (strong, nonatomic) UIBarButtonItem *revealButtonItem;

@property (strong, nonatomic) IBOutlet UIView *emptyProfileView;

@property (strong, nonatomic) IBOutlet UIView *profileView;

@property (weak, nonatomic) IBOutlet UIView *profileCardView;

@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *profileTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (nonatomic, strong) UIGestureRecognizer *revealPanGestureRecognizer;
@property (nonatomic, strong) IBOutlet UISwipeGestureRecognizer *leftGestureRecognizer;
@property (nonatomic, strong) IBOutlet UISwipeGestureRecognizer *rightGestureRecognizer;

@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *viewProfileButton;


@property (nonatomic, strong) CRUser *currentProfile;

@property (nonatomic, strong) NSTimer *updateTimer;

@property (nonatomic, assign) CRProfileCardAnimation profileCardAnimation;

@property (nonatomic, assign) CRHomeViewControllerState state;

@end

@implementation CRHomeViewController

#pragma mark - Properties

#pragma mark - View lifecycle

- (void)dealloc {
    self.leftGestureRecognizer.delegate = nil;
    self.rightGestureRecognizer.delegate = nil;
}

- (void)awakeFromNib {
    _showsLoginScreenAnimated = NO;
    _profileCardAnimation = CRProfileCardAnimationNone;
    _state = CRHomeViewControllerStateNoProfilesFound;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.revealViewController.rearViewRevealWidth = 270.0f;
    self.revealViewController.rightViewRevealWidth = 270.0f;
    
    
    NSString *profileViewNibName = @"HomeScreenProfileView";
    if ([UIScreen mainScreen].bounds.size.height == 568) {
        profileViewNibName = @"HomeScreenProfileView-568h";
    }
    [[NSBundle mainBundle] loadNibNamed:profileViewNibName owner:self options:nil];
    
    
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"RevealIcon"]
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:self.revealViewController
                                                                            action:@selector(revealToggle:)];
    [self.viewProfileButton setTitle:@"VIEW\nPROFILE" forState:UIControlStateNormal];
    self.revealPanGestureRecognizer = self.revealViewController.panGestureRecognizer;
    [self.view addGestureRecognizer:self.revealPanGestureRecognizer];
    
/////////quick code.
    
    UIView *filterButtonView = [[NSBundle mainBundle] loadNibNamed:@"HomeScreenCaptionView" owner:self options:nil][0];
    UIButton *filterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    filterButton.frame = filterButtonView.frame;
    
    [filterButton addTarget:self action:@selector(titleTap:) forControlEvents:UIControlEventTouchUpInside];
    [filterButton addSubview:filterButtonView];
    self.navigationItem.titleView = filterButton;
    
    
/////////
    
    self.profileTypeLabel.adjustsFontSizeToFitWidth = YES;
    
}

- (void)titleTap:(id)sender
{
    [self performSegueWithIdentifier:@"FilterScreenSegue" sender:self];
}

- (void)viewWillAppear:(BOOL)animated {
    CRUser* currentUser = [CRUser currentUser];
    if (!currentUser) {
        [self performSegueWithIdentifier:@"LoginSegue" sender:self];
    } else {
        self.currentProfile = [CRRatingManager sharedInstance].currentProfile;
        if (self.currentProfile) {
            [self updateProfileView:self.profileCardAnimation];
        } else {
            [self setState:CRHomeViewControllerStateNoProfilesFound];
        }
    }
    
    self.revealPanGestureRecognizer.delegate = self;
    
    CRMenuController *menu = (CRMenuController *)self.revealViewController.rearViewController;
    [menu selectMenuItemAtIndex:CRMenuItemHome];
    
    [CRRatingManager sharedInstance].delegate = self;
    
    
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.revealPanGestureRecognizer.delegate = nil;
    [CRRatingManager sharedInstance].delegate = nil;
    self.profileCardAnimation = CRProfileCardAnimationNone;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"LoginSegue"]) {
        self.currentProfile = nil;
        CRCustomModalSegue *customSegue = (CRCustomModalSegue *)segue;
        customSegue.animated = self.showsLoginScreenAnimated;
        customSegue.toolbarHidden = YES;
        customSegue.statusBarHidden = YES;
        id destinationViewController = segue.destinationViewController;
        if ([destinationViewController respondsToSelector:@selector(topViewController)]) {
            destinationViewController = [destinationViewController topViewController];
        }
        if ([destinationViewController respondsToSelector:@selector(setSignupDelegate:)]) {
            [destinationViewController setSignupDelegate:self];
        }
    } else if ([segue.identifier isEqualToString:@"ProfileSegue"]) {
        CRProfileController *profileVC = segue.destinationViewController;
        profileVC.profile = self.currentProfile;
        profileVC.delegate = self;
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:nil
                                                                                action:nil];
        profileVC.navigationItem.titleView = [self buttonsViewForViewController:profileVC];
        profileVC.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_more"]
                                                                                       style:UIBarButtonItemStyleBordered
                                                                                      target:nil
                                                                                      action:nil];
    }
}

#pragma mark - Actions

- (IBAction)viewProfileButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"ProfileSegue" sender:self];
}

- (IBAction)yesButtonTapped:(id)sender {
    [self rateProfile:CRPreferenceLike];
}

- (IBAction)noButtonTapped:(id)sender {
    [self rateProfile:CRPreferenceDislike];
}

- (IBAction)handleLeftSwipe:(id)sender {
    [self rateProfile:CRPreferenceDislike];
}

- (IBAction)handleRightSwipe:(id)sender {
    [self rateProfile:CRPreferenceLike];
}

- (IBAction)messageButtonTapped:(id)sender {
    [self.revealViewController rightRevealToggleAnimated:YES];
}

#pragma mark - CRSignupViewControllerDelegate protocol implementation

- (void)signupViewControllerSuccessfullySignup:(CRSignupViewController *)signupViewController {
    [[CRRatingManager sharedInstance] reloadProfiles];
    if ([CRUser currentUser].isNew) {
        CRMenuController* menu = (CRMenuController*)self.revealViewController.rearViewController;
        [menu performSegueWithIdentifier:@"ProfileSegue" sender:self];
    }
    
    // TODO: Temporary solution
    id rightVC = self.revealViewController.rightViewController;
    if ([rightVC conformsToProtocol:@protocol(CRFriendsManagerDelegate)]) {
        [CRFriendsManager sharedInstance].delegate = (id<CRFriendsManagerDelegate>)rightVC;
        [[CRFriendsManager sharedInstance] reloadFriends];
    }
}

#pragma mark - CRProfileControllerDelegate protocol implementation

- (void)profileController:(CRProfileController *)controller didRateProfile:(BOOL)liked {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self rateProfile:(liked ? CRPreferenceLike : CRPreferenceDislike)];
}

#pragma mark - UIGestureRecognizerDelegate protocol implementation

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    // If MenuVC or EmptyProfileView is shown ignore all gestures except revealPanGesture
    if (self.revealViewController.frontViewPosition == FrontViewPositionRight ||
        self.revealViewController.frontViewPosition == FrontViewPositionLeftSide ||
        self.emptyProfileView.alpha == 1)
    {
        return gestureRecognizer == self.revealViewController.panGestureRecognizer;
    } else {
        // PanGesture ignored in area of ProfileCardView
        if (gestureRecognizer == self.revealViewController.panGestureRecognizer) {
            CGPoint touchLocation = [touch locationInView:self.view];
            return !CGRectContainsPoint(self.profileCardView.frame, touchLocation);
        } else {
            return YES;
        }
    }
}

#pragma mark - CRRatingManagerDelegate protocol implementation

- (void)ratingManager:(CRRatingManager *)manager didLoadProfile:(CRUser *)loadedProfile {
    self.currentProfile = loadedProfile;
    [self updateProfileView:self.profileCardAnimation];
}

- (void)ratingManagerDidNotFoundProfiles:(CRRatingManager *)manager {
    if (self.currentProfile) {
        self.currentProfile = nil;
    } else {
        self.profileCardAnimation = CRProfileCardAnimationNone;
    }
    
    self.state = CRHomeViewControllerStateNoProfilesFound;
}

#pragma mark - Utility methods

- (void)rateProfile:(CRPreferenceType)preferenceType {
    if (preferenceType == CRPreferenceLike) {
        [self setState:CRHomeViewControllerStateLikeProfile];
    } else {
        [self setState:CRHomeViewControllerStateDislikeProfile];
    }
    
    [[CRRatingManager sharedInstance] rateCurrentProfile:preferenceType];
}

- (void)updateProfileView:(CRProfileCardAnimation)animation {
    if (self.currentProfile) {
        [self.currentProfile requestMainPhotoWithCompletitionBlock:^(CRPhoto *photo, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error.localizedDescription);
                self.state  = CRHomeViewControllerStateNoProfilesFound;
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSData *photoData = [photo.photoFile getData];
                UIImage *photoImage = nil;
                if (photoData) {
                    photoImage = [UIImage imageWithData:photoData];
                } else {
                    photoImage = [UIImage imageNamed:@"noImage"];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (animation != CRProfileCardAnimationNone) {
                        [self hideProfileCardWithAnimation:animation];
                    }
                    
                    self.profilePicture.image = photoImage;
                    self.nameLabel.text = self.currentProfile.fullName;
                    self.profileTypeLabel.text = [self.currentProfile.profileTypes stringOfAllProfileTypesWithDelimiter:@" \u2022 "];
                    
                    if ([self.currentProfile.profileTypes profileForType:CRProfileTypeModel]) {
                        self.statusLabel.hidden = NO;
                        self.statusLabel.text = self.currentProfile.agencyName ? self.currentProfile.agencyName : @"unsigned talent";
                    } else {
                        self.statusLabel.text = @"";
                        self.statusLabel.hidden = YES;
                    }
                    
                    
                    if (![self.profileView isDescendantOfView:self.view]) {
                        self.profileView.alpha = 0;
                        [self.view addSubview:self.profileView];
                    }
                    
                    [UIView animateWithDuration:0.2
                                          delay:0
                                        options:UIViewAnimationOptionTransitionCrossDissolve
                                     animations:^
                     {
                         self.profileView.alpha = 1;
                         self.emptyProfileView.alpha = 0;
                     }
                                     completion:^(BOOL finished) {
                                         
                                     }];
                    self.state = CRHomeViewControllerStateDefault;
                });
            });
        }];
    } else {
        self.state = CRHomeViewControllerStateNoProfilesFound;
    }
}

- (void)hideProfileCardWithAnimation:(CRProfileCardAnimation)animation {
    if (animation != CRProfileCardAnimationNone) {
        __block UIImageView *profileCardImageView = [[UIImageView alloc] initWithImage:[UIImage imageWithView:self.profileCardView]];
        profileCardImageView.frame = self.profileCardView.frame;
        [self.view insertSubview:profileCardImageView aboveSubview:self.profileCardView];
        
        [UIView animateWithDuration:0.5 animations:^{
            CGRect frame = profileCardImageView.frame;
            frame.origin.x += (animation == CRProfileCardAnimationRight ? 480 : -480);
            profileCardImageView.frame = frame;
        } completion:^(BOOL finished) {
            [profileCardImageView removeFromSuperview];
        }];
    }
}

- (UIView *)buttonsViewForViewController:(id)viewController {
    UIView *buttons = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 38 * 2 + 10, 38)];
    buttons.backgroundColor = [UIColor clearColor];
    UIButton *noButton = [self buttonWithTitle:@"NO" target:viewController selector:@selector(noButtonTapped:)];
    [buttons addSubview:noButton];
    noButton.frame = CGRectMake(0, 0, 38, 38);
    UIButton *yesButton = [self buttonWithTitle:@"YES" target:viewController selector:@selector(yesButtonTapped:)];
    [buttons addSubview:yesButton];
    yesButton.frame = CGRectMake(38+10, 0, 38, 38);
    return buttons;
}

- (UIButton *)buttonWithTitle:(NSString *)title target:(id)target selector:(SEL)selector {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 38, 38);
    button.backgroundColor = [UIColor clearColor];
    button.layer.borderWidth = 1;
    button.layer.rasterizationScale = [UIScreen mainScreen].scale;
    button.layer.borderColor = [UIColor whiteColor].CGColor;
    button.layer.cornerRadius = button.frame.size.width / 2;
    
    button.titleLabel.font = [UIFont boldSystemFontOfSize:10];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateHighlighted];
    
    [button setShowsTouchWhenHighlighted:YES];
    
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (void)setState:(CRHomeViewControllerState)state {
    dispatch_async(dispatch_get_main_queue(), ^{
        _state = state;
        switch (state) {
            case CRHomeViewControllerStateDefault:
            {
                self.yesButton.enabled = YES;
                self.noButton.enabled = YES;
                self.viewProfileButton.enabled = YES;
                self.leftGestureRecognizer.enabled = YES;
                self.rightGestureRecognizer.enabled = YES;
            }
                break;
                
            case CRHomeViewControllerStateLikeProfile:
            case CRHomeViewControllerStateDislikeProfile:
            {
                self.yesButton.enabled = NO;
                self.noButton.enabled = NO;
                self.viewProfileButton.enabled = NO;
                self.leftGestureRecognizer.enabled = NO;
                self.rightGestureRecognizer.enabled = NO;
                self.profileCardAnimation = state == CRHomeViewControllerStateLikeProfile ?
                CRProfileCardAnimationRight : CRProfileCardAnimationLeft;
            }
                break;
                
            case CRHomeViewControllerStateNoProfilesFound:
            {
                if (self.profileCardAnimation != CRProfileCardAnimationNone) {
                    [self hideProfileCardWithAnimation:self.profileCardAnimation];
                }
                
                self.profilePicture.image = [UIImage imageNamed:@"noImage"];
                self.nameLabel.text = @"Not specified";
                self.profileTypeLabel.text = @"Not specified";
                
                [UIView animateWithDuration:0.2
                                      delay:0
                                    options:UIViewAnimationOptionTransitionCrossDissolve
                                 animations:^
                 {
                     self.profileView.alpha = 0;
                     self.emptyProfileView.alpha = 1;
                 }
                                 completion:nil];
            }
                break;
                
            case CRHomeViewControllerStateProfileLoaded:
            {
                
            }
                break;
                
                
            default:
                break;
        }
    });
}

@end
