//
//  CRLoginController.m
//  Talent
//
//  Created by Vlad Soroka on 4/4/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRLoginController.h"

#import "CRUser.h"
#import "CRUtils.h"
#import "Constants.h"
#import "CRLoginManager.h"
#import "CRProfileGenerator.h"

#import "CRTextFieldTableViewCell.h"
#import <Parse/PF_MBProgressHUD.h>

#import "CRSignupViewController.h"
#import "SWRevealViewController.h"
#import "CRRatingManager.h"

#import "CRFriendsListViewController.h"
#import "CRFriendsManager.h"

@interface CRLoginController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIAlertViewDelegate, CRUserManagerDelegate>

@property (nonatomic, strong) CRLoginManager *userManager;
@property (nonatomic, strong) NSArray *items;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, weak) IBOutlet UIButton *forgotPasswordButton;
@property (nonatomic, strong) PF_MBProgressHUD* HUD;

- (IBAction)cancelButtonClicked:(id)sender;
- (IBAction)loginButtonClicked:(id)sender;
- (IBAction)loginWithFacebookClicked:(id)sender;

@end

static const NSUInteger kRegisterAlertTag   = 100;
static const NSUInteger kLinkAlertTag       = 200;

@implementation CRLoginController

- (void)awakeFromNib {
    self.items = @[ @{ @"Placeholder"       : @"Email",
                       @"CellIdentifier"    : @"SignupEmailCell" },
                    
                    @{ @"Placeholder"       : @"Password",
                       @"CellIdentifier"    : @"SignupSecureCell" } ];
    
    self.userManager = [CRLoginManager manager];
    self.userManager.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
#ifdef DEBUG
    UIBarButtonItem *testUserLogin = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                   target:self
                                                                                   action:@selector(loginTestUser:)];
    UIBarButtonItem *createUsers = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                 target:self
                                                                                 action:@selector(createUsers:)];
    UIBarButtonItem *removeAllData = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                                                                 target:self
                                                                                 action:@selector(removeAllData:)];
    self.navigationItem.rightBarButtonItems = @[ createUsers, testUserLogin, removeAllData ];
#endif
    
    // Adding underline to forgotPasswordButton
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc]
                                                initWithString:self.forgotPasswordButton.titleLabel.text];
    
    [commentString addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:NSMakeRange(0, commentString.length)];
    
    [commentString addAttribute:NSForegroundColorAttributeName
                          value:[UIColor whiteColor]
                          range:NSMakeRange(0, commentString.length)];
    
    [self.forgotPasswordButton setAttributedTitle:commentString forState:UIControlStateNormal];
    
    // For keyboard dismissal on touch
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    self.HUD = [[PF_MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SignupSegue"]) {
        [segue.destinationViewController signupFacebookButtonTapped:nil];
    }
}

#pragma mark - Actions

- (IBAction)cancelButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)loginButtonClicked:(id)sender {
    self.HUD.labelText = @"Authorizing";
    [self.HUD show:YES];
    [self.view endEditing:YES];
    
    NSDictionary *credentials = @{ CRLoginManagerEmailKey     : self.emailTextField.text,
                                   CRLoginManagerPasswordKey  : self.passwordTextField.text };
    [self.userManager login:credentials];
}

- (IBAction)loginWithFacebookClicked:(id)sender {
    self.HUD.labelText = @"Connecting to Facebook";
    [self.HUD show:YES];
    [self.view endEditing:YES];
    
    [self.userManager loginWithFacebook];
}

- (void)dismissKeyboard:(id)sender {
    [self.view endEditing:NO];
}

#pragma mark - UITextFieldDelegate protocol implementation

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if([textField isEqual:self.emailTextField]) {
        [self.passwordTextField becomeFirstResponder];
    } else if([textField isEqual:self.passwordTextField]) {
        [self loginButtonClicked:nil];
    }
    return YES;
}

#pragma mark - UITableViewDataSource protocol implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = self.items[indexPath.row][@"CellIdentifier"];
    CRTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.textField.placeholder = self.items[indexPath.row][@"Placeholder"];
    
    if ([cellIdentifier isEqualToString:@"SignupEmailCell"]) {
        self.emailTextField = cell.textField;
    } else if ([cellIdentifier isEqualToString:@"SignupSecureCell"]) {
        self.passwordTextField = cell.textField;
    }
    return cell;
}

#pragma mark - UIAlertViewDelegate protocol implementation

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kLinkAlertTag)
    {
        if (buttonIndex != alertView.cancelButtonIndex) {
            [self.userManager linkFacebookUser:[alertView textFieldAtIndex:0].text];
        }
    }
    else if (alertView.tag == kRegisterAlertTag)
    {
        [[CRUser currentUser] delete];
        [self performSegueWithIdentifier:@"SignupSegue" sender:self];
    }
}

- (void)alertViewCancel:(UIAlertView *)alertView {
    // TODO: check for empty facebook account
    // if user enter background during registration process
    [CRUser logOut];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    if (alertView.tag == kLinkAlertTag) {
        return [[alertView textFieldAtIndex:0] text].length != 0;
    } else return YES;
}

#pragma mark - CRUserManagerDelegate protocol implementation

- (void)userManagerDidSuccessfullLogin:(CRLoginManager *)maanger {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.HUD hide:YES];
        [self.revealViewController.rearViewController performSegueWithIdentifier:@"HomeSegue" sender:nil];
        [self dismissViewControllerAnimated:YES completion:^{
            if ([CRUser currentUser]) {
                
                // TODO: TEMPORARY SOLUTION
                if ([[[UIApplication sharedApplication].windows[0] rootViewController] isKindOfClass:[SWRevealViewController class]]) {
                    SWRevealViewController *revealVC = (SWRevealViewController *)[[UIApplication sharedApplication].windows[0] rootViewController];
                    id rightVC = revealVC.rightViewController;
                    if ([rightVC conformsToProtocol:@protocol(CRFriendsManagerDelegate)]) {
                        [CRFriendsManager sharedInstance].delegate = (id<CRFriendsManagerDelegate>)rightVC;
                        [[CRFriendsManager sharedInstance] reloadFriends];
                    }
                }
                
                [[CRRatingManager sharedInstance] login];
                [[CRFriendsManager sharedInstance] reloadFriends];
                
#if !TARGET_IPHONE_SIMULATOR
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                currentInstallation[@"user"] = [CRUser currentUser].objectId;
                [currentInstallation saveInBackground];
#endif
            }
        }];
    });
}

- (void)userManagerDidFail:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.HUD hide:YES];
        [[CRUtils sharedInstance] showAlertWithMessage:error.localizedDescription];
    });
    NSLog(@"%@", error.localizedDescription);
}

- (void)userManagerNeedsToLinkFacebookAccount:(CRLoginManager *)userManager {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.HUD hide:YES];
        UIAlertView *linkAlert = [[UIAlertView alloc] initWithTitle:@"Slayt"
                                                            message:@"If you want to link Facebook with your account enter password"
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Link", nil];
        [linkAlert setAlertViewStyle:UIAlertViewStyleSecureTextInput];
        linkAlert.tag = kLinkAlertTag;
        [linkAlert show];
    });
}

- (void)userManagerNeedsToRegisterFacebookAccount:(CRLoginManager *)userManager {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.HUD hide:YES];
        UIAlertView *registerAlert = [[UIAlertView alloc] initWithTitle:@"Slayt"
                                                                message:@"Do you want to register a new account?"
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel"
                                                      otherButtonTitles:@"Register", nil];
        registerAlert.tag = kRegisterAlertTag;
        [registerAlert show];
    });
}

#pragma mark - Debug buttons handlers

#ifdef DEBUG
- (void)loginTestUser:(id)sender {
    NSDictionary *credentials = @{ CRLoginManagerEmailKey     : PARSE_TEST_USER_EMAIL,
                                   CRLoginManagerPasswordKey  : PARSE_TEST_USER_PASSWORD };
    [self.userManager login:credentials];
}

- (void)createUsers:(id)sender {
    [[[CRProfileGenerator alloc] init] generateUsers:1];
}

- (void)removeAllData:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Remove all data?"
                                                       delegate:[CRUtils sharedInstance]
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:@"Remove"
                                              otherButtonTitles:nil, nil];
    [sheet showInView:self.view];
}
#endif

@end
