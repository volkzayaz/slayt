//
//  CRProfileTypeViewController.h
//  Talent
//
//  Created by Dmitry Utenkov on 4/29/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

@class CRProfileTypeViewController;

@protocol CRProfileTypeViewControllerDelegate <NSObject>

- (void)profileTypeViewController:(CRProfileTypeViewController *)profileTypeViewController didSelectProfileTypes:(NSArray *)types;

@end

@interface CRProfileTypeViewController : UIViewController

@property (weak) id<CRProfileTypeViewControllerDelegate> delegate;
@property (strong) NSMutableArray *selectedItems;

@end
