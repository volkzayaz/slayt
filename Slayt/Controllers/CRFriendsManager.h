//
//  CRFriendsManager.h
//  Slayt
//
//  Created by Dmitry Utenkov on 6/2/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CRFriendsManager;
@class CRUser;

@protocol CRFriendsManagerDelegate <NSObject>

- (void)friendsManager:(CRFriendsManager *)manager didLoadFriends:(NSArray *)loadedProfile;
- (void)friendsManagerDidFetchFriendData:(CRUser *)friendUser;
//- (void)friendsManagerDidFetchFriendPhoto:(UIImage *)friendUserPhoto;
- (void)friendsManagerDidNotFoundFriends:(CRFriendsManager *)manager;

@end

@interface CRFriendsManager : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic, weak) id<CRFriendsManagerDelegate> delegate;

@property (nonatomic, strong, readonly) NSArray *friends;

- (void)reloadFriends;

@end
