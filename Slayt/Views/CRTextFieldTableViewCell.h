//
//  CRSignupTableViewCell.h
//  Talent
//
//  Created by Dmitry Utenkov on 4/29/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRTextFieldTableViewCell : UITableViewCell

typedef NS_ENUM(NSUInteger, CRSignupCellIndex)
{
    CRSignupNameCell = 0,
    CRSignupEmailCell,
    CRSignupPasswordCell,
    CRSignupProfileTypeCell,
};

@property (weak) IBOutlet UITextField *textField;

@end
