//
//  CRSignupTableViewCell.m
//  Talent
//
//  Created by Dmitry Utenkov on 4/29/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRTextFieldTableViewCell.h"

@implementation CRTextFieldTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

@end
