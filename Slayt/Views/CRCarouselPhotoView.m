//
//  CRPhotoView.m
//  Talent
//
//  Created by Vlad Soroka on 4/15/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRCarouselPhotoView.h"

@interface CRCarouselPhotoView ()

@property (nonatomic, weak) UIView* containerView;

@end

@implementation CRCarouselPhotoView



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView* containerPhotoView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, 100, 100)];
        containerPhotoView.layer.masksToBounds = YES;
        containerPhotoView.layer.cornerRadius = 3.f;
        
        PFImageView* photoView = [[PFImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        
        photoView.contentMode = UIViewContentModeCenter;
        photoView.animationDuration = 0.7;
        
        [containerPhotoView addSubview:photoView];
        self.photoView = photoView;
        
        [self addSubview:containerPhotoView];
        self.containerView = containerPhotoView;
        
        CGFloat buttonSize = 40;
        //self.clipsToBounds = NO;
            
        UIButton* deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width - buttonSize, frame.size.width - buttonSize, buttonSize, buttonSize)];
        [deleteButton setBackgroundImage:[UIImage imageNamed:@"deletePhotoButton"] forState:UIControlStateNormal];
        [deleteButton setBackgroundImage:[UIImage imageNamed:@"deletePhotoButtonPressed"] forState:UIControlStateSelected];
        [deleteButton addTarget:self action:@selector(didPressDelete:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:deleteButton];
    }
    return self;
}

- (void)didPressDelete:(id)sender
{
    if([self.delegate respondsToSelector:@selector(didPressDeleteOnPhotoView:)])
    {
        [self.delegate didPressDeleteOnPhotoView:self];
    }
}

- (void)setHighligted:(BOOL)isHiglighted
{
    if(isHiglighted)
    {
        self.containerView.layer.borderColor = [UIColor orangeColor].CGColor;
        self.containerView.layer.borderWidth = 3.0f;
    }
    else
    {
        self.containerView.layer.borderColor = [UIColor clearColor].CGColor;
        self.containerView.layer.borderWidth = 3.0f;
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
