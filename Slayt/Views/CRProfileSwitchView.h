//
//  CRProfileSwitchView.h
//  Talent
//
//  Created by Vlad Soroka on 5/14/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CRProfileSwitchView;

@protocol CRSwitchProileProtocol <NSObject>
@optional

- (void)switchProfileView:(CRProfileSwitchView*)view didChangeItemAtIndex:(NSUInteger)index;

@end

@interface CRProfileSwitchView : UIView

@property (nonatomic, assign) id<CRSwitchProileProtocol>delegate;

- (instancetype)initWithFrame:(CGRect)frame
                     delegate:(id<CRSwitchProileProtocol>)delegate
                        items:(NSArray*)items;

- (void)refreshWithItems:(NSArray*)items;

@end
