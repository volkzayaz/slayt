//
//  CRProfileSwitchView.m
//  Talent
//
//  Created by Vlad Soroka on 5/14/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "CRProfileSwitchView.h"
#import "DZNSegmentedControl.h"

@interface CRProfileSwitchView ()
@property (nonatomic, weak) DZNSegmentedControl* segmentedControl;

@end

@implementation CRProfileSwitchView

- (id)initWithFrame:(CGRect)frame
           delegate:(id<CRSwitchProileProtocol>)delegate
              items:(NSArray *)items
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _delegate = delegate;
        
        NSArray* arrayOfFormattedItems = [self arrayOfFormatedStringsFromArray:items];
        
        [self createSegmentedControlWithItems:arrayOfFormattedItems];
    }
    return self;
}

- (void)refreshWithItems:(NSArray *)items
{
    if([self.segmentedControl.items isEqualToArray:items])
    {
        return;
    }

    NSArray* formatedItems = [self arrayOfFormatedStringsFromArray:items];
    
    self.segmentedControl.items = formatedItems;
    [self.segmentedControl configureAllSegments];
    
    self.segmentedControl.selectedSegmentIndex = 0;
}

#pragma makr - UIControl callbacks

- (void)segmentedControlValueChanged:(DZNSegmentedControl*)sender
{
    if ([self.delegate respondsToSelector:@selector(switchProfileView:didChangeItemAtIndex:)])
    {
        [self.delegate switchProfileView:self didChangeItemAtIndex:sender.selectedSegmentIndex];
    }
}

#pragma mark - private methods

- (NSArray*)arrayOfFormatedStringsFromArray:(NSArray*)items
{
    NSMutableArray* arrayOfFormattedItems = [NSMutableArray arrayWithCapacity:items.count];
    
    [items enumerateObjectsUsingBlock:^(NSString* obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:[NSString class]])
        {
            NSString* string = [obj stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
            [arrayOfFormattedItems addObject:[string uppercaseString]];
        }
    }];
    
    return arrayOfFormattedItems;
}

- (void)createSegmentedControlWithItems:(NSArray*)items
{
    DZNSegmentedControl* segmentedControl = [[DZNSegmentedControl alloc] initWithItems:items];
    
    [segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    segmentedControl.showsCount = NO;
    [segmentedControl setSelectedSegmentIndex:0];
    
    segmentedControl.tintColor = [UIColor colorWithRed:240.f/255.f green:113.f/255.f blue:87.f/255.f alpha:1.];
    [segmentedControl setTitleColor:[UIColor colorWithRed:159.f/255.f green:159.f/255.f blue:159.f/255.f alpha:1.] forState:UIControlStateNormal];
    [segmentedControl setTitleColor:[UIColor colorWithRed:89.f/255.f green:89.f/255.f blue:89.f/255.f alpha:1.] forState:UIControlStateSelected];
    
    segmentedControl.autoAdjustSelectionIndicatorWidth = NO;
    
    self.segmentedControl = segmentedControl;
    [self addSubview:segmentedControl];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
