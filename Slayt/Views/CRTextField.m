//
//  CRTextField.m
//  Talent
//
//  Created by Vlad Soroka on 4/8/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRTextField.h"

@implementation CRTextField

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset( bounds , 15 , 0);
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 15, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 15, 0);
}

@end
