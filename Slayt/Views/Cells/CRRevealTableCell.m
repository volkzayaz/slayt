//
//  CRRevealTableCell.m
//  Talent
//
//  Created by Vlad Soroka on 4/7/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRRevealTableCell.h"

@implementation CRRevealTableCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.imageBounds = CGRectMake(0, 0, 0, 0);
}

- (void)setSelected:(BOOL)selected {
    [self setSelected:selected animated:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if(selected)
    {
        UIColor* selectionColor = [UIColor colorWithRed:0xf0/255.0 green:0x71/255.0 blue:0x58/255.0 alpha:1];
        self.imageView.tintColor = selectionColor;
        self.textLabel.textColor = selectionColor;
        
    }
    else
    {
        self.imageView.tintColor = [UIColor whiteColor];
        self.textLabel.textColor = [UIColor whiteColor];
    }
}

- (void)setIcon:(UIImage *)icon {
    if (icon) {
        UIImage* imageTemplate = [icon imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.imageView.image = imageTemplate;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if(!CGRectIsEmpty(self.imageBounds))
    {
       // self.imageView.bounds = self.imageBounds;
    }
}

@end
