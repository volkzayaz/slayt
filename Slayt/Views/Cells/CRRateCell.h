//
//  CRRateCell.h
//  Talent
//
//  Created by Vlad Soroka on 4/17/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CRRateCell;

@protocol CRRateCellDelegate <NSObject>
@optional
- (void)didEnterValue:(NSString*)value inRateCell:(CRRateCell*)cell;

@end

@interface CRRateCell : UITableViewCell

@property (nonatomic, weak) id<CRRateCellDelegate> delegate;
@property (nonatomic, weak) UITextField* field;

@end
