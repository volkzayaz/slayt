//
//  CRRoleTypeCell.h
//  Talent
//
//  Created by Vlad Soroka on 4/29/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "SWTableViewCell.h"

@interface CRRoleTypeCell : SWTableViewCell

@property (nonatomic, assign, getter = isShowProfileType) BOOL showProfileType;

@end
