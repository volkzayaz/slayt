//
//  CRRoleTypeCell.m
//  Talent
//
//  Created by Vlad Soroka on 4/29/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRRoleTypeCell.h"

@interface CRRoleTypeCell ()

@property (weak, nonatomic) IBOutlet UIImageView *showRoleICon;

@end

@implementation CRRoleTypeCell

- (void) setShowProfileType:(BOOL)showProfileType
{
    if(_showProfileType == showProfileType)
    {
        return;
    }
    
    if(showProfileType)
    {
        self.showRoleICon.image = [UIImage imageNamed:@"profileVisibleIcon"];
    }
    else
    {
        self.showRoleICon.image = [UIImage imageNamed:@"profileInvisibleIcon"];
    }
    
    _showProfileType = showProfileType;
}

@end
