//
//  CRRevealTableCell.h
//  Talent
//
//  Created by Vlad Soroka on 4/7/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRRevealTableCell : UITableViewCell

/**
 *  property will be used to set self.imageview.bounds
 */
@property (nonatomic, assign) CGRect imageBounds;

- (void)setIcon:(UIImage*)icon;//allows icon tinting

@end
