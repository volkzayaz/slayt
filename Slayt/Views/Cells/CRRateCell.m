//
//  CRRateCell.m
//  Talent
//
//  Created by Vlad Soroka on 4/17/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRRateCell.h"

@interface CRRateCell()<UITextFieldDelegate>

@end

@implementation CRRateCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIColor* textColor = [UIColor colorWithRed:0x80/255.f green:0x7f/255.f blue:0xb0/255.f alpha:1];
        
        self.textLabel.textColor = textColor;
        
        UITextField* field = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - 15, self.frame.size.height)];
        field.textAlignment = NSTextAlignmentRight;
        field.textColor = textColor;
        
        field.placeholder = @"Not Specified";
        field.delegate = self;
        [field setKeyboardType:UIKeyboardTypeNumberPad];
        
        self.field = field;
        [self addSubview:field];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([self.delegate respondsToSelector:@selector(didEnterValue:inRateCell:)])
    {
        [self.delegate didEnterValue:[textField.text stringByReplacingCharactersInRange:range withString:string] inRateCell:self];
    }
    
    return YES;
}

@end
