//
//  CRPhotoView.h
//  Talent
//
//  Created by Vlad Soroka on 4/15/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Parse/Parse.h>

@class CRCarouselPhotoView;
@class CRPhoto;

@protocol CRPhotoViewDelegate <NSObject>
@optional
- (void)didPressDeleteOnPhotoView:(CRCarouselPhotoView*)view;

@end

@interface CRCarouselPhotoView : UIView

@property (nonatomic, weak) PFImageView* photoView;
@property (nonatomic, weak) id<CRPhotoViewDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame;

/**
 *  makes this photoView highlighted
 */
- (void)setHighligted:(BOOL)isHiglighted;

@end
