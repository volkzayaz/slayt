//
//  CRGrabKitConfigurator.m
//  Talent
//
//  Created by Dmitry Utenkov on 5/7/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRGrabKitConfigurator.h"

#import "GRKPickerViewController.h"


@implementation CRGrabKitConfigurator


// Connection to services


// Facebook - https://developers.facebook.com/apps

- (NSString*)facebookAppId {
    
#if DEBUG
	return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"];
#else
#warning Facebook AppId should be set
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"];
#endif
    
}



// Flickr - http://www.flickr.com/services/apps/create/apply/

- (NSString*)flickrApiKey {
#if DEBUG
    return @"";
#else
#warning FlickR Api Key should be set
    return @"";
#endif
    
}

- (NSString*)flickrApiSecret {
#if DEBUG
    return @"";
#else
#warning FlickR API Secret should be set
    return @"";
#endif
    
    
}

- (NSString*)flickrRedirectUri{
#if DEBUG
    return @"";
#else
#warning FlickR Redirect Uri should be set
    return @"";
#endif
    
    
}



// Instragram - http://instagram.com/developer/clients/register/

- (NSString*)instagramAppId {
    
#if DEBUG
    return @"";
#else
#warning Instagram AppId should be set
    return @"";
#endif
    
}

- (NSString*)instagramRedirectUri {
#if DEBUG
    return @"";
#else
#warning Instagram Redirect Uri  should be set
    return @"";
#endif
    
}



// Picasa - https://code.google.com/apis/console/

- (NSString*)picasaClientId {
#if DEBUG
    return @"";
#else
#warning Picasa Client Id should be set
    return @"";
#endif
    
    
}

- (NSString*)picasaClientSecret {
#if DEBUG
    return  @"";
#else
#warning Picasa Client Secret  should be set
    return @"";
#endif
    
}



// Others

// The name of the album "Tagged photos" on Facebook, as you want GrabKit to return it.
// Hint : You can use the default localization here.
- (NSString*)facebookTaggedPhotosAlbumName {
    
    return GRK_i18n(@"GRK_FACEBOOK_TAGGED_PHOTOS", @"Tagged photos");
    
}




/* The Picasa connector doesn't open a Safari page to let the user authenticate. Instead, it presents a viewController.
 When using the GrabKitPicker, the viewController is presented in its navigation hierarchy, there is nothing you have to handle.
 
 But when you use GrabKit as a library, Picasa's Auth viewController needs to be displayed.
 The viewController returned by this configuration method will present Picasa's auth controller.
 
 
 If your custom viewController is an instance of UINavigationController, GrabKit will call the configuration method 'customViewControllerShouldPresentPicasaAuthControllerModally' to know if Picasa's Auth Controller must be presented "modally", or pushed in the navigation hierarchy.
 
 If your custom viewController is not a navigationController, GrabKit will present Picasa's auth controller "modally"
 
 */
/*
 -(UIViewController *)customViewControllerToPresentPicasaAuthController {
 
 return  yourUIViewController;
 }*/


/* If you use GrabKit as a library (i.e. without GrabKitPicker), and if the custom view controller you return in "customViewControllerToPresentPicasaAuthController" is an instance of UINavigationController, then this method will help you define if Picasa's Auth Controller must be pushed in the navigation hierarchy, or must be presented modally.
 
 */
/*
 -(BOOL)customViewControllerShouldPresentPicasaAuthControllerModally {
 return YES;
 }
 */

@end
