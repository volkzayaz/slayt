//
//  CRGrabKitConfigurator.h
//  Talent
//
//  Created by Dmitry Utenkov on 5/7/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRKConfiguratorProtocol.h"

@interface CRGrabKitConfigurator : NSObject <GRKConfiguratorProtocol>

@end
