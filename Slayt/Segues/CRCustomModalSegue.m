//
//  CRSegue.m
//  Talent
//
//  Created by Dmitry Utenkov on 5/4/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

#import "CRCustomModalSegue.h"

@implementation CRCustomModalSegue

- (instancetype)init {
    if (self = [super init]) {
        _animated = YES;
        _toolbarHidden = YES;
        _statusBarHidden = NO;
    }
    return self;
}

- (void)perform {
    id destinationViewController = self.destinationViewController;
    if (![self.destinationViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"SWFrontNavigationController"];
        navigationController.toolbarHidden = self.toolbarHidden;
        [UIApplication sharedApplication].statusBarHidden = self.statusBarHidden;
        [destinationViewController setNeedsStatusBarAppearanceUpdate];
        navigationController.viewControllers = @[self.destinationViewController];
        destinationViewController = navigationController;
    }
    [self.sourceViewController presentViewController:destinationViewController animated:self.animated completion:nil];
}

@end
