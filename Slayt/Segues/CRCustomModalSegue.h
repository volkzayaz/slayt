//
//  CRSegue.h
//  Talent
//
//  Created by Dmitry Utenkov on 5/4/14.
//  Copyright (c) 2014 Vlad. All rights reserved.
//

@interface CRCustomModalSegue : UIStoryboardSegue

@property (nonatomic) BOOL animated;
@property (nonatomic) BOOL toolbarHidden;
@property (nonatomic) BOOL statusBarHidden;


@end
